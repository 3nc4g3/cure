package com.inkg.cure;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by inkg on 2017. 3. 5..
 */

public class DurActivity extends AppCompatActivity {

    private String keyword = "";
    private String title = "";

    private ImageView img_dur;
    private TextView txt_dur_1;
    private TextView txt_dur_2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dur);
        Intent intent = getIntent();
        keyword = intent.getStringExtra("keyword");

        setupViews();
        setCustomActionbar();
    }

    private void setupViews() {

        img_dur = (ImageView) findViewById(R.id.img_dur);
        txt_dur_1 = (TextView) findViewById(R.id.txt_dur_1);
        txt_dur_2 = (TextView) findViewById(R.id.txt_dur_2);

        switch (keyword) {
            case "mix":
                title = "병용 주의 성분";
                img_dur.setImageDrawable(getResources().getDrawable(R.drawable.mix_active, null));
                txt_dur_1.setText(getString(R.string.mix_1));
                txt_dur_2.setText(getString(R.string.mix_2));
                break;
            case "baby":
                title = "연령 주의 성분";
                img_dur.setImageDrawable(getResources().getDrawable(R.drawable.baby_active, null));
                txt_dur_1.setText(getString(R.string.baby_1));
                txt_dur_2.setText(getString(R.string.baby_2));
                break;
            case "pregnant":
                title = "임부 주의 성분";
                img_dur.setImageDrawable(getResources().getDrawable(R.drawable.pregant_active, null));
                txt_dur_1.setText(getString(R.string.pregnant_1));
                txt_dur_2.setText(getString(R.string.pregnant_2));
                break;
            case "contain":
                title = "용량 주의 성분";
                img_dur.setImageDrawable(getResources().getDrawable(R.drawable.contain_active, null));
                txt_dur_1.setText(getString(R.string.contain_1));
                txt_dur_2.setText(getString(R.string.contain_2));
                break;
            case "period":
                title = "투여 기간 주의 성분";
                img_dur.setImageDrawable(getResources().getDrawable(R.drawable.date_active, null));
                txt_dur_1.setText(getString(R.string.period_1));
                txt_dur_2.setText(getString(R.string.period_2));
                break;
            case "old":
                title = "노인 주의 성분";
                img_dur.setImageDrawable(getResources().getDrawable(R.drawable.older_active, null));
                txt_dur_1.setText(getString(R.string.old_1));
                txt_dur_2.setText(getString(R.string.old_2));
                break;
            case "blood":
                title = "헌혈 주의 성분";
                img_dur.setImageDrawable(getResources().getDrawable(R.drawable.blood_active, null));
                txt_dur_1.setText(getString(R.string.blood_1));
                txt_dur_2.setText(getString(R.string.blood_2));
                break;
            default:
                break;

        }
    }

    private void setCustomActionbar() {

        ActionBar actionBar = getSupportActionBar();

        // Custom Actionbar를 사용하기 위해 CustomEnabled을 true 시키고 필요 없는 것은 false 시킨다

        actionBar.setDisplayShowCustomEnabled(true);

        actionBar.setDisplayHomeAsUpEnabled(false);

        actionBar.setDisplayShowTitleEnabled(false);

        // Set custom view layout

        View mCustomView = LayoutInflater.from(this).inflate(R.layout.actionbar_simple, null);

        actionBar.setCustomView(mCustomView);


        // Set no padding both side

        Toolbar parent = (Toolbar) mCustomView.getParent(); // first get parent toolbar of current action bar

        parent.setContentInsetsAbsolute(0, 0);              // set padding programmatically to 0dp

        // Set actionbar background image

        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.border_layout_bottom));

        // Set actionbar layout layoutparams

        ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);

        actionBar.setCustomView(mCustomView, params);

        ImageView img_actionbar_back = (ImageView) findViewById(R.id.img_actionbar_back);
        img_actionbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        TextView txt_actionbar = (TextView) findViewById(R.id.txt_actionbar_simple);
        txt_actionbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        txt_actionbar.setText(title);

    }
}