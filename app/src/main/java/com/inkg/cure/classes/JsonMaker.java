package com.inkg.cure.classes;

import android.util.Pair;

import org.json.JSONException;
import org.json.JSONStringer;

/**
 * Created by inkg on 2016. 11. 25..
 */

public class JsonMaker {

    public JsonMaker() {

    }

    public String makeJson(Pair<String, String>... pairs) {
        String json = "";
        JSONStringer jsonStringer = new JSONStringer();
        int count = 0;

        try {

            jsonStringer = jsonStringer.object();

            for (Pair<String, String> pair : pairs) {
                jsonStringer = jsonStringer.key(pair.first).value(pair.second);
            }

            jsonStringer = jsonStringer.endObject();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        json = jsonStringer.toString();
        return json;
    }
}
