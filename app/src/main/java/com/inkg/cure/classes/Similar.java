package com.inkg.cure.classes;

import java.io.Serializable;

/**
 * Created by inkg on 2017. 3. 4..
 */

public class Similar implements Serializable {
    private String id;
    private String image;
    private String name;
    private String company;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
