package com.inkg.cure.classes;

import java.io.Serializable;

/**
 * Created by inkg on 2017. 3. 1..
 */

public class Review implements Serializable {

    private String age;
    private String bad_point;
    private String good_point;
    private String date;
    private String evaluation;
    private String gender;
    private String id;
    private String likes;
    private String member;
    private String nickname;
    private String period;
    private String picture;
    private String product_name;
    private String comments;
    private String profile;
    private String repurchase;
    private String target;
    private String tip;
    private String type;
    private String views;

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getBad_point() {
        return bad_point;
    }

    public void setBad_point(String bad_point) {
        this.bad_point = bad_point;
    }

    public String getGood_point() {
        return good_point;
    }

    public void setGood_point(String good_point) {
        this.good_point = good_point;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(String evaluation) {
        this.evaluation = evaluation;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    public String getMember() {
        return member;
    }

    public void setMember(String member) {
        this.member = member;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getRepurchase() {
        return repurchase;
    }

    public void setRepurchase(String repurchase) {
        this.repurchase = repurchase;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getViews() {
        return views;
    }

    public void setViews(String views) {
        this.views = views;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }
}
