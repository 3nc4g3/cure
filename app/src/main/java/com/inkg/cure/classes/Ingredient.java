package com.inkg.cure.classes;

import java.io.Serializable;

/**
 * Created by inkg on 2017. 3. 5..
 */

public class Ingredient implements Serializable {

    private String kor_name;
    private String eng_name;
    private String name;
    private String fda_comment;
    private String fda_grade;
    private String type;

    public String getKor_name() {
        return kor_name;
    }

    public void setKor_name(String kor_name) {
        this.kor_name = kor_name;
    }

    public String getEng_name() {
        return eng_name;
    }

    public void setEng_name(String eng_name) {
        this.eng_name = eng_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFda_comment() {
        return fda_comment;
    }

    public void setFda_comment(String fda_comment) {
        this.fda_comment = fda_comment;
    }

    public String getFda_grade() {
        return fda_grade;
    }

    public void setFda_grade(String fda_grade) {
        this.fda_grade = fda_grade;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
