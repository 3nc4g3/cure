package com.inkg.cure.classes;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by inkg on 2016. 12. 12..
 */

public class Healthfood implements Serializable {

    int timems = 0;
    Items items = new Items();

    private class Items {

        int found = 0;
        int size = 0;
        int start = 0;

        Hit hit = new Hit();

    }

    private class Hit {

        String id = new String();
        Fields fields = new Fields();
    }

    private class Fields {

        ArrayList<String> category = new ArrayList<>();
        ArrayList<String> company = new ArrayList<>();
        ArrayList<String> etc_ingredient = new ArrayList<>();
        ArrayList<String> functional_ingredient = new ArrayList<>();
        ArrayList<String> name = new ArrayList<>();
        ArrayList<String> type = new ArrayList<>();
        String select_image = new String();
    }

}


