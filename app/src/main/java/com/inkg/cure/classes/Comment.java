package com.inkg.cure.classes;

import java.io.Serializable;

/**
 * Created by inkg on 2017. 3. 1..
 */

public class Comment implements Serializable {
    private String profile;
    private String nickname;
    private String content;
    private String time;

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
