package com.inkg.cure.classes;

import java.io.Serializable;

/**
 * Created by inkg on 2017. 2. 13..
 */

public class RankData implements Serializable {

    private String company;
    private String id;
    private String name;
    private String select_image;
    private Scores scores;
    private String type;

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSelect_image() {
        return select_image;
    }

    public void setSelect_image(String select_image) {
        this.select_image = select_image;
    }

    public Scores getScores() {
        return scores;
    }

    public void setScores(Scores scores) {
        this.scores = scores;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
