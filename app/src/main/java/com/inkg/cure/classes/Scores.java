package com.inkg.cure.classes;

import java.io.Serializable;

/**
 * Created by inkg on 2017. 2. 13..
 */

public class Scores implements Serializable {

    private String sales;
    private String shares;
    private String reviews;
    private String unlikes;
    private String likes;
    private String bookmarks;
    private String views;
    private String review_unlikes;
    private String review_likes;

    public String getSales() {
        return sales;
    }

    public void setSales(String sales) {
        this.sales = sales;
    }
    
    public String getShares() {
        return shares;
    }

    public void setShares(String shares) {
        this.shares = shares;
    }

    public String getReviews() {
        return reviews;
    }

    public void setReviews(String reviews) {
        this.reviews = reviews;
    }

    public String getUnlikes() {
        return unlikes;
    }

    public void setUnlikes(String unlikes) {
        this.unlikes = unlikes;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    public String getBookmarks() {
        return bookmarks;
    }

    public void setBookmarks(String bookmarks) {
        this.bookmarks = bookmarks;
    }

    public String getViews() {
        return views;
    }

    public void setViews(String views) {
        this.views = views;
    }

    public String getReview_unlikes() {
        return review_unlikes;
    }

    public void setReview_unlikes(String review_unlikes) {
        this.review_unlikes = review_unlikes;
    }

    public String getReview_likes() {
        return review_likes;
    }

    public void setReview_likes(String review_likes) {
        this.review_likes = review_likes;
    }
}
