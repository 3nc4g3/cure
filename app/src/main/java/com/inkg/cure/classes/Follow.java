package com.inkg.cure.classes;

import java.io.Serializable;

/**
 * Created by inkg on 2017. 3. 3..
 */

public class Follow implements Serializable {

    private String nickname;
    private String target;
    private String profile;

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }
}
