package com.inkg.cure.classes;

import android.widget.ImageView;

import java.io.Serializable;

/**
 * Created by inkg on 2017. 2. 27..
 */

public class Ingr1 implements Serializable {

    private String kor_name;
    private String eng_name;
    private String type;

    public String getKor_name() {
        return kor_name;
    }

    public void setKor_name(String kor_name) {
        this.kor_name = kor_name;
    }

    public String getEng_name() {
        return eng_name;
    }

    public void setEng_name(String eng_name) {
        this.eng_name = eng_name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
