package com.inkg.cure.classes;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.util.Pair;

import com.inkg.cure.R;

import org.apache.http.client.methods.HttpPost;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ExecutionException;

/**
 * Created by inkg on 2016. 11. 25..
 */

public class HttpConnect {

    private ProgressDialog authProgress;

    private Context mContext;

    private CureSP csp;

    private JsonMaker jsonMaker;

    public HttpConnect(Context context) {
        mContext = context;
        csp = new CureSP(mContext);
        jsonMaker = new JsonMaker();
    }

    public String getServerWithVersion() {
        return mContext.getString(R.string.server_url) + mContext.getString(R.string.cureya_version);
    }

    //Accept-Encoding : gzip 썸네일 압축
    public String send(String jsonMsg, String serverURL, String basicAuth) {
        String result = "";
        HttpPostAsync hpa = new HttpPostAsync();
        try {
            result = hpa.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, jsonMsg, serverURL, basicAuth).get();
        } catch (InterruptedException e) {
            Log.e("InterruptedException", "?", e);
        } catch (ExecutionException e) {
            Log.e("ExecutionException", "?", e);
        }
        return result;
    }

    public String send(String jsonMsg, String serverURL, String basicAuth, String type) {
        String result = "";
        HttpPostAsync hpa = new HttpPostAsync();
        try {
            result = hpa.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, jsonMsg, serverURL, basicAuth, type).get();
        } catch (InterruptedException e) {
            Log.e("InterruptedException", "?", e);
        } catch (ExecutionException e) {
            Log.e("ExecutionException", "?", e);
        }
        return result;
    }

    public class HttpPostAsync extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            authProgress = new ProgressDialog(mContext);
//            authProgress.setTitle("로그인 중...");
//            authProgress.setCancelable(false);
//            authProgress.setMessage("서버와 통신중입니다. 잠시만 기다려주세요.");
        }

        @Override
        protected String doInBackground(String... strings) {
            String response = "";
//            Log.e("Send??", strings[0] + strings[1] + strings[2]);
            if (strings.length > 3) {
                if ("GET".equals(strings[3])) {
                    response = sendDataToServerGet(strings[0], strings[1], strings[2], strings[3]);
                } else {
                    response = sendDataToServerOther(strings[0], strings[1], strings[2], strings[3]);
                }
            } else {
                response = sendDataToServer(strings[0], strings[1], strings[2]);
            }
            return response;
        }

        @Override
        protected void onPostExecute(String result) {

//            authProgress.dismiss();

        }
    }

    public String sendDataToServer(String jsonMsg, String serverURL, String basicAuth) {
        OutputStream os = null;
        InputStream is = null;
        ByteArrayOutputStream baos = null;
        HttpURLConnection conn = null;
        String response = "";

        try {
            URL url = new URL(serverURL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setConnectTimeout(7 * 1000);
            conn.setReadTimeout(7 * 1000);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Cache-Control", "no-cache");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            if (!"".equals(basicAuth)) {
                conn.setRequestProperty("Authorization", basicAuth);
            }
            conn.setDoOutput(true);
            conn.setDoInput(true);

            os = conn.getOutputStream();
            os.write(jsonMsg.getBytes());
            os.flush();
            int responseCode = conn.getResponseCode();

            Log.e("response code", responseCode + " / " + conn.getResponseMessage());

            if (responseCode == HttpURLConnection.HTTP_OK) {

                is = conn.getInputStream();
                baos = new ByteArrayOutputStream();
                byte[] byteBuffer = new byte[1024];
                byte[] byteData = null;
                int nLength = 0;
                while ((nLength = is.read(byteBuffer, 0, byteBuffer.length)) != -1) {
                    baos.write(byteBuffer, 0, nLength);
                }
                byteData = baos.toByteArray();

                response = new String(byteData);
                return response;
            } else if (responseCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
                if (!"".equals(basicAuth)) { // expired?
                    // exp check?
                    if (checkExpired()) { //expired
                        // resend process
                        Log.e("resend procedure", serverURL);
                        ResendAsync ra = new ResendAsync(jsonMsg, serverURL, "POST");
                        response = ra.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR).get();

                    } else { // other error
                        Log.e("url no expired", serverURL);
                        response = String.valueOf(conn.getResponseMessage());
                    }

                } else {
                    Log.e("url no basicAuth", serverURL);
                    response = String.valueOf(conn.getResponseMessage());
                }

            } else {
                Log.e("url", serverURL);
                response = String.valueOf(conn.getResponseMessage());
            }
            
        } catch (Exception e) {
            Log.e("kkk", "Exception", e);
            response = String.valueOf(e);
        } finally {
            return response;
        }
    }

    public String sendDataToServerGet(String jsonMsg, String serverURL, String basicAuth, String method) {
        HttpURLConnection conn = null;
        String response = "";

        try {
            URL url = new URL(serverURL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setConnectTimeout(7 * 1000);
            conn.setReadTimeout(7 * 1000);
            conn.setRequestMethod(method);
            conn.setRequestProperty("Cache-Control", "no-cache");
            conn.setRequestProperty("Content-Type", "application/json");
            if (!"".equals(basicAuth)) {
                conn.setRequestProperty("Authorization", basicAuth);
            }

            int responseCode = conn.getResponseCode();

            Log.e("response code", responseCode + " / " + conn.getResponseMessage());

            if (responseCode == HttpURLConnection.HTTP_OK) {

                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();

                response = sb.toString();
                return response;

            } else if (responseCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
                if (!"".equals(basicAuth)) {
                    // exp check?
                    if (checkExpired()) { //expired
                        // resend process
                        Log.e("resend procedure", serverURL);
                        ResendAsync ra = new ResendAsync(jsonMsg, serverURL, method);
                        response = ra.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR).get();

                    } else { // other error
                        Log.e("url no expired", serverURL);
                        response = String.valueOf(conn.getResponseMessage());
                    }

                } else {
                    Log.e("url no basicAuth", serverURL);
                    response = String.valueOf(conn.getResponseMessage());
                }

            } else {
                Log.e("url", serverURL);
                response = String.valueOf(conn.getResponseMessage());
                return response;
            }

        } catch (Exception e) {
            Log.e("kkk", "Exception", e);
            response = String.valueOf(e);
            return response;
        } finally {
            return response;
        }
    }

    public String sendDataToServerOther(String jsonMsg, String serverURL, String basicAuth, String method) {

        OutputStream os = null;
        InputStream is = null;
        ByteArrayOutputStream baos = null;
        HttpURLConnection conn = null;
        String response = "";

        try {
            URL url = new URL(serverURL);
            conn = (HttpURLConnection) url.openConnection();
            conn.setConnectTimeout(7 * 1000);
            conn.setReadTimeout(7 * 1000);
            conn.setRequestProperty("Cache-Control", "no-cache");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            if ("PATCH".equals(method)) {
                Log.e("PATCH????", method);
//                conn.setRequestProperty("X-HTTP-Method-Override", method);
                conn.setRequestMethod("PATCH");
            } else {
                conn.setRequestMethod(method);
            }

            if (!"".equals(basicAuth)) {
                conn.setRequestProperty("Authorization", basicAuth);
            }
            conn.setDoOutput(true);
            conn.setDoInput(true);

            os = conn.getOutputStream();
            os.write(jsonMsg.getBytes());
            os.flush();
            int responseCode = conn.getResponseCode();

            Log.e("response code", responseCode + " / " + conn.getResponseMessage());

            if (responseCode == HttpURLConnection.HTTP_OK) {

                is = conn.getInputStream();
                baos = new ByteArrayOutputStream();
                byte[] byteBuffer = new byte[1024];
                byte[] byteData = null;
                int nLength = 0;
                while ((nLength = is.read(byteBuffer, 0, byteBuffer.length)) != -1) {
                    baos.write(byteBuffer, 0, nLength);
                }
                byteData = baos.toByteArray();

                response = new String(byteData);
                return response;

            } else if (responseCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
                if (!"".equals(basicAuth)) {
                    // exp check?
                    if (checkExpired()) { //expired
                        // resend process
                        Log.e("resend procedure", serverURL);
                        ResendAsync ra = new ResendAsync(jsonMsg, serverURL, method);
                        response = ra.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR).get();

                    } else { // other error
                        Log.e("url no expired", serverURL);
                        response = String.valueOf(conn.getResponseMessage());
                    }
                } else {
                    Log.e("url no basicAuth", serverURL);
                    response = String.valueOf(conn.getResponseMessage());
                }

            } else {
                Log.e("url", serverURL);
                response = String.valueOf(conn.getResponseMessage());
                return response;
            }

        } catch (Exception e) {
            Log.e("kkk", "Exception", e);
            response = String.valueOf(e);
            return response;
        } finally {
            return response;
        }
    }

    private boolean checkExpired() {
        String expire = csp.getValue("expire", "");
        if ("".equals(expire)) {
            return false;
        }
        boolean result = false;
        SimpleDateFormat sdf;
        if (expire.contains("GMT")) {
//            sdf = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss");
            sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            String year = expire.substring(12, 16);
            String day = expire.substring(5, 7);
            String time = expire.substring(17, 25);
            String month = expire.substring(8, 11);
            switch (month) {
                case "Jan":
                    month = "01";
                    break;
                case "Feb":
                    month = "02";
                    break;
                case "Mar":
                    month = "03";
                    break;
                case "Apr":
                    month = "04";
                    break;
                case "May":
                    month = "05";
                    break;
                case "Jun":
                    month = "06";
                    break;
                case "Jul":
                    month = "07";
                    break;
                case "Aug":
                    month = "08";
                    break;
                case "Sep":
                    month = "09";
                    break;
                case "Oct":
                    month = "10";
                    break;
                case "Nov":
                    month = "11";
                    break;
                case "Dec":
                    month = "12";
                    break;
            }

            expire = year+"-"+month+"-"+day+"T"+time;
            Log.e("ex", expire);

        } else {
            sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        }
        SimpleDateFormat output = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        String expire_time = "";
//        Log.e("expire", expire);
        try {
            Date d = sdf.parse(expire);
//            expire_time = output.format(d);
//            Date expireTime = output.parse(expire_time);
            Calendar c = Calendar.getInstance();
//            Log.e("current", c.getTime().toString());
//            Date currentTime = output.parse(c.getTime().toString());
            Date currentTime = new Date();

            Log.e("time?", d.toString() + " / " + currentTime.toString());

            if (currentTime.after(d)) { //expired
                result = true;
            } else {
                result = false;
            }
        } catch (Exception e) {
            Log.e("httpconnect", "time parse excep", e);
        } finally {
            return result;
        }
    }

    private void refresh_user_data(String data) {

        Log.e("refresh result", data);
        try {
            JSONObject json_data = new JSONObject(data);
            String access_token = json_data.getString("access_token");
            String expire = json_data.getString("exp");
//            String refresh_token = json_data.getString("refresh_token");
//            String refresh_expire = json_data.getString("refresh_exp");

            csp.put("access_token", access_token);
            csp.put("expire", expire);
//            csp.put("refresh_token", refresh_token);
//            csp.put("refresh_expire", refresh_expire);
        } catch (Exception e) {
            Log.e("httpconnect", "refresh excep", e);
        }
    }

    class ResendAsync extends AsyncTask<String, String, String> {

        String mData = "";
        String mJson = "";
        String mUrl = "";
        String mMethod = "";

        ResendAsync() {

        }

        ResendAsync(String data) {
            this.mData = data;
        }

        ResendAsync(String json, String url, String method) {
            this.mJson = json;
            this.mUrl = url;
            this.mMethod = method;
        }

        ResendAsync(String data, String json, String url, String method) {
            this.mData = data;
            this.mJson = json;
            this.mUrl = url;
            this.mMethod = method;
        }

        @Override
        protected void onPreExecute() {
            Log.e("??", mData + " / " + mUrl + " / " + mMethod);
            // request new token
            String id = csp.getValue("id", "");
            String refresh_token = csp.getValue("refresh_token", "");
            String send_json = jsonMaker.makeJson(new Pair("id", id));
            Log.e("refresh_token", refresh_token);
            Log.e("id", id);

            try {
                HttpPostAsync hpa = new HttpPostAsync();
                mData = hpa.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, send_json, getServerWithVersion() + mContext.getString(R.string.rest_tokens), refresh_token, "PATCH").get();
//                mData = sendDataToServerOther(send_json, getServerWithVersion() + mContext.getString(R.string.rest_tokens), refresh_token, "PATCH");
            } catch (Exception e) {
                Log.e("httpconnect", "refresh token excep", e);
            }

            refresh_user_data(mData);
        }

        @Override
        protected String doInBackground(String... strings) {

            String result = "";
            HttpPostAsync hpa = new HttpPostAsync();
            try {
                if ("POST".equals(mMethod)) {
                    result = hpa.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, mJson, mUrl, csp.getValue("access_token", "")).get();
                } else {
                    result = hpa.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, mJson, mUrl, csp.getValue("access_token", ""), mMethod).get();
                }
            } catch (Exception e) {
                Log.e("http", "hpa exception", e);
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

        }
    }
}
