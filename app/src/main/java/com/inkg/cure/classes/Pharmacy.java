package com.inkg.cure.classes;

import java.io.Serializable;

/**
 * Created by inkg on 2017. 2. 15..
 */

public class Pharmacy implements Serializable {

    private String dutyaddr;
    private String dutyetc;
    private String dutyinf;
    private String dutymapimg;
    private String dutyname;
    private String dutytel1;
    private String dutytime1c;
    private String dutytime1s;
    private String dutytime2c;
    private String dutytime2s;
    private String dutytime3c;
    private String dutytime3s;
    private String dutytime4c;
    private String dutytime4s;
    private String dutytime5c;
    private String dutytime5s;
    private String dutytime6c;
    private String dutytime6s;
    private String dutytime7c;
    private String dutytime7s;
    private String dutytime8c;
    private String dutytime8s;
    private String hpid;
    private String postcdn1;
    private String postcdn2;
    private String wgs84lat;
    private String wgs84lon;
    private String distance;
    private String likes;

    public String getDutyaddr() {
        return dutyaddr;
    }

    public void setDutyaddr(String dutyaddr) {
        this.dutyaddr = dutyaddr;
    }

    public String getDutyetc() {
        return dutyetc;
    }

    public void setDutyetc(String dutyetc) {
        this.dutyetc = dutyetc;
    }

    public String getDutyinf() {
        return dutyinf;
    }

    public void setDutyinf(String dutyinf) {
        this.dutyinf = dutyinf;
    }

    public String getDutymapimg() {
        return dutymapimg;
    }

    public void setDutymapimg(String dutymapimg) {
        this.dutymapimg = dutymapimg;
    }

    public String getDutyname() {
        return dutyname;
    }

    public void setDutyname(String dutyname) {
        this.dutyname = dutyname;
    }

    public String getDutytel1() {
        return dutytel1;
    }

    public void setDutytel1(String dutytel1) {
        this.dutytel1 = dutytel1;
    }

    public String getDutytime1c() {
        return dutytime1c;
    }

    public void setDutytime1c(String dutytime1c) {
        this.dutytime1c = dutytime1c;
    }

    public String getDutytime1s() {
        return dutytime1s;
    }

    public void setDutytime1s(String dutytime1s) {
        this.dutytime1s = dutytime1s;
    }

    public String getDutytime2c() {
        return dutytime2c;
    }

    public void setDutytime2c(String dutytime2c) {
        this.dutytime2c = dutytime2c;
    }

    public String getDutytime2s() {
        return dutytime2s;
    }

    public void setDutytime2s(String dutytime2s) {
        this.dutytime2s = dutytime2s;
    }

    public String getDutytime3c() {
        return dutytime3c;
    }

    public void setDutytime3c(String dutytime3c) {
        this.dutytime3c = dutytime3c;
    }

    public String getDutytime3s() {
        return dutytime3s;
    }

    public void setDutytime3s(String dutytime3s) {
        this.dutytime3s = dutytime3s;
    }

    public String getDutytime4c() {
        return dutytime4c;
    }

    public void setDutytime4c(String dutytime4c) {
        this.dutytime4c = dutytime4c;
    }

    public String getDutytime4s() {
        return dutytime4s;
    }

    public void setDutytime4s(String dutytime4s) {
        this.dutytime4s = dutytime4s;
    }

    public String getDutytime5c() {
        return dutytime5c;
    }

    public void setDutytime5c(String dutytime5c) {
        this.dutytime5c = dutytime5c;
    }

    public String getDutytime5s() {
        return dutytime5s;
    }

    public void setDutytime5s(String dutytime5s) {
        this.dutytime5s = dutytime5s;
    }

    public String getDutytime6c() {
        return dutytime6c;
    }

    public void setDutytime6c(String dutytime6c) {
        this.dutytime6c = dutytime6c;
    }

    public String getDutytime6s() {
        return dutytime6s;
    }

    public void setDutytime6s(String dutytime6s) {
        this.dutytime6s = dutytime6s;
    }

    public String getDutytime7c() {
        return dutytime7c;
    }

    public void setDutytime7c(String dutytime7c) {
        this.dutytime7c = dutytime7c;
    }

    public String getDutytime7s() {
        return dutytime7s;
    }

    public void setDutytime7s(String dutytime7s) {
        this.dutytime7s = dutytime7s;
    }

    public String getDutytime8c() {
        return dutytime8c;
    }

    public void setDutytime8c(String dutytime8c) {
        this.dutytime8c = dutytime8c;
    }

    public String getDutytime8s() {
        return dutytime8s;
    }

    public void setDutytime8s(String dutytime8s) {
        this.dutytime8s = dutytime8s;
    }

    public String getHpid() {
        return hpid;
    }

    public void setHpid(String hpid) {
        this.hpid = hpid;
    }

    public String getPostcdn1() {
        return postcdn1;
    }

    public void setPostcdn1(String postcdn1) {
        this.postcdn1 = postcdn1;
    }

    public String getPostcdn2() {
        return postcdn2;
    }

    public void setPostcdn2(String postcdn2) {
        this.postcdn2 = postcdn2;
    }

    public String getWgs84lat() {
        return wgs84lat;
    }

    public void setWgs84lat(String wgs84lat) {
        this.wgs84lat = wgs84lat;
    }

    public String getWgs84lon() {
        return wgs84lon;
    }

    public void setWgs84lon(String wgs84lon) {
        this.wgs84lon = wgs84lon;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }
}
