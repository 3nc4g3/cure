package com.inkg.cure.classes;

import java.io.Serializable;

/**
 * Created by inkg on 2017. 3. 5..
 */

public class Addr implements Serializable {

    private String mountain; // ""
    private String mainAddress; // 242,
    private String point_wx; // 400166,
    private String point_wy; // -11708,
    private String isNewAddress; // Y,
    private String buildingAddress; // 카카오스페이스닷원,
    private String title; // 제주특별자치도 제주시 첨단로 242,
    private String placeName; // Not avaliable,
    private String zipcode; // 690140,
    private String newAddress; // 영평동 2181,
    private String localName_2; // 제주시,
    private String localName_3; // 첨단로,
    private String localName_1; // 제주특별자치도,
    private String lat; // 33.4506803453,
    private String point_x; // 126.5704935278,
    private String lng; // 126.5704935278,
    private String zone_no; // 63309,
    private String subAddress; // 0,
    private String id; // N30664967,
    private String point_y; // 33.4506803453

    public String getMountain() {
        return mountain;
    }

    public void setMountain(String mountain) {
        this.mountain = mountain;
    }

    public String getMainAddress() {
        return mainAddress;
    }

    public void setMainAddress(String mainAddress) {
        this.mainAddress = mainAddress;
    }

    public String getPoint_wx() {
        return point_wx;
    }

    public void setPoint_wx(String point_wx) {
        this.point_wx = point_wx;
    }

    public String getPoint_wy() {
        return point_wy;
    }

    public void setPoint_wy(String point_wy) {
        this.point_wy = point_wy;
    }

    public String getIsNewAddress() {
        return isNewAddress;
    }

    public void setIsNewAddress(String isNewAddress) {
        this.isNewAddress = isNewAddress;
    }

    public String getBuildingAddress() {
        return buildingAddress;
    }

    public void setBuildingAddress(String buildingAddress) {
        this.buildingAddress = buildingAddress;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getNewAddress() {
        return newAddress;
    }

    public void setNewAddress(String newAddress) {
        this.newAddress = newAddress;
    }

    public String getLocalName_2() {
        return localName_2;
    }

    public void setLocalName_2(String localName_2) {
        this.localName_2 = localName_2;
    }

    public String getLocalName_3() {
        return localName_3;
    }

    public void setLocalName_3(String localName_3) {
        this.localName_3 = localName_3;
    }

    public String getLocalName_1() {
        return localName_1;
    }

    public void setLocalName_1(String localName_1) {
        this.localName_1 = localName_1;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getPoint_x() {
        return point_x;
    }

    public void setPoint_x(String point_x) {
        this.point_x = point_x;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getZone_no() {
        return zone_no;
    }

    public void setZone_no(String zone_no) {
        this.zone_no = zone_no;
    }

    public String getSubAddress() {
        return subAddress;
    }

    public void setSubAddress(String subAddress) {
        this.subAddress = subAddress;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPoint_y() {
        return point_y;
    }

    public void setPoint_y(String point_y) {
        this.point_y = point_y;
    }
}
