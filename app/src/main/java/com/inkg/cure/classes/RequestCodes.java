package com.inkg.cure.classes;

/**
 * Created by inkg on 2017. 3. 9..
 */

public class RequestCodes {

    public static final int PICK_FROM_ALBUM = 200;
    public static final int PICK_FROM_CAMERA = 201;
    public static final int CROP_FROM_IMAGE = 202;
}
