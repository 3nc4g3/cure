package com.inkg.cure.classes;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

import com.inkg.cure.R;

/**
 * Created by inkg on 2017. 2. 25..
 */

public class LoadingDialog extends Dialog {

    private ImageView img_loading_dialog;
    private AnimationDrawable animationDrawable;
    private Context mContext;

    public LoadingDialog(Context context) {
        super(context);
        this.mContext = context;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        setContentView(R.layout.layout_loading_dialog);
        img_loading_dialog = (ImageView) findViewById(R.id.img_loading_dialog);
        img_loading_dialog.setImageResource(R.drawable.loading_animation);
        img_loading_dialog.setBackgroundColor(Color.TRANSPARENT);
        animationDrawable = (AnimationDrawable) img_loading_dialog.getDrawable();
    }

    public LoadingDialog(Context context, int theme) {
        super(context, theme);
    }

    @Override
    public void show() {
        setView();
        super.show();
    }

    @Override
    public void dismiss() {
        removeView();
        super.dismiss();
    }

    public void setView() {
        img_loading_dialog.setVisibility(View.VISIBLE);
        animationDrawable.start();
    }

    public void removeView() {
        img_loading_dialog.setVisibility(View.GONE);
        animationDrawable.stop();
    }
}
