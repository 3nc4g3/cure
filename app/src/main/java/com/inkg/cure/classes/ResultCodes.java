package com.inkg.cure.classes;

/**
 * Created by inkg on 2016. 11. 25..
 */

public class ResultCodes {

    public static final int FB_LOGIN = 1001;
    public static final int GG_LOGIN = 1010;
    public static final int NV_LOGIN = 1011;
    public static final int AN_LOGIN = 1100;
    public static final int KK_LOGIN = 1101;
    public static final int AN_JOIN = 1110;

    public static final int BACK = 100;

    public static final int CURE_LOGOUT = 9999;

}
