package com.inkg.cure.classes;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.inkg.cure.R;
import com.inkg.cure.share.FacebookShareActivity;
import com.inkg.cure.share.KaKaoTalkShareActivity;

/**
 * Created by inkg on 2017. 3. 8..
 */

public class ShareDialog extends Dialog {

    private Context mContext;

    public ShareDialog(Context context) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_share);
        this.mContext = context;
    }

    public void setupDialog(final String title, final String contents, final String image, final String link) {
        TextView txt_share_title = (TextView) findViewById(R.id.txt_share_title);
        ImageView img_share_kakaotalk = (ImageView) findViewById(R.id.img_share_kakaotalk);
        ImageView img_share_kakaostory = (ImageView) findViewById(R.id.img_share_kakaostory);
        ImageView img_share_facebook = (ImageView) findViewById(R.id.img_share_facebook);

        img_share_kakaotalk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent kakao_intent = new Intent(mContext, KaKaoTalkShareActivity.class);
                kakao_intent.putExtra("title", title);
                kakao_intent.putExtra("image", image);
                kakao_intent.putExtra("link", link);
                kakao_intent.putExtra("contents", contents);
                mContext.startActivity(kakao_intent);
                dismiss();
            }
        });
        img_share_kakaostory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((Activity) mContext).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(mContext.getApplicationContext(), "준비중입니다.", Toast.LENGTH_SHORT).show();
                        dismiss();
                    }
                });

            }
        });
        img_share_facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent facebook_intent = new Intent(mContext, FacebookShareActivity.class);
                facebook_intent.putExtra("title", title);
                facebook_intent.putExtra("image", image);
                facebook_intent.putExtra("link", link);
                facebook_intent.putExtra("contents", contents);
                mContext.startActivity(facebook_intent);
                dismiss();
            }
        });
    }
}
