package com.inkg.cure.classes;

/**
 * Created by inkg on 2017. 2. 27..
 */

public class Fonts {

    public static final String typeface_kr_black = "NotoSansKR-Black.otf";
    public static final String typeface_kr_bold = "NotoSansKR-Bold.otf";
    public static final String typeface_kr_medium = "NotoSansKR-Medium.otf";
    public static final String typeface_kr_regular = "NotoSansKR-Regular.otf";
    public static final String typeface_cjkkr_demilight = "NotoSansCJKkr-DemiLight.otf";
    public static final String typeface_cjkkr_medium = "NotoSansCJKkr-Medium.otf";
    public static final String typeface_cjkkr_regular = "NotoSansCJKkr-Regular.otf";
    public static final String typeface_cjksc_demilight = "NotoSansCJKsc-DemiLight.otf";

}