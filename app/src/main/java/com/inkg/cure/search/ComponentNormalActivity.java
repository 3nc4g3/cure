package com.inkg.cure.search;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.inkg.cure.R;
import com.inkg.cure.adapters.DialogAdapter;
import com.inkg.cure.adapters.Ingr1Adapter;
import com.inkg.cure.classes.CureSP;
import com.inkg.cure.classes.HttpConnect;
import com.inkg.cure.classes.Ingredient;
import com.inkg.cure.classes.JsonMaker;
import com.inkg.cure.review.EditActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by inkg on 2017. 2. 14..
 */

public class ComponentNormalActivity extends AppCompatActivity {

    private ArrayList<Ingredient> ingr_array;
    private ListView list_component_normal;
    private Ingr1Adapter ingr1Adapter;
    private TextView txt_component_normal_ingredient;
    private TextView txt_component_normal_functional;

    // dur
    private ImageView img_component_warn_combi;
    private TextView txt_component_warn_combi;
    private ImageView img_component_warn_period;
    private TextView txt_component_warn_period;
    private ImageView img_component_warn_divide;
    private TextView txt_component_warn_divide;
    private ImageView img_component_warn_old;
    private TextView txt_component_warn_old;
    private ImageView img_component_warn_blood;
    private TextView txt_component_warn_blood;
    private ImageView img_component_warn_pregnant;
    private TextView txt_component_warn_pregnant;
    private ImageView img_component_warn_baby;
    private TextView txt_component_warn_baby;

    private ImageView img_component_ingredient_bar_red;
    private ImageView img_component_ingredient_bar_blue;

    private Button btn_component_edit;

    private int[] dur_checker;
    private int dur_count = 0;

    private String product_name;
    private String company_name;
    private String id;
    private Bitmap main_bm;

    private CureSP csp;
    private HttpConnect httpConnect;
    private JsonMaker jsonMaker;
    private boolean is_like;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_component_normal);
        Intent intent = getIntent();
        String result = intent.getStringExtra("ingredient");
        dur_checker = intent.getIntArrayExtra("dur");
        dur_count = intent.getIntExtra("dur_count", 0);
        product_name = intent.getStringExtra("product_name");
        company_name = intent.getStringExtra("company_name");
        byte[] bytes = intent.getByteArrayExtra("bitmap");
        main_bm = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
        id = intent.getStringExtra("id");

        csp = new CureSP(getApplicationContext());
        httpConnect = new HttpConnect(getApplicationContext());
        jsonMaker = new JsonMaker();

        setData(result);
        setUpViews();
        setCustomActionbar();
    }

    private void setData(String result) {
        ingr_array = new ArrayList<>();
        Log.e("ingr result", result);
        try {
            JSONArray ingr_json = new JSONArray(result);
            for (int i = 0; i < ingr_json.length(); i++) {
                JSONObject ingr_object = ingr_json.getJSONObject(i);
                Ingredient ingr = new Ingredient();
                String name = ingr_object.getString("name");
                String kor_name = name.substring(0, name.indexOf("("));
                String eng_name = name.substring(name.indexOf("(") + 1, name.indexOf(")"));
                ingr.setName(name);
                ingr.setKor_name(kor_name);
                ingr.setEng_name(eng_name);

                Pair<String, String> ingr_pair;
                if (ingr_object.has("fda_comment")) {
                    ingr.setFda_comment(ingr_object.getString("fda_comment"));
                }
                if (ingr_object.has("fda_grade")) {
                    ingr.setFda_grade(ingr_object.getString("fda_grade"));
                }

                ingr_array.add(ingr);
            }
        } catch (Exception e) {
            Log.e("ingr norm", "ingr excep", e);
        }
    }

    private void setUpViews() {
        list_component_normal = (ListView) findViewById(R.id.list_component_normal);
        ingr1Adapter = new Ingr1Adapter(getApplicationContext(), R.layout.adapter_ingr1_item, ingr_array);
        list_component_normal.setAdapter(ingr1Adapter);
        list_component_normal.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int position, long l) {
                ArrayList<Pair<String, String>> pair_list = new ArrayList<>();
                pair_list.add(0, new Pair<String, String>(ingr_array.get(position).getFda_comment(), ingr_array.get(position).getFda_grade()));
                final DialogAdapter dialog_adapter = new DialogAdapter(getApplicationContext(), R.layout.dialog_content, pair_list);
                final LayoutInflater title_inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                AlertDialog.Builder ab = new AlertDialog.Builder(view.getRootView().getContext());
                View title_view = title_inflater.inflate(R.layout.dialog_title, null);
                TextView txt_dialog_kor = (TextView) title_view.findViewById(R.id.txt_dialog_kor);
                TextView txt_dialog_eng = (TextView) title_view.findViewById(R.id.txt_dialog_eng);
                ab.setCustomTitle(title_view);
                txt_dialog_kor.setText(ingr_array.get(position).getKor_name());
                txt_dialog_eng.setText(ingr_array.get(position).getEng_name());
                final ImageView img_dialog_good = (ImageView) title_view.findViewById(R.id.img_dialog_good);
                img_dialog_good.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (is_like) {
                            is_like = false;
                            img_dialog_good.setBackground(getResources().getDrawable(R.drawable.product_icon_heart_nor, null));

                            BookmarkAsync ba = new BookmarkAsync(ingr_array.get(position).getName(), false);
                            ba.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        } else {
                            is_like = true;
                            img_dialog_good.setBackground(getResources().getDrawable(R.drawable.product_icon_heart_active, null));

                            BookmarkAsync ba = new BookmarkAsync(ingr_array.get(position).getName(), true);
                            ba.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        }
                    }
                });
                ab.setAdapter(dialog_adapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                }).create();
                final AlertDialog ad = ab.create();
                Button btn_dialog_title = (Button) title_view.findViewById(R.id.btn_dialog_title);
                btn_dialog_title.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ad.dismiss();
                    }
                });
                ad.show();
            }
        });


        txt_component_normal_ingredient = (TextView)

                findViewById(R.id.txt_component_normal_ingredient);

        txt_component_normal_functional = (TextView)

                findViewById(R.id.txt_component_normal_functional);

        img_component_warn_combi = (ImageView)

                findViewById(R.id.img_component_warn_combi);

        txt_component_warn_combi = (TextView)

                findViewById(R.id.txt_component_warn_combi);

        img_component_warn_period = (ImageView)

                findViewById(R.id.img_component_warn_period);

        txt_component_warn_period = (TextView)

                findViewById(R.id.txt_component_warn_period);

        img_component_warn_divide = (ImageView)

                findViewById(R.id.img_component_warn_divide);

        txt_component_warn_divide = (TextView)

                findViewById(R.id.txt_component_warn_divide);

        img_component_warn_old = (ImageView)

                findViewById(R.id.img_component_warn_old);

        txt_component_warn_old = (TextView)

                findViewById(R.id.txt_component_warn_old);

        img_component_warn_blood = (ImageView)

                findViewById(R.id.img_component_warn_blood);

        txt_component_warn_blood = (TextView)

                findViewById(R.id.txt_component_warn_blood);

        img_component_warn_pregnant = (ImageView)

                findViewById(R.id.img_component_warn_pregnant);

        txt_component_warn_pregnant = (TextView)

                findViewById(R.id.txt_component_warn_pregnant);

        img_component_warn_baby = (ImageView)

                findViewById(R.id.img_component_warn_baby);

        txt_component_warn_baby = (TextView)

                findViewById(R.id.txt_component_warn_baby);

        btn_component_edit = (Button)

                findViewById(R.id.btn_component_edit);
        btn_component_edit.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View view) {
                Intent edit_intent = new Intent(ComponentNormalActivity.this, EditActivity.class);
                edit_intent.putExtra("bitmap", main_bm);
                edit_intent.putExtra("product_name", product_name);
                edit_intent.putExtra("company_name", company_name);
                edit_intent.putExtra("id", id);
                startActivity(edit_intent);

            }
        });

        img_component_ingredient_bar_red = (ImageView)

                findViewById(R.id.img_component_ingredient_bar_red);

        img_component_ingredient_bar_blue = (ImageView)

                findViewById(R.id.img_component_ingredient_bar_blue);

        final float scale = getResources().getDisplayMetrics().density;
        int width_red = (int) ((315 * scale) * (dur_count / 7.0f));
        int width_blue = (int) ((315 * scale) * ((7 - dur_count) / 7.0f));
        int height = (int) (14.75 * scale);

        RelativeLayout.LayoutParams lp_red = new RelativeLayout.LayoutParams(width_red, height);
        RelativeLayout.LayoutParams lp_blue = new RelativeLayout.LayoutParams(width_blue, height);
        lp_red.setMargins((int) (22.5 * scale), (int) (12.25 * scale), 0, 0);
        lp_red.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
        lp_red.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
        lp_blue.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
        lp_blue.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
        lp_blue.setMargins(0, (int) (12.25 * scale), (int) (22.5 * scale), 0);
        img_component_ingredient_bar_red.setLayoutParams(lp_red);
        img_component_ingredient_bar_blue.setLayoutParams(lp_blue);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (dur_checker[0] == 0) {
                    img_component_warn_combi.setImageDrawable(getResources().getDrawable(R.drawable.mix_inactive, null));
                    txt_component_warn_combi.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.taupe_gray));
                } else {
                    img_component_warn_combi.setImageDrawable(getResources().getDrawable(R.drawable.mix_active, null));
                    txt_component_warn_combi.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.pastel_red));
                }

                if (dur_checker[1] == 0) {
                    img_component_warn_period.setImageDrawable(getResources().getDrawable(R.drawable.date_inactive, null));
                    txt_component_warn_period.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.taupe_gray));
                } else {
                    img_component_warn_period.setImageDrawable(getResources().getDrawable(R.drawable.date_active, null));
                    txt_component_warn_period.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.pastel_red));
                }

                if (dur_checker[2] == 0) {
                    img_component_warn_divide.setImageDrawable(getResources().getDrawable(R.drawable.break_inactive, null));
                    txt_component_warn_divide.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.taupe_gray));
                } else {
                    img_component_warn_divide.setImageDrawable(getResources().getDrawable(R.drawable.break_active, null));
                    txt_component_warn_divide.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.pastel_red));
                }

                if (dur_checker[3] == 0) {
                    img_component_warn_old.setImageDrawable(getResources().getDrawable(R.drawable.older_inactive, null));
                    txt_component_warn_old.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.taupe_gray));
                } else {
                    img_component_warn_old.setImageDrawable(getResources().getDrawable(R.drawable.older_active, null));
                    txt_component_warn_old.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.pastel_red));
                }

                if (dur_checker[4] == 0) {
                    img_component_warn_blood.setImageDrawable(getResources().getDrawable(R.drawable.blood_inactive, null));
                    txt_component_warn_blood.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.taupe_gray));
                } else {
                    img_component_warn_blood.setImageDrawable(getResources().getDrawable(R.drawable.blood_active, null));
                    txt_component_warn_blood.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.pastel_red));
                }

                if (dur_checker[5] == 0) {
                    img_component_warn_pregnant.setImageDrawable(getResources().getDrawable(R.drawable.pregnant_inactive, null));
                    txt_component_warn_pregnant.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.taupe_gray));
                } else {
                    img_component_warn_pregnant.setImageDrawable(getResources().getDrawable(R.drawable.pregant_active, null));
                    txt_component_warn_pregnant.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.pastel_red));
                }

                if (dur_checker[6] == 0) {
                    img_component_warn_baby.setImageDrawable(getResources().getDrawable(R.drawable.baby_inactive, null));
                    txt_component_warn_baby.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.taupe_gray));
                } else {
                    img_component_warn_baby.setImageDrawable(getResources().getDrawable(R.drawable.baby_active, null));
                    txt_component_warn_baby.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.pastel_red));
                }

            }
        });


    }

    class BookmarkAsync extends AsyncTask<String, String, String> {

        private boolean is_like = false;
        private String name;

        BookmarkAsync(String name, boolean is_like) {
            this.name = name;
            this.is_like = is_like;
        }

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            String json = jsonMaker.makeJson(new Pair("name", name));
            if (is_like) {
                result = httpConnect.send(json, httpConnect.getServerWithVersion() + getString(R.string.rest_like_ingredient), csp.getValue("access_token", ""));
            } else {
                result = httpConnect.send(json, httpConnect.getServerWithVersion() + getString(R.string.rest_like_ingredient), csp.getValue("access_token", ""), "DELETE");
            }

            return result;
        }

    }

    private void setCustomActionbar() {

        ActionBar actionBar = getSupportActionBar();

        // Custom Actionbar를 사용하기 위해 CustomEnabled을 true 시키고 필요 없는 것은 false 시킨다

        actionBar.setDisplayShowCustomEnabled(true);

        actionBar.setDisplayHomeAsUpEnabled(false);

        actionBar.setDisplayShowTitleEnabled(false);

        // Set custom view layout

        View mCustomView = LayoutInflater.from(this).inflate(R.layout.actionbar_simple, null);

        actionBar.setCustomView(mCustomView);

        // Set no padding both side

        Toolbar parent = (Toolbar) mCustomView.getParent(); // first get parent toolbar of current action bar

        parent.setContentInsetsAbsolute(0, 0);              // set padding programmatically to 0dp

        // Set actionbar background image

        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.border_layout_bottom));

        // Set actionbar layout layoutparams

        ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);

        actionBar.setCustomView(mCustomView, params);

        ImageView img_actionbar_back = (ImageView) findViewById(R.id.img_actionbar_back);
        img_actionbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        TextView txt_actionbar = (TextView) findViewById(R.id.txt_actionbar_simple);
        txt_actionbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        txt_actionbar.setText("성분");
    }
}
