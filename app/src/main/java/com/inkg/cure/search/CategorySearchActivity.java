package com.inkg.cure.search;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.inkg.cure.R;
import com.inkg.cure.adapters.SearchResultAdapter;
import com.inkg.cure.classes.CureSP;
import com.inkg.cure.classes.GlobalApplication;
import com.inkg.cure.classes.HttpConnect;
import com.inkg.cure.classes.JsonMaker;
import com.inkg.cure.classes.LoadingDialog;
import com.inkg.cure.classes.RankData;
import com.inkg.cure.classes.Scores;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by inkg on 2017. 2. 13..
 */

public class CategorySearchActivity extends AppCompatActivity implements AbsListView.OnScrollListener {

    private String category = "";
    private String access_token = "";
    private HttpConnect httpConnect;
    private JsonMaker jsonMaker;
    private CureSP csp;
    private ArrayList<String> health_sub_array = new ArrayList<>();
    private ArrayList<String> medicine_sub_array = new ArrayList<>();
    private ArrayList<RankData> rank_array = new ArrayList<>();
    private ArrayList<RankData> rank_partial_array = new ArrayList<>();
    private ArrayList<Button> sub_category_array = new ArrayList<>();

//    private Button btn_category_search_order;
//    private TextView txt_category_search_order;

    private ListView list_category_search_item;
    private SearchResultAdapter adapter_rank_data;

    private TextView txt_category_search_medicine;
    private TextView txt_category_search_healthfood;

    private String temp_medicine;
    private String temp_healthfood;

    private String type = "medicine";

    //    private ProgressDialog loadingDialog;
    private LayoutInflater mInflater;
    private LoadingDialog loadingDialog;

    private boolean mLockListView;
    private int current_index = 10;
    private boolean lastitemVisibleFlag = false;

    private String m_sub_category = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_search);

        GlobalApplication.setCurrentActivity(this);
        loadingDialog = new LoadingDialog(CategorySearchActivity.this);

        rank_array = new ArrayList<>();
        rank_partial_array = new ArrayList<>();

        httpConnect = new HttpConnect(getApplicationContext());
        jsonMaker = new JsonMaker();
        csp = new CureSP(getApplicationContext());

        access_token = csp.getValue("access_token", "");

        Intent intent = getIntent();
        category = intent.getStringExtra("category");
        Log.e("category", category);

        setupViews();
        setCustomActionbar();

        InitCategoryAsync ipAsync = new InitCategoryAsync();
        ipAsync.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    private void setupViews() {
        txt_category_search_medicine = (TextView) findViewById(R.id.txt_category_search_medicine);
        txt_category_search_healthfood = (TextView) findViewById(R.id.txt_category_search_healthfood);

        mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        list_category_search_item = (ListView) findViewById(R.id.list_category_search_item);
        list_category_search_item.setSmoothScrollbarEnabled(true);
        list_category_search_item.setFastScrollEnabled(false);
//        list_category_search_item.addFooterView(mInflater.inflate(R.layout.dialog_custom_title, null));
        list_category_search_item.setOnScrollListener(this);
        adapter_rank_data = new SearchResultAdapter(getApplicationContext(), R.layout.adapter_category_search, rank_partial_array, type);
        list_category_search_item.setAdapter(adapter_rank_data);

        list_category_search_item.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent detail_intent = null;
                if ("medicine".equals(type)) {
                    detail_intent = new Intent(CategorySearchActivity.this, MedicineDetailActivity.class);
                } else if ("healthfood".equals(type)) {
                    detail_intent = new Intent(CategorySearchActivity.this, HealthfoodDetailActivity.class);
                } else {

                }
                detail_intent.putExtra("id", rank_partial_array.get(position).getId());
                detail_intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                view.getContext().startActivity(detail_intent);
            }
        });

        txt_category_search_medicine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                type = "medicine";
                rank_array.clear();
                rank_partial_array.clear();
                current_index = 10;
                txt_category_search_healthfood.setTextColor(ContextCompat.getColor(CategorySearchActivity.this, R.color.trolley_grey1));
                txt_category_search_medicine.setTextColor(ContextCompat.getColor(CategorySearchActivity.this, R.color.maya_blue1));
                adapter_rank_data = new SearchResultAdapter(getApplicationContext(), R.layout.adapter_category_search, rank_partial_array, type);
                list_category_search_item.setAdapter(adapter_rank_data);
                InitCategoryAsync ipAsync = new InitCategoryAsync("medicine");
                ipAsync.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        });

        txt_category_search_healthfood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                type = "healthfood";
                rank_array.clear();
                rank_partial_array.clear();
                current_index = 10;
                txt_category_search_healthfood.setTextColor(ContextCompat.getColor(CategorySearchActivity.this, R.color.maya_blue1));
                txt_category_search_medicine.setTextColor(ContextCompat.getColor(CategorySearchActivity.this, R.color.trolley_grey1));
                adapter_rank_data = new SearchResultAdapter(getApplicationContext(), R.layout.adapter_category_search, rank_partial_array, type);
                list_category_search_item.setAdapter(adapter_rank_data);
                InitCategoryAsync ipAsync = new InitCategoryAsync("healthfood");
                ipAsync.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        });
    }

    @Override
    public void onScrollStateChanged(AbsListView absListView, int scrollState) {
        if (lastitemVisibleFlag && mLockListView == false) {
            if (rank_array.size() == rank_partial_array.size()) {
                mLockListView = true;
            } else {
                Log.e("se re ac", "Loading next items");
                // 아이템을 추가하는 동안 중복 요청을 방지하기 위해 락을 걸어둡니다.
                mLockListView = true;
                addItems(10);
            }
        }
    }

    @Override
    public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        // 현재 가장 처음에 보이는 셀번호와 보여지는 셀번호를 더한값이
        // 전체의 숫자와 동일해지면 가장 아래로 스크롤 되었다고 가정합니다.
        int count = totalItemCount - visibleItemCount;
        lastitemVisibleFlag = (totalItemCount > 0) && (firstVisibleItem + visibleItemCount >= totalItemCount);
    }

    private void addItems(final int size) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                loadingDialog.show();
            }
        });
        Runnable run = new Runnable() {
            @Override
            public void run() {
                for (int i = current_index; i < current_index + size; i++) {

                    if (current_index >= rank_array.size()) {
                        break;
                    }
                    try {
                        rank_partial_array.add(rank_array.get(i));
                    } catch (Exception e) {
                        Log.e("cate ac", "partial_array excep", e);
                    } finally {
                        mLockListView = true;
                    }
                }
                current_index = current_index + size;

                // 모든 데이터를 로드하여 적용하였다면 어댑터에 알리고
                // 리스트뷰의 락을 해제합니다.
                adapter_rank_data.notifyDataSetChanged();
                mLockListView = false;
                loadingDialog.dismiss();
            }
        };

        runOnUiThread(run);
    }

    class InitCategoryAsync extends AsyncTask<String, String, String> {

        String m_type = "";

        public InitCategoryAsync() {

        }

        public InitCategoryAsync(String type) {
            this.m_type = type;
        }

        public InitCategoryAsync(String type, String sub_category) {
            this.m_type = type;
            m_sub_category = sub_category;
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            result = httpConnect.send("", getString(R.string.server_url) + getString(R.string.cureya_version) + getString(R.string.rest_category_sub) + "?category=" + category, access_token, "GET");
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
//            Log.e("sub result", result);

            try {
                JSONObject sub_result = new JSONObject(result);
                String medicine = sub_result.getString("medicine");
                String healthfood = sub_result.getString("healthfood");

                temp_medicine = medicine;
                temp_healthfood = healthfood;

                InitRankAsync irAsync = new InitRankAsync(medicine, healthfood, m_type, m_sub_category);
                irAsync.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

            } catch (Exception e) {
                Log.e("search result", "get json exception", e);
            }
        }
    }

    class InitRankAsync extends AsyncTask<String, String, String> {

        String m_medicine;
        String m_healthfood;
        String m_type;
        String m_sub_category;

        public InitRankAsync(String medicine, String healthfood) {
            this.m_medicine = medicine;
            this.m_healthfood = healthfood;
            this.m_type = type;
        }

        public InitRankAsync(String medicine, String healthfood, String my_type) {
            this.m_medicine = medicine;
            this.m_healthfood = healthfood;
            this.m_type = my_type;
            if ("".equals(my_type)) {
                this.m_type = "medicine";
            }
        }

        public InitRankAsync(String medicine, String healthfood, String my_type, String m_sub_category) {
            this.m_medicine = medicine;
            this.m_healthfood = healthfood;
            this.m_type = my_type;
            if ("".equals(my_type)) {
                this.m_type = "medicine";
            }
            this.m_sub_category = m_sub_category;
            if ("".equals(m_sub_category)) {
                this.m_sub_category = "전체";
            }
        }

        @Override
        protected void onPreExecute() {
            getSubCategories(m_medicine, m_healthfood);
        }

        private void getSubCategories(String medicine, String healthfood) {

            try {
                medicine_sub_array.clear();
                health_sub_array.clear();

                JSONArray medicine_array = new JSONArray(medicine);
                JSONArray healthfood_array = new JSONArray(healthfood);

                for (int i = 0; i < medicine_array.length(); i++) {
                    medicine_sub_array.add(i + 1, medicine_array.get(i).toString());
                }
                for (int j = 0; j < healthfood_array.length(); j++) {
                    health_sub_array.add(j + 1, healthfood_array.get(j).toString());
                }
            } catch (Exception e) {
                Log.e("Search result", "get subcate excep", e);
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            Log.e("my_type", m_type);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    sub_category_array.clear();
                    HorizontalScrollView scroll_category_search_sub_category = (HorizontalScrollView) findViewById(R.id.scroll_category_search_sub_category);
                    LinearLayout layout_category_search_sub_category = (LinearLayout) findViewById(R.id.layout_category_search_sub_category);
                    scroll_category_search_sub_category.removeAllViews();
                    layout_category_search_sub_category.removeAllViews();
                    if ("medicine".equals(m_type)) {
                        for (int i = 0; i < medicine_sub_array.size(); i++) {
                            Button btn_medicine = new Button(CategorySearchActivity.this);
                            btn_medicine.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
                            btn_medicine.setText(medicine_sub_array.get(i).toString());
                            btn_medicine.setTextSize(TypedValue.COMPLEX_UNIT_SP, (float) 13.5);
                            btn_medicine.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                            btn_medicine.setTextColor(ContextCompat.getColor(CategorySearchActivity.this, R.color.manatee2));
                            btn_medicine.setBackground(null);
                            sub_category_array.add(i, btn_medicine);
                            layout_category_search_sub_category.addView(btn_medicine);
                        }
                        scroll_category_search_sub_category.addView(layout_category_search_sub_category);

                        final int sub_size = sub_category_array.size();

                        for (int i = 0; i < sub_size; i++) {
                            final int finalI = i;
                            sub_category_array.get(i).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    for (int j = 0; j < sub_size; j++) {
                                        if (finalI == j) {
//                                            sub_category_array.get(j).setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, R.drawable.main_menubar_bottom);
                                            sub_category_array.get(j).setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.maya_blue1));
                                        } else {
//                                            sub_category_array.get(j).setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                                            sub_category_array.get(j).setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.manatee2));
                                        }
                                    }

                                    InitSubCategoryAsync isca = new InitSubCategoryAsync(m_type, medicine_sub_array.get(finalI).toString());
                                    isca.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                                }
                            });
                        }
                    } else {
                        for (int i = 0; i < health_sub_array.size(); i++) {
                            Button btn_healthfood = new Button(CategorySearchActivity.this);
                            btn_healthfood.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
                            btn_healthfood.setText(health_sub_array.get(i).toString());
                            btn_healthfood.setTextSize(TypedValue.COMPLEX_UNIT_SP, (float) 13.5);
                            btn_healthfood.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                            btn_healthfood.setTextColor(ContextCompat.getColor(CategorySearchActivity.this, R.color.manatee2));
                            btn_healthfood.setBackground(null);
                            sub_category_array.add(i, btn_healthfood);
                            layout_category_search_sub_category.addView(btn_healthfood);
                        }
                        scroll_category_search_sub_category.addView(layout_category_search_sub_category);

                        final int sub_size = sub_category_array.size();

                        for (int i = 0; i < sub_size; i++) {
                            final int finalI = i;
                            sub_category_array.get(i).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    for (int j = 0; j < sub_size; j++) {
                                        if (finalI == j) {
//                                            sub_category_array.get(j).setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, R.drawable.main_menubar_bottom);
                                            sub_category_array.get(j).setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.maya_blue1));
                                        } else {
//                                            sub_category_array.get(j).setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                                            sub_category_array.get(j).setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.manatee2));
                                        }
                                    }

                                    InitSubCategoryAsync isca = new InitSubCategoryAsync(m_type, health_sub_array.get(finalI).toString());
                                    isca.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                                }
                            });
                        }
                    }
                }
            });

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            InitSubCategoryAsync isca = new InitSubCategoryAsync(m_type, m_sub_category);
            isca.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        }
    }

    class InitSubCategoryAsync extends AsyncTask<String, String, String> {

        String m_type = "";

        InitSubCategoryAsync(String type, String sub_category) {
            this.m_type = type;
            m_sub_category = sub_category;
        }

        @Override
        protected void onPreExecute() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loadingDialog.show();
                    if ("".equals(m_sub_category)) {
                        sub_category_array.get(0).setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.maya_blue1));
                    } else {
                        int temp_idx = 0;
                        temp_idx = sub_category_array.indexOf(m_sub_category);
                        if (temp_idx != -1)
                            sub_category_array.get(temp_idx).setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.maya_blue1));
                    }
                }
            });
        }

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            String sub_category = m_sub_category;
            try {
                if ("medicine".equals(m_type)) {
                    if ("".equals(m_sub_category)) {
                        sub_category = URLEncoder.encode("", "utf-8");
                    } else {
                        sub_category = URLEncoder.encode(m_sub_category, "utf-8");
                    }

                } else {
                    if ("".equals(m_sub_category)) {
                        sub_category = URLEncoder.encode("", "utf-8");
                    } else {
                        sub_category = URLEncoder.encode(m_sub_category, "utf-8");
                    }

                }
            } catch (Exception e) {
                Log.e("Se re ac", "category encode excep", e);
            }
            result = httpConnect.send("", getString(R.string.server_url) + getString(R.string.cureya_version) + getString(R.string.rest_rank) + "?type=" + m_type + "&category=" + sub_category, csp.getValue("access_token", ""), "GET");

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            InitViewAsync ivAsync = new InitViewAsync(result);
            ivAsync.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    class InitViewAsync extends AsyncTask<String, String, String> {

        String result = "";

        public InitViewAsync(String result) {
            this.result = result;
        }

        @Override
        protected void onPreExecute() {
            getRankData(result);
        }

        @Override
        protected String doInBackground(String... strings) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if ("".equals(m_sub_category)) {
                        sub_category_array.get(0).setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.maya_blue1));
                    }
                    adapter_rank_data.notifyDataSetChanged();
                }
            });
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loadingDialog.dismiss();
                }
            });
        }
    }

    private void getRankData(String result) {
        try {
//            Log.e("cate result", result);
            rank_array.clear();
            rank_partial_array.clear();
            JSONArray rank_json_array = new JSONArray(result);
            for (int i = 0; i < rank_json_array.length(); i++) {
                if (i == 20) { // 20개 제한
                    break;
                }
                JSONObject rank_json_object = rank_json_array.getJSONObject(i);

                RankData rankData = new RankData();
                rankData.setCompany(rank_json_object.getString("company"));
                rankData.setId(rank_json_object.getString("id"));
                rankData.setName(rank_json_object.getString("name"));
                rankData.setSelect_image(rank_json_object.getString("select_image"));
//                rankData.setSelect_image(rank_json_object.getString("thumbnail"));

                JSONObject rank_json_scores = rank_json_object.getJSONObject("scores");
                Scores scores = new Scores();
                scores.setBookmarks(rank_json_scores.getString("bookmarks"));
                scores.setLikes(rank_json_scores.getString("likes"));
                scores.setReviews(rank_json_scores.getString("reviews"));
                scores.setShares(rank_json_scores.getString("shares"));
                scores.setUnlikes(rank_json_scores.getString("unlikes"));
                if (rank_json_scores.has("views")) {
                    scores.setViews(rank_json_scores.getString("views"));
                }
                rankData.setScores(scores);

                rank_array.add(i, rankData);
            }
            int temp_index = 0;
            if (rank_array.size() > 10) {
                temp_index = 10;
            } else {
                temp_index = rank_array.size();
            }
            for (int i = 0; i < temp_index; i++) {
                rank_partial_array.add(rank_array.get(i));
            }
        } catch (Exception e) {
            Log.e("search result", "get rank data excep", e);
        }
    }

    private void setCustomActionbar() {

        ActionBar actionBar = getSupportActionBar();

        // Custom Actionbar를 사용하기 위해 CustomEnabled을 true 시키고 필요 없는 것은 false 시킨다

        actionBar.setDisplayShowCustomEnabled(true);

        actionBar.setDisplayHomeAsUpEnabled(false);

        actionBar.setDisplayShowTitleEnabled(false);

        // Set custom view layout

        View mCustomView = LayoutInflater.from(this).inflate(R.layout.actionbar_simple, null);

        actionBar.setCustomView(mCustomView);

        // Set no padding both side

        Toolbar parent = (Toolbar) mCustomView.getParent(); // first get parent toolbar of current action bar

        parent.setContentInsetsAbsolute(0, 0);              // set padding programmatically to 0dp

        // Set actionbar background image

        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.border_layout_bottom));

        // Set actionbar layout layoutparams

        ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);

        actionBar.setCustomView(mCustomView, params);

        ImageView img_actionbar_back = (ImageView) findViewById(R.id.img_actionbar_back);
        img_actionbar_back.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    finish();
                }
                return false;
            }
        });

        TextView txt_actionbar = (TextView) findViewById(R.id.txt_actionbar_simple);
        txt_actionbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        txt_actionbar.setText(this.category);
    }

}
