package com.inkg.cure.search;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.inkg.cure.NearPharmacyActivity;
import com.inkg.cure.R;
import com.inkg.cure.adapters.ProductReviewAdapter;
import com.inkg.cure.classes.CureSP;
import com.inkg.cure.classes.HttpConnect;
import com.inkg.cure.classes.JsonMaker;
import com.inkg.cure.classes.LoadingDialog;
import com.inkg.cure.classes.Review;
import com.inkg.cure.classes.Scores;
import com.inkg.cure.classes.ShareDialog;
import com.inkg.cure.review.EditActivity;
import com.inkg.cure.review.ReadActivity;
import com.inkg.cure.review.WriteActivity;
import com.inkg.cure.shopping.NaverShoppingActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by inkg on 2017. 2. 13..
 */

public class HealthfoodDetailActivity extends AppCompatActivity {

    private String access_token;
    private HttpConnect httpConnect;
    private JsonMaker jsonMaker;
    private CureSP csp;

    private String company;
    private String warning;
    private String advice;
    private static String id;
    private String use;
    private String effect;
    private Scores scores;
    private String LCNS_NO;
    private String name;
    private String select_image;
    private String maker;
    private ArrayList<String> functional_ingredient_array = new ArrayList<>();
    private ArrayList<String> capsule_ingredient_array = new ArrayList<>();
    private ArrayList<String> etc_ingredient_array = new ArrayList<>();
    private String expire_date;
    private String save;
    private String detail_result;

    private RelativeLayout layout_health_detail_need_review;

    private ArrayList<Review> review_array = new ArrayList<>();

    private ImageView img_health_detail_medicine;
    private TextView txt_health_detail_company;
    private TextView txt_health_detail_product;
    private TextView txt_health_detail_like_count;
    private TextView txt_health_detail_dislike_count;
    private Button btn_health_detail_how_to_use;
    private Button btn_health_detail_warning;
    private Button btn_health_detail_price;
    private Button btn_health_detail_modify;
    private Button btn_health_detail_ingredient;
    private Button btn_health_detail_review_all;
    private Button btn_health_detail_write;
    private Button btn_review_write;

    private ArrayList<String> image_list = new ArrayList<>();
    private ArrayList<byte[]> bitmap_list = new ArrayList<>();

    private TextView txt_health_detail_effect;
//    private TextView txt_health_detail_advice;

    private ImageView img_health_detail_ingredient_bar_blue;
    private ImageView img_health_detail_ingredient_bar_gray;

    private TextView txt_health_detail_functional_count;

    private RelativeLayout layout_health_detail_no_review;
    private ListView list_health_detail_review;
    private ProductReviewAdapter productReviewAdapter;

    private ImageView img_health_detail_review_picture1;
    private ImageView img_health_detail_review_picture2;
    private ImageView img_health_detail_review_picture3;
    private TextView txt_health_detail_id;
    private Button btn_health_detail_review_more;

    private Button btn_health_detail_like;
    private Button btn_health_detail_dislike;

    private String functional_ingredient;
    private String capsule_ingredient;
    private String etc_ingredient;

    private String my_review = "0";
    private Bitmap main_bm;
    private String product_name;
    private String company_name;

    private boolean is_bookmark = false;
    private boolean is_like = false;
    private boolean is_unlike = false;

    private String current_likes = "0";
    private String current_unlikes = "0";

    private LoadingDialog loadingDialog;

    private long start;
    private long end;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_healthfood_detail);

        start = System.currentTimeMillis();

        httpConnect = new HttpConnect(getApplicationContext());
        csp = new CureSP(getApplicationContext());
        access_token = csp.getValue("access_token", "");
        jsonMaker = new JsonMaker();

        my_review = csp.getValue("my_review", "0");

        loadingDialog = new LoadingDialog(HealthfoodDetailActivity.this);

        Intent intent = getIntent();
        id = intent.getStringExtra("id");

        setCustomActionbar();

    }

    @Override
    protected void onStart() {
        super.onStart();
        InitDataAsync ida = new InitDataAsync();
        ida.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        end = System.currentTimeMillis();
        csp.put(id, String.valueOf((end - start) / 1000));
    }

    class InitDataAsync extends AsyncTask<String, String, String> {

        private void getDetailData(String result) {
            try {
                detail_result = result;
                JSONObject detail_json_data = new JSONObject(result);
                JSONObject score_object = detail_json_data.getJSONObject("scores");

                scores = new Scores();
                scores.setShares(score_object.get("shares").toString());
                scores.setReviews(score_object.get("reviews").toString());
                scores.setUnlikes(score_object.get("unlikes").toString());
                scores.setLikes(score_object.get("likes").toString());
                current_likes = scores.getLikes();
                current_unlikes = scores.getUnlikes();
                scores.setBookmarks(score_object.get("bookmarks").toString());
//            scores.setReview_likes(score_object.get("review_likes").toString());
//            scores.setReview_unlikes(score_object.get("review_unlikes").toString());

                JSONArray image_array = detail_json_data.getJSONArray("images");
                image_list.clear();
//            for (int i = 0; i < image_array.length(); i++) {
//                image_list.add(i, getString(R.string.cdn) + "/healthfood/" + image_array.getString(i));
//            }

                warning = detail_json_data.getString("warning");
//            advice = detail_json_data.getString("counseling");
                use = detail_json_data.getString("use");
                effect = detail_json_data.getString("effect");
                name = detail_json_data.getString("name");
                product_name = name;
                select_image = detail_json_data.getString("select_image");
//            select_image = detail_json_data.getString("thumbnail");
                company = detail_json_data.getString("company");
                company_name = company;
                expire_date = detail_json_data.getString("expire_date");

                JSONArray func_array = detail_json_data.getJSONArray("functional");
                JSONArray capsule_array = detail_json_data.getJSONArray("capsule_ingredient");
                JSONArray etc_array = detail_json_data.getJSONArray("etc_ingredient");

                functional_ingredient = func_array.toString();
                capsule_ingredient = capsule_array.toString();
                etc_ingredient = etc_array.toString();

                functional_ingredient_array.clear();
                capsule_ingredient_array.clear();
                etc_ingredient_array.clear();

                for (int i = 0; i < func_array.length(); i++) {
                    functional_ingredient_array.add(i, func_array.getString(i));
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        txt_health_detail_functional_count.setText("총 " + functional_ingredient_array.size() + "가지 기능성 원료가 포함되어 있어요.");
                    }
                });

                for (int i = 0; i < capsule_array.length(); i++) {
                    capsule_ingredient_array.add(i, capsule_array.getString(i));
                }
                for (int i = 0; i < etc_array.length(); i++) {
                    etc_ingredient_array.add(i, etc_array.getString(i));
                }

//            maker = detail_json_data.getString("maker");
            } catch (Exception e) {
                Log.e("health detail", "getDetailData excep", e);
            }
        }

        @Override
        protected void onPreExecute() {
            String result = "";
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    loadingDialog.show();
                    setUpViews();
                }
            });
            result = httpConnect.send("", getString(R.string.server_url) + getString(R.string.cureya_version) + getString(R.string.rest_detail_healthfood) + "/" + id, access_token, "GET");
            getDetailData(result);
        }

        @Override
        protected String doInBackground(String... strings) {
            ReviewDataAsync ira = new ReviewDataAsync();
            ira.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            LikeCheckAsync lca = new LikeCheckAsync();
            lca.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            UnlikeCheckAsync uca = new UnlikeCheckAsync();
            uca.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    class InitReviewAsync extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            result = httpConnect.send("", httpConnect.getServerWithVersion() + getString(R.string.rest_members), csp.getValue("access_token", ""), "GET");
            return result;
        }

        @Override
        protected void onPostExecute(final String result) {
            super.onPostExecute(result);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        JSONObject my_json = new JSONObject(result);
                        JSONObject scores_json = my_json.getJSONObject("scores");
                        my_review = scores_json.getString("reviews");
                    } catch (Exception e) {
                        Log.e("my ac", "get json excep", e);
                    }
                }
            });

        }
    }

    class LikeCheckAsync extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            result = httpConnect.send("", httpConnect.getServerWithVersion() + getString(R.string.rest_likes), csp.getValue("access_token", ""), "GET");
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                JSONObject like_json = new JSONObject(result);
                JSONArray like_array = like_json.getJSONArray("items");
                for (int i = 0; i < like_array.length(); i++) {
                    if (id.equals(like_array.getJSONObject(i).getString("target"))) {
                        is_bookmark = true;
                        break;
                    }
                }
            } catch (Exception e) {
                Log.e("medi ac", "like check excep", e);
                is_bookmark = false;
            }
        }
    }

    class UnlikeCheckAsync extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            result = httpConnect.send("", httpConnect.getServerWithVersion() + getString(R.string.rest_unlikes), csp.getValue("access_token", ""), "GET");
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                JSONObject unlike_json = new JSONObject(result);
                JSONArray unlike_array = unlike_json.getJSONArray("items");
                for (int i = 0; i < unlike_array.length(); i++) {
                    if (id.equals(unlike_array.getJSONObject(i).getString("target"))) {
                        is_unlike = true;
                        break;
                    }
                }
            } catch (Exception e) {
                Log.e("medi ac", "like check excep", e);
                is_unlike = false;
            }
        }
    }

    class LikeAsync extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            String type = strings[0];
            String json = jsonMaker.makeJson(new Pair("target", id));
            if ("post".equals(type)) {
                result = httpConnect.send(json, httpConnect.getServerWithVersion() + getString(R.string.rest_likes), csp.getValue("access_token", ""));
            } else {
                result = httpConnect.send(json, httpConnect.getServerWithVersion() + getString(R.string.rest_likes), csp.getValue("access_token", ""), "DELETE");
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
        }
    }

    class UnlikeAsync extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            String type = strings[0];
            String json = jsonMaker.makeJson(new Pair("target", id));
            if ("post".equals(type)) {
                result = httpConnect.send(json, httpConnect.getServerWithVersion() + getString(R.string.rest_unlikes), csp.getValue("access_token", ""));
            } else {
                result = httpConnect.send(json, httpConnect.getServerWithVersion() + getString(R.string.rest_unlikes), csp.getValue("access_token", ""), "DELETE");
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
        }
    }

    class InitPhotoAsync extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            result = httpConnect.send("", httpConnect.getServerWithVersion() + getString(R.string.rest_review_picture) + "?id=" + id, csp.getValue("access_token", ""), "GET");
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                JSONObject photo_json = new JSONObject(result);
                JSONArray photo_item = photo_json.getJSONArray("Items");
                for (int i = 0; i < photo_item.length(); i++) {
                    JSONObject photo_object = photo_item.getJSONObject(i);
                    image_list.add(i, photo_object.getString("image"));
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (loadingDialog.isShowing())
                            loadingDialog.dismiss();
                    }
                });
            } catch (Exception e) {
                Log.e("health ac", "photo excep", e);
            }
        }
    }

    class ReviewDataAsync extends AsyncTask<String, String, String> {


        ReviewDataAsync() {
        }

        @Override
        protected void onPreExecute() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loadingDialog.show();
                }
            });

        }

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            String url = httpConnect.getServerWithVersion() + getString(R.string.rest_review_list) + "?id=" + id;

            result = httpConnect.send("", url, csp.getValue("access_token", ""), "GET");
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            setReviewData(result);

            InitPhotoAsync ipa = new InitPhotoAsync();
            ipa.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }

        private void setReviewData(final String json) {
            review_array.clear();

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        JSONObject result = new JSONObject(json);
                        JSONArray review_json_array = result.getJSONArray("Items");
                        for (int i = 0; i < review_json_array.length(); i++) {
                            if (i == 3) {
                                break;
                            }
                            JSONObject review_object = review_json_array.getJSONObject(i);
                            Review review = new Review();
                            review.setType(review_object.getString("type"));
                            review.setAge(review_object.getString("age"));
                            review.setBad_point(review_object.getString("bad_point"));
                            review.setDate(review_object.getString("date"));
//            review.setEvaluation(review_object.getString("evaluation"));
                            review.setGender(review_object.getString("gender"));
                            review.setGood_point(review_object.getString("good_point"));
                            review.setId(review_object.getString("id"));
                            review.setLikes(review_object.getString("likes"));
                            review.setMember(review_object.getString("member"));
                            review.setNickname(review_object.getString("nickname"));
                            review.setPeriod(review_object.getString("period"));
//            review.setPicture()
                            review.setProduct_name(review_object.getString("product_name"));
                            if (review_object.has("profile")) {
                                review.setProfile(review_object.getString("profile"));
                            }
                            review.setRepurchase(review_object.getString("repurchase"));
                            review.setTarget(review_object.getString("target"));
                            review.setTip(review_object.getString("tip"));
                            review.setViews(review_object.getString("views"));

                            review_array.add(i, review);
                        }

                        productReviewAdapter.notifyDataSetChanged();
                    } catch (Exception e) {
                        Log.e("re ac", "get review excep", e);
                    }

                    if (Integer.valueOf(my_review) > 0) {

                    } else {
                        layout_health_detail_need_review.setVisibility(View.VISIBLE);
                        if (review_array.size() > 2)
                            review_array.remove(2);
                        if (review_array.size() > 1)
                            review_array.remove(1);

                        if (review_array.size() == 0) {
                            layout_health_detail_no_review.setVisibility(View.VISIBLE);
                        }

                        productReviewAdapter.notifyDataSetChanged();
                    }

                    txt_health_detail_company.setText(company);
                    txt_health_detail_product.setText(name);
                    txt_health_detail_like_count.setText(scores.getLikes() + "명");
                    txt_health_detail_dislike_count.setText(scores.getUnlikes() + "명");

                    txt_health_detail_effect.setText(effect);
//                txt_health_detail_advice.setText(advice);

                    final URL[] imageURL = new URL[1];
                    final Bitmap[] bm = new Bitmap[1];

                    Thread mThread = new Thread() {
                        @Override
                        public void run() {
                            try {
                                if ("null".equals(select_image)) {

                                } else {
                                    imageURL[0] = new URL(select_image);

                                    HttpURLConnection conn = (HttpURLConnection) imageURL[0].openConnection();
                                    BufferedInputStream bis = new BufferedInputStream(conn.getInputStream(), 1024);
                                    bm[0] = BitmapFactory.decodeStream(bis);
                                    bis.close();

                                }
                            } catch (Exception e) {
                                Log.e("medi deta acti", "image url excep", e);
                            }
                        }
                    };

                    mThread.start();

                    try {
                        mThread.join();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if ("null".equals(select_image)) {
                                    img_health_detail_medicine.setImageResource(R.drawable.no_image);
                                } else {
                                    img_health_detail_medicine.setImageBitmap(bm[0]);
                                    main_bm = bm[0];
                                }
                            }
                        });
                    } catch (Exception e) {
                        Log.e("medi deta acti", "img set excep", e);
                    }

                    final URL[] photo_url = new URL[image_list.size()];
                    final Bitmap[] photo_bitmap = new Bitmap[image_list.size()];

                    Thread photo_thread = new Thread() {
                        @Override
                        public void run() {

                            for (int i = 0; i < image_list.size(); i++) {
                                try {
                                    if ("null".equals(image_list.get(i))) {
                                    } else {
                                        photo_url[0] = new URL(image_list.get(i));

                                        HttpURLConnection conn = (HttpURLConnection) photo_url[0].openConnection();
                                        BufferedInputStream bis = new BufferedInputStream(conn.getInputStream(), 1024);
                                        photo_bitmap[i] = BitmapFactory.decodeStream(bis);
                                        byte[] temp_bitmap = bitmapChanger(photo_bitmap[i]);
                                        bitmap_list.add(i, temp_bitmap);
                                        bis.close();

                                    }
                                } catch (Exception e) {
                                    Log.e("medi deta acti", "image url excep", e);
                                }
                            }
                        }
                    };

                    photo_thread.start();

                    try {
                        photo_thread.join();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                int counter = 3 < image_list.size() ? 3 : image_list.size();
                                for (int i = 0; i < counter; i++) {
                                    if ("null".equals(image_list.get(i))) {
                                        img_health_detail_medicine.setImageResource(R.drawable.no_image);
                                    } else {
                                        img_health_detail_medicine.setImageBitmap(photo_bitmap[i]);
                                    }
                                }
                            }
                        });
                    } catch (Exception e) {
                        Log.e("medi deta acti", "img set excep", e);
                    }

                    final float scale = getResources().getDisplayMetrics().density;
                    final int width_red = (int) ((315 * scale) * (functional_ingredient_array.size() / (float) (functional_ingredient_array.size() + etc_ingredient_array.size())));
                    final int width_blue = (int) ((315 * scale) * (etc_ingredient_array.size() / (float) (functional_ingredient_array.size() + etc_ingredient_array.size())));
                    final int height = (int) (14.75 * scale);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            RelativeLayout.LayoutParams lp_blue = new RelativeLayout.LayoutParams(width_red, height);
                            RelativeLayout.LayoutParams lp_gray = new RelativeLayout.LayoutParams(width_blue, height);
                            lp_blue.setMargins((int) (22.5 * scale), (int) (12.25 * scale), 0, 0);
                            lp_blue.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
                            lp_blue.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
                            lp_gray.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
                            lp_gray.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
                            lp_gray.setMargins(0, (int) (12.25 * scale), (int) (22.5 * scale), 0);
                            img_health_detail_ingredient_bar_blue.setLayoutParams(lp_blue);
                            img_health_detail_ingredient_bar_gray.setLayoutParams(lp_gray);
                        }
                    });

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            txt_health_detail_like_count.setText(scores.getLikes() + "명");
                            txt_health_detail_dislike_count.setText(scores.getUnlikes() + "명");
                            btn_health_detail_review_all.setText(scores.getReviews() + "개 리뷰 더 보기");
                        }
                    });

                }
            });
        }
    }

    private void setUpViews() {

        img_health_detail_medicine = (ImageView) findViewById(R.id.img_health_detail_medicine);
        txt_health_detail_company = (TextView) findViewById(R.id.txt_health_detail_company);
        txt_health_detail_product = (TextView) findViewById(R.id.txt_health_detail_product);
        txt_health_detail_like_count = (TextView) findViewById(R.id.txt_health_detail_like_count);
        txt_health_detail_dislike_count = (TextView) findViewById(R.id.txt_health_detail_dislike_count);
        btn_health_detail_how_to_use = (Button) findViewById(R.id.btn_health_detail_how_to_use);
        btn_health_detail_warning = (Button) findViewById(R.id.btn_health_detail_warning);
        btn_health_detail_price = (Button) findViewById(R.id.btn_health_detail_price);
        btn_health_detail_modify = (Button) findViewById(R.id.btn_health_detail_modify);
        txt_health_detail_effect = (TextView) findViewById(R.id.txt_health_detail_effect);
//        txt_health_detail_advice = (TextView) findViewById(R.id.txt_health_detail_advice);
        txt_health_detail_functional_count = (TextView) findViewById(R.id.txt_health_detail_functional_count);
        btn_health_detail_like = (Button) findViewById(R.id.btn_health_detail_like);
        btn_health_detail_dislike = (Button) findViewById(R.id.btn_health_detail_dislike);

        layout_health_detail_no_review = (RelativeLayout) findViewById(R.id.layout_health_detail_no_review);

        list_health_detail_review = (ListView) findViewById(R.id.list_health_detail_review);
        productReviewAdapter = new ProductReviewAdapter(getApplicationContext(), R.layout.adapter_product_review, review_array);
        list_health_detail_review.setAdapter(productReviewAdapter);

        list_health_detail_review.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent intent = new Intent(HealthfoodDetailActivity.this, ReadActivity.class);
                intent.putExtra("id", id);
                startActivity(intent);
            }
        });

        img_health_detail_review_picture1 = (ImageView) findViewById(R.id.img_health_detail_review_picture1);
        img_health_detail_review_picture2 = (ImageView) findViewById(R.id.img_health_detail_review_picture2);
        img_health_detail_review_picture3 = (ImageView) findViewById(R.id.img_health_detail_review_picture3);

        txt_health_detail_id = (TextView) findViewById(R.id.txt_health_detail_id);
        btn_health_detail_review_more = (Button) findViewById(R.id.btn_health_detail_review_more);

        img_health_detail_ingredient_bar_blue = (ImageView) findViewById(R.id.img_health_detail_ingredient_bar_blue);
        img_health_detail_ingredient_bar_gray = (ImageView) findViewById(R.id.img_health_detail_ingredient_bar_gray);

        btn_review_write = (Button) findViewById(R.id.btn_health_review_write);
        btn_review_write.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent write_intent = new Intent(HealthfoodDetailActivity.this, WriteActivity.class);
                write_intent.putExtra("id", id);
                startActivity(write_intent);
            }
        });
        btn_health_detail_ingredient = (Button) findViewById(R.id.btn_health_detail_ingredient);
        btn_health_detail_write = (Button) findViewById(R.id.btn_health_detail_write);
        btn_health_detail_write.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent write_intent = new Intent(HealthfoodDetailActivity.this, WriteActivity.class);
                write_intent.putExtra("id", id);
                startActivity(write_intent);
            }
        });

        btn_health_detail_review_all = (Button) findViewById(R.id.btn_health_detail_review_all);
        btn_health_detail_review_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Integer.valueOf(my_review) > 0) {
                    Intent read_intent = new Intent(HealthfoodDetailActivity.this, ReadActivity.class);
                    read_intent.putExtra("id", id);
                    startActivity(read_intent);
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "리뷰를 1개만 등록하시면 모든 리뷰를 보실 수 있습니다.", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });

        layout_health_detail_need_review = (RelativeLayout) findViewById(R.id.layout_health_detail_need_review);

        btn_health_detail_ingredient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ingredient_intent = new Intent(HealthfoodDetailActivity.this, ComponentHealthActivity.class);
                ingredient_intent.putExtra("functional", functional_ingredient);
                ingredient_intent.putExtra("capsule", capsule_ingredient);
                ingredient_intent.putExtra("etc", etc_ingredient);
                byte[] temp_bitmap = bitmapChanger(main_bm);
                ingredient_intent.putExtra("bitmap", temp_bitmap);
                ingredient_intent.putExtra("product_name", product_name);
                ingredient_intent.putExtra("company_name", company_name);
                ingredient_intent.putExtra("id", id);
                startActivity(ingredient_intent);
            }
        });

        btn_health_detail_how_to_use.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final LayoutInflater title_inflater = (LayoutInflater) getApplicationContext().getSystemService(LAYOUT_INFLATER_SERVICE);
                AlertDialog.Builder ab = new AlertDialog.Builder(HealthfoodDetailActivity.this);
                View title_view = title_inflater.inflate(R.layout.dialog_custom_title, null);
                TextView txt_dialog_title = (TextView) title_view.findViewById(R.id.txt_dialog_custom_title);
                ab.setCustomTitle(title_view);
                txt_dialog_title.setText("사용법");
                ab.setMessage(use);
                Button btn_dialog_title = (Button) title_view.findViewById(R.id.btn_dialog_custom_title);
                final AlertDialog ad = ab.create();
                btn_dialog_title.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ad.dismiss();
                    }
                });
                ad.show();
            }
        });
        btn_health_detail_warning.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final LayoutInflater title_inflater = (LayoutInflater) getApplicationContext().getSystemService(LAYOUT_INFLATER_SERVICE);
                AlertDialog.Builder ab = new AlertDialog.Builder(HealthfoodDetailActivity.this);
                View title_view = title_inflater.inflate(R.layout.dialog_custom_title, null);
                TextView txt_dialog_title = (TextView) title_view.findViewById(R.id.txt_dialog_custom_title);
                ab.setCustomTitle(title_view);
                txt_dialog_title.setText("주의사항");
                ab.setMessage(warning);
                Button btn_dialog_title = (Button) title_view.findViewById(R.id.btn_dialog_custom_title);
                final AlertDialog ad = ab.create();
                btn_dialog_title.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ad.dismiss();
                    }
                });
                ad.show();
            }
        });
        btn_health_detail_price.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent price_intent = new Intent(HealthfoodDetailActivity.this, NaverShoppingActivity.class);
                price_intent.putExtra("company", company_name);
                price_intent.putExtra("keyword", name);
                startActivity(price_intent);
            }
        });
        btn_health_detail_modify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent edit_intent = new Intent(HealthfoodDetailActivity.this, EditActivity.class);
                byte[] temp_bitmap = bitmapChanger(main_bm);
                edit_intent.putExtra("bitmap", temp_bitmap);
                edit_intent.putExtra("product_name", product_name);
                edit_intent.putExtra("company_name", company_name);
                edit_intent.putExtra("id", id);
                startActivity(edit_intent);
            }
        });

        btn_health_detail_review_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent image_intent = new Intent(HealthfoodDetailActivity.this, ImageActivity.class);
                image_intent.putExtra("image_list", image_list);
                image_intent.putExtra("bitmap_list", bitmap_list);
                startActivity(image_intent);
            }
        });

//        btn_health_detail_like.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (is_like) { // 좋아요 취소
//                    is_like = false;
//                    current_likes = String.valueOf(Integer.valueOf(current_likes) - 1);
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            txt_health_detail_like_count.setText(current_likes + "명");
//                        }
//                    });
//
//                    LikeAsync la = new LikeAsync();
//                    la.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "delete");
//                } else { // 좋아요 등록
//                    is_like = true;
//                    current_likes = String.valueOf(Integer.valueOf(current_likes) + 1);
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            txt_health_detail_like_count.setText(current_likes + "명");
//                        }
//                    });
//
//                    LikeAsync la = new LikeAsync();
//                    la.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "post");
//
//                    if (is_unlike) { // 싫어요 눌러진 상태
//                        is_unlike = false;
//                        current_unlikes = String.valueOf(Integer.valueOf(current_unlikes) - 1);
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                txt_health_detail_dislike_count.setText(current_unlikes + "명");
//                            }
//                        });
//
//                        UnlikeAsync ua = new UnlikeAsync();
//                        ua.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "delete");
//                    }
//                }
//            }
//        });

//        btn_health_detail_dislike.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (is_unlike) { // 싫어요 취소
//                    is_unlike = false;
//                    current_unlikes = String.valueOf(Integer.valueOf(current_unlikes) - 1);
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            txt_health_detail_dislike_count.setText(current_unlikes + "명");
//                        }
//                    });
//
//                    UnlikeAsync ua = new UnlikeAsync();
//                    ua.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "delete");
//                } else { // 싫어요 등록
//
//                    is_unlike = true;
//                    current_unlikes = String.valueOf(Integer.valueOf(current_unlikes) + 1);
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            txt_health_detail_dislike_count.setText(current_unlikes + "명");
//                        }
//                    });
//
//                    UnlikeAsync ua = new UnlikeAsync();
//                    ua.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "post");
//
//                    if (is_like) { // 좋아요 눌러진 상태
//                        is_like = false;
//                        current_likes = String.valueOf(Integer.valueOf(current_likes) - 1);
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                txt_health_detail_like_count.setText(current_likes + "명");
//                            }
//                        });
//
//                        LikeAsync la = new LikeAsync();
//                        la.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "delete");
//                    }
//                }
//            }
//        });

    }

    private void setCustomActionbar() {

        ActionBar actionBar = getSupportActionBar();

        // Custom Actionbar를 사용하기 위해 CustomEnabled을 true 시키고 필요 없는 것은 false 시킨다

        actionBar.setDisplayShowCustomEnabled(true);

        actionBar.setDisplayHomeAsUpEnabled(false);

        actionBar.setDisplayShowTitleEnabled(false);

        // Set custom view layout

        View mCustomView = LayoutInflater.from(this).inflate(R.layout.actionbar_product, null);

        actionBar.setCustomView(mCustomView);

        // Set no padding both side

        Toolbar parent = (Toolbar) mCustomView.getParent(); // first get parent toolbar of current action bar

        parent.setContentInsetsAbsolute(0, 0);              // set padding programmatically to 0dp

        // Set actionbar background image

        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.border_layout_bottom));

        // Set actionbar layout layoutparams

        ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);

        actionBar.setCustomView(mCustomView, params);

        Button btn_actionbar_alarm = (Button) findViewById(R.id.btn_actionbar_alarm);
        btn_actionbar_alarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShareDialog sd = new ShareDialog(HealthfoodDetailActivity.this);
                sd.setupDialog(name, effect + "\n" + warning, select_image, "");
                sd.show();
            }
        });

        final Button btn_actionbar_like = (Button) findViewById(R.id.btn_actionbar_like);
        btn_actionbar_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (is_bookmark) {
                            is_bookmark = false;
                            btn_actionbar_like.setBackground(getResources().getDrawable(R.drawable.product_icon_heart_nor, null));

                            BookmarkAsync ba = new BookmarkAsync(id, false);
                            ba.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        } else {
                            is_bookmark = true;
                            btn_actionbar_like.setBackground(getResources().getDrawable(R.drawable.product_icon_heart_active, null));

                            BookmarkAsync ba = new BookmarkAsync(id, true);
                            ba.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        }
                    }
                });
            }
        });

        ImageView img_actionbar_back = (ImageView) findViewById(R.id.img_actionbar_product_back);
        img_actionbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        TextView txt_actionbar = (TextView) findViewById(R.id.txt_actionbar_product);
        txt_actionbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        txt_actionbar.setText("약품상세");
    }

    class BookmarkAsync extends AsyncTask<String, String, String> {

        private boolean is_like = false;
        private String target_id;

        BookmarkAsync(String id, boolean is_like) {
            this.target_id = id;
            this.is_like = is_like;
        }

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            String json = jsonMaker.makeJson(new Pair("target", id));
            if (is_like) {
                result = httpConnect.send(json, httpConnect.getServerWithVersion() + getString(R.string.rest_bookmarks), csp.getValue("access_token", ""));
            } else {
                result = httpConnect.send(json, httpConnect.getServerWithVersion() + getString(R.string.rest_bookmarks), csp.getValue("access_token", ""), "DELETE");
            }

            return result;
        }
    }

    private byte[] bitmapChanger(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] bytes = stream.toByteArray();
        return bytes;
    }
}
