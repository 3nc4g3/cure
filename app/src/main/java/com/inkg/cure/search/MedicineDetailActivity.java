package com.inkg.cure.search;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.Pair;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.inkg.cure.NearPharmacyActivity;
import com.inkg.cure.R;
import com.inkg.cure.adapters.PictoAdapter;
import com.inkg.cure.adapters.ProductReviewAdapter;
import com.inkg.cure.adapters.ReviewAdapter;
import com.inkg.cure.classes.CureSP;
import com.inkg.cure.classes.Dur;
import com.inkg.cure.classes.HttpConnect;
import com.inkg.cure.classes.JsonMaker;
import com.inkg.cure.classes.LoadingDialog;
import com.inkg.cure.classes.Review;
import com.inkg.cure.classes.Scores;
import com.inkg.cure.classes.ShareDialog;
import com.inkg.cure.review.EditActivity;
import com.inkg.cure.review.ReadActivity;
import com.inkg.cure.review.ReviewDetailActivity;
import com.inkg.cure.review.WriteActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by inkg on 2017. 1. 13..
 */

public class MedicineDetailActivity extends AppCompatActivity {

    private String access_token;
    private HttpConnect httpConnect;
    private JsonMaker jsonMaker;
    private CureSP csp;

    private String saler;
    private String warning;
    private String advice;
    private static String id;
    private String use;
    private String effect;
    private Scores scores;
    private Dur dur;
    //    private Similar similar;
    private String name;
    private String select_image;
    private String maker;
    private String ingredient;

    private ArrayList<String> picto_array;
//    private ArrayList<Similar> similar_list;

    private ImageView img_medicine_detail_medicine;
    private TextView txt_medicine_detail_company;
    private TextView txt_medicine_detail_product;
    private TextView txt_medicine_detail_like_count;
    private TextView txt_medicine_detail_dislike_count;
    private TextView txt_medicine_detail_effect;
    private TextView txt_medicine_detail_advice;

    private Button btn_medicine_detail_how_to_use;
    private Button btn_medicine_detail_warning;
    private Button btn_medicine_detail_pharmacy;
    private Button btn_medicine_detail_modify;
    private Button btn_medicine_detail_ingredient;

    private Button btn_medicine_detail_review_more;
    private Button btn_review_write;

    //dur
    private ImageView img_medicine_detail_warn_combi;
    private TextView txt_medicine_detail_warn_combi;
    private ImageView img_medicine_detail_warn_period;
    private TextView txt_medicine_detail_warn_period;
    private ImageView img_medicine_detail_warn_divide;
    private TextView txt_medicine_detail_warn_divide;
    private ImageView img_medicine_detail_warn_old;
    private TextView txt_medicine_detail_warn_old;
    private ImageView img_medicine_detail_warn_blood;
    private TextView txt_medicine_detail_warn_blood;
    private ImageView img_medicine_detail_warn_pregnant;
    private TextView txt_medicine_detail_warn_pregnant;
    private ImageView img_medicine_detail_warn_baby;
    private TextView txt_medicine_detail_warn_baby;

    private ImageView img_medicine_detail_ingredient_bar_red;
    private ImageView img_medicine_detail_ingredient_bar_blue;

    private ImageView img_medicine_detail_product_same1;
    private TextView txt_medicine_detail_product_company1;
    private TextView txt_medicine_detail_product_name1;
    private ImageView img_medicine_detail_product_same2;
    private TextView txt_medicine_detail_product_company2;
    private TextView txt_medicine_detail_product_name2;
    private ImageView img_medicine_detail_product_same3;
    private TextView txt_medicine_detail_product_company3;
    private TextView txt_medicine_detail_product_name3;
    private Button btn_medicine_detail_product_same1;
    private Button btn_medicine_detail_product_same2;
    private Button btn_medicine_detail_product_same3;

    private RelativeLayout layout_medicine_detail_no_review;

    private ListView list_medicine_detail_review;
    private ProductReviewAdapter reviewAdapter;

    private Button btn_medicine_detail_like;
    private Button btn_medicine_detail_dislike;

    private Button btn_medicine_detail_review_all;
    private RelativeLayout layout_medicine_detail_need_review;
    private TextView txt_medicine_detail_id;
    private Button btn_medicine_detail_write;

    private ArrayList<Review> review_array = new ArrayList<>();

    private int[] dur_checker;
    private int dur_count = 0;

    private String atc_code = "";
    private Bitmap main_bm;
    private String my_review = "0";

    private ArrayList<String> image_list = new ArrayList<>();
    private ArrayList<byte[]> bitmap_list = new ArrayList<>();

    private boolean is_bookmark = false;
    private boolean is_like = false;
    private boolean is_unlike = false;

    private String current_likes = "0";
    private String current_unlikes = "0";

    private LoadingDialog loadingDialog;
    private long start;
    private long end;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medicine_detail);

        start = System.currentTimeMillis();

        httpConnect = new HttpConnect(getApplicationContext());
        jsonMaker = new JsonMaker();
        picto_array = new ArrayList<>();
        loadingDialog = new LoadingDialog(MedicineDetailActivity.this);

        csp = new CureSP(getApplicationContext());
        access_token = csp.getValue("access_token", "");

        my_review = csp.getValue("my_review", "0");

        Intent intent = getIntent();
        id = intent.getStringExtra("id");

        dur_checker = new int[7];
        for (int i = 0; i < 7; i++) {
            dur_checker[i] = 0;
        }
        setCustomActionbar();
    }

    @Override
    protected void onStart() {
        super.onStart();
        InitDataAsync idAsync = new InitDataAsync();
        idAsync.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        end = System.currentTimeMillis();
        csp.put(id, String.valueOf((end - start) / 1000));
    }

    class InitDataAsync extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    loadingDialog.show();
                    setUpViews();
                }
            });
        }

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            result = httpConnect.send("", getString(R.string.server_url) + getString(R.string.cureya_version) + getString(R.string.rest_detail_medicine) + "/" + id, access_token, "GET");
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            getDetailData(result);
            InitPhotoAsync ipa = new InitPhotoAsync();
            ipa.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

            LikeCheckAsync lca = new LikeCheckAsync();
            lca.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

            UnlikeCheckAsync uca = new UnlikeCheckAsync();
            uca.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    class LikeCheckAsync extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            result = httpConnect.send("", httpConnect.getServerWithVersion() + getString(R.string.rest_likes), csp.getValue("access_token", ""), "GET");
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.e("result", result);
            try {
                JSONObject like_json = new JSONObject(result);
                JSONArray like_array = like_json.getJSONArray("items");
                for (int i = 0; i < like_array.length(); i++) {
                    Log.e("ids", id + " / " + like_array.getJSONObject(i).getString("id"));
                    if (id.equals(like_array.getJSONObject(i).getString("target"))) {
                        is_like = true;
                        break;
                    }
                }
            } catch (Exception e) {
                Log.e("medi ac", "like check excep", e);
                is_like = false;
            }
        }
    }

    class UnlikeCheckAsync extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            result = httpConnect.send("", httpConnect.getServerWithVersion() + getString(R.string.rest_unlikes), csp.getValue("access_token", ""), "GET");
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                JSONObject unlike_json = new JSONObject(result);
                JSONArray unlike_array = unlike_json.getJSONArray("items");
                for (int i = 0; i < unlike_array.length(); i++) {
                    if (id.equals(unlike_array.getJSONObject(i).getString("target"))) {
                        is_unlike = true;
                        break;
                    }
                }
            } catch (Exception e) {
                Log.e("medi ac", "like check excep", e);
                is_unlike = false;
            }
        }
    }

    class LikeAsync extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            String type = strings[0];
            String json = jsonMaker.makeJson(new Pair("target", id));
            if ("post".equals(type)) {
                result = httpConnect.send(json, httpConnect.getServerWithVersion() + getString(R.string.rest_likes), csp.getValue("access_token", ""));
            } else {
                result = httpConnect.send(json, httpConnect.getServerWithVersion() + getString(R.string.rest_likes), csp.getValue("access_token", ""), "DELETE");
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
        }
    }

    class UnlikeAsync extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            String type = strings[0];
            String json = jsonMaker.makeJson(new Pair("target", id));
            if ("post".equals(type)) {
                result = httpConnect.send(json, httpConnect.getServerWithVersion() + getString(R.string.rest_unlikes), csp.getValue("access_token", ""));
            } else {
                result = httpConnect.send(json, httpConnect.getServerWithVersion() + getString(R.string.rest_unlikes), csp.getValue("access_token", ""), "DELETE");
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
        }
    }

    class InitPhotoAsync extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            result = httpConnect.send("", httpConnect.getServerWithVersion() + getString(R.string.rest_review_picture) + "?id=" + id, csp.getValue("access_token", ""), "GET");
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                JSONObject photo_json = new JSONObject(result);
                JSONArray photo_item = photo_json.getJSONArray("Items");
                for (int i = 0; i < photo_item.length(); i++) {
                    JSONObject photo_object = photo_item.getJSONObject(i);
                    image_list.add(i, photo_object.getString("image"));
                    Log.e("image", photo_object.getString("image"));
                }

            } catch (Exception e) {
                Log.e("medicine ac", "photo excep", e);
            }
        }
    }

    private void setReviewData(final String json) {
        review_array.clear();

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    JSONObject result = new JSONObject(json);
                    JSONArray review_json_array = result.getJSONArray("reviews");
                    for (int i = 0; i < review_json_array.length(); i++) {
                        if (i == 3) {
                            break;
                        }
                        JSONObject review_object = review_json_array.getJSONObject(i);
                        Review review = new Review();
                        review.setType(review_object.getString("type"));
                        review.setAge(review_object.getString("age"));
                        review.setBad_point(review_object.getString("bad_point"));
                        review.setDate(review_object.getString("date"));
//            review.setEvaluation(review_object.getString("evaluation"));
                        review.setGender(review_object.getString("gender"));
                        review.setGood_point(review_object.getString("good_point"));
                        review.setId(review_object.getString("id"));
                        review.setLikes(review_object.getString("likes"));
                        review.setMember(review_object.getString("member"));
                        review.setNickname(review_object.getString("nickname"));
                        review.setPeriod(review_object.getString("period"));
//            review.setPicture()
                        review.setProduct_name(review_object.getString("product_name"));
                        if (review_object.has("profile")) {
                            review.setProfile(review_object.getString("profile"));
                        }
                        review.setRepurchase(review_object.getString("repurchase"));
                        review.setTarget(review_object.getString("target"));
                        review.setTip(review_object.getString("tip"));
                        review.setViews(review_object.getString("views"));

                        review_array.add(i, review);
                    }

                    reviewAdapter.notifyDataSetChanged();
                } catch (Exception e) {
                    Log.e("re ac", "get review excep", e);
                }


                final URL[] imageURL = new URL[1];
                final Bitmap[] bm = new Bitmap[1];

                Thread mThread = new Thread() {
                    @Override
                    public void run() {
                        try {
                            if ("null".equals(select_image)) {

                            } else {
                                imageURL[0] = new URL(select_image);

                                HttpURLConnection conn = (HttpURLConnection) imageURL[0].openConnection();
                                BufferedInputStream bis = new BufferedInputStream(conn.getInputStream(), 1024);
                                bm[0] = BitmapFactory.decodeStream(bis);
                                bis.close();

                            }
                        } catch (Exception e) {
                            Log.e("medi deta acti", "image url excep", e);
                        }
                    }
                };

                mThread.start();

                try {
                    mThread.join();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if ("null".equals(select_image)) {
                                img_medicine_detail_medicine.setImageResource(R.drawable.no_image);
                            } else {
                                img_medicine_detail_medicine.setImageBitmap(bm[0]);
                                main_bm = bm[0];
                            }
                        }
                    });
                } catch (Exception e) {
                    Log.e("medi deta acti", "img set excep", e);
                }

                final URL[] photo_url = new URL[image_list.size()];
                final Bitmap[] photo_bitmap = new Bitmap[image_list.size()];

                Thread photo_thread = new Thread() {
                    @Override
                    public void run() {

                        for (int i = 0; i < image_list.size(); i++) {
                            try {
                                if ("null".equals(image_list.get(i))) {
                                } else {
                                    photo_url[0] = new URL(image_list.get(i));

                                    HttpURLConnection conn = (HttpURLConnection) photo_url[0].openConnection();
                                    BufferedInputStream bis = new BufferedInputStream(conn.getInputStream(), 1024);
                                    photo_bitmap[i] = BitmapFactory.decodeStream(bis);
                                    byte[] temp_bitmap = bitmapChanger(photo_bitmap[i]);
                                    bitmap_list.add(i, temp_bitmap);
                                    bis.close();

                                }
                            } catch (Exception e) {
                                Log.e("medi deta acti", "image url excep", e);
                            }
                        }
                    }
                };

                photo_thread.start();

                try {
                    photo_thread.join();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            int counter = 3 < image_list.size() ? 3 : image_list.size();
                            for (int i = 0; i < counter; i++) {
                                if ("null".equals(image_list.get(i))) {
                                    img_medicine_detail_medicine.setImageResource(R.drawable.no_image);
                                } else {
                                    img_medicine_detail_medicine.setImageBitmap(photo_bitmap[i]);
                                }
                            }
                        }
                    });
                } catch (Exception e) {
                    Log.e("medi deta acti", "img set excep", e);
                }

                if (review_array.size() == 0) {
                    layout_medicine_detail_no_review.setVisibility(View.VISIBLE);
                }

                if (Integer.valueOf(my_review) > 0) {

                } else {
                    layout_medicine_detail_need_review.setVisibility(View.VISIBLE);
                    if (review_array.size() > 2) {
                        review_array.remove(2);
                        review_array.remove(1);
                    }
                    if (review_array.size() > 1)
                        review_array.remove(1);

                    reviewAdapter.notifyDataSetChanged();
                }

                final float scale = getResources().getDisplayMetrics().density;
                int width_red = (int) ((315 * scale) * (dur_count / 7.0f));
                int width_blue = (int) ((315 * scale) * ((7 - dur_count) / 7.0f));
                int height = (int) (14.75 * scale);

                RelativeLayout.LayoutParams lp_red = new RelativeLayout.LayoutParams(width_red, height);
                RelativeLayout.LayoutParams lp_blue = new RelativeLayout.LayoutParams(width_blue, height);
                lp_red.setMargins((int) (22.5 * scale), (int) (12.25 * scale), 0, 0);
                lp_red.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
                lp_red.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
                lp_blue.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
                lp_blue.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
                lp_blue.setMargins(0, (int) (12.25 * scale), (int) (22.5 * scale), 0);
                img_medicine_detail_ingredient_bar_red.setLayoutParams(lp_red);
                img_medicine_detail_ingredient_bar_blue.setLayoutParams(lp_blue);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (dur_checker[0] == 0) {
                            img_medicine_detail_warn_combi.setImageDrawable(getResources().getDrawable(R.drawable.mix_inactive, null));
                            txt_medicine_detail_warn_combi.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.taupe_gray));
                        } else {
                            img_medicine_detail_warn_combi.setImageDrawable(getResources().getDrawable(R.drawable.mix_active, null));
                            txt_medicine_detail_warn_combi.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.pastel_red));
                        }

                        if (dur_checker[1] == 0) {
                            img_medicine_detail_warn_period.setImageDrawable(getResources().getDrawable(R.drawable.date_inactive, null));
                            txt_medicine_detail_warn_period.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.taupe_gray));
                        } else {
                            img_medicine_detail_warn_period.setImageDrawable(getResources().getDrawable(R.drawable.date_active, null));
                            txt_medicine_detail_warn_period.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.pastel_red));
                        }

                        if (dur_checker[2] == 0) {
                            img_medicine_detail_warn_divide.setImageDrawable(getResources().getDrawable(R.drawable.break_inactive, null));
                            txt_medicine_detail_warn_divide.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.taupe_gray));
                        } else {
                            img_medicine_detail_warn_divide.setImageDrawable(getResources().getDrawable(R.drawable.break_active, null));
                            txt_medicine_detail_warn_divide.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.pastel_red));
                        }

                        if (dur_checker[3] == 0) {
                            img_medicine_detail_warn_old.setImageDrawable(getResources().getDrawable(R.drawable.older_inactive, null));
                            txt_medicine_detail_warn_old.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.taupe_gray));
                        } else {
                            img_medicine_detail_warn_old.setImageDrawable(getResources().getDrawable(R.drawable.older_active, null));
                            txt_medicine_detail_warn_old.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.pastel_red));
                        }

                        if (dur_checker[4] == 0) {
                            img_medicine_detail_warn_blood.setImageDrawable(getResources().getDrawable(R.drawable.blood_inactive, null));
                            txt_medicine_detail_warn_blood.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.taupe_gray));
                        } else {
                            img_medicine_detail_warn_blood.setImageDrawable(getResources().getDrawable(R.drawable.blood_active, null));
                            txt_medicine_detail_warn_blood.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.pastel_red));
                        }

                        if (dur_checker[5] == 0) {
                            img_medicine_detail_warn_pregnant.setImageDrawable(getResources().getDrawable(R.drawable.pregnant_inactive, null));
                            txt_medicine_detail_warn_pregnant.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.taupe_gray));
                        } else {
                            img_medicine_detail_warn_pregnant.setImageDrawable(getResources().getDrawable(R.drawable.pregant_active, null));
                            txt_medicine_detail_warn_pregnant.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.pastel_red));
                        }

                        if (dur_checker[6] == 0) {
                            img_medicine_detail_warn_baby.setImageDrawable(getResources().getDrawable(R.drawable.baby_inactive, null));
                            txt_medicine_detail_warn_baby.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.taupe_gray));
                        } else {
                            img_medicine_detail_warn_baby.setImageDrawable(getResources().getDrawable(R.drawable.baby_active, null));
                            txt_medicine_detail_warn_baby.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.pastel_red));
                        }
                        txt_medicine_detail_company.setText(maker);
                        txt_medicine_detail_product.setText(name);
                        txt_medicine_detail_like_count.setText(scores.getLikes() + "명");
                        txt_medicine_detail_dislike_count.setText(scores.getUnlikes() + "명");

                        txt_medicine_detail_advice.setText(advice);
                    }
                });

                btn_medicine_detail_review_all.setText(scores.getReviews() + "개 리뷰 더 보기");
                btn_medicine_detail_review_all.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (Integer.valueOf(my_review) > 0) {
                            Intent read_intent = new Intent(MedicineDetailActivity.this, ReadActivity.class);
                            read_intent.putExtra("id", id);
                            startActivity(read_intent);
                        } else {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getApplicationContext(), "리뷰를 1개만 등록하시면 모든 리뷰를 보실 수 있습니다.", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    }
                });

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        HorizontalScrollView scroll_medicine_detail_picto = (HorizontalScrollView) findViewById(R.id.scroll_medicine_detail_picto);
                        LinearLayout layout_medicine_detail_picto_item = (LinearLayout) findViewById(R.id.layout_medicine_detail_picto_item);
                        scroll_medicine_detail_picto.removeAllViewsInLayout();
                        layout_medicine_detail_picto_item.removeAllViewsInLayout();
                        for (int i = 0; i < picto_array.size(); i++) {
                            int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (float) 52.25, getResources().getDisplayMetrics());
                            int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (float) 62.25, getResources().getDisplayMetrics());
                            Button btn_picto = new Button(MedicineDetailActivity.this);
                            final int pictoId = getResources().getIdentifier(picto_array.get(i).toLowerCase(), "drawable", getPackageName());
                            int check = getResources().getIdentifier(picto_array.get(i).toLowerCase(), "drawable", getPackageName());
                            if (check != 0) {
                                btn_picto.setBackground(getResources().getDrawable(pictoId, null));
                            } else {
                                continue;
                            }
//                    btn_picto.setPadding(0, (int) 6.75, (int) 9.5, (int) 6.75);
                            btn_picto.setLayoutParams(new LinearLayout.LayoutParams(width, height));
                            btn_picto.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    final Dialog dialog = new Dialog(MedicineDetailActivity.this);
                                    dialog.setContentView(R.layout.adapter_picto_image);
                                    ImageView img_picto = (ImageView) dialog.findViewById(R.id.img_picto);
                                    img_picto.setImageDrawable(getResources().getDrawable(pictoId, null));
                                    img_picto.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    dialog.dismiss();
                                                }
                                            });
                                        }
                                    });
                                    dialog.show();
                                }
                            });
                            layout_medicine_detail_picto_item.addView(btn_picto);
                        }
                        scroll_medicine_detail_picto.addView(layout_medicine_detail_picto_item);
                    }
                });


                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loadingDialog.dismiss();
                    }
                });
            }
        });
    }

    private void getDetailData(String result) {
        try {
            Log.e("result", result);
            JSONObject detail_json_data = new JSONObject(result);

            JSONObject dur_object = detail_json_data.getJSONObject("DUR");

            dur = new Dur();
            dur_count = 0;
            dur.setBlood(dur_object.getString("헌혈금지"));
            if (!"null".equals(dur.getBlood())) {
//            if (dur.getBlood() != null) {
                dur_checker[4] = 1;
                dur_count++;
            }
            dur.setCombi(dur_object.getString("병용금기"));
            if (!"null".equals(dur.getCombi())) {
//            if (dur.getCombi() != null) {
                dur_checker[0] = 1;
                dur_count++;
            }
            dur.setAge(dur_object.getString("연령금기"));
            if (!"null".equals(dur.getAge())) {
//            if (dur.getAge() != null) {
                dur_checker[6] = 1;
                dur_count++;
            }
            dur.setPregnant(dur_object.getString("임부금기"));
            if (!"null".equals(dur.getPregnant())) {
//            if (dur.getPregnant() != null) {
                dur_checker[5] = 1;
                dur_count++;
            }
            dur.setOld(dur_object.getString("노인주의"));
            if (!"null".equals(dur.getOld())) {
//            if (dur.getOld() != null) {
                dur_checker[3] = 1;
                dur_count++;
            }
            dur.setDivide(dur_object.getString("분할주의"));
            if (!"null".equals(dur.getDivide())) {
//            if (dur.getDivide() != null) {
                dur_checker[2] = 1;
                dur_count++;
            }
            dur.setPeriod(dur_object.getString("용량/투여기간주의"));
            if (!"null".equals(dur.getPeriod())) {
//            if (dur.getPeriod() != null) {
                dur_checker[1] = 1;
                dur_count++;
            }

            JSONObject score_object = detail_json_data.getJSONObject("scores");
            JSONArray ingredient_array = detail_json_data.getJSONArray("ingredient");
            JSONArray picto_json_array = detail_json_data.getJSONArray("picto");
            picto_array = new ArrayList<>();
            for (int i = 0; i < picto_json_array.length(); i++) {
                picto_array.add(i, picto_json_array.get(i).toString());
            }

            ingredient = ingredient_array.toString();

            scores = new Scores();
            Log.e("score json", score_object.toString());
//            scores.setSales(score_object.getString("sales"));
            scores.setShares(score_object.get("shares").toString());
            scores.setReviews(score_object.get("reviews").toString());
            scores.setUnlikes(score_object.get("unlikes").toString());
            scores.setLikes(score_object.get("likes").toString());
            current_likes = scores.getLikes();
            current_unlikes = scores.getUnlikes();
            scores.setBookmarks(score_object.get("bookmarks").toString());

            warning = detail_json_data.getString("warning");
            advice = detail_json_data.getString("counseling");
            txt_medicine_detail_advice.setText(advice);
            use = detail_json_data.getString("use");
            effect = detail_json_data.getString("effect");
            txt_medicine_detail_effect.setText(effect);
            name = detail_json_data.getString("name");
//            select_image = detail_json_data.getString("select_image");
            if (detail_json_data.has("thumbnail")) {
                select_image = detail_json_data.getString("thumbnail");
            }
            maker = detail_json_data.getString("maker");
//            maker = detail_json_data.getString("company");
            atc_code = detail_json_data.getString("ATC_Code");
            atc_code = atc_code.split(":")[0].toString();
            atc_code = atc_code.replaceAll(" ", "");

            setReviewData(result);

        } catch (Exception e) {
            Log.e("medi detail", "getDetailData excep", e);
        }
    }

    private void setUpViews() {

        img_medicine_detail_medicine = (ImageView) findViewById(R.id.img_medicine_detail_medicine);
        txt_medicine_detail_company = (TextView) findViewById(R.id.txt_medicine_detail_company);
        txt_medicine_detail_product = (TextView) findViewById(R.id.txt_medicine_detail_product);
        txt_medicine_detail_like_count = (TextView) findViewById(R.id.txt_medicine_detail_like_count);
        txt_medicine_detail_dislike_count = (TextView) findViewById(R.id.txt_medicine_detail_dislike_count);
        txt_medicine_detail_effect = (TextView) findViewById(R.id.txt_medicine_detail_effect);
        txt_medicine_detail_advice = (TextView) findViewById(R.id.txt_medicine_detail_advice);
        btn_medicine_detail_how_to_use = (Button) findViewById(R.id.btn_medicine_detail_how_to_use);
        btn_medicine_detail_warning = (Button) findViewById(R.id.btn_medicine_detail_warning);
        btn_medicine_detail_pharmacy = (Button) findViewById(R.id.btn_medicine_detail_pharmacy);
        btn_medicine_detail_modify = (Button) findViewById(R.id.btn_medicine_detail_modify);
        btn_medicine_detail_ingredient = (Button) findViewById(R.id.btn_medicine_detail_ingredient);
        btn_medicine_detail_like = (Button) findViewById(R.id.btn_medicine_detail_like);
        btn_medicine_detail_dislike = (Button) findViewById(R.id.btn_medicine_detail_dislike);

        img_medicine_detail_warn_combi = (ImageView) findViewById(R.id.img_medicine_detail_warn_combi);
        txt_medicine_detail_warn_combi = (TextView) findViewById(R.id.txt_medicine_detail_warn_combi);
        img_medicine_detail_warn_period = (ImageView) findViewById(R.id.img_medicine_detail_warn_period);
        txt_medicine_detail_warn_period = (TextView) findViewById(R.id.txt_medicine_detail_warn_period);
        img_medicine_detail_warn_divide = (ImageView) findViewById(R.id.img_medicine_detail_warn_divide);
        txt_medicine_detail_warn_divide = (TextView) findViewById(R.id.txt_medicine_detail_warn_divide);
        img_medicine_detail_warn_old = (ImageView) findViewById(R.id.img_medicine_detail_warn_old);
        txt_medicine_detail_warn_old = (TextView) findViewById(R.id.txt_medicine_detail_warn_old);
        img_medicine_detail_warn_blood = (ImageView) findViewById(R.id.img_medicine_detail_warn_blood);
        txt_medicine_detail_warn_blood = (TextView) findViewById(R.id.txt_medicine_detail_warn_blood);
        img_medicine_detail_warn_pregnant = (ImageView) findViewById(R.id.img_medicine_detail_warn_pregnant);
        txt_medicine_detail_warn_pregnant = (TextView) findViewById(R.id.txt_medicine_detail_warn_pregnant);
        img_medicine_detail_warn_baby = (ImageView) findViewById(R.id.img_medicine_detail_warn_baby);
        txt_medicine_detail_warn_baby = (TextView) findViewById(R.id.txt_medicine_detail_warn_baby);
        txt_medicine_detail_id = (TextView) findViewById(R.id.txt_medicine_detail_id);

        layout_medicine_detail_no_review = (RelativeLayout) findViewById(R.id.layout_medicine_detail_no_review);

        btn_medicine_detail_write = (Button) findViewById(R.id.btn_medicine_detail_write);
        list_medicine_detail_review = (ListView) findViewById(R.id.list_medicine_detail_review);
        reviewAdapter = new ProductReviewAdapter(getApplicationContext(), R.layout.adapter_product_review, review_array);
        list_medicine_detail_review.setAdapter(reviewAdapter);

        list_medicine_detail_review.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent detail_intent = new Intent(MedicineDetailActivity.this, ReadActivity.class);
                detail_intent.putExtra("id", id);
                startActivity(detail_intent);
            }
        });

        layout_medicine_detail_need_review = (RelativeLayout) findViewById(R.id.layout_medicine_detail_need_review);

        btn_medicine_detail_review_all = (Button) findViewById(R.id.btn_medicine_detail_review_all);

        btn_medicine_detail_write.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent write_intent = new Intent(MedicineDetailActivity.this, WriteActivity.class);
                write_intent.putExtra("id", id);
                startActivity(write_intent);
            }
        });

        btn_review_write = (Button) findViewById(R.id.btn_medicine_review_write);
        btn_review_write.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent write_intent = new Intent(MedicineDetailActivity.this, WriteActivity.class);
                write_intent.putExtra("id", id);
                startActivity(write_intent);
            }
        });

//        img_medicine_detail_product_same1 = (ImageView) findViewById(R.id.img_medicine_detail_product_same1);
//        txt_medicine_detail_product_company1 = (TextView) findViewById(R.id.txt_medicine_detail_product_company1);
//        txt_medicine_detail_product_name1 = (TextView) findViewById(R.id.txt_medicine_detail_product_name1);
//        img_medicine_detail_product_same2 = (ImageView) findViewById(R.id.img_medicine_detail_product_same2);
//        txt_medicine_detail_product_company2 = (TextView) findViewById(R.id.txt_medicine_detail_product_company2);
//        txt_medicine_detail_product_name2 = (TextView) findViewById(R.id.txt_medicine_detail_product_name2);
//        img_medicine_detail_product_same3 = (ImageView) findViewById(R.id.img_medicine_detail_product_same3);
//        txt_medicine_detail_product_company3 = (TextView) findViewById(R.id.txt_medicine_detail_product_company3);
//        txt_medicine_detail_product_name3 = (TextView) findViewById(R.id.txt_medicine_detail_product_name3);
//        btn_medicine_detail_product_same1 = (Button) findViewById(R.id.btn_medicine_detail_product_same1);
//        btn_medicine_detail_product_same2 = (Button) findViewById(R.id.btn_medicine_detail_product_same2);
//        btn_medicine_detail_product_same3 = (Button) findViewById(R.id.btn_medicine_detail_product_same3);

//        final String[] similar_image = {""};
//
//        final Thread m_Thread = new Thread() {
//            @Override
//            public void run() {
//                try {
//                    if ("null".equals(similar_image[0])) {
//
//                    } else {
//                        imageURL[0] = new URL(similar_image[0]);
//
//                        HttpURLConnection conn = (HttpURLConnection) imageURL[0].openConnection();
//                        BufferedInputStream bis = new BufferedInputStream(conn.getInputStream(), 1024);
//                        bm[0] = BitmapFactory.decodeStream(bis);
//                        bis.close();
//
//                    }
//                } catch (Exception e) {
//                    Log.e("medi deta acti", "image url excep", e);
//                }
//            }
//        };
//
//        if (similar_list.size() > 3) {
//            for (int i = 0; i < 3; i++) {
//                final int finalI = i;
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        if (finalI == 0) {
//                            txt_medicine_detail_product_company1.setText(similar_list.get(finalI).getCompany());
//                            txt_medicine_detail_product_name1.setText(similar_list.get(finalI).getName());
//                            similar_image[0] = similar_list.get(finalI).getImage();
//                            m_Thread.start();
//
//                            try {
//                                m_Thread.join();
//                                runOnUiThread(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        if ("null".equals(similar_image[0])) {
//                                            img_medicine_detail_product_same1.setImageResource(R.drawable.no_image);
//                                        } else {
//                                            img_medicine_detail_product_same1.setImageBitmap(bm[0]);
//                                        }
//                                    }
//                                });
//                            } catch (Exception e) {
//                                Log.e("medi deta acti", "sim img excep", e);
//                            }
//
//                        } else if (finalI == 1) {
//                            txt_medicine_detail_product_company2.setText(similar_list.get(finalI).getCompany());
//                            txt_medicine_detail_product_name2.setText(similar_list.get(finalI).getName());
//                            similar_image[0] = similar_list.get(finalI).getImage();
//                            m_Thread.start();
//
//                            try {
//                                m_Thread.join();
//                                runOnUiThread(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        if ("null".equals(similar_image[0])) {
//                                            img_medicine_detail_product_same2.setImageResource(R.drawable.no_image);
//                                        } else {
//                                            img_medicine_detail_product_same2.setImageBitmap(bm[0]);
//                                        }
//                                    }
//                                });
//                            } catch (Exception e) {
//                                Log.e("medi deta acti", "sim img excep", e);
//                            }
//                        } else {
//                            txt_medicine_detail_product_company3.setText(similar_list.get(finalI).getCompany());
//                            txt_medicine_detail_product_name3.setText(similar_list.get(finalI).getName());
//                            similar_image[0] = similar_list.get(finalI).getImage();
//                            m_Thread.start();
//
//                            try {
//                                m_Thread.join();
//                                runOnUiThread(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        if ("null".equals(similar_image[0])) {
//                                            img_medicine_detail_product_same3.setImageResource(R.drawable.no_image);
//                                        } else {
//                                            img_medicine_detail_product_same3.setImageBitmap(bm[0]);
//                                        }
//                                    }
//                                });
//                            } catch (Exception e) {
//                                Log.e("medi deta acti", "sim img excep", e);
//                            }
//                        }
//                    }
//                });
//            }
//        }


//        btn_medicine_detail_product_same1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (similar_list.size() > 0) {
//                    Intent sim_intent = new Intent(MedicineDetailActivity.this, MedicineDetailActivity.class);
//                    sim_intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    sim_intent.putExtra("id", similar_list.get(0).getId());
//                    startActivity(sim_intent);
//                }
//
//            }
//        });
//        btn_medicine_detail_product_same2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (similar_list.size() > 1) {
//                    Intent sim_intent = new Intent(MedicineDetailActivity.this, MedicineDetailActivity.class);
//                    sim_intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    sim_intent.putExtra("id", similar_list.get(1).getId());
//                    startActivity(sim_intent);
//                }
//            }
//        });
//        btn_medicine_detail_product_same3.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (similar_list.size() > 2) {
//                    Intent sim_intent = new Intent(MedicineDetailActivity.this, MedicineDetailActivity.class);
//                    sim_intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    sim_intent.putExtra("id", similar_list.get(2).getId());
//                    startActivity(sim_intent);
//                }
//            }
//        });


        img_medicine_detail_ingredient_bar_red = (ImageView) findViewById(R.id.img_medicine_detail_ingredient_bar_red);
        img_medicine_detail_ingredient_bar_blue = (ImageView) findViewById(R.id.img_medicine_detail_ingredient_bar_blue);

        btn_medicine_detail_ingredient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ingredient_intent = new Intent(MedicineDetailActivity.this, ComponentNormalActivity.class);
                ingredient_intent.putExtra("ingredient", ingredient);
                ingredient_intent.putExtra("dur", dur_checker);
                ingredient_intent.putExtra("dur_count", dur_count);
                byte[] temp_bitmap = bitmapChanger(main_bm);
                ingredient_intent.putExtra("bitmap", temp_bitmap);
                ingredient_intent.putExtra("product_name", name);
                ingredient_intent.putExtra("company_name", maker);
                ingredient_intent.putExtra("id", id);
                startActivity(ingredient_intent);
            }
        });

        btn_medicine_detail_how_to_use.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final LayoutInflater title_inflater = (LayoutInflater) getApplicationContext().getSystemService(LAYOUT_INFLATER_SERVICE);
                AlertDialog.Builder ab = new AlertDialog.Builder(MedicineDetailActivity.this);
                View title_view = title_inflater.inflate(R.layout.dialog_custom_title, null);
                TextView txt_dialog_title = (TextView) title_view.findViewById(R.id.txt_dialog_custom_title);
                ab.setCustomTitle(title_view);
                txt_dialog_title.setText("사용법");
                ab.setMessage(use);
                Button btn_dialog_title = (Button) title_view.findViewById(R.id.btn_dialog_custom_title);
                final AlertDialog ad = ab.create();
                btn_dialog_title.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {
                        if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                            ad.dismiss();
                        }
                        return false;
                    }
                });
                ad.show();
            }
        });

        btn_medicine_detail_warning.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final LayoutInflater title_inflater = (LayoutInflater) getApplicationContext().getSystemService(LAYOUT_INFLATER_SERVICE);
                AlertDialog.Builder ab = new AlertDialog.Builder(MedicineDetailActivity.this);
                View title_view = title_inflater.inflate(R.layout.dialog_custom_title, null);
                TextView txt_dialog_title = (TextView) title_view.findViewById(R.id.txt_dialog_custom_title);
                ab.setCustomTitle(title_view);
                txt_dialog_title.setText("주의사항");
                ab.setMessage(warning);
                Button btn_dialog_title = (Button) title_view.findViewById(R.id.btn_dialog_custom_title);
                final AlertDialog ad = ab.create();
                btn_dialog_title.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {
                        if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                            ad.dismiss();
                        }
                        return false;
                    }
                });
                ad.show();
            }
        });

        btn_medicine_detail_pharmacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pharmacy_intent = new Intent(MedicineDetailActivity.this, NearPharmacyActivity.class);
                startActivity(pharmacy_intent);
            }
        });

        btn_medicine_detail_modify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent edit_intent = new Intent(MedicineDetailActivity.this, EditActivity.class);
                edit_intent.putExtra("product_name", name);
                edit_intent.putExtra("company_name", maker);
                byte[] temp_bitmap = bitmapChanger(main_bm);
                edit_intent.putExtra("bitmap", temp_bitmap);
                edit_intent.putExtra("id", id);
                startActivity(edit_intent);
            }
        });

//        final PictoAdapter picto_adapter = new PictoAdapter(getApplicationContext(), R.layout.adapter_picto_image, picto_array);

        btn_medicine_detail_review_more = (Button) findViewById(R.id.btn_medicine_detail_review_more);
        btn_medicine_detail_review_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent image_intent = new Intent(MedicineDetailActivity.this, ImageActivity.class);
                image_intent.putExtra("image_list", image_list);
                image_intent.putExtra("bitmap_list", bitmap_list);
                startActivity(image_intent);
            }
        });

//        btn_medicine_detail_like.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (is_like) { // 좋아요 취소
//                    is_like = false;
//                    current_likes = String.valueOf(Integer.valueOf(current_likes) - 1);
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            txt_medicine_detail_like_count.setText(current_likes + "명");
//                        }
//                    });
//
//                    LikeAsync la = new LikeAsync();
//                    la.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "delete");
//                } else { // 좋아요 등록
//                    is_like = true;
//                    current_likes = String.valueOf(Integer.valueOf(current_likes) + 1);
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            txt_medicine_detail_like_count.setText(current_likes + "명");
//                        }
//                    });
//
//                    LikeAsync la = new LikeAsync();
//                    la.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "post");
//
//                    if (is_unlike) { // 싫어요 눌러진 상태
//                        is_unlike = false;
//                        current_unlikes = String.valueOf(Integer.valueOf(current_unlikes) - 1);
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                txt_medicine_detail_dislike_count.setText(current_unlikes + "명");
//                            }
//                        });
//
//                        UnlikeAsync ua = new UnlikeAsync();
//                        ua.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "delete");
//                    }
//                }
//            }
//        });

//        btn_medicine_detail_dislike.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (is_unlike) { // 싫어요 취소
//                    is_unlike = false;
//                    current_unlikes = String.valueOf(Integer.valueOf(current_unlikes) - 1);
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            txt_medicine_detail_dislike_count.setText(current_unlikes + "명");
//                        }
//                    });
//
//                    UnlikeAsync ua = new UnlikeAsync();
//                    ua.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "delete");
//                } else { // 싫어요 등록
//
//                    is_unlike = true;
//                    current_unlikes = String.valueOf(Integer.valueOf(current_unlikes) + 1);
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            txt_medicine_detail_dislike_count.setText(current_unlikes + "명");
//                        }
//                    });
//
//                    UnlikeAsync ua = new UnlikeAsync();
//                    ua.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "post");
//
//                    if (is_like) { // 좋아요 눌러진 상태
//                        is_like = false;
//                        current_likes = String.valueOf(Integer.valueOf(current_likes) - 1);
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                txt_medicine_detail_like_count.setText(current_likes + "명");
//                            }
//                        });
//
//                        LikeAsync la = new LikeAsync();
//                        la.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "delete");
//                    }
//                }
//            }
//        });

    }

    private void setCustomActionbar() {

        ActionBar actionBar = getSupportActionBar();

        // Custom Actionbar를 사용하기 위해 CustomEnabled을 true 시키고 필요 없는 것은 false 시킨다

        actionBar.setDisplayShowCustomEnabled(true);

        actionBar.setDisplayHomeAsUpEnabled(false);

        actionBar.setDisplayShowTitleEnabled(false);

        // Set custom view layout

        View mCustomView = LayoutInflater.from(this).inflate(R.layout.actionbar_product, null);

        actionBar.setCustomView(mCustomView);

        // Set no padding both side

        Toolbar parent = (Toolbar) mCustomView.getParent(); // first get parent toolbar of current action bar

        parent.setContentInsetsAbsolute(0, 0);              // set padding programmatically to 0dp

        // Set actionbar background image

        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.border_layout_bottom));

        // Set actionbar layout layoutparams

        ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);

        actionBar.setCustomView(mCustomView, params);

        Button btn_actionbar_alarm = (Button) findViewById(R.id.btn_actionbar_alarm);
        btn_actionbar_alarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShareDialog sd = new ShareDialog(MedicineDetailActivity.this);
                sd.setupDialog(name, effect + "\n" + warning, select_image, "");
                sd.show();
            }
        });


        final Button btn_actionbar_like = (Button) findViewById(R.id.btn_actionbar_like);
        btn_actionbar_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (is_bookmark) {
                            is_bookmark = false;
                            btn_actionbar_like.setBackground(getResources().getDrawable(R.drawable.product_icon_heart_nor, null));

                            BookmarkAsync ba = new BookmarkAsync(id, false);
                            ba.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        } else {
                            is_bookmark = true;
                            btn_actionbar_like.setBackground(getResources().getDrawable(R.drawable.product_icon_heart_active, null));

                            BookmarkAsync ba = new BookmarkAsync(id, true);
                            ba.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        }
                    }
                });
            }
        });

        ImageView img_actionbar_back = (ImageView) findViewById(R.id.img_actionbar_product_back);
        img_actionbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        TextView txt_actionbar = (TextView) findViewById(R.id.txt_actionbar_product);
        txt_actionbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        txt_actionbar.setText("약품상세");
    }

    class BookmarkAsync extends AsyncTask<String, String, String> {

        private boolean is_like = false;
        private String target_id;

        BookmarkAsync(String id, boolean is_like) {
            this.target_id = id;
            this.is_like = is_like;
        }

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            String json = jsonMaker.makeJson(new Pair("target", id));
            if (is_like) {
                result = httpConnect.send(json, httpConnect.getServerWithVersion() + getString(R.string.rest_bookmarks), csp.getValue("access_token", ""));
            } else {
                result = httpConnect.send(json, httpConnect.getServerWithVersion() + getString(R.string.rest_bookmarks), csp.getValue("access_token", ""), "DELETE");
            }

            return result;
        }
    }

    private byte[] bitmapChanger(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] bytes = stream.toByteArray();
        return bytes;
    }
}