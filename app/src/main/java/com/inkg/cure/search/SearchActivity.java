package com.inkg.cure.search;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.inkg.cure.R;
import com.inkg.cure.adapters.CompleteAdapter;
import com.inkg.cure.adapters.RecentAdapter;
import com.inkg.cure.classes.CureSP;
import com.inkg.cure.classes.HttpConnect;
import com.inkg.cure.classes.JsonMaker;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by inkg on 2016. 12. 6..
 */

public class SearchActivity extends AppCompatActivity {

    private ArrayList<String> recent_search = new ArrayList<>();
    private String recent = "";
    private HttpConnect httpConnect;
    private JsonMaker jsonMaker;
    private CureSP csp;
    private String keyword;
    private ListView list_recent_search;
    private RecentAdapter recentAdapter;
    private String access_token;
    private TextView txt_no_recent;
    private TextView txt_search_recent_query;
    private ImageView img_search_recent_delete;
    private RelativeLayout layout_auto_complete;
    private ListView list_auto_complete;
    private ArrayList<String> complete_list = new ArrayList<>();
    private CompleteAdapter complete_adapter;
    private EditText edit_actionbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        httpConnect = new HttpConnect(getApplicationContext());
        jsonMaker = new JsonMaker();
        csp = new CureSP(getApplicationContext());

        access_token = csp.getValue("access_token", "");
        csp.put("recent", "");
        recent = csp.getValue("recent", "");
        String[] temp_recent = recent.split("/");
        for (int i = 0; i < temp_recent.length; i++) {
            if ("".equals(temp_recent[i])) {

            } else {
                recent_search.add(temp_recent[i]);
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        setupViews();
        setCustomActionbar();
    }

    private void setupViews() {
        txt_no_recent = (TextView) findViewById(R.id.txt_no_recent);
        list_recent_search = (ListView) findViewById(R.id.list_recent_search);
        recentAdapter = new RecentAdapter(getApplicationContext(), R.layout.adapter_search_recent, recent_search);
        list_recent_search.setAdapter(recentAdapter);

        layout_auto_complete = (RelativeLayout) findViewById(R.id.layout_auto_complete);
        list_auto_complete = (ListView) findViewById(R.id.list_auto_complete);
        complete_adapter = new CompleteAdapter(getApplicationContext(), complete_list);
        list_auto_complete.setAdapter(complete_adapter);
        list_auto_complete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                edit_actionbar.setText(complete_list.get(position));
                layout_auto_complete.setVisibility(View.GONE);
            }
        });

        if (recent_search.size() == 0) {
            txt_no_recent.setVisibility(View.VISIBLE);
            list_recent_search.setVisibility(View.GONE);
        } else {
            txt_no_recent.setVisibility(View.GONE);
            list_recent_search.setVisibility(View.VISIBLE);
        }
        list_recent_search.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int position, long l) {
                Log.e("position", position + "");
                RelativeLayout layout_search_result_query = (RelativeLayout) adapterView.findViewById(R.id.layout_search_result_query);
                layout_search_result_query.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {
                        if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                            Intent result_intent = new Intent(SearchActivity.this, SearchResultActivity.class);
                            result_intent.putExtra("keyword", recent_search.get(position));
                            startActivity(result_intent);
                        }
                        return false;
                    }
                });

                RelativeLayout layout_search_result_delete = (RelativeLayout) adapterView.findViewById(R.id.layout_search_result_delete);
                layout_search_result_delete.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {
                        if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                            recent_search.remove(position);
                            if (recent_search.size() == 0) {
                                txt_no_recent.setVisibility(View.VISIBLE);
                                list_recent_search.setVisibility(View.GONE);
                            }
                            recentAdapter.notifyDataSetChanged();
                        }
                        return false;
                    }
                });
            }
        });
    }

    private void setCustomActionbar() {

        ActionBar actionBar = getSupportActionBar();

        // Custom Actionbar를 사용하기 위해 CustomEnabled을 true 시키고 필요 없는 것은 false 시킨다

        actionBar.setDisplayShowCustomEnabled(true);

        actionBar.setDisplayHomeAsUpEnabled(false);

        actionBar.setDisplayShowTitleEnabled(false);

        // Set custom view layout

        View mCustomView = LayoutInflater.from(this).inflate(R.layout.actionbar_simple, null);

        actionBar.setCustomView(mCustomView);

        // Set no padding both side

        Toolbar parent = (Toolbar) mCustomView.getParent(); // first get parent toolbar of current action bar

        parent.setContentInsetsAbsolute(0, 0);              // set padding programmatically to 0dp

        // Set actionbar background image

        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.border_layout_bottom));

        // Set actionbar layout layoutparams

        ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);

        actionBar.setCustomView(mCustomView, params);

        ImageView img_actionbar_back = (ImageView) findViewById(R.id.img_actionbar_back);
        img_actionbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        TextView txt_actionbar = (TextView) findViewById(R.id.txt_actionbar_simple);
        txt_actionbar.setVisibility(View.GONE);

        edit_actionbar = (EditText) findViewById(R.id.edit_actionbar_simple);
        edit_actionbar.setVisibility(View.VISIBLE);
        edit_actionbar.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                switch (actionId) {
                    case EditorInfo.IME_ACTION_SEARCH:
                        String query = "";
                        query = edit_actionbar.getText().toString();
                        if ("".equals(query)) {
                            Toast.makeText(SearchActivity.this, "검색어를 입력해주세요.", Toast.LENGTH_SHORT).show();
                        } else {
                            txt_no_recent.setVisibility(View.GONE);
                            list_recent_search.setVisibility(View.VISIBLE);
                            Intent result_intent = new Intent(SearchActivity.this, SearchResultActivity.class);
                            result_intent.putExtra("keyword", query);
                            startActivity(result_intent);

                            query = query.replaceAll("/", "");
                            if (recent_search.contains(query)) {
                                recent_search.remove(recent_search.indexOf(query));
                                recent_search.add(0, query);
                            } else {
                                recent_search.add(0, query);
                                if (recent_search.size() > 4) {
                                    recent_search.remove(recent_search.size() - 1);
                                }
                            }
                            recent = "";
                            for (int i = 0; i < recent_search.size(); i++) {
                                recent = recent + query;
                                if (i != recent_search.size() - 1) {
                                    recent = recent + "/";
                                }
                            }

                            csp.put("recent", recent);
                        }
                        break;

                    default:
                        return false;
                }
                return true;

            }
        });

        TextWatcher complete_watcher = new TextWatcher() {
            String query = "";

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                query = charSequence.toString();

                if ("".equals(query)) {
                    complete_list.clear();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            complete_adapter.notifyDataSetChanged();
                        }
                    });
                }
            }
            
            @Override
            public void afterTextChanged(Editable editable) {
                layout_auto_complete.setVisibility(View.VISIBLE);
                CompleteDataAsync cd = new CompleteDataAsync(query);
                cd.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        };

        edit_actionbar.addTextChangedListener(complete_watcher);

        ImageView img_search_icon = (ImageView) findViewById(R.id.img_search_icon);
        img_search_icon.setVisibility(View.VISIBLE);

        ImageView img_actionbar_x = (ImageView) findViewById(R.id.img_actionbar_x);
        img_actionbar_x.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            edit_actionbar.setText("");
                        }
                    });
                }
                return false;
            }
        });
        img_actionbar_x.setVisibility(View.VISIBLE);
    }

    class CompleteDataAsync extends AsyncTask<String, String, String> {

        String m_query;

        public CompleteDataAsync() {

        }

        public CompleteDataAsync(String query) {
            this.m_query = query;
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            result = httpConnect.send("", httpConnect.getServerWithVersion() + getString(R.string.rest_search_suggest) + "?query=" + m_query, access_token, "GET");
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.e("result", result);
            try {
                complete_list.clear();
                JSONArray jsonArray = new JSONArray(result);
                for (int i = 0; i < jsonArray.length(); i++) {
                    if (i == 5) {
                        break;
                    }
                    JSONObject item_object = jsonArray.getJSONObject(i);
                    if (item_object.has("suggestion")) {
                        String suggestion = item_object.getString("suggestion");
                        complete_list.add(suggestion);
                    }
                }

                complete_adapter.notifyDataSetChanged();
            } catch (Exception e) {
                Log.e("se ac", "get complete excep", e);
            }
        }
    }
}