package com.inkg.cure.search;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.inkg.cure.R;
import com.inkg.cure.adapters.DialogAdapter;
import com.inkg.cure.adapters.Ingr1Adapter;
import com.inkg.cure.adapters.Ingr4Adapter;
import com.inkg.cure.classes.CureSP;
import com.inkg.cure.classes.HttpConnect;
import com.inkg.cure.classes.Ingredient;
import com.inkg.cure.classes.JsonMaker;
import com.inkg.cure.review.EditActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by inkg on 2017. 2. 27..
 */

public class ComponentHealthActivity extends AppCompatActivity {

    private ArrayList<Ingredient> ingr1_array;
    private ArrayList<Ingredient> ingr4_array;
    private ListView list_component_health;
    private ListView list_component_health_etc;
    private Ingr1Adapter ingr1Adapter;
    private Ingr4Adapter ingr4Adapter;

    private TextView txt_component_health_functional;
    private TextView txt_component_health_etc;
    private Button btn_component_edit;

    private String product_name;
    private String company_name;
    private String id;
    private Bitmap main_bm;

    private CureSP csp;
    private HttpConnect httpConnect;
    private JsonMaker jsonMaker;
    private boolean is_like;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_component_health);
        Intent intent = getIntent();
        String func_result = intent.getStringExtra("functional");
        String capsule_result = intent.getStringExtra("capsule");
        String etc_result = intent.getStringExtra("etc");
        product_name = intent.getStringExtra("product_name");
        company_name = intent.getStringExtra("company_name");
        byte[] bytes = intent.getByteArrayExtra("bitmap");
        main_bm = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
        id = intent.getStringExtra("id");

        csp = new CureSP(getApplicationContext());
        httpConnect = new HttpConnect(getApplicationContext());
        jsonMaker = new JsonMaker();

        setData(func_result, etc_result);
        setUpViews();
        setCustomActionbar();
    }

    private void setData(String func_result, String etc_result) {
        ingr1_array = new ArrayList<>();
        ingr4_array = new ArrayList<>();
        Log.e("ing", func_result);
        Log.e("sda", etc_result);
        try {
            JSONArray func_ingr_json = new JSONArray(func_result);
            for (int i = 0; i < func_ingr_json.length(); i++) {
                Ingredient ingr1 = new Ingredient();
                ingr1.setKor_name(func_ingr_json.get(i).toString());
                ingr1.setEng_name("");
                ingr1.setType("functional");
                ingr1_array.add(ingr1);
            }
            JSONArray etc_ingr_json = new JSONArray(etc_result);
            for (int j = 0; j < etc_ingr_json.length(); j++) {
                JSONObject func_object = etc_ingr_json.getJSONObject(j);
                Ingredient ingr4 = new Ingredient();
                ingr4.setKor_name(func_object.getString("name"));
                ingr4.setType("etc");
                ingr4.setEng_name("");
                ingr4_array.add(ingr4);
            }
        } catch (Exception e) {
            Log.e("ingr norm", "ingr excep", e);
        }
    }

    private void setUpViews() {
        list_component_health = (ListView) findViewById(R.id.list_component_health);
        ingr1Adapter = new Ingr1Adapter(getApplicationContext(), R.layout.adapter_ingr1_item, ingr1_array);
        list_component_health.setAdapter(ingr1Adapter);
        list_component_health.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int position, long l) {
                ArrayList<Pair<String, String>> pair_list = new ArrayList<>();
                pair_list.add(0, new Pair<String, String>(ingr1_array.get(position).getFda_comment(), ingr1_array.get(position).getFda_grade()));
                final DialogAdapter dialog_adapter = new DialogAdapter(getApplicationContext(), R.layout.dialog_content, pair_list);
                final LayoutInflater title_inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                AlertDialog.Builder ab = new AlertDialog.Builder(view.getRootView().getContext());
                View title_view = title_inflater.inflate(R.layout.dialog_title, null);
                TextView txt_dialog_kor = (TextView) title_view.findViewById(R.id.txt_dialog_kor);
                TextView txt_dialog_eng = (TextView) title_view.findViewById(R.id.txt_dialog_eng);
                ab.setCustomTitle(title_view);
                txt_dialog_kor.setText(ingr1_array.get(position).getKor_name());
                txt_dialog_eng.setText(ingr1_array.get(position).getEng_name());
                final ImageView img_dialog_good = (ImageView) title_view.findViewById(R.id.img_dialog_good);
                img_dialog_good.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (is_like) {
                            is_like = false;
                            img_dialog_good.setBackground(getResources().getDrawable(R.drawable.product_icon_heart_nor, null));

                            BookmarkAsync ba = new BookmarkAsync(ingr1_array.get(position).getName(), false);
                            ba.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        } else {
                            is_like = true;
                            img_dialog_good.setBackground(getResources().getDrawable(R.drawable.product_icon_heart_active, null));

                            BookmarkAsync ba = new BookmarkAsync(ingr1_array.get(position).getName(), true);
                            ba.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        }
                    }
                });
                ab.setAdapter(dialog_adapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                }).create();
                final AlertDialog ad = ab.create();
                Button btn_dialog_title = (Button) title_view.findViewById(R.id.btn_dialog_title);
                btn_dialog_title.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ad.dismiss();
                    }
                });
                ad.show();
            }
        });

        list_component_health_etc = (ListView) findViewById(R.id.list_component_health_etc);
        ingr4Adapter = new Ingr4Adapter(getApplicationContext(), R.layout.adapter_ingr4_item, ingr4_array);
        list_component_health_etc.setAdapter(ingr4Adapter);
        list_component_health_etc.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int position, long l) {
                ArrayList<Pair<String, String>> pair_list = new ArrayList<>();
                pair_list.add(0, new Pair<String, String>(ingr4_array.get(position).getFda_comment(), ingr4_array.get(position).getFda_grade()));
                final DialogAdapter dialog_adapter = new DialogAdapter(getApplicationContext(), R.layout.dialog_content, pair_list);
                final LayoutInflater title_inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                AlertDialog.Builder ab = new AlertDialog.Builder(view.getRootView().getContext());
                View title_view = title_inflater.inflate(R.layout.dialog_title, null);
                TextView txt_dialog_kor = (TextView) title_view.findViewById(R.id.txt_dialog_kor);
                TextView txt_dialog_eng = (TextView) title_view.findViewById(R.id.txt_dialog_eng);
                ab.setCustomTitle(title_view);
                txt_dialog_kor.setText(ingr4_array.get(position).getKor_name());
                txt_dialog_eng.setText(ingr4_array.get(position).getEng_name());
                final ImageView img_dialog_good = (ImageView) title_view.findViewById(R.id.img_dialog_good);
                img_dialog_good.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (is_like) {
                            is_like = false;
                            img_dialog_good.setBackground(getResources().getDrawable(R.drawable.product_icon_heart_nor, null));

                            BookmarkAsync ba = new BookmarkAsync(ingr4_array.get(position).getName(), false);
                            ba.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        } else {
                            is_like = true;
                            img_dialog_good.setBackground(getResources().getDrawable(R.drawable.product_icon_heart_active, null));

                            BookmarkAsync ba = new BookmarkAsync(ingr4_array.get(position).getName(), true);
                            ba.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        }
                    }
                });
                ab.setAdapter(dialog_adapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                }).create();
                final AlertDialog ad = ab.create();
                Button btn_dialog_title = (Button) title_view.findViewById(R.id.btn_dialog_title);
                btn_dialog_title.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ad.dismiss();
                    }
                });
                ad.show();
            }
        });

        txt_component_health_functional = (TextView) findViewById(R.id.txt_component_health_functional);
        txt_component_health_etc = (TextView) findViewById(R.id.txt_component_health_etc);

        btn_component_edit = (Button) findViewById(R.id.btn_component_edit);
        btn_component_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent edit_intent = new Intent(ComponentHealthActivity.this, EditActivity.class);
                edit_intent.putExtra("bitmap", main_bm);
                edit_intent.putExtra("product_name", product_name);
                edit_intent.putExtra("company_name", company_name);
                edit_intent.putExtra("id", id);
                startActivity(edit_intent);
            }
        });
    }

    private void setCustomActionbar() {

        ActionBar actionBar = getSupportActionBar();

        // Custom Actionbar를 사용하기 위해 CustomEnabled을 true 시키고 필요 없는 것은 false 시킨다

        actionBar.setDisplayShowCustomEnabled(true);

        actionBar.setDisplayHomeAsUpEnabled(false);

        actionBar.setDisplayShowTitleEnabled(false);

        // Set custom view layout

        View mCustomView = LayoutInflater.from(this).inflate(R.layout.actionbar_simple, null);

        actionBar.setCustomView(mCustomView);

        // Set no padding both side

        Toolbar parent = (Toolbar) mCustomView.getParent(); // first get parent toolbar of current action bar

        parent.setContentInsetsAbsolute(0, 0);              // set padding programmatically to 0dp

        // Set actionbar background image

        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.border_layout_bottom));

        // Set actionbar layout layoutparams

        ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);

        actionBar.setCustomView(mCustomView, params);

        ImageView img_actionbar_back = (ImageView) findViewById(R.id.img_actionbar_back);
        img_actionbar_back.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    finish();
                }
                return false;
            }
        });

        TextView txt_actionbar = (TextView) findViewById(R.id.txt_actionbar_simple);
        txt_actionbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        txt_actionbar.setText("성분");
    }

    class BookmarkAsync extends AsyncTask<String, String, String> {

        private boolean is_like = false;
        private String name;

        BookmarkAsync(String name, boolean is_like) {
            this.name = name;
            this.is_like = is_like;
        }

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            String json = jsonMaker.makeJson(new Pair("name", name));
            if (is_like) {
                result = httpConnect.send(json, httpConnect.getServerWithVersion() + getString(R.string.rest_like_ingredient), csp.getValue("access_token", ""));
            } else {
                result = httpConnect.send(json, httpConnect.getServerWithVersion() + getString(R.string.rest_like_ingredient), csp.getValue("access_token", ""), "DELETE");
            }

            return result;
        }

    }
}
