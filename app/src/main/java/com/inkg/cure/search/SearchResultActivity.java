package com.inkg.cure.search;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.inkg.cure.R;
import com.inkg.cure.adapters.SearchResultAdapter;
import com.inkg.cure.classes.CureSP;
import com.inkg.cure.classes.GlobalApplication;
import com.inkg.cure.classes.HttpConnect;
import com.inkg.cure.classes.JsonMaker;
import com.inkg.cure.classes.LoadingDialog;
import com.inkg.cure.classes.RankData;
import com.inkg.cure.classes.Scores;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by inkg on 2017. 2. 26..
 */

public class SearchResultActivity extends AppCompatActivity implements AbsListView.OnScrollListener {

    private String keyword = "";
    private String access_token = "";
    private HttpConnect httpConnect;
    private JsonMaker jsonMaker;
    private CureSP csp;
    private ArrayList<String> health_sub_array = new ArrayList<>();
    private ArrayList<String> medicine_sub_array = new ArrayList<>();
    private ArrayList<String> expert_sub_array = new ArrayList<>();
    private ArrayList<RankData> rank_array = new ArrayList<>();
    private ArrayList<RankData> rank_partial_array = new ArrayList<>();

//    private Button btn_search_result_order;
//    private TextView txt_search_result_order;

    private Button btn_search_result_total;
    private Button btn_search_result_normal;
    private Button btn_search_result_health;
    private Button btn_search_result_expert;

    private TextView txt_search_result_total;
    private TextView txt_search_result_normal;
    private TextView txt_search_result_health;
    private TextView txt_search_result_expert;

    private RelativeLayout layout_search_result_total;
    private RelativeLayout layout_search_result_normal;
    private RelativeLayout layout_search_result_health;
    private RelativeLayout layout_search_result_expert;

    private ListView list_search_result_item;
    private SearchResultAdapter adapter_rank_data;

    //    private ProgressDialog loadingDialog;
    private LoadingDialog loadingDialog;

    private String type = "total";

    private LayoutInflater mInflater;

    private boolean mLockListView;
    private int current_index = 10;
    private boolean lastitemVisibleFlag = false;

    private TextView txt_search_result_count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);
        Intent keyword_intent = getIntent();
        keyword = keyword_intent.getStringExtra("keyword");
        GlobalApplication.setCurrentActivity(this);

        loadingDialog = new LoadingDialog(SearchResultActivity.this);
        rank_array = new ArrayList<>();
        rank_partial_array = new ArrayList<>();

        httpConnect = new HttpConnect(getApplicationContext());
        jsonMaker = new JsonMaker();
        csp = new CureSP(getApplicationContext());


        setupViews();
        setCustomActionbar();
    }

    @Override
    protected void onStart() {
        super.onStart();
        InitRankAsync irAsync = new InitRankAsync("total");
        irAsync.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }


    private void setupViews() {
        layout_search_result_total = (RelativeLayout) findViewById(R.id.layout_search_result_total);
        layout_search_result_total.setBackgroundColor(ContextCompat.getColor(SearchResultActivity.this, R.color.maya_blue1));
        layout_search_result_normal = (RelativeLayout) findViewById(R.id.layout_search_result_normal);
        layout_search_result_health = (RelativeLayout) findViewById(R.id.layout_search_result_health);
        layout_search_result_expert = (RelativeLayout) findViewById(R.id.layout_search_result_expert);

        txt_search_result_count = (TextView) findViewById(R.id.txt_search_result_count);

        txt_search_result_total = (TextView) findViewById(R.id.txt_search_result_total);
        txt_search_result_total.setTextColor(ContextCompat.getColor(SearchResultActivity.this, R.color.white));
        txt_search_result_normal = (TextView) findViewById(R.id.txt_search_result_normal);
        txt_search_result_health = (TextView) findViewById(R.id.txt_search_result_health);
        txt_search_result_expert = (TextView) findViewById(R.id.txt_search_result_expert);

        list_search_result_item = (ListView) findViewById(R.id.list_search_result_item);
        list_search_result_item.setSmoothScrollbarEnabled(true);
        list_search_result_item.setFastScrollEnabled(false);
//        list_search_result_item.addFooterView(mInflater.inflate(R.layout.dialog_custom_title, null));
        list_search_result_item.setOnScrollListener(this);
        adapter_rank_data = new SearchResultAdapter(getApplicationContext(), R.layout.adapter_category_search, rank_partial_array, type);
        list_search_result_item.setAdapter(adapter_rank_data);

        list_search_result_item.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent detail_intent = null;
                if ("medicine".equals(type)) {
                    detail_intent = new Intent(SearchResultActivity.this, MedicineDetailActivity.class);
                } else if ("healthfood".equals(type)) {
                    detail_intent = new Intent(SearchResultActivity.this, HealthfoodDetailActivity.class);
                } else {
                    detail_intent = new Intent(SearchResultActivity.this, ExpertDetailActivity.class);
                }
                detail_intent.putExtra("id", rank_partial_array.get(position).getId());
                detail_intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                view.getContext().startActivity(detail_intent);
            }
        });

        btn_search_result_total = (Button) findViewById(R.id.btn_search_result_total);
        btn_search_result_total.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                type = "total";
                rank_array.clear();
                rank_partial_array.clear();
                current_index = 10;
                setDefaultColor();
                layout_search_result_total.setBackgroundColor(ContextCompat.getColor(SearchResultActivity.this, R.color.maya_blue1));
                txt_search_result_total.setTextColor(ContextCompat.getColor(SearchResultActivity.this, R.color.white));
                adapter_rank_data = new SearchResultAdapter(getApplicationContext(), R.layout.adapter_category_search, rank_partial_array, type);
                list_search_result_item.setAdapter(adapter_rank_data);

                InitRankAsync ira = new InitRankAsync(type);
                ira.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        });
        btn_search_result_normal = (Button) findViewById(R.id.btn_search_result_normal);
        btn_search_result_normal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                type = "medicine";
                rank_array.clear();
                rank_partial_array.clear();
                current_index = 10;
                setDefaultColor();
                layout_search_result_normal.setBackgroundColor(ContextCompat.getColor(SearchResultActivity.this, R.color.maya_blue1));
                txt_search_result_normal.setTextColor(ContextCompat.getColor(SearchResultActivity.this, R.color.white));
                adapter_rank_data = new SearchResultAdapter(getApplicationContext(), R.layout.adapter_category_search, rank_partial_array, type);
                list_search_result_item.setAdapter(adapter_rank_data);

                InitRankAsync ira = new InitRankAsync(type);
                ira.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        });
        btn_search_result_health = (Button) findViewById(R.id.btn_search_result_health);
        btn_search_result_health.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                type = "healthfood";
                rank_array.clear();
                rank_partial_array.clear();
                current_index = 10;
                setDefaultColor();
                layout_search_result_health.setBackgroundColor(ContextCompat.getColor(SearchResultActivity.this, R.color.maya_blue1));
                txt_search_result_health.setTextColor(ContextCompat.getColor(SearchResultActivity.this, R.color.white));
                adapter_rank_data = new SearchResultAdapter(getApplicationContext(), R.layout.adapter_category_search, rank_partial_array, type);
                list_search_result_item.setAdapter(adapter_rank_data);

                InitRankAsync ira = new InitRankAsync(type);
                ira.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        });
        btn_search_result_expert = (Button) findViewById(R.id.btn_search_result_expert);
        btn_search_result_expert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        Toast.makeText(getApplicationContext(), "준비중입니다.", Toast.LENGTH_SHORT).show();
//                    }
//                });
                type = "medicine2";
                rank_array.clear();
                rank_partial_array.clear();
                current_index = 10;
                setDefaultColor();
                layout_search_result_expert.setBackgroundColor(ContextCompat.getColor(SearchResultActivity.this, R.color.maya_blue1));
                txt_search_result_expert.setTextColor(ContextCompat.getColor(SearchResultActivity.this, R.color.white));
                adapter_rank_data = new SearchResultAdapter(getApplicationContext(), R.layout.adapter_category_search, rank_partial_array, type);
                list_search_result_item.setAdapter(adapter_rank_data);

                InitRankAsync ira = new InitRankAsync(type);
                ira.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        });


//        btn_search_result_order = (Button) findViewById(R.id.btn_search_result_order);
//        txt_search_result_order = (TextView) findViewById(R.id.txt_search_result_order);
//        final LayoutInflater title_inflater = (LayoutInflater) getApplicationContext().getSystemService(LAYOUT_INFLATER_SERVICE);
//        btn_search_result_order.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                AlertDialog.Builder ab = new AlertDialog.Builder(SearchResultActivity.this);
//                View title_view = title_inflater.inflate(R.layout.dialog_custom_title, null);
//                TextView txt_dialog_title = (TextView) title_view.findViewById(R.id.txt_dialog_title);
//                ab.setCustomTitle(title_view);
//                txt_dialog_title.setText("정렬");
//                final String[] orders = new String[]{"최신순", "인기순", "리뷰순"};
//                ab.setItems(orders, new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(final DialogInterface dialogInterface, final int i) {
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                txt_search_result_order.setText(orders[i]);
//                            }
//                        });
//                    }
//                });
//                ab.show();
//            }
//        });
    }

    @Override
    public void onScrollStateChanged(AbsListView absListView, int scrollState) {
        if (lastitemVisibleFlag && mLockListView == false) {
            if (rank_array.size() == rank_partial_array.size()) {

            } else {
                Log.e("se re ac", "Loading next items");
                // 아이템을 추가하는 동안 중복 요청을 방지하기 위해 락을 걸어둡니다.
                mLockListView = true;
                addItems(10);
            }
        }
    }

    @Override
    public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        // 현재 가장 처음에 보이는 셀번호와 보여지는 셀번호를 더한값이
        // 전체의 숫자와 동일해지면 가장 아래로 스크롤 되었다고 가정합니다.
        int count = totalItemCount - visibleItemCount;
        lastitemVisibleFlag = (totalItemCount > 0) && (firstVisibleItem + visibleItemCount >= totalItemCount);
    }

    private void addItems(final int size) {

        loadingDialog.show();
        Runnable run = new Runnable() {
            @Override
            public void run() {
                for (int i = current_index; i < current_index + size; i++) {

                    if (current_index >= rank_array.size()) {
                        break;
                    }

                    rank_partial_array.add(rank_array.get(i));
                }
                current_index = current_index + size;

                // 모든 데이터를 로드하여 적용하였다면 어댑터에 알리고
                // 리스트뷰의 락을 해제합니다.
                adapter_rank_data.notifyDataSetChanged();
                mLockListView = false;
                loadingDialog.dismiss();
            }
        };

        runOnUiThread(run);
    }

    class InitRankAsync extends AsyncTask<String, String, String> {

        String m_medicine;
        String m_healthfood;
        String m_type;

        public InitRankAsync(String my_type) {
            this.m_type = my_type;
        }

        public InitRankAsync(String medicine, String healthfood) {
            this.m_medicine = medicine;
            this.m_healthfood = healthfood;
            this.m_type = type;
        }

        public InitRankAsync(String medicine, String healthfood, String my_type) {
            this.m_medicine = medicine;
            this.m_healthfood = healthfood;
            this.m_type = my_type;
            if ("".equals(my_type)) {
                this.m_type = "medicine";
            }
        }

        @Override
        protected void onPreExecute() {
            medicine_sub_array.clear();
            health_sub_array.clear();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loadingDialog.show();
                }
            });
        }

        @Override
        protected String doInBackground(String... strings) {
            Log.e("my_type, keyword", m_type + " / " + keyword);
            String result = "";
            result = httpConnect.send("", getString(R.string.server_url) + getString(R.string.cureya_version) + getString(R.string.rest_search) + "?query=" + keyword, csp.getValue("access_token", ""), "GET");
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.e("rank result", result);

            InitViewAsync ivAsync = new InitViewAsync(result);
            ivAsync.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    class InitViewAsync extends AsyncTask<String, String, String> {

        String result = "";

        public InitViewAsync(String result) {
            this.result = result;
        }

        @Override
        protected void onPreExecute() {
            getRankData(result);
        }

        @Override
        protected String doInBackground(String... strings) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    adapter_rank_data.notifyDataSetChanged();
                    loadingDialog.dismiss();
                }
            });
            return result;
        }
        
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
        }
    }

    private void setDefaultColor() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                layout_search_result_total.setBackground(getResources().getDrawable(R.drawable.border_content_outer, null));
                layout_search_result_normal.setBackground(getResources().getDrawable(R.drawable.border_content_outer, null));
                layout_search_result_health.setBackground(getResources().getDrawable(R.drawable.border_content_outer, null));
                layout_search_result_expert.setBackground(getResources().getDrawable(R.drawable.border_content_outer, null));

                txt_search_result_total.setTextColor(ContextCompat.getColor(SearchResultActivity.this, R.color.taupe_gray));
                txt_search_result_normal.setTextColor(ContextCompat.getColor(SearchResultActivity.this, R.color.taupe_gray));
                txt_search_result_health.setTextColor(ContextCompat.getColor(SearchResultActivity.this, R.color.taupe_gray));
                txt_search_result_expert.setTextColor(ContextCompat.getColor(SearchResultActivity.this, R.color.taupe_gray));

            }
        });
    }

    private void getRankData(String result) {
        rank_array.clear();
        try {
            Log.e("result", result);
            JSONObject search_json = new JSONObject(result);
            String current_type = this.type;
            JSONObject item_json = search_json.getJSONObject("Items");
            final JSONArray hit_json = item_json.getJSONArray("hit");
            int index = 0;
            for (int i = 0; i < hit_json.length(); i++) {
                if (i == 20) { // 20개 제한
                    break;
                }
                String temp_type = "";
                JSONObject content_json = hit_json.getJSONObject(i);
                JSONObject field_json = content_json.getJSONObject("fields");
                temp_type = field_json.getJSONArray("type").get(0).toString();
                if ("total".equals(current_type)) {
                    RankData rankData = new RankData();
                    if (field_json.has("company")) {
                        rankData.setCompany(field_json.getJSONArray("company").get(0).toString());
                    } else if (field_json.has("maker")) {
                        rankData.setCompany(field_json.getJSONArray("maker").get(0).toString());
                    } else {

                    }
                    rankData.setId(content_json.getString("id"));
                    rankData.setName(field_json.getJSONArray("name").get(0).toString());
                    if (field_json.has("select_image")) {
                        rankData.setSelect_image(field_json.getJSONArray("select_image").getString(0).replaceAll("\\\\", ""));
                    } else {
                        rankData.setSelect_image("null");
                    }

                    JSONObject rank_json_scores = field_json.getJSONObject("scores");
                    Scores scores = new Scores();
                    scores.setBookmarks(rank_json_scores.getString("bookmarks"));
                    scores.setLikes(rank_json_scores.getString("likes"));
                    scores.setReviews(rank_json_scores.getString("reviews"));
                    scores.setShares(rank_json_scores.getString("shares"));
                    scores.setUnlikes(rank_json_scores.getString("unlikes"));
                    if (rank_json_scores.has("views")) {
                        scores.setViews(rank_json_scores.getString("views"));
                    }
                    rankData.setScores(scores);
                    rank_array.add(index, rankData);
                    index = index + 1;
                } else {
                    if (!temp_type.equals(current_type)) {
                        continue;
                    } else {
                        if ("medicine".equals(current_type)) {
                            RankData rankData = new RankData();
                            if (field_json.has("company")) {
                                rankData.setCompany(field_json.getJSONArray("company").get(0).toString());
                            } else if (field_json.has("maker")) {
                                rankData.setCompany(field_json.getJSONArray("maker").get(0).toString());
                            } else {

                            }
                            rankData.setId(content_json.getString("id"));
                            rankData.setName(field_json.getJSONArray("name").get(0).toString());
                            if (field_json.has("select_image")) {
                                rankData.setSelect_image(field_json.getJSONArray("select_image").getString(0).replaceAll("\\\\", ""));
                            } else {
                                rankData.setSelect_image("null");
                            }
                            JSONObject rank_json_scores = field_json.getJSONObject("scores");
                            Scores scores = new Scores();
                            scores.setBookmarks(rank_json_scores.getString("bookmarks"));
                            scores.setLikes(rank_json_scores.getString("likes"));
                            scores.setReviews(rank_json_scores.getString("reviews"));
                            scores.setShares(rank_json_scores.getString("shares"));
                            scores.setUnlikes(rank_json_scores.getString("unlikes"));
                            if (rank_json_scores.has("views")) {
                                scores.setViews(rank_json_scores.getString("views"));
                            }
                            rankData.setScores(scores);
                            rank_array.add(index, rankData);
                            index = index + 1;
                        } else if ("healthfood".equals(current_type)) {
                            RankData rankData = new RankData();
                            if (field_json.has("company")) {
                                rankData.setCompany(field_json.getJSONArray("company").get(0).toString());
                            } else if (field_json.has("maker")) {
                                rankData.setCompany(field_json.getJSONArray("maker").get(0).toString());
                            } else {

                            }
                            rankData.setName(field_json.getJSONArray("name").get(0).toString());
                            if (field_json.has("select_image")) {
                                rankData.setSelect_image(field_json.getJSONArray("select_image").getString(0).replaceAll("\\\\", ""));
                            } else {
                                rankData.setSelect_image("null");
                            }
                            JSONObject rank_json_scores = field_json.getJSONObject("scores");
                            Scores scores = new Scores();
                            scores.setBookmarks(rank_json_scores.getString("bookmarks"));
                            scores.setLikes(rank_json_scores.getString("likes"));
                            scores.setReviews(rank_json_scores.getString("reviews"));
                            scores.setShares(rank_json_scores.getString("shares"));
                            scores.setUnlikes(rank_json_scores.getString("unlikes"));
                            if (rank_json_scores.has("views")) {
                                scores.setViews(rank_json_scores.getString("views"));
                            }

                            rankData.setScores(scores);
                            rank_array.add(index, rankData);
                            index = index + 1;
                        } else {
                            RankData rankData = new RankData();
                            if (field_json.has("company")) {
                                rankData.setCompany(field_json.getJSONArray("company").get(0).toString());
                            } else if (field_json.has("maker")) {
                                rankData.setCompany(field_json.getJSONArray("maker").get(0).toString());
                            } else {
                                rankData.setCompany("");
                            }
                            rankData.setId(content_json.getString("id"));
                            rankData.setName(field_json.getJSONArray("name").get(0).toString());
                            if (field_json.has("select_image")) {
                                rankData.setSelect_image(field_json.getJSONArray("select_image").getString(0).replaceAll("\\\\", ""));
                            } else {
                                rankData.setSelect_image("null");
                            }
                            JSONObject rank_json_scores = field_json.getJSONObject("scores");
                            Scores scores = new Scores();
                            scores.setBookmarks(rank_json_scores.getString("bookmarks"));
                            scores.setLikes(rank_json_scores.getString("likes"));
                            scores.setReviews(rank_json_scores.getString("reviews"));
                            scores.setShares(rank_json_scores.getString("shares"));
                            scores.setUnlikes(rank_json_scores.getString("unlikes"));
                            scores.setViews(rank_json_scores.getString("views"));
                            rankData.setScores(scores);
                            rank_array.add(index, rankData);
                            index = index + 1;
                        }
                    }
                }
            }
            for (int i = 0; i < 10; i++) {
                rank_partial_array.add(rank_array.get(i));
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    adapter_rank_data.notifyDataSetChanged();
                    if (hit_json.length() > 20) {
                        txt_search_result_count.setText("제품 " + 20 + "개");
                    } else {
                        txt_search_result_count.setText("제품 " + hit_json.length() + "개");
                    }
                }
            });
        } catch (Exception e) {
            Log.e("se re activity", "get search result excep", e);
        }
    }

    private void setCustomActionbar() {

        ActionBar actionBar = getSupportActionBar();

        // Custom Actionbar를 사용하기 위해 CustomEnabled을 true 시키고 필요 없는 것은 false 시킨다

        actionBar.setDisplayShowCustomEnabled(true);

        actionBar.setDisplayHomeAsUpEnabled(false);

        actionBar.setDisplayShowTitleEnabled(false);

        // Set custom view layout

        View mCustomView = LayoutInflater.from(this).inflate(R.layout.actionbar_simple, null);

        actionBar.setCustomView(mCustomView);

        // Set no padding both side

        Toolbar parent = (Toolbar) mCustomView.getParent(); // first get parent toolbar of current action bar

        parent.setContentInsetsAbsolute(0, 0);              // set padding programmatically to 0dp

        // Set actionbar background image

        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.border_layout_bottom));

        // Set actionbar layout layoutparams

        ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);

        actionBar.setCustomView(mCustomView, params);

        ImageView img_actionbar_back = (ImageView) findViewById(R.id.img_actionbar_back);
        img_actionbar_back.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    finish();
                }
                return false;
            }
        });

        TextView txt_actionbar = (TextView) findViewById(R.id.txt_actionbar_simple);
        txt_actionbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        txt_actionbar.setText(this.keyword);
    }
}