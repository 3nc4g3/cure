package com.inkg.cure.shopping;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.inkg.cure.R;

import java.util.ArrayList;

/**
 * Created by inkg on 2017. 1. 30..
 */

public class ShoppingMainActivity extends AppCompatActivity {
    final static String clientKey = "ar8whH4o7Xvw5S378VG6";
    final static String clientSecret = "YuLH4AGZX_";
    /*
     * 한 화면에 출력된 제품수를 명시한다.
     */
    final static int NUM_PRODUCT_PER_SCREEN = 10;
    /*
     * 출력할 리스트의 시작 위치를 지정한다.
     */
    int nStartPosition = 1;
    /*
     * 제품 리스트를 관리하는 리스트 뷰를 선언한다.
     */
    ListView list_shopping_product_list;

    PriceListAdapter adapter_priceList;

    PriceListParser naverSearchPaser;

    ArrayList<PriceDataStruct> priceDataList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_main);

        if (android.os.Build.VERSION.SDK_INT > 9) {

            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

            StrictMode.setThreadPolicy(policy);
            
        }

        list_shopping_product_list = (ListView) findViewById(R.id.list_shopping_product_list);

        naverSearchPaser = new PriceListParser(clientKey, clientSecret);

        Intent intent = getIntent();
        Bundle myBundle = intent.getExtras();
       /*
        * productName 변수 앞에 final을 붙이는 이유
        * productName에 final을 붙이지 않는다면 onCreate 함수가 종료되는 시점에
        * productName값이 사라지게 되어서, mProductListview 리스너에서
        * productName값을 참조하지 못합니다. final을 붙이면 productName이 상수처럼 변해서
        * mProductListview 리스너가 동작될 때 productName값을 참조할 수 있습니다.
        */
        final String productName = myBundle.getString("PRODUCT_NAME");
       /*
        * 처음 화면에 쇼핑 리스트를 출력한다.
        */

        getShopList(productName);

        list_shopping_product_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                int Last = adapter_priceList.getCount();
                //adapter 전체 갯수 카운터

                if ((Last - 1) == arg2) {
                                      /*
                                       * 다음 페이지를 가져온다.
                                       * 다음 페이지를 가져오려면 start의 값에다 NUM_PRODUCT_PER_SCREEN 만큼 이동 시켜주어야 한다.
                                       */
                    nStartPosition += NUM_PRODUCT_PER_SCREEN;
                    getShopList(productName);
                } else {
                    String[] StringArrayData = StringArrayData(arg2);
                    Intent intent = new Intent(ShoppingMainActivity.this, PriceDetailActivity.class);
                    Bundle myData = new Bundle();
                    myData.putStringArray("key", StringArrayData);
                    intent.putExtras(myData);
                    startActivity(intent);
                }
            }
        });
    }

    @SuppressLint("DefaultLocale")
    public void getShopList(String searchTxt) {
        String debugStr = String.format("[ Get shopping list ] display=%d, start=%d", NUM_PRODUCT_PER_SCREEN, nStartPosition);

        Log.d("api.naver.search", debugStr);

        priceDataList = naverSearchPaser.getXmlData(searchTxt, NUM_PRODUCT_PER_SCREEN, nStartPosition);

        adapter_priceList = new PriceListAdapter(this, R.layout.list_shopping_price, priceDataList);
        list_shopping_product_list.setAdapter(adapter_priceList);
    }

    public String[] StringArrayData(int arg2) {
        PriceDataStruct priceData = priceDataList.get(arg2);

        String[] StringArrayData = new String[6];

        StringArrayData[0] = priceData.getTitle();
        StringArrayData[1] = priceData.getMallName();
        StringArrayData[2] = priceData.getLink();
        StringArrayData[3] = String.valueOf(priceData.getLprice());
        StringArrayData[4] = String.valueOf(priceData.getHprice());
        StringArrayData[5] = priceData.getImage();

        return StringArrayData;
    }


}
