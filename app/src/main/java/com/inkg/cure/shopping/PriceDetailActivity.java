package com.inkg.cure.shopping;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.widget.ImageView;
import android.widget.TextView;

import com.inkg.cure.R;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by inkg on 2017. 1. 30..
 */

public class PriceDetailActivity extends AppCompatActivity {

    TextView textView1;
    TextView textView2;
    TextView textView3;
    TextView textView4;
    TextView textView5;

    ImageView imageView;


    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub

        super.onCreate(savedInstanceState);

        setContentView(R.layout.shopping_price_detail);

        textView1 = (TextView) findViewById(R.id.TextView01);
        textView2 = (TextView) findViewById(R.id.TextView02);
        textView3 = (TextView) findViewById(R.id.TextView03);
        textView4 = (TextView) findViewById(R.id.TextView04);
        textView5 = (TextView) findViewById(R.id.TextView05);

        imageView = (ImageView) findViewById(R.id.ImageView01);


        Intent intent = getIntent();
        Bundle myBundle = intent.getExtras();
        String StringArrayData[] = myBundle.getStringArray("key");


        URL imageURL;
        try {
            imageURL = new URL(StringArrayData[5]);

            HttpURLConnection conn = (HttpURLConnection) imageURL.openConnection();
            BufferedInputStream bis = new BufferedInputStream(conn.getInputStream(), 10240);
            Bitmap bm = BitmapFactory.decodeStream(bis);
            bis.close();
            imageView.setImageBitmap(bm);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //imageView.setImageURI(Uri.parse(StringArrayData[5]));

        textView1.setText(StringArrayData[0]);
        textView2.setText("판매처:" + StringArrayData[1]);
        textView3.setText("최저가:" + StringArrayData[3] + "원");
        textView4.setText("최고가:" + StringArrayData[4] + "원");

        textView5.setText(
                Html.fromHtml("<a href='" + StringArrayData[2] + "'>"
                        + " 링크 바로가기" + "</a>"));
        textView5.setMovementMethod(LinkMovementMethod.getInstance());

    }
}
