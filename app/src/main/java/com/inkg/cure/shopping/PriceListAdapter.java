package com.inkg.cure.shopping;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.inkg.cure.R;

import java.util.ArrayList;

/**
 * Created by inkg on 2017. 1. 30..
 */

public class PriceListAdapter extends ArrayAdapter<Object> {

    private ArrayList<PriceDataStruct> items;

    View view;

    public PriceListAdapter(Context context, int textViewResourceId,
                            ArrayList items) {
        super(context, textViewResourceId, items);
        this.items = items;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        view = convertView;

        if (view == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
            view = vi.inflate(R.layout.list_shopping_price, null);
        }

        int Last = this.getCount();
        // adapter ��ü ���� ī����

        if ((Last - 1) == position) {
            // getNewList(Data);
            TextView tv1 = (TextView) view.findViewById(R.id.dataItem01);
            TextView tv2 = (TextView) view.findViewById(R.id.dataItem02);
            TextView tv3 = (TextView) view.findViewById(R.id.dataItem03);
            tv1.setText("");
            tv2.setText("");
            tv3.setText("");
        } else {

            PriceDataStruct xmlData = (PriceDataStruct) items.get(position);
            if (xmlData != null) {
                TextView tv1 = (TextView) view.findViewById(R.id.dataItem01);
                TextView tv2 = (TextView) view.findViewById(R.id.dataItem02);
                TextView tv3 = (TextView) view.findViewById(R.id.dataItem03);

                tv1.setText(xmlData.getTitle());
                tv2.setText("회사:" + xmlData.getMallName());
                tv3.setText("최저가:" + xmlData.getLprice() + "원");
            }
        }

        return view;
    }
}
