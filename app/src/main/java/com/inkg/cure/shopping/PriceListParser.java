package com.inkg.cure.shopping;

import android.util.Log;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by inkg on 2017. 1. 30..
 */

public class PriceListParser {

    String clientKey;
    String clientSecret;

    ArrayList<PriceDataStruct> priceDataList;

    public PriceListParser(String key) {
        this.clientKey = key;
    }

    public PriceListParser(String clientKey, String clientSecret) {
        this.clientKey = clientKey;
        this.clientSecret = clientSecret;
    }

    public ArrayList<PriceDataStruct> getXmlData(String searchTxt, int display, int start) {

        priceDataList = new ArrayList<PriceDataStruct>();

        PriceDataStruct priceData = null;

        String m_searchTxt = "";

        try {
            m_searchTxt = URLEncoder.encode(searchTxt, "UTF8");
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }
               /*
                * 쇼핑 리스트를 가져온 후에 파싱한다.
                */
        try {
            boolean startItemTag = false;

            String tagName = "";

            URL url_text = new URL(
                    "https://openapi.naver.com/v1/search/shop.xml?"
                            + "query=" + m_searchTxt
                            + "&display=" + display
                            + "&start=" + start
                            + "&target=shop&sort=asc");

            HttpURLConnection connection =
                    (HttpURLConnection) url_text.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Accept", "application/xml");
            connection.setRequestProperty("X-Naver-Client-Id", clientKey);
            connection.setRequestProperty("X-Naver-Client-Secret", clientSecret);


            XmlPullParserFactory parserCreator = XmlPullParserFactory
                    .newInstance();

            XmlPullParser parser = parserCreator.newPullParser();

            parser.setInput(connection.getInputStream(), null);

            int eventType = parser.getEventType();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                
                switch (eventType) {
                    case XmlPullParser.START_DOCUMENT: {
                        break;
                    }

                    case XmlPullParser.END_DOCUMENT: {
                        break;
                    }

                    case XmlPullParser.START_TAG: {
                        tagName = parser.getName();

                         /*
                          * 태그가 'item' 인 경우에만 처리한다.
                          */
                        if (tagName.equals("item") == true) {
                            startItemTag = true;

                            Log.d("api.naver.search", "MAKE item data structure");
                            priceData = new PriceDataStruct();
                        }

                        break;
                    }

                    case XmlPullParser.TEXT: {
                        if (startItemTag == false) {
                            break;
                        }

                        if (tagName.equals("total")) {
                            //Log.d("api.naver.search", "total - TEXT TAG");
                            priceData.setTotal(Integer.parseInt(parser.getText()));
                        } else if (tagName.equals("start")) {
                            //Log.d("api.naver.search", "start - TEXT TAG");
                            priceData.setStart(Integer.parseInt(parser.getText()));
                        } else if (tagName.equals("display")) {
                            //Log.d("api.naver.search", "display - TEXT TAG");
                            priceData.setDisplay(Integer.parseInt(parser.getText()));
                        } else if (tagName.equals("title")) {
                            //Log.d("api.naver.search", "title - TEXT TAG");
                            priceData.setTitle(parser.getText());
                        } else if (tagName.equals("link")) {
                            //Log.d("api.naver.search", "link - TEXT TAG");
                            priceData.setLink(parser.getText());
                        } else if (tagName.equals("image")) {
                            //Log.d("api.naver.search", "image - TEXT TAG");
                            priceData.setImage(parser.getText());
                        } else if (tagName.equals("lprice")) {
                            //Log.d("api.naver.search", "lprice - TEXT TAG");
                            priceData.setLprice(Integer.parseInt(parser.getText()));
                        } else if (tagName.equals("hprice")) {
                            //Log.d("api.naver.search", "hprice - TEXT TAG");
                            priceData.setHprice(Integer.parseInt(parser.getText()));
                        } else if (tagName.equals("mallName")) {
                            //Log.d("api.naver.search", "mallName - TEXT TAG");
                            priceData.setMallName(parser.getText());
                        } else if (tagName.equals("productId")) {
                            //Log.d("api.naver.search", parser.getText() + " - productId - TEXT TAG");
                            priceData.setProductId(Long.parseLong(parser.getText()));
                        }

                        break;
                    }

                    case XmlPullParser.END_TAG: {
                        tagName = parser.getName();

                        if (tagName.equals("item") == true) {
                            startItemTag = false;
                            Log.e("api.naver.search", "ADD item data structure");
                            priceDataList.add(priceData);
                        }
                        break;
                    }
                }
                eventType = parser.next();
            }

        } catch (Exception e) {
            Log.e("api.naver.search", "PriceListParser", e);
        }

        return priceDataList;
    }

}

