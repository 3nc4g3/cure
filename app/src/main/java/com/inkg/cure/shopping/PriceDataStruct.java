package com.inkg.cure.shopping;

/**
 * Created by inkg on 2017. 1. 30..
 */

public class PriceDataStruct {
    private int total;
    private int start;
    private int display;
    private String title;
    private String link;
    private String image;
    private int lprice;
    private int hprice;
    private String mallName;
    private long productId;

    /*
     * 검색 결과 문서의 총 개수를 의미합니다.
     */
    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    /*
     * 검색 결과 문서 중, 문서의 시작점을 의미합니다.
     */
    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }


    /*
     * 검색된 결과의 개수입니다.
     */
    public int getDisplay() {
        return display;
    }

    public void setDisplay(int display) {
        this.display = display;
    }


    /*
     * 검색 결과 문서의 제목을 나타냅니다. 제목에서 검색어와 일치하는 부분은 태그로 감싸져 있습니다.
     */
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    /*
     * 검색 결과 문서의 하이퍼텍스트 link를 나타냅니다.
     */
    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }


    /*
     * 썸네일 이미지의 URL입니다. 이미지가 있는 경우만 나타납니다.
     */
    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


    /*
     * 최저가 정보입니다. 최저가 정보가 없는 경우 0으로 표시되며,
     * 가격 비교 데이터가 없는 경우 이 필드는 가격을 나타냅니다.
     */
    public int getLprice() {
        return lprice;
    }

    public void setLprice(int lprice) {
        this.lprice = lprice;
    }


    /*
     * 최고가 정보입니다. 최고가 정보가 없거나 가격 비교 데이터가 없는 경우 0으로 표시됩니다.
     */
    public int getHprice() {
        return hprice;
    }

    public void setHprice(int hprice) {
        this.hprice = hprice;
    }


    /*
     * 상품을 판매하는 쇼핑몰의 상호입니다. 정보가 없을 경우 네이버로 표기 됩니다.
     */
    public String getMallName() {
        return mallName;
    }

    public void setMallName(String mallName) {
        this.mallName = mallName;
    }


    /*
     * 해당 상품에 대한 ID 입니다.
     */
    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }
}
