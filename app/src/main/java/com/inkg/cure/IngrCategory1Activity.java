package com.inkg.cure;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by inkg on 2016. 12. 18..
 */

public class IngrCategory1Activity extends AppCompatActivity {

    String component = new String();
    String title = new String();
    ImageView img_ingr1_icon;
    TextView txt_ingr1_info;
    TextView txt_ingr1_bottom_info_prsc;
    TextView txt_ingr1_bottom_info_illegal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        component = intent.getStringExtra("component");
        setContentView(R.layout.activity_ingr1);

        switch (component) {
            case "kids":
                title = getString(R.string.ingr_kids);
                break;
            case "old":
                title = getString(R.string.ingr_old);
                break;
            case "preg_1st":
                title = getString(R.string.rstr_title);
                break;
            case "preg_2nd":
                title = getString(R.string.warn_title);
                break;
            case "combination":
                title = getString(R.string.ingr_combination);
                break;
            case "period":
                title = getString(R.string.ingr_period);
                break;
            case "contains":
                title = getString(R.string.ingr_contains);
                break;
            case "medicine":
                title = getString(R.string.ingr_medicine);
                break;
        }

        setCustomActionbar(title);
        setupViews(component);
    }

    private void setCustomActionbar(String title) {

        ActionBar actionBar = getSupportActionBar();

        // Custom Actionbar를 사용하기 위해 CustomEnabled을 true 시키고 필요 없는 것은 false 시킨다

        actionBar.setDisplayShowCustomEnabled(true);

        actionBar.setDisplayHomeAsUpEnabled(false);

        actionBar.setDisplayShowTitleEnabled(false);

        // Set custom view layout

        View mCustomView = LayoutInflater.from(this).inflate(R.layout.actionbar_simple, null);

        actionBar.setCustomView(mCustomView);


        // Set no padding both side

        Toolbar parent = (Toolbar) mCustomView.getParent(); // first get parent toolbar of current action bar

        parent.setContentInsetsAbsolute(0, 0);              // set padding programmatically to 0dp

        // Set actionbar background image

        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.border_layout_bottom));

        // Set actionbar layout layoutparams

        ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);

        actionBar.setCustomView(mCustomView, params);

        ImageView img_actionbar_back = (ImageView) findViewById(R.id.img_actionbar_back);
        img_actionbar_back.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    finish();
                }
                return false;
            }
        });

        TextView txt_actionbar = (TextView) findViewById(R.id.txt_actionbar_simple);
        txt_actionbar.setText(title);
    }

    private void setupViews(String comp) {

        img_ingr1_icon = (ImageView) findViewById(R.id.img_ingr1_icon);
        txt_ingr1_info = (TextView) findViewById(R.id.txt_ingr1_info);
        txt_ingr1_bottom_info_prsc = (TextView) findViewById(R.id.txt_ingr1_bottom_info_prsc);
        txt_ingr1_bottom_info_illegal = (TextView) findViewById(R.id.txt_ingr1_bottom_info_illegal);

        txt_ingr1_bottom_info_prsc.setText(R.string.bottom_info_prsc);
        txt_ingr1_bottom_info_illegal.setText(R.string.bottom_info_illegal);

        switch (comp) {
            case "kids":
                img_ingr1_icon.setImageResource(R.drawable.product_icon_ingredient_baby);
                txt_ingr1_info.setText(R.string.kids_info);
                break;
            case "old":
                img_ingr1_icon.setImageResource(R.drawable.product_icon_ingredient_old);
                txt_ingr1_info.setText(R.string.old_info);
                break;
            case "preg_1st":
                txt_ingr1_bottom_info_prsc.setText("");
                txt_ingr1_bottom_info_illegal.setText("");
                break;
            case "preg_2nd":
                txt_ingr1_bottom_info_prsc.setText("");
                txt_ingr1_bottom_info_illegal.setText("");
                break;
            case "combination":
                img_ingr1_icon.setImageResource(R.drawable.donot);
                txt_ingr1_info.setText(R.string.combi_info);
                break;
            case "period":
                img_ingr1_icon.setImageResource(R.drawable.period);
                txt_ingr1_info.setText(R.string.period_info);
                txt_ingr1_bottom_info_prsc.setText(getString(R.string.ingr_bottom_info));
                break;
            case "contains":
                img_ingr1_icon.setImageResource(R.drawable.contain);
                txt_ingr1_info.setText(R.string.contains_info);
                break;
            case "medicine":
                img_ingr1_icon.setImageResource(R.drawable.difference);
                txt_ingr1_info.setText(R.string.medicine_info);
                txt_ingr1_bottom_info_prsc.setText("");
                txt_ingr1_bottom_info_illegal.setText("");
                break;
        }

    }
}