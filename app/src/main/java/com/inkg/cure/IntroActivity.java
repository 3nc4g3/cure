package com.inkg.cure;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.inkg.cure.shopping.ShoppingMainActivity;

import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;

/**
 * Created by inkg on 2016. 11. 18..
 */

public class IntroActivity extends Activity {

    GifDrawable gifDrawable;
    int gif_count;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        GifImageView intro_gif = (GifImageView) findViewById(R.id.img_intro);
        gifDrawable = (GifDrawable) intro_gif.getDrawable();
        gifDrawable.setSpeed(0.5f);
        gifDrawable.setLoopCount(1);
        gif_count = gifDrawable.getNumberOfFrames();
        gifDrawable.start();
        mHandler.postDelayed(mMyTask, 4000);
    }
    
    private Handler mHandler = new Handler();
    private Runnable mMyTask = new Runnable() {
        @Override
        public void run() {
            Intent main_intent = new Intent(IntroActivity.this, MainActivity.class);
//            main_intent.putExtra("PRODUCT_NAME", "홍삼");
            startActivity(main_intent);
            finish();
        }
    };
}