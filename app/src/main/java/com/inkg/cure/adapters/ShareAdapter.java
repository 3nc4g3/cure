package com.inkg.cure.adapters;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.inkg.cure.R;
import com.inkg.cure.classes.Pharmacy;

import java.util.ArrayList;

/**
 * Created by inkg on 2017. 3. 5..
 */

public class ShareAdapter extends ArrayAdapter<Pharmacy> {

    private Context mContext;
    private ArrayList<Pharmacy> items;

    public ShareAdapter(@NonNull Context context, @LayoutRes int resource, ArrayList<Pharmacy> items) {
        super(context, resource);
        this.mContext = context;
        this.items = items;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.dialog_share, null);
        }

        TextView txt_share_title = (TextView) view.findViewById(R.id.txt_share_title);
        ImageView img_share_kakaotalk = (ImageView) view.findViewById(R.id.img_share_kakaotalk);
        ImageView img_share_kakaostory = (ImageView) view.findViewById(R.id.img_share_kakaostory);
        ImageView img_share_facebook = (ImageView) view.findViewById(R.id.img_share_facebook);

        return view;
    }
}