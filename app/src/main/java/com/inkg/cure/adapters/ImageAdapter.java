package com.inkg.cure.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.inkg.cure.R;
import com.inkg.cure.classes.GlobalApplication;

import java.io.BufferedInputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by inkg on 2017. 3. 4..
 */

public class ImageAdapter extends ArrayAdapter<byte[]> {

    private Context mContext;
    private ArrayList<byte[]> items;
    private ImageView img_image;
    private Bitmap bm;

    public ImageAdapter(@NonNull Context context, @LayoutRes int resource) {
        super(context, resource);
    }

    public ImageAdapter(@NonNull Context context, @LayoutRes int resource, ArrayList<byte[]> items) {
        super(context, resource, items);
        this.mContext = context;
        this.items = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = vi.inflate(R.layout.adapter_image, null);
        }

        img_image = (ImageView) view.findViewById(R.id.img_image);
        byte[] bytes = items.get(position);
        bm = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
        img_image.setImageBitmap(bm);

//        IconAsync ia = new IconAsync(position);
//        ia.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        return view;
    }

    class IconAsync extends AsyncTask<String, String, String> {

        int position;

        IconAsync() {

        }

        IconAsync(int position) {
            this.position = position;
        }

        @Override
        protected String doInBackground(String... strings) {
            final URL[] imageURL = new URL[1];

            try {
                if ("null".equals(items.get(position)) || "None".equals(items.get(position))) {

                } else {
//                    imageURL[0] = new URL(items.get(position));

                    HttpURLConnection conn = (HttpURLConnection) imageURL[0].openConnection();
                    BufferedInputStream bis = new BufferedInputStream(conn.getInputStream(), 1024);
                    bm = BitmapFactory.decodeStream(bis);
                    bis.close();

                }
            } catch (Exception e) {
                Log.e("search result adap", "image url excep", e);
            }
            return "ok";
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            GlobalApplication.getCurrentActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if ("null".equals(items.get(position)) || "None".equals(items.get(position))) {
                        img_image.setImageResource(R.drawable.no_image);
                    } else {
                        img_image.setImageBitmap(bm);
                    }
                }
            });
        }
    }

}
