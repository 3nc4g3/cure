package com.inkg.cure.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.inkg.cure.R;
import com.inkg.cure.classes.CureSP;
import com.inkg.cure.classes.Follow;
import com.inkg.cure.classes.HttpConnect;
import com.inkg.cure.classes.JsonMaker;

import java.io.BufferedInputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by inkg on 2017. 3. 3..
 */
public class FollowingAdapter extends ArrayAdapter<Follow> {

    private Context mContext;
    private ArrayList<Follow> items;
    private ImageView img_my_following_profile;
    private TextView txt_my_following_nickname;
    private RelativeLayout layout_my_following;
    private Button btn_my_following;
    private HttpConnect httpConnect;
    private JsonMaker jsonMaker;
    private CureSP csp;

    public FollowingAdapter(Context context, int resource) {
        super(context, resource);
    }

    public FollowingAdapter(Context context, int resource, ArrayList<Follow> items) {
        super(context, resource, items);
        this.mContext = context;
        this.items = items;
        httpConnect = new HttpConnect(mContext.getApplicationContext());
        jsonMaker = new JsonMaker();
        csp = new CureSP(mContext.getApplicationContext());
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.adapter_my_following, null);
        }
        img_my_following_profile = (ImageView) view.findViewById(R.id.img_my_following_profile);
        IconAsync ia = new IconAsync(items.get(position).getProfile());
        ia.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        txt_my_following_nickname = (TextView) view.findViewById(R.id.txt_my_following_nickname);
        txt_my_following_nickname.setText(items.get(position).getNickname());
        layout_my_following = (RelativeLayout) view.findViewById(R.id.layout_my_following);
        btn_my_following = (Button) view.findViewById(R.id.btn_my_following);
        btn_my_following.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // following
                layout_my_following.setBackground(mContext.getResources().getDrawable(R.drawable.border_gray_rounded, null));
                btn_my_following.setBackground(mContext.getResources().getDrawable(R.drawable.follow, null));

                FollowAsync fa = new FollowAsync();
                fa.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, items.get(position).getTarget());
            }
        });

        return view;
    }

    class FollowAsync extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            String json = jsonMaker.makeJson(new Pair("id", strings[0]));
            result = httpConnect.send(json, httpConnect.getServerWithVersion() + mContext.getString(R.string.rest_follow), csp.getValue("access_token", ""), "DELETE");
            return result;
        }
    }

    class IconAsync extends AsyncTask<String, String, String> {

        private String image;
        private Bitmap bm;

        IconAsync() {

        }

        IconAsync(String image) {
            this.image = image;
        }

        @Override
        protected String doInBackground(String... strings) {
            final URL[] imageURL = new URL[1];

            try {
                if ("null".equals(image) || "None".equals(image)) {

                } else {
                    imageURL[0] = new URL(image);

                    HttpURLConnection conn = (HttpURLConnection) imageURL[0].openConnection();
                    BufferedInputStream bis = new BufferedInputStream(conn.getInputStream(), 1024);
                    bm = BitmapFactory.decodeStream(bis);
                    bis.close();

                }
            } catch (Exception e) {
                Log.e("search result adap", "image url excep", e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if ("null".equals(image) || "None".equals(image)) {
                img_my_following_profile.setImageResource(R.drawable.profile);
            } else {
                img_my_following_profile.setImageBitmap(bm);
            }
            img_my_following_profile.invalidate();
        }
    }
}