package com.inkg.cure.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.inkg.cure.classes.GlobalApplication;
import com.inkg.cure.search.HealthfoodDetailActivity;
import com.inkg.cure.search.MedicineDetailActivity;
import com.inkg.cure.R;
import com.inkg.cure.classes.RankData;

import java.io.BufferedInputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by inkg on 2017. 2. 13..
 */

public class SearchResultAdapter extends ArrayAdapter<RankData> {
    private Context mContext;
    private ArrayList<RankData> items;
    private String type;
    private Bitmap bm;
    private SearchResultHolder srh;
    private View view;

    public SearchResultAdapter(Context context, int resource) {
        super(context, resource);
    }

    public SearchResultAdapter(Context context, int textViewResourceId, ArrayList<RankData> items) {
        super(context, textViewResourceId, items);
        this.mContext = context;
        this.items = items;
    }

    public SearchResultAdapter(Context context, int textViewResourceId, ArrayList<RankData> items, String type) {
        super(context, textViewResourceId, items);
        this.mContext = context;
        this.items = items;
        this.type = type;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        view = convertView;
        if (view == null) {
            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = vi.inflate(R.layout.adapter_category_search, null);
            srh = new SearchResultHolder();
            srh.layout_category_search = (RelativeLayout) view.findViewById(R.id.layout_category_search);
            srh.img_search_result_item_icon = (ImageView) view.findViewById(R.id.img_search_result_item_icon);
            srh.txt_search_result_item_company = (TextView) view.findViewById(R.id.txt_search_result_item_company);
            srh.txt_search_result_item_name = (TextView) view.findViewById(R.id.txt_search_result_item_name);
            srh.txt_search_result_item_good = (TextView) view.findViewById(R.id.txt_search_result_item_good);
            srh.txt_search_result_item_bad = (TextView) view.findViewById(R.id.txt_search_result_item_bad);
            srh.txt_search_result_item_review = (TextView) view.findViewById(R.id.txt_search_result_item_review);
//            srh.btn_search_result_item = (Button) view.findViewById(R.id.btn_search_result_item);
            view.setTag(srh);
        } else {
            srh = (SearchResultHolder) view.getTag();
        }
        String result = "";
        IconAsync ia = new IconAsync(position);
        try {
            result = ia.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR).get();
        } catch (Exception e) {
            Log.e("se re ad", "get result excep", e);
        }

        srh.txt_search_result_item_company.setText(items.get(position).getCompany());
        srh.txt_search_result_item_name.setText(items.get(position).getName());
        if ("bookmark".equals(type)) {
        } else {
            srh.txt_search_result_item_good.setText(items.get(position).getScores().getLikes() + "개");
            srh.txt_search_result_item_bad.setText(items.get(position).getScores().getUnlikes() + "개");
        }
        if ("expert".equals(type)) {
            srh.txt_search_result_item_review.setVisibility(View.GONE);
        } else {
            srh.txt_search_result_item_review.setText("리뷰 " + items.get(position).getScores().getReviews() + "개");
        }
//        srh.btn_search_result_item.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
//
//                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
//                    srh.layout_category_search.setBackgroundColor(ContextCompat.getColor(mContext.getApplicationContext(), R.color.bright_cerulean));
//                }
//
//                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
//                    srh.layout_category_search.setBackground(mContext.getResources().getDrawable(R.drawable.border_layout_bottom, null));
//
//                    Intent detail_intent;
//                    if ("medicine".equals(type)) {
//                        detail_intent = new Intent(getContext(), MedicineDetailActivity.class);
//                    } else {
//                        detail_intent = new Intent(getContext(), HealthfoodDetailActivity.class);
//                    }
//                    detail_intent.putExtra("id", items.get(position).getId());
//                    detail_intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    view.getContext().startActivity(detail_intent);
//                }
//                return false;
//            }
//        });
        return view;
    }

    static class SearchResultHolder {
        RelativeLayout layout_category_search;
        ImageView img_search_result_item_icon;
        TextView txt_search_result_item_company;
        TextView txt_search_result_item_name;
        TextView txt_search_result_item_good;
        TextView txt_search_result_item_bad;
        TextView txt_search_result_item_review;
//        Button btn_search_result_item;
    }

    class IconAsync extends AsyncTask<String, String, String> {

        int position;

        IconAsync() {

        }

        IconAsync(int position) {
            this.position = position;
        }

        @Override
        protected String doInBackground(String... strings) {
            final URL[] imageURL = new URL[1];

            try {
                if ("null".equals(items.get(position).getSelect_image()) || "None".equals(items.get(position).getSelect_image())) {

                } else {
                    imageURL[0] = new URL(items.get(position).getSelect_image());

                    HttpURLConnection conn = (HttpURLConnection) imageURL[0].openConnection();
                    BufferedInputStream bis = new BufferedInputStream(conn.getInputStream(), 1024);
                    bm = BitmapFactory.decodeStream(bis);
                    bis.close();

                }

//                Log.e("what image?", items.get(position).getSelect_image());
                if ("null".equals(items.get(position).getSelect_image()) || "None".equals(items.get(position).getSelect_image())) {
                    srh.img_search_result_item_icon.setImageResource(R.drawable.no_image);
                } else {
                    srh.img_search_result_item_icon.setImageBitmap(bm);
                }
            } catch (Exception e) {
                Log.e("search result adap", "image url excep", e);
            }

            return "ok";
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
        }
    }
}
