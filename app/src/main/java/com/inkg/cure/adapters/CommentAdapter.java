package com.inkg.cure.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.inkg.cure.R;
import com.inkg.cure.classes.Comment;

import java.util.ArrayList;

/**
 * Created by inkg on 2017. 3. 1..
 */

public class CommentAdapter extends ArrayAdapter<Comment> {

    private ArrayList<Comment> items;
    private Context mContext;
    private CommentHolder ch;
    private View view;

    public CommentAdapter(Context context, int resource) {
        super(context, resource);
    }

    public CommentAdapter(Context context, int resource, ArrayList<Comment> items) {
        super(context, resource, items);
        this.mContext = context;
        this.items = items;
        this.ch = new CommentHolder();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        view = convertView;
        if (view == null) {
            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = vi.inflate(R.layout.adapter_review_comment, null);

            ch.img_review_comment_profile = (ImageView) view.findViewById(R.id.img_review_comment_profile);
            ch.txt_review_comment_content = (TextView) view.findViewById(R.id.txt_review_comment_content);
            ch.txt_review_comment_time = (TextView) view.findViewById(R.id.txt_review_comment_time);
            view.setTag(ch);
        } else {
            ch = (CommentHolder) view.getTag();
        }

        ch.txt_review_comment_content.setText(items.get(position).getNickname() + " " + items.get(position).getContent());
        ch.txt_review_comment_time.setText(items.get(position).getTime());

        return view;
    }

    static class CommentHolder {
        ImageView img_review_comment_profile;
        TextView txt_review_comment_content;
        TextView txt_review_comment_time;
    }
}
