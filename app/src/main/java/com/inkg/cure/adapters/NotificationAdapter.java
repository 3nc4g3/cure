package com.inkg.cure.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.inkg.cure.R;
import com.inkg.cure.classes.Notification;
import com.inkg.cure.mypage.NotificationActivity;

import java.util.ArrayList;

/**
 * Created by inkg on 2017. 3. 4..
 */

public class NotificationAdapter extends ArrayAdapter<Notification> {

    private Context mContext;
    private ArrayList<Notification> noti_list;
//    private ArrayList<String> child_list;
//    private HashMap<String, String> child_map;

    public NotificationAdapter(Context context, int resource) {
        super(context, resource);
    }

    public NotificationAdapter(Context context, int resource, ArrayList<Notification> noti_list) {
        super(context, resource, noti_list);
        this.mContext = context;
        this.noti_list = noti_list;
//        this.child_map = child_map;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.adapter_notification, null);
        }

        TextView txt_noti_parent_title = (TextView) view.findViewById(R.id.txt_noti_parent_title);
        txt_noti_parent_title.setText(noti_list.get(position).getTitle());
        TextView txt_noti_parent_date = (TextView) view.findViewById(R.id.txt_noti_parent_date);
        txt_noti_parent_date.setText(noti_list.get(position).getDate());
        String url = noti_list.get(position).getUrl();
        Button btn_noti_parent = (Button) view.findViewById(R.id.btn_noti_parent);
        btn_noti_parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent noti_intent = new Intent(mContext, NotificationActivity.class);
                noti_intent.putExtra("url", noti_list.get(position).getUrl());
                noti_intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(noti_intent);
            }
        });

        return view;
    }


//    @Override
//    public int getGroupCount() {
//        return noti_list.size();
//    }
//
//    @Override
//    public int getChildrenCount(int group_position) {
//        return child_map.get(this.noti_list.get(group_position)).length();
//    }
//
//    @Override
//    public Notification getGroup(int group_position) {
//        return noti_list.get(group_position);
//    }
//
//    @Override
//    public String getChild(int group_position, int child_position) {
//        return child_map.get(this.noti_list.get(group_position).getIdx());
//    }
//
//    @Override
//    public long getGroupId(int group_position) {
//        return group_position;
//    }
//
//    @Override
//    public long getChildId(int group_position, int child_position) {
//        return child_position;
//    }
//
//    @Override
//    public boolean hasStableIds() {
//        return false;
//    }
//
//    @Override
//    public View getGroupView(int group_position, boolean isExpanded, View convertView, ViewGroup viewGroup) {
//        if (convertView == null) {
//            LayoutInflater groupInfla = (LayoutInflater) this.mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            // ParentList의 layout 연결. root로 argument 중 parent를 받으며 root로 고정하지는 않음
//            convertView = groupInfla.inflate(R.layout.adapter_notification, viewGroup, false);
//        }
//
//        TextView txt_noti_parent_title = (TextView) convertView.findViewById(R.id.txt_noti_parent_title);
//        txt_noti_parent_title.setText(getGroup(group_position).getTitle());
//        TextView txt_noti_parent_date = (TextView) convertView.findViewById(R.id.txt_noti_parent_date);
//        txt_noti_parent_date.setText(getGroup(group_position).getDate());
//
//        return convertView;
//    }
//
//    @Override
//    public View getChildView(int group_position, int child_position, boolean is_last_child, View convertView, ViewGroup viewGroup) {
//        String content = getChild(group_position, child_position);
//        if (convertView == null) {
//            LayoutInflater childInfla = (LayoutInflater) this.mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            convertView = childInfla.inflate(R.layout.activity_notification, null);
//        }
//
//        TextView txt_noti_child_content
//        return convertView;
//    }
//
//    @Override
//    public boolean isChildSelectable(int i, int i1) {
//        return false;
//    }
}
