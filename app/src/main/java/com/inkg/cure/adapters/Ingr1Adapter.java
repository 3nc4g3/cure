package com.inkg.cure.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.inkg.cure.R;
import com.inkg.cure.classes.CureSP;
import com.inkg.cure.classes.HttpConnect;
import com.inkg.cure.classes.Ingredient;
import com.inkg.cure.classes.JsonMaker;

import java.util.ArrayList;

/**
 * Created by inkg on 2017. 2. 27..
 */

public class Ingr1Adapter extends ArrayAdapter<Ingredient> {

    private Context mContext;
    private ArrayList<Ingredient> items;
    private boolean is_like = false;

    private HttpConnect httpConnect;
    private JsonMaker jsonMaker;
    private CureSP csp;

    public Ingr1Adapter(Context context, int resource) {
        super(context, resource);
    }

    public Ingr1Adapter(Context context, int resource, ArrayList<Ingredient> items) {
        super(context, resource, items);
        this.mContext = context;
        this.items = items;
        httpConnect = new HttpConnect(mContext.getApplicationContext());
        jsonMaker = new JsonMaker();
        csp = new CureSP(mContext.getApplicationContext());
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = vi.inflate(R.layout.adapter_ingr1_item, null);
        }

        ImageView img_ingr1_item = (ImageView) view.findViewById(R.id.img_ingr1_item);
        TextView txt_ingr1_item_kor = (TextView) view.findViewById(R.id.txt_ingr1_item_kor);
        TextView txt_ingr1_item_eng = (TextView) view.findViewById(R.id.txt_ingr1_item_eng);
        ArrayList<Pair<String, String>> pair_list = new ArrayList<>();
        pair_list.add(0, new Pair<String, String>(items.get(position).getFda_comment(), items.get(position).getFda_grade()));
        final DialogAdapter dialog_adapter = new DialogAdapter(mContext, R.layout.dialog_content, pair_list);
        final LayoutInflater title_inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

//        Button btn_ingr1_item_select = (Button) view.findViewById(R.id.img_ingr1_item_select);
//        btn_ingr1_item_select.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                AlertDialog.Builder ab = new AlertDialog.Builder(view.getRootView().getContext());
//                View title_view = title_inflater.inflate(R.layout.dialog_title, null);
//                TextView txt_dialog_kor = (TextView) title_view.findViewById(R.id.txt_dialog_kor);
//                TextView txt_dialog_eng = (TextView) title_view.findViewById(R.id.txt_dialog_eng);
//                ab.setCustomTitle(title_view);
//                txt_dialog_kor.setText(items.get(position).getKor_name());
//                txt_dialog_eng.setText(items.get(position).getEng_name());
//                final ImageView img_dialog_good = (ImageView) title_view.findViewById(R.id.img_dialog_good);
//                img_dialog_good.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        if (is_like) {
//                            is_like = false;
//                            img_dialog_good.setBackground(mContext.getResources().getDrawable(R.drawable.product_icon_heart_nor, null));
//
//                            BookmarkAsync ba = new BookmarkAsync(items.get(position).getName(), false);
//                            ba.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//                        } else {
//                            is_like = true;
//                            img_dialog_good.setBackground(mContext.getResources().getDrawable(R.drawable.product_icon_heart_active, null));
//
//                            BookmarkAsync ba = new BookmarkAsync(items.get(position).getName(), true);
//                            ba.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//                        }
//                    }
//                });
//                ab.setAdapter(dialog_adapter, new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
//
//                    }
//                }).create();
//                final AlertDialog ad = ab.create();
//                Button btn_dialog_title = (Button) title_view.findViewById(R.id.btn_dialog_title);
//                btn_dialog_title.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        ad.dismiss();
//                    }
//                });
//                ad.show();
//            }
//        });

        if ("functional".equals(items.get(position).getType())) {
            img_ingr1_item.setImageDrawable(view.getResources().getDrawable(R.drawable.pills, null));
        } else {
            if ("".equals(items.get(position).getFda_comment())) {
                img_ingr1_item.setImageDrawable(view.getResources().getDrawable(R.drawable.medicine1, null));
                txt_ingr1_item_kor.setTextColor(ContextCompat.getColor(mContext.getApplicationContext(), R.color.maya_blue1));
            } else {
                img_ingr1_item.setImageDrawable(view.getResources().getDrawable(R.drawable.medicine3, null));
                txt_ingr1_item_kor.setTextColor(ContextCompat.getColor(mContext.getApplicationContext(), R.color.pastel_red));
            }
        }
        txt_ingr1_item_kor.setText(items.get(position).getKor_name());
        txt_ingr1_item_eng.setText(items.get(position).getEng_name());

        return view;
    }

    class BookmarkAsync extends AsyncTask<String, String, String> {

        private boolean is_like = false;
        private String name;

        BookmarkAsync(String name, boolean is_like) {
            this.name = name;
            this.is_like = is_like;
        }

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            String json = jsonMaker.makeJson(new Pair("name", name));
            if (is_like) {
                result = httpConnect.send(json, httpConnect.getServerWithVersion() + mContext.getString(R.string.rest_like_ingredient), csp.getValue("access_token", ""));
            } else {
                result = httpConnect.send(json, httpConnect.getServerWithVersion() + mContext.getString(R.string.rest_like_ingredient), csp.getValue("access_token", ""), "DELETE");
            }

            return result;
        }
    }

}
