package com.inkg.cure.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.inkg.cure.R;

import java.util.ArrayList;

/**
 * Created by inkg on 2017. 2. 28..
 */

public class PictoAdapter extends ArrayAdapter<String> {

    private ArrayList<String> data;
    private Context mContext;

    public PictoAdapter(Context context, int resource) {
        super(context, resource);
    }

    public PictoAdapter(Context context, int resource, ArrayList<String> data) {
        super(context, resource, data);
        mContext = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.adapter_picto_image, null);
        }

        ImageView img_picto = (ImageView) view.findViewById(R.id.img_picto);
        int pictoId = mContext.getResources().getIdentifier(data.get(position).toLowerCase(), "drawable", mContext.getPackageName());
        img_picto.setImageDrawable(ContextCompat.getDrawable(mContext.getApplicationContext(), pictoId));
        final float scale = mContext.getResources().getDisplayMetrics().density;
        int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (float) 252, mContext.getResources().getDisplayMetrics());
        int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (float) 336, mContext.getResources().getDisplayMetrics());
        img_picto.setLayoutParams(new RelativeLayout.LayoutParams(width, height));

        return view;
    }
}
