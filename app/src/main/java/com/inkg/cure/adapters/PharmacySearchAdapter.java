package com.inkg.cure.adapters;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.inkg.cure.R;
import com.inkg.cure.classes.Addr;

import java.util.ArrayList;

/**
 * Created by inkg on 2017. 3. 5..
 */

public class PharmacySearchAdapter extends ArrayAdapter<Addr> {

    private Context mContext;
    private ArrayList<Addr> items;

    public PharmacySearchAdapter(@NonNull Context context, @LayoutRes int resource) {
        super(context, resource);
    }

    public PharmacySearchAdapter(@NonNull Context context, @LayoutRes int resource, ArrayList<Addr> items) {
        super(context, resource, items);
        this.mContext = context;
        this.items = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.adapter_pharmacy_search_result, null);
        }

        TextView txt_pharmacy_item_address_road = (TextView) view.findViewById(R.id.txt_pharmacy_item_address_road);
        TextView txt_pharmacy_item_address_building = (TextView) view.findViewById(R.id.txt_pharmacy_item_address_building);

        txt_pharmacy_item_address_road.setText(items.get(position).getTitle());
        txt_pharmacy_item_address_building.setText(items.get(position).getBuildingAddress());

        return view;
    }
}
