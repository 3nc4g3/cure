package com.inkg.cure.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.inkg.cure.R;
import com.inkg.cure.classes.Review;
import com.inkg.cure.review.ReviewDetailActivity;

import java.io.BufferedInputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by inkg on 2017. 3. 1..
 */

public class ReviewAdapter extends ArrayAdapter<Review> {

    private ArrayList<Review> items;
    private View view;
    private ReviewHolder rh;
    private Context mContext;
    private Bitmap bm;

    public ReviewAdapter(Context context, int resource) {
        super(context, resource);
    }

    public ReviewAdapter(Context context, int resource, ArrayList<Review> items) {
        super(context, resource, items);
        this.mContext = context;
        this.items = items;
        rh = new ReviewHolder();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        view = convertView;
        if (view == null) {
            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = vi.inflate(R.layout.adapter_review_read, null);
            rh.img_review_read_profile = (ImageView) view.findViewById(R.id.img_review_read_profile);
            rh.txt_review_read_nickname = (TextView) view.findViewById(R.id.txt_review_read_nickname);
            rh.btn_review_read_age = (Button) view.findViewById(R.id.btn_review_read_age);
            rh.txt_review_read_timestamp = (TextView) view.findViewById(R.id.txt_review_read_timestamp);
            rh.txt_review_read_good = (TextView) view.findViewById(R.id.txt_review_read_good);
            rh.txt_review_read_bad = (TextView) view.findViewById(R.id.txt_review_read_bad);
            rh.txt_review_read_tip = (TextView) view.findViewById(R.id.txt_review_read_tip);
            rh.btn_review_read_more = (Button) view.findViewById(R.id.btn_review_read_more);
            rh.txt_review_read_likes = (TextView) view.findViewById(R.id.txt_review_read_likes);
            rh.txt_review_read_reply = (TextView) view.findViewById(R.id.txt_review_read_reply);
            rh.txt_review_read_view = (TextView) view.findViewById(R.id.txt_review_read_view);
            view.setTag(rh);
        } else {
            rh = (ReviewHolder) view.getTag();
        }
        IconAsync ia = new IconAsync(position);
        ia.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

//        rh.img_review_read_profile
        rh.txt_review_read_nickname.setText(items.get(position).getNickname());
        rh.btn_review_read_age.setText(items.get(position).getAge());
        rh.txt_review_read_timestamp.setText(items.get(position).getDate());
        rh.txt_review_read_good.setText(items.get(position).getGood_point());
        rh.txt_review_read_bad.setText(items.get(position).getBad_point());
        rh.txt_review_read_tip.setText(items.get(position).getTip());
        rh.btn_review_read_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent detail_intent = new Intent(mContext, ReviewDetailActivity.class);
                detail_intent.putExtra("id", items.get(position).getId());
                detail_intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(detail_intent);
            }
        });
        rh.txt_review_read_likes.setText(items.get(position).getLikes());
        rh.txt_review_read_reply.setText(items.get(position).getComments());
        rh.txt_review_read_view.setText(items.get(position).getViews());


        return view;
    }

    class IconAsync extends AsyncTask<String, String, String> {

        int position;

        IconAsync() {

        }

        IconAsync(int position) {
            this.position = position;
        }

        @Override
        protected String doInBackground(String... strings) {
            final URL[] imageURL = new URL[1];

            try {
                if ("null".equals(items.get(position).getProfile()) || "None".equals(items.get(position).getProfile())) {

                } else {
                    imageURL[0] = new URL(items.get(position).getProfile());
                    HttpURLConnection conn = (HttpURLConnection) imageURL[0].openConnection();
                    BufferedInputStream bis = new BufferedInputStream(conn.getInputStream(), 1024);
                    bm = BitmapFactory.decodeStream(bis);
                    bis.close();

                }
            } catch (Exception e) {
                Log.e("review adap", "image url excep", e);
            }
            return null;
        }
        
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if ("null".equals(items.get(position).getProfile()) || "None".equals(items.get(position).getProfile())) {
                rh.img_review_read_profile.setImageResource(R.drawable.product_no_pic);
            } else {
                rh.img_review_read_profile.setImageBitmap(bm);
            }
            view.invalidate();
        }
    }

    static class ReviewHolder {
        ImageView img_review_read_profile;
        TextView txt_review_read_nickname;
        Button btn_review_read_age;
        TextView txt_review_read_timestamp;
        TextView txt_review_read_good;
        TextView txt_review_read_bad;
        TextView txt_review_read_tip;
        Button btn_review_read_more;
        TextView txt_review_read_likes;
        TextView txt_review_read_reply;
        TextView txt_review_read_view;
    }
}
