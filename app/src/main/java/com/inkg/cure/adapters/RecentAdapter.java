package com.inkg.cure.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.inkg.cure.R;
import com.inkg.cure.search.SearchActivity;
import com.inkg.cure.search.SearchResultActivity;

import java.util.ArrayList;

/**
 * Created by inkg on 2017. 3. 5..
 */

public class RecentAdapter extends ArrayAdapter<String> {
    private Context mContext;
    private ArrayList<String> items;

    public RecentAdapter(@NonNull Context context, @LayoutRes int resource) {
        super(context, resource);
    }

    public RecentAdapter(@NonNull Context context, @LayoutRes int resource, ArrayList<String> items) {
        super(context, resource, items);
        this.mContext = context;
        this.items = items;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.adapter_search_recent, null);
        }

        TextView txt_search_recent_query = (TextView) view.findViewById(R.id.txt_search_recent_query);
        txt_search_recent_query.setText(items.get(position));

        return view;
    }
}
