package com.inkg.cure.adapters;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.inkg.cure.R;
import com.inkg.cure.classes.Faq;

import java.util.ArrayList;

/**
 * Created by inkg on 2017. 3. 6..
 */

public class FaqAdapter extends ArrayAdapter<Faq> {

    private Context mContext;
    private ArrayList<Faq> items;

    public FaqAdapter(@NonNull Context context, @LayoutRes int resource) {
        super(context, resource);
    }

    public FaqAdapter(@NonNull Context context, @LayoutRes int resource, ArrayList<Faq> items) {
        super(context, resource, items);
        this.mContext = context;
        this.items = items;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.adapter_faq, null);
        }

        TextView txt_faq_title = (TextView) view.findViewById(R.id.txt_faq_title);
        ImageView img_faq_title = (ImageView) view.findViewById(R.id.img_faq_title);
        RelativeLayout layout_faq_more = (RelativeLayout) view.findViewById(R.id.layout_faq_more);
        TextView txt_faq_content = (TextView) view.findViewById(R.id.txt_faq_content);

        return view;
    }
}
