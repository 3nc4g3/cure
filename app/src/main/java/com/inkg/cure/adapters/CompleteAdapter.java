package com.inkg.cure.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.inkg.cure.R;

import java.util.ArrayList;

/**
 * Created by inkg on 2017. 3. 12..
 */

public class CompleteAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<String> items;

    public CompleteAdapter(Context context, ArrayList<String> items) {
        this.mContext = context;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View convertView = view;
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate((R.layout.adapter_search_complete), null);
        }

        TextView txt_search_complete = (TextView) convertView.findViewById(R.id.txt_search_complete);
        txt_search_complete.setText(items.get(i));

        return convertView;
    }
}
