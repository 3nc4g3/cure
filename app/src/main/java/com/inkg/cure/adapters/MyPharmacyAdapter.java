package com.inkg.cure.adapters;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.inkg.cure.R;
import com.inkg.cure.classes.Pharmacy;

import java.util.ArrayList;

/**
 * Created by inkg on 2017. 3. 4..
 */

public class MyPharmacyAdapter extends ArrayAdapter<Pharmacy> {

    private Context mContext;
    private ArrayList<Pharmacy> items;

    public MyPharmacyAdapter(@NonNull Context context, @LayoutRes int resource) {
        super(context, resource);
    }

    public MyPharmacyAdapter(@NonNull Context context, @LayoutRes int resource, ArrayList<Pharmacy> items) {
        super(context, resource, items);
        this.mContext = context;
        this.items = items;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.adapter_my_pharmacy, null);
        }

        TextView txt_my_pharmacy_name = (TextView) view.findViewById(R.id.txt_my_pharmacy_name);
        TextView txt_my_pharmacy_addr_road = (TextView) view.findViewById(R.id.txt_my_pharmacy_addr_road);
        TextView txt_my_pharmacy_addr = (TextView) view.findViewById(R.id.txt_my_pharmacy_addr);

        txt_my_pharmacy_name.setText(items.get(position).getDutyname());
        String[] addr = items.get(position).getDutyaddr().split("\\(");
        txt_my_pharmacy_addr_road.setText(addr[0]);
        if (addr.length > 1) {
            txt_my_pharmacy_addr.setText("(" + addr[1]);
        }
        return view;
    }
}
