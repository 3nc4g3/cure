package com.inkg.cure.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.inkg.cure.R;

import java.util.ArrayList;

/**
 * Created by inkg on 2016. 12. 12..
 */

public class SearchItemAdapter extends ArrayAdapter<Object> {

    private Context mContext;
    private ArrayList<Object> items;
    
    public SearchItemAdapter(Context context, int resource) {
        super(context, resource);
    }

    public SearchItemAdapter(Context context, int textViewResourceId, ArrayList<Object> items) {
        super(context, textViewResourceId, items);
        this.mContext = context;
        this.items = items;
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = vi.inflate(R.layout.adapter_search_item, null);
        }

        ImageView img_search_item = (ImageView) view.findViewById(R.id.img_search_item);
        TextView txt_search_manufacturer = (TextView) view.findViewById(R.id.txt_search_manufacturer);
        TextView txt_search_product = (TextView) view.findViewById(R.id.txt_search_product);
        TextView txt_search_good = (TextView) view.findViewById(R.id.txt_search_good);
        TextView txt_search_bad = (TextView) view.findViewById(R.id.txt_search_bad);
        TextView txt_search_review = (TextView) view.findViewById(R.id.txt_search_review);
        Button btn_search_select = (Button) view.findViewById(R.id.btn_search_select);

        return view;
    }
}
