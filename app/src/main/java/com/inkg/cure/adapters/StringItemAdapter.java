package com.inkg.cure.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.inkg.cure.R;

import java.util.ArrayList;

/**
 * Created by inkg on 2016. 12. 12..
 */

public class StringItemAdapter extends ArrayAdapter<String> {

    private ArrayList<String> items;
    private Context mContext;
    private TextView txt_string_item;
    private Button btn_string_item_delete;

    public StringItemAdapter(Context context, int resource) {
        super(context, resource);
    }

    public StringItemAdapter(Context context, int textViewResourceId, ArrayList<String> items) {
        super(context, textViewResourceId, items);
        this.mContext = context;
        this.items = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = vi.inflate(R.layout.adapter_string_item, null);
        }

        txt_string_item = (TextView) view.findViewById(R.id.txt_string_item);
        btn_string_item_delete = (Button) view.findViewById(R.id.btn_string_item_delete);

        return view;
    }
}
