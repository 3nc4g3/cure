package com.inkg.cure.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.inkg.cure.R;

import java.util.ArrayList;

/**
 * Created by inkg on 2016. 11. 25..
 */

public class JoinAdapter extends ArrayAdapter<String> {

    private ArrayList<String> data;
    private Context mContext;

    public JoinAdapter(Context context, int resource) {
        super(context, resource);
    }


    public JoinAdapter(Context context, int resource, ArrayList<String> data) {
        super(context, resource, data);
        mContext = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.adapter_join, null);
        }

        TextView txt_join_item = (TextView) view.findViewById(R.id.txt_join_item);
        txt_join_item.setText(data.get(position));

        return view;
    }

}
