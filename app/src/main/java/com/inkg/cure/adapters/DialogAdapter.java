package com.inkg.cure.adapters;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.inkg.cure.R;

import java.util.ArrayList;

/**
 * Created by inkg on 2017. 3. 4..
 */

public class DialogAdapter extends ArrayAdapter<Pair<String, String>> {

    private ArrayList<Pair<String, String>> m_pair;
    private Context mContext;

    public DialogAdapter(@NonNull Context context, @LayoutRes int resource) {
        super(context, resource);
    }

    public DialogAdapter(@NonNull Context context, @LayoutRes int resource, ArrayList<Pair<String, String>> pair) {
        super(context, resource, pair);
        this.mContext = context;
        this.m_pair = pair;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.dialog_content, null);
        }
        TextView txt_dialog_effect = (TextView) view.findViewById(R.id.txt_dialog_effect);
        TextView txt_dialog_warning = (TextView) view.findViewById(R.id.txt_dialog_warning);
        
        txt_dialog_effect.setText(m_pair.get(position).first);
        if ("".equals(m_pair.get(position).second)) {
            txt_dialog_warning.setText("주의사항 없음");
        } else {
            txt_dialog_warning.setText(m_pair.get(position).second);
        }

        return view;
    }
}
