package com.inkg.cure.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.inkg.cure.R;
import com.inkg.cure.classes.Review;

import java.io.BufferedInputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by inkg on 2017. 3. 8..
 */

public class ProductReviewAdapter extends ArrayAdapter<Review> {

    private Context mContext;
    private ArrayList<Review> items;
    private ProductReviewHolder prh;
    private Bitmap bm;
    private View view;

    public ProductReviewAdapter(@NonNull Context context, @LayoutRes int resource) {
        super(context, resource);
    }

    public ProductReviewAdapter(@NonNull Context context, @LayoutRes int resource, ArrayList<Review> items) {
        super(context, resource, items);
        this.mContext = context;
        this.items = items;
        this.prh = new ProductReviewHolder();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.adapter_product_review, null);
            prh.img_product_review_profile = (ImageView) view.findViewById(R.id.img_product_review_profile);
            prh.txt_product_review_id = (TextView) view.findViewById(R.id.txt_product_review_id);
            prh.txt_product_review_age = (TextView) view.findViewById(R.id.txt_product_review_age);
            prh.txt_product_review_date = (TextView) view.findViewById(R.id.txt_product_review_date);
            prh.txt_product_review_good = (TextView) view.findViewById(R.id.txt_product_review_good);
            prh.txt_product_review_bad = (TextView) view.findViewById(R.id.txt_product_review_bad);
            view.setTag(prh);
        } else {
            prh = (ProductReviewHolder) view.getTag();
        }

        IconAsync ia = new IconAsync(position);
        ia.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        prh.txt_product_review_id.setText(items.get(position).getNickname());
        prh.txt_product_review_age.setText(items.get(position).getAge());
        prh.txt_product_review_date.setText(items.get(position).getDate());
        prh.txt_product_review_good.setText(items.get(position).getGood_point());
        prh.txt_product_review_bad.setText(items.get(position).getBad_point());

        return view;
    }

    class IconAsync extends AsyncTask<String, String, String> {

        int position;

        IconAsync() {

        }

        IconAsync(int position) {
            this.position = position;
        }

        @Override
        protected String doInBackground(String... strings) {
            final URL[] imageURL = new URL[1];

            try {
                if ("null".equals(items.get(position).getProfile()) || "None".equals(items.get(position).getProfile())) {

                } else {
                    imageURL[0] = new URL(items.get(position).getProfile());
                    HttpURLConnection conn = (HttpURLConnection) imageURL[0].openConnection();
                    BufferedInputStream bis = new BufferedInputStream(conn.getInputStream(), 1024);
                    bm = BitmapFactory.decodeStream(bis);
                    bis.close();

                }
            } catch (Exception e) {
                Log.e("review adap", "image url excep", e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if ("null".equals(items.get(position).getProfile()) || "None".equals(items.get(position).getProfile())) {
                prh.img_product_review_profile.setImageResource(R.drawable.product_no_pic);
            } else {
                prh.img_product_review_profile.setImageBitmap(bm);
            }
            view.invalidate();
        }
    }

    static class ProductReviewHolder {
        ImageView img_product_review_profile;
        TextView txt_product_review_id;
        TextView txt_product_review_age;
        TextView txt_product_review_date;
        TextView txt_product_review_good;
        TextView txt_product_review_bad;
    }
}
