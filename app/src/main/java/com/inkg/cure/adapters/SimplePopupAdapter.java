package com.inkg.cure.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.inkg.cure.R;

import java.util.ArrayList;

/**
 * Created by inkg on 2017. 2. 8..
 */

public class SimplePopupAdapter extends ArrayAdapter<String> {

    private Context mContext;
    private ArrayList<String> items;

    public SimplePopupAdapter(Context context, int resource) {
        super(context, resource);
    }

    public SimplePopupAdapter(Context context, int textViewResourceId, ArrayList<String> items) {
        super(context, textViewResourceId, items);
        this.mContext = context;
        this.items = items;
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = vi.inflate(R.layout.adapter_string_item, null);
        }

        return view;
    }
}
