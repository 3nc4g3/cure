package com.inkg.cure.adapters;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.inkg.cure.R;
import com.inkg.cure.classes.Ingredient;

import java.util.ArrayList;

/**
 * Created by inkg on 2017. 3. 6..
 */

public class IngredientAdapter extends ArrayAdapter<Ingredient> {

    private Context mContext;
    private ArrayList<Ingredient> items;

    public IngredientAdapter(@NonNull Context context, @LayoutRes int resource) {
        super(context, resource);
    }

    public IngredientAdapter(@NonNull Context context, @LayoutRes int resource, ArrayList<Ingredient> items) {
        super(context, resource, items);
        this.mContext = context;
        this.items = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.adapter_ingredient, null);
        }
        
        TextView txt_ingredient_item_kor = (TextView) view.findViewById(R.id.txt_ingredient_item_kor);
        TextView txt_ingredient_item_eng = (TextView) view.findViewById(R.id.txt_ingredient_item_eng);
        String name = items.get(position).getName();
        String kor_name = name.substring(0, name.indexOf("("));
        String eng_name = name.substring(name.indexOf("(") + 1, name.indexOf(")"));
        txt_ingredient_item_kor.setText(kor_name);
        txt_ingredient_item_eng.setText(eng_name);

        return view;
    }
}
