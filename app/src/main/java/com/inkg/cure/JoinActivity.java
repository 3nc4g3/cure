package com.inkg.cure;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.inkg.cure.adapters.JoinAdapter;
import com.inkg.cure.classes.CureSP;
import com.inkg.cure.classes.HttpConnect;
import com.inkg.cure.classes.JsonMaker;
import com.inkg.cure.classes.ResultCodes;
import com.inkg.cure.classes.Utils;
import com.inkg.cure.settings.EULAActivity;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.ExecutionException;

/**
 * A login screen that offers login via email/password.
 */

//The password is at least 8 characters
//The nickname already exists
//The email address already exists
//The password is at least 8 characters
//Invalid argument fields
//Invalid argument

public class JoinActivity extends AppCompatActivity {

    private Button btn_join_path;
    private Button btn_join_age;
    private Button btn_join_duplicate;
    private Button btn_join_start;
    private TextView txt_join_path;
    private TextView txt_join_age;
    private TextView txt_join_privacy;
    private TextView txt_join_terms;
    private TextView txt_join_allow;
    private RelativeLayout layout_join_eula;

    private RadioGroup rg_join_gender;
    private RadioButton rb_join_male;
    private RadioButton rb_join_female;

    private ToggleButton toggle_btn_join_agree;

    private EditText edit_join_email;
    private EditText edit_join_passwd;
    private EditText edit_join_passwd_confirm;
    private EditText edit_join_nickname;

    private String pathtojoin;
    private String email;
    private String password;
    private String password_confirm;
    private String nickname;
    private String gender;
    private String age;

    private HttpConnect httpConnect;
    private JsonMaker jsonMaker;
    private CureSP csp;

    private int[] join_checker;
    private Utils utils;
    private boolean is_done = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join);
        httpConnect = new HttpConnect(getApplicationContext());
        jsonMaker = new JsonMaker();
        csp = new CureSP(getApplicationContext());
        utils = new Utils();
        join_checker = new int[8];
        for (int i = 0; i < 8; i++) {
            join_checker[i] = 0;
        }

        setupViews();
    }

    private void setupViews() {

        btn_join_path = (Button) findViewById(R.id.btn_join_path);
        btn_join_age = (Button) findViewById(R.id.btn_join_age);
        btn_join_start = (Button) findViewById(R.id.btn_join_start);

        txt_join_path = (TextView) findViewById(R.id.txt_join_path);
        txt_join_age = (TextView) findViewById(R.id.txt_join_age);

        txt_join_privacy = (TextView) findViewById(R.id.txt_join_privacy);
        txt_join_terms = (TextView) findViewById(R.id.txt_join_terms);

        edit_join_email = (EditText) findViewById(R.id.edit_join_email);
        edit_join_passwd = (EditText) findViewById(R.id.edit_join_passwd);
        edit_join_passwd_confirm = (EditText) findViewById(R.id.edit_join_passwd_confirm);
        edit_join_nickname = (EditText) findViewById(R.id.edit_join_nickname);

        rg_join_gender = (RadioGroup) findViewById(R.id.rg_join_gender);
        rb_join_male = (RadioButton) findViewById(R.id.rb_join_male);
        rb_join_female = (RadioButton) findViewById(R.id.rb_join_female);

        btn_join_duplicate = (Button) findViewById(R.id.btn_join_duplicate);
        txt_join_allow = (TextView) findViewById(R.id.txt_join_allow);

        toggle_btn_join_agree = (ToggleButton) findViewById(R.id.toggle_btn_join_agree);
        toggle_btn_join_agree.setChecked(false);
        layout_join_eula = (RelativeLayout) findViewById(R.id.layout_join_eula);

        txt_join_privacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent privacy_intent = new Intent(JoinActivity.this, EULAActivity.class);
                privacy_intent.putExtra("fragment", "privacy");
                privacy_intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(privacy_intent);
            }
        });

        txt_join_terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent terms_intent = new Intent(JoinActivity.this, EULAActivity.class);
                terms_intent.putExtra("fragment", "terms");
                terms_intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(terms_intent);
            }
        });

        btn_join_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (is_done) {

                    password = edit_join_passwd.getText().toString();
                    password_confirm = edit_join_passwd_confirm.getText().toString();

                    if (!password.equals(password_confirm)) {
                        Toast.makeText(getApplicationContext(), "패스워드와 패스워드 확인란이 일치하지 않습니다", Toast.LENGTH_SHORT).show();
                    } else {
                        nickname = edit_join_nickname.getText().toString();
                        email = edit_join_email.getText().toString();

                        StartAsync sa = new StartAsync();
                        sa.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                }
            }
        });

        setValuechecker();
        setCustomActionbar();
    }

    private void setValuechecker() {
        TextWatcher emailTextWatcher = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (utils.validateEmail(charSequence.toString())) {
                    join_checker[1] = 1;
                } else {
                    join_checker[1] = 0;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                finish_checker();
            }
        };

        edit_join_email.addTextChangedListener(emailTextWatcher);

        TextWatcher pwTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (utils.validatePassword(charSequence.toString())) {
                    join_checker[2] = 1;
                } else {
                    join_checker[2] = 0;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                finish_checker();
            }
        };

        edit_join_passwd.addTextChangedListener(pwTextWatcher);

        TextWatcher pw_confirmWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (utils.validatePassword(charSequence.toString())) {
                    join_checker[3] = 1;
                } else {
                    join_checker[3] = 0;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                finish_checker();
            }
        };

        edit_join_passwd_confirm.addTextChangedListener(pw_confirmWatcher);

        TextWatcher nicknameWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                join_checker[4] = 0;
                txt_join_allow.setVisibility(View.GONE);
                btn_join_duplicate.setBackgroundColor(ContextCompat.getColor(JoinActivity.this, R.color.silver));
            }

            @Override
            public void afterTextChanged(Editable editable) {
                join_checker[4] = 0;
                finish_checker();
            }
        };

        edit_join_nickname.addTextChangedListener(nicknameWatcher);

        final ArrayList<String> pathes = new ArrayList<>();
        pathes.add("카카오톡");
        pathes.add("카카오 스토리");
        pathes.add("페이스북");
        pathes.add("인스타그램");
        pathes.add("네이버 밴드");
        pathes.add("다음(Daum) 앱");
        pathes.add("유튜브");

        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        final ArrayList<String> ages = new ArrayList<>();
        for (int i = currentYear; i > 1939; i--) {
            ages.add(String.valueOf(i));
        }

        final JoinAdapter path_adapter = new JoinAdapter(getApplicationContext(), R.layout.adapter_join, pathes);
        final JoinAdapter age_adapter = new JoinAdapter(getApplicationContext(), R.layout.adapter_join, ages);

        final LayoutInflater title_inflater = (LayoutInflater) getApplicationContext().getSystemService(LAYOUT_INFLATER_SERVICE);
        btn_join_path.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder ab = new AlertDialog.Builder(JoinActivity.this);
                View title_view = title_inflater.inflate(R.layout.dialog_custom_title, null);
                TextView txt_dialog_title = (TextView) title_view.findViewById(R.id.txt_dialog_custom_title);
                ab.setCustomTitle(title_view);
                txt_dialog_title.setText("가입경로");
                ab.setAdapter(path_adapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        txt_join_path.setText(pathes.get(i));
                        pathtojoin = pathes.get(i);
                        dialogInterface.dismiss();
                        join_checker[0] = 1;
                        finish_checker();
                    }
                }).create();
                ab.show();
            }
        });

        btn_join_age.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder ab = new AlertDialog.Builder(JoinActivity.this);
                View title_view = title_inflater.inflate(R.layout.dialog_custom_title, null);
                TextView txt_dialog_title = (TextView) title_view.findViewById(R.id.txt_dialog_custom_title);
                ab.setCustomTitle(title_view);
                txt_dialog_title.setText("출생년도");
                ab.setAdapter(age_adapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        txt_join_age.setText(ages.get(i));
                        age = ages.get(i);
                        dialogInterface.dismiss();
                        join_checker[6] = 1;
                        finish_checker();
                    }
                }).create();
                ab.show();
            }
        });

        rb_join_female.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                join_checker[5] = 1;
                gender = "female";
                finish_checker();
            }
        });

        rb_join_male.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                join_checker[5] = 1;
                gender = "male";
                finish_checker();
            }
        });

        if (rg_join_gender.getCheckedRadioButtonId() != -1) {
            join_checker[5] = 1;
            if (rg_join_gender.getCheckedRadioButtonId() == R.id.rb_join_male) {
                gender = "male";
            } else {
                gender = "female";
            }
        }

        toggle_btn_join_agree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (toggle_btn_join_agree.isChecked()) { // agree behavior
                    layout_join_eula.setBackgroundColor(ContextCompat.getColor(JoinActivity.this, R.color.maya_blue1));
                    toggle_btn_join_agree.setChecked(true);
                    join_checker[7] = 1;
                    Log.e("disagree", "fuck");
                    finish_checker();
                } else { // disagree behavior
                    layout_join_eula.setBackgroundColor(ContextCompat.getColor(JoinActivity.this, R.color.silver));
                    toggle_btn_join_agree.setChecked(false);
                    join_checker[7] = 0;
                    Log.e("agree", "fuck");
                    finish_checker();
                }
            }
        });

        btn_join_duplicate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nickname = "";
                nickname = edit_join_nickname.getText().toString();
                txt_join_allow.setVisibility(View.VISIBLE);
                if ("".equals(nickname)) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            txt_join_allow.setText("닉네임을 입력해주세요");
                            txt_join_allow.setTextColor(ContextCompat.getColor(JoinActivity.this, R.color.pastel_red));
                            btn_join_duplicate.setBackgroundColor(ContextCompat.getColor(JoinActivity.this, R.color.silver));
                            finish_checker();
                        }
                    });
                } else {
                    String result = "";
                    DuplicateCheckAsync dca = new DuplicateCheckAsync(nickname);
                    try {
                        result = dca.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR).get();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }

                    Log.e("result dup", result);

                    if ("The nickname already exists".equals(result)) {
                        txt_join_allow.setText("중복된 닉네임입니다");
                        txt_join_allow.setTextColor(ContextCompat.getColor(JoinActivity.this, R.color.pastel_red));
                        btn_join_duplicate.setBackgroundColor(ContextCompat.getColor(JoinActivity.this, R.color.silver));
                        finish_checker();
                    } else {
                        txt_join_allow.setText("사용 가능합니다");
                        txt_join_allow.setTextColor(ContextCompat.getColor(JoinActivity.this, R.color.maya_blue1));
                        btn_join_duplicate.setBackgroundColor(ContextCompat.getColor(JoinActivity.this, R.color.maya_blue1));
                        join_checker[4] = 1;
                        finish_checker();
                    }
                }
            }
        });

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

    }

    private void finish_checker() {
        boolean is_ok = true;
        for (int i = 0; i < join_checker.length; i++) {
            if (join_checker[i] == 0) {
                is_ok = false;
            }
        }

//        Log.e("finish check", join_checker[0] + " / " + join_checker[1] + " / " + join_checker[2] + " / " + join_checker[3] + " / " + join_checker[4] + " / " + join_checker[5] + " / " + join_checker[6] + " / " + join_checker[7]);

        if (is_ok) {
            is_done = true;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    btn_join_start.setBackgroundColor(ContextCompat.getColor(JoinActivity.this, R.color.maya_blue1));
                }
            });
        } else {
            is_done = false;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    btn_join_start.setBackgroundColor(ContextCompat.getColor(JoinActivity.this, R.color.silver));
                }
            });
        }
    }

    class DuplicateCheckAsync extends AsyncTask<String, String, String> {

        String nickname = "";

        public DuplicateCheckAsync(String nickname) {
            this.nickname = nickname;
        }

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            result = httpConnect.send("", getString(R.string.server_url) + getString(R.string.cureya_version) + getString(R.string.rest_member_duplicate) + "?type=nickname&data=" + this.nickname, "", "GET");
            return result;
        }
    }

    class StartAsync extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(String... strings) {
            String json_join_data = jsonMaker.makeJson(new Pair("nickname", nickname), new Pair("email", email), new Pair("gender", gender), new Pair("age", age), new Pair("password", password), new Pair("pathtojoin", pathtojoin));
            String join_result = "";
            join_result = httpConnect.send(json_join_data, getString(R.string.server_url) + getString(R.string.rest_email_join), "");
            Log.e("join_result", join_result);
            if ("400".equals(join_result)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "중복된 이메일입니다. 다른 이메일을 사용해주세요.", Toast.LENGTH_SHORT).show();
                    }
                });
                return "no";
            } else {
                return join_result;
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            JoinDataAsync jda = new JoinDataAsync(result);
            jda.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    class JoinDataAsync extends AsyncTask<String, String, String> {

        String result = "";

        public JoinDataAsync(String result) {
            this.result = result;
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                JSONObject result_json = new JSONObject(result);
                String access_token = result_json.getString("access_token");
                String expire = result_json.getString("expire");
                String refresh_exp = result_json.getString("refresh_exp");
                String refresh_token = result_json.getString("refresh_token");

                csp.put("access_token", access_token);
                csp.put("expire", expire);
                csp.put("refresh_exp", refresh_exp);
                csp.put("refresh_token", refresh_token);
            } catch (Exception e) {
                Log.e("jo ac", "get json excep", e);
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            setResult(ResultCodes.AN_JOIN);
            finish();
        }
    }

    private void setCustomActionbar() {

        ActionBar actionBar = getSupportActionBar();

        // Custom Actionbar를 사용하기 위해 CustomEnabled을 true 시키고 필요 없는 것은 false 시킨다

        actionBar.setDisplayShowCustomEnabled(true);

        actionBar.setDisplayHomeAsUpEnabled(false);

        actionBar.setDisplayShowTitleEnabled(false);

        // Set custom view layout

        View mCustomView = LayoutInflater.from(this).inflate(R.layout.actionbar_simple, null);

        actionBar.setCustomView(mCustomView);

        // Set no padding both side

        Toolbar parent = (Toolbar) mCustomView.getParent(); // first get parent toolbar of current action bar

        parent.setContentInsetsAbsolute(0, 0);              // set padding programmatically to 0dp

        // Set actionbar background image

        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.border_layout_bottom));

        // Set actionbar layout layoutparams

        ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);

        actionBar.setCustomView(mCustomView, params);

        ImageView img_actionbar_back = (ImageView) findViewById(R.id.img_actionbar_back);
        img_actionbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        TextView txt_actionbar = (TextView) findViewById(R.id.txt_actionbar_simple);
        txt_actionbar.setText(getString(R.string.join_another));
    }
}

