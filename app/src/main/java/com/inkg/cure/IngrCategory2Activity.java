package com.inkg.cure;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by inkg on 2016. 12. 18..
 */

public class IngrCategory2Activity extends AppCompatActivity {

    String component = new String();
    String title = new String();
    ImageView img_ingr2_icon;
    TextView txt_ingr2_info;
    TextView txt_ingr2_bottom_info_prsc;
    TextView txt_ingr2_bottom_info_illegal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        component = intent.getStringExtra("component");
        setContentView(R.layout.activity_ingr2);

        switch (component) {
            case "dur":
                title = getString(R.string.ingr_dur);
                break;
            case "ensemble":
                title = getString(R.string.ingr_ensemble);
                break;
        }

        setCustomActionbar(title);
        setupViews(component);
    }

    private void setCustomActionbar(String title) {

        ActionBar actionBar = getSupportActionBar();

        // Custom Actionbar를 사용하기 위해 CustomEnabled을 true 시키고 필요 없는 것은 false 시킨다

        actionBar.setDisplayShowCustomEnabled(true);

        actionBar.setDisplayHomeAsUpEnabled(false);

        actionBar.setDisplayShowTitleEnabled(false);

        // Set custom view layout

        View mCustomView = LayoutInflater.from(this).inflate(R.layout.actionbar_simple, null);

        actionBar.setCustomView(mCustomView);


        // Set no padding both side

        Toolbar parent = (Toolbar) mCustomView.getParent(); // first get parent toolbar of current action bar

        parent.setContentInsetsAbsolute(0, 0);              // set padding programmatically to 0dp

        // Set actionbar background image

        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.border_layout_bottom));

        // Set actionbar layout layoutparams

        ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);

        actionBar.setCustomView(mCustomView, params);

        ImageView img_actionbar_back = (ImageView) findViewById(R.id.img_actionbar_back);
        img_actionbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        TextView txt_actionbar = (TextView) findViewById(R.id.txt_actionbar_simple);
        txt_actionbar.setText(title);

    }

    private void setupViews(String comp) {

        img_ingr2_icon = (ImageView) findViewById(R.id.img_ingr2_icon);
        txt_ingr2_info = (TextView) findViewById(R.id.txt_ingr2_info);
        txt_ingr2_bottom_info_prsc = (TextView) findViewById(R.id.txt_ingr2_bottom_info_prsc);
        txt_ingr2_bottom_info_illegal = (TextView) findViewById(R.id.txt_ingr2_bottom_info_illegal);

        txt_ingr2_bottom_info_prsc.setText(R.string.bottom_info_prsc);
        txt_ingr2_bottom_info_illegal.setText(R.string.bottom_info_illegal);

        switch (comp) {
            case "dur":
                img_ingr2_icon.setImageResource(R.drawable.dur);
                txt_ingr2_info.setText(R.string.dur_info);
                txt_ingr2_bottom_info_prsc.setText("");
                txt_ingr2_bottom_info_illegal.setText("");
                break;
            case "ensemble":
                img_ingr2_icon.setImageResource(R.drawable.combi);
                txt_ingr2_info.setText(R.string.ensemble_info);
                txt_ingr2_bottom_info_prsc.setText(R.string.ingr_bottom_info);
                break;
        }

    }
}
