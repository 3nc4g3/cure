package com.inkg.cure.settings;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.inkg.cure.R;
import com.inkg.cure.adapters.MainAdapter;
import com.inkg.cure.classes.NonSwipeViewPager;
import com.inkg.cure.fragments.PrivacyFragment;
import com.inkg.cure.fragments.TermsFragment;

import java.util.List;
import java.util.Vector;

/**
 * Created by inkg on 2017. 2. 4..
 */

public class EULAActivity extends AppCompatActivity {

    private Button btn_eula_privacy;
    private Button btn_eula_service;
    private NonSwipeViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eula);
        Intent intent = getIntent();
        String start = "";
        start = intent.getStringExtra("fragment");
        if (start != null) {
            if ("privacy".equals(start)) {
                changeFragment(new PrivacyFragment());
            } else {
                changeFragment(new TermsFragment());
            }
        }

        setupViews();

        initialisePaging();
        setCustomActionbar();
    }

    private void setupViews() {

        btn_eula_privacy = (Button) findViewById(R.id.btn_eula_privacy);
        btn_eula_service = (Button) findViewById(R.id.btn_eula_service);

        btn_eula_privacy.setTextColor(getResources().getColor(R.color.maya_blue1, null));
        btn_eula_privacy.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, R.drawable.main_menubar_bottom_small);

    }

    private void changeFragment(Fragment targetFragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.vp_eula, targetFragment, "fragment")
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    private void initialisePaging() {

        List<Fragment> fragments = new Vector<Fragment>();
        fragments.add(Fragment.instantiate(this, PrivacyFragment.class.getName()));
        fragments.add(Fragment.instantiate(this, TermsFragment.class.getName()));
        MainAdapter mainAdapter = new MainAdapter(super.getSupportFragmentManager(), fragments);

        viewPager = (NonSwipeViewPager) super.findViewById(R.id.vp_eula);
        viewPager.setEnabled(false);
        viewPager.setAdapter(mainAdapter);

        viewPager.addOnPageChangeListener(new NonSwipeViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onPageSelected(int position) {
                btn_eula_privacy.setTextColor(getResources().getColor(R.color.dark_jungle_green, null));
                btn_eula_service.setTextColor(getResources().getColor(R.color.dark_jungle_green, null));
                btn_eula_privacy.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                btn_eula_service.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

                switch (position) {
                    case 0:
                        btn_eula_privacy.setTextColor(getResources().getColor(R.color.maya_blue1, null));
                        btn_eula_privacy.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, R.drawable.main_menubar_bottom_small);
                        break;
                    case 1:
                        btn_eula_service.setTextColor(getResources().getColor(R.color.maya_blue1, null));
                        btn_eula_service.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, R.drawable.main_menubar_bottom_small);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        btn_eula_privacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewPager.setCurrentItem(0);
            }
        });

        btn_eula_service.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewPager.setCurrentItem(1);
            }
        });

    }

    private void setCustomActionbar() {

        ActionBar actionBar = getSupportActionBar();

        // Custom Actionbar를 사용하기 위해 CustomEnabled을 true 시키고 필요 없는 것은 false 시킨다

        actionBar.setDisplayShowCustomEnabled(true);

        actionBar.setDisplayHomeAsUpEnabled(false);

        actionBar.setDisplayShowTitleEnabled(false);

        // Set custom view layout

        View mCustomView = LayoutInflater.from(this).inflate(R.layout.actionbar_simple, null);

        actionBar.setCustomView(mCustomView);


        // Set no padding both side

        Toolbar parent = (Toolbar) mCustomView.getParent(); // first get parent toolbar of current action bar

        parent.setContentInsetsAbsolute(0, 0);              // set padding programmatically to 0dp

        // Set actionbar background image

        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.border_layout_bottom));

        // Set actionbar layout layoutparams

        ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);

        actionBar.setCustomView(mCustomView, params);

        ImageView img_actionbar_back = (ImageView) findViewById(R.id.img_actionbar_back);
        img_actionbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        TextView txt_actionbar = (TextView) findViewById(R.id.txt_actionbar_simple);
        txt_actionbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        txt_actionbar.setText(getString(R.string.title_eula));

    }
}