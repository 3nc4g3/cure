package com.inkg.cure.settings;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.inkg.cure.R;
import com.inkg.cure.classes.CureSP;
import com.inkg.cure.classes.HttpConnect;
import com.inkg.cure.classes.JsonMaker;

/**
 * Created by inkg on 2017. 2. 22..
 */

public class AskActivity extends AppCompatActivity {

    private LinearLayout layout_ask_category;
    private RelativeLayout layout_ask_content;
    private EditText edit_ask_content;

    private TextView txt_ask_category;
    private Button btn_ask_error;
    private Button btn_ask_comment;
    private Button btn_ask_uncomfortable;
    private Button btn_ask_alliance;
    private Button btn_ask_other;
    private Button btn_ask_send;
    private String type;
    private String question;

    private boolean is_done = false;

    private JsonMaker jsonMaker;
    private HttpConnect httpConnect;
    private CureSP cureSP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ask);
        jsonMaker = new JsonMaker();
        httpConnect = new HttpConnect(getApplicationContext());
        cureSP = new CureSP(getApplicationContext());

        setUpViews();
        setCustomActionbar();
    }

    private void setUpViews() {
        layout_ask_category = (LinearLayout) findViewById(R.id.layout_ask_category);
        layout_ask_content = (RelativeLayout) findViewById(R.id.layout_ask_content);
        edit_ask_content = (EditText) findViewById(R.id.edit_ask_content);

        txt_ask_category = (TextView) findViewById(R.id.txt_ask_category);
        btn_ask_error = (Button) findViewById(R.id.btn_ask_error);
        btn_ask_comment = (Button) findViewById(R.id.btn_ask_comment);
        btn_ask_uncomfortable = (Button) findViewById(R.id.btn_ask_uncomfortable);
        btn_ask_alliance = (Button) findViewById(R.id.btn_ask_alliance);
        btn_ask_other = (Button) findViewById(R.id.btn_ask_other);
        btn_ask_send = (Button) findViewById(R.id.btn_ask_send);

        btn_ask_error.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layout_ask_category.setVisibility(View.INVISIBLE);
                layout_ask_content.setVisibility(View.VISIBLE);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        txt_ask_category.setText("1. 오류가 있어요");
                    }
                });

                type = "error";
            }
        });

        btn_ask_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layout_ask_category.setVisibility(View.INVISIBLE);
                layout_ask_content.setVisibility(View.VISIBLE);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        txt_ask_category.setText("2. 큐어에게 한마디");
                    }
                });

                type = "comment";
            }
        });

        btn_ask_uncomfortable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layout_ask_category.setVisibility(View.INVISIBLE);
                layout_ask_content.setVisibility(View.VISIBLE);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        txt_ask_category.setText("3. 이용하기 불편해요");
                    }
                });

                type = "uncomfortable";
            }
        });

        btn_ask_alliance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layout_ask_category.setVisibility(View.INVISIBLE);
                layout_ask_content.setVisibility(View.VISIBLE);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        txt_ask_category.setText("4. 제휴 문의");
                    }
                });

                type = "alliance";
            }
        });

        btn_ask_other.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layout_ask_category.setVisibility(View.INVISIBLE);
                layout_ask_content.setVisibility(View.VISIBLE);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        txt_ask_category.setText("5. 기타");
                    }
                });

                type = "other";
            }
        });

        TextWatcher ask_watcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }
            
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    is_done = true;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            btn_ask_other.setBackgroundColor(ContextCompat.getColor(AskActivity.this, R.color.maya_blue1));
                        }
                    });
                } else {
                    is_done = false;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            btn_ask_other.setBackgroundColor(ContextCompat.getColor(AskActivity.this, R.color.silver));
                        }
                    });
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };

        edit_ask_content.addTextChangedListener(ask_watcher);

        btn_ask_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (is_done) {
                    question = edit_ask_content.getText().toString();
                    AskAsync aa = new AskAsync();
                    aa.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
            }
        });

    }

    class AskAsync extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            String json = "";
            String result = "";
            json = jsonMaker.makeJson(new Pair("type", type), new Pair("question", question), new Pair("email", cureSP.getValue("email", "")));
            result = httpConnect.send(json, httpConnect.getServerWithVersion() + getString(R.string.rest_qna), cureSP.getValue("access_token", ""));
            Log.e("ask result", result);
            return null;
        }
    }

    private void setCustomActionbar() {

        ActionBar actionBar = getSupportActionBar();

        // Custom Actionbar를 사용하기 위해 CustomEnabled을 true 시키고 필요 없는 것은 false 시킨다

        actionBar.setDisplayShowCustomEnabled(true);

        actionBar.setDisplayHomeAsUpEnabled(false);

        actionBar.setDisplayShowTitleEnabled(false);

        // Set custom view layout

        View mCustomView = LayoutInflater.from(this).inflate(R.layout.actionbar_simple, null);

        actionBar.setCustomView(mCustomView);


        // Set no padding both side

        Toolbar parent = (Toolbar) mCustomView.getParent(); // first get parent toolbar of current action bar

        parent.setContentInsetsAbsolute(0, 0);              // set padding programmatically to 0dp

        // Set actionbar background image

        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.border_layout_bottom));

        // Set actionbar layout layoutparams

        ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);

        actionBar.setCustomView(mCustomView, params);

        ImageView img_actionbar_back = (ImageView) findViewById(R.id.img_actionbar_back);
        img_actionbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        TextView txt_actionbar = (TextView) findViewById(R.id.txt_actionbar_simple);
        txt_actionbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        txt_actionbar.setText(getString(R.string.title_ask));

    }
}
