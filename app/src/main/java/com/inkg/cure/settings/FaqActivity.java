package com.inkg.cure.settings;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.inkg.cure.R;
import com.inkg.cure.adapters.FaqAdapter;
import com.inkg.cure.classes.CureSP;
import com.inkg.cure.classes.Faq;
import com.inkg.cure.classes.HttpConnect;
import com.inkg.cure.classes.JsonMaker;

import java.util.ArrayList;

/**
 * Created by inkg on 2017. 3. 6..
 */

public class FaqActivity extends AppCompatActivity {

    private JsonMaker jsonMaker;
    private HttpConnect httpConnect;
    private CureSP cureSP;
    private ListView list_faq;
    private FaqAdapter faqAdapter;
    private ArrayList<Faq> faq_list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);
        jsonMaker = new JsonMaker();
        httpConnect = new HttpConnect(getApplicationContext());
        cureSP = new CureSP(getApplicationContext());

        setUpViews();
        setCustomActionbar();
    }

    private void setUpViews() {
        list_faq = (ListView) findViewById(R.id.list_faq);
        faqAdapter = new FaqAdapter(getApplicationContext(), R.layout.adapter_faq, faq_list);
        list_faq.setAdapter(faqAdapter);
    }

    private void setCustomActionbar() {

        ActionBar actionBar = getSupportActionBar();

        // Custom Actionbar를 사용하기 위해 CustomEnabled을 true 시키고 필요 없는 것은 false 시킨다

        actionBar.setDisplayShowCustomEnabled(true);

        actionBar.setDisplayHomeAsUpEnabled(false);

        actionBar.setDisplayShowTitleEnabled(false);

        // Set custom view layout

        View mCustomView = LayoutInflater.from(this).inflate(R.layout.actionbar_simple, null);

        actionBar.setCustomView(mCustomView);


        // Set no padding both side

        Toolbar parent = (Toolbar) mCustomView.getParent(); // first get parent toolbar of current action bar

        parent.setContentInsetsAbsolute(0, 0);              // set padding programmatically to 0dp

        // Set actionbar background image

        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.border_layout_bottom));

        // Set actionbar layout layoutparams

        ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);

        actionBar.setCustomView(mCustomView, params);

        ImageView img_actionbar_back = (ImageView) findViewById(R.id.img_actionbar_back);
        img_actionbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        TextView txt_actionbar = (TextView) findViewById(R.id.txt_actionbar_simple);
        txt_actionbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        txt_actionbar.setText("FAQ");

    }
}
