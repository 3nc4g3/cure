package com.inkg.cure.mypage;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.inkg.cure.R;
import com.inkg.cure.adapters.SearchResultAdapter;
import com.inkg.cure.classes.CureSP;
import com.inkg.cure.classes.GlobalApplication;
import com.inkg.cure.classes.HttpConnect;
import com.inkg.cure.classes.LoadingDialog;
import com.inkg.cure.classes.Medicine;
import com.inkg.cure.classes.RankData;
import com.inkg.cure.classes.Scores;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by inkg on 2017. 2. 22..
 */

public class MyProductActivity extends AppCompatActivity {

    private ImageView img_my_product_default;
    private TextView txt_my_product_default;
    private ListView list_my_product;
    private HttpConnect httpConnect;
    private CureSP csp;
    private SearchResultAdapter searchResultAdapter;

    private ArrayList<RankData> product_list = new ArrayList<>();
    private LoadingDialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_product);
        httpConnect = new HttpConnect(getApplicationContext());
        csp = new CureSP(getApplicationContext());
        GlobalApplication.setCurrentActivity(this);
        loadingDialog = new LoadingDialog(MyProductActivity.this);

        setCustomActionbar();

        setupView();
        BookmarkAsync ba = new BookmarkAsync();
        ba.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void setupView() {
        img_my_product_default = (ImageView) findViewById(R.id.img_my_product_default);
        txt_my_product_default = (TextView) findViewById(R.id.txt_my_product_default);
        list_my_product = (ListView) findViewById(R.id.list_my_product);
        searchResultAdapter = new SearchResultAdapter(getApplicationContext(), R.layout.adapter_category_search, product_list, "bookmark");
        list_my_product.setAdapter(searchResultAdapter);
    }

    private void setCustomActionbar() {

        ActionBar actionBar = getSupportActionBar();

        // Custom Actionbar를 사용하기 위해 CustomEnabled을 true 시키고 필요 없는 것은 false 시킨다

        actionBar.setDisplayShowCustomEnabled(true);

        actionBar.setDisplayHomeAsUpEnabled(false);

        actionBar.setDisplayShowTitleEnabled(false);

        // Set custom view layout

        View mCustomView = LayoutInflater.from(this).inflate(R.layout.actionbar_simple, null);

        actionBar.setCustomView(mCustomView);


        // Set no padding both side

        Toolbar parent = (Toolbar) mCustomView.getParent(); // first get parent toolbar of current action bar

        parent.setContentInsetsAbsolute(0, 0);              // set padding programmatically to 0dp

        // Set actionbar background image

        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.border_layout_bottom));

        // Set actionbar layout layoutparams

        ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);

        actionBar.setCustomView(mCustomView, params);

        ImageView img_actionbar_back = (ImageView) findViewById(R.id.img_actionbar_back);
        img_actionbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        TextView txt_actionbar = (TextView) findViewById(R.id.txt_actionbar_simple);
        txt_actionbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        txt_actionbar.setText("즐겨찾는 제품");
    }

    class BookmarkAsync extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loadingDialog.show();
                }
            });
        }

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            result = httpConnect.send("", httpConnect.getServerWithVersion() + getString(R.string.rest_bookmarks), csp.getValue("access_token", ""), "GET");
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.e("result", result);
            try {
                JSONObject product_json = new JSONObject(result);
                final String count = product_json.getString("count");
                if (Integer.valueOf(count) == 0) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            img_my_product_default.setVisibility(View.VISIBLE);
                            txt_my_product_default.setVisibility(View.VISIBLE);
                            list_my_product.setVisibility(View.GONE);
                        }
                    });

                } else {
                    JSONArray bookmark_array = product_json.getJSONArray("bookmarks");
                    for (int i = 0; i < bookmark_array.length(); i++) {
                        JSONObject bookmark_object = bookmark_array.getJSONObject(i);
                        RankData rankData = new RankData();
                        rankData.setId(bookmark_object.getString("target"));
                        rankData.setType(bookmark_object.getString("type"));

                        String data_result = "";
                        DataAsync da = new DataAsync();
                        data_result = da.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, rankData.getType(), rankData.getId()).get();

                        JSONObject data_json = new JSONObject(data_result);
                        rankData.setName(data_json.getString("name"));
                        if (data_json.has("company")) {
                            rankData.setCompany(data_json.getString("company"));
                        } else if (data_json.has("maker")) {
                            rankData.setCompany(data_json.getString("maker"));
                        } else {

                        }
                        JSONObject score_json = data_json.getJSONObject("scores");
                        Scores scores = new Scores();
                        scores.setLikes(score_json.getString("likes"));
                        scores.setUnlikes(score_json.getString("unlikes"));
                        scores.setReviews(score_json.getString("reviews"));
                        rankData.setScores(scores);
                        rankData.setSelect_image(data_json.getString("thumbnail"));

                        product_list.add(i, rankData);
                    }
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        searchResultAdapter.notifyDataSetChanged();
                    }
                });

            } catch (Exception e) {
                Log.e("myproduct", "get json excep", e);
            } finally {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loadingDialog.dismiss();
                    }
                });
            }
        }
    }

    class DataAsync extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            String type = strings[0];
            String id = strings[1];
            result = httpConnect.send("", httpConnect.getServerWithVersion() + "/" + type + "s/" + id, csp.getValue("access_token", ""), "GET");
            return result;
        }
    }
}
