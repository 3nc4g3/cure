package com.inkg.cure.mypage;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.inkg.cure.R;
import com.inkg.cure.adapters.NotificationAdapter;
import com.inkg.cure.classes.CureSP;
import com.inkg.cure.classes.HttpConnect;
import com.inkg.cure.classes.Notification;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by inkg on 2017. 3. 4..
 */

public class MyNotificationActivity extends AppCompatActivity {

    private ListView list_my_notification;
    private HttpConnect httpConnect;
    private CureSP csp;

    private ArrayList<Notification> noti_list = new ArrayList<>();
    private NotificationAdapter notificationAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_notification);

        httpConnect = new HttpConnect(getApplicationContext());
        csp = new CureSP(getApplicationContext());

        setCustomActionbar();
        
        NotiAsync na = new NotiAsync();
        na.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void setupViews() {
        list_my_notification = (ListView) findViewById(R.id.list_my_notification);
        notificationAdapter = new NotificationAdapter(getApplicationContext(), R.layout.adapter_notification, noti_list);
        list_my_notification.setAdapter(notificationAdapter);
    }

    class NotiAsync extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            result = httpConnect.send("", httpConnect.getServerWithVersion() + getString(R.string.rest_notice), csp.getValue("access_token", ""), "GET");
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                JSONArray noti_array = new JSONArray(result);
                noti_list = new ArrayList<>();
                for (int i = 0; i < noti_array.length(); i++) {
                    JSONObject noti_object = noti_array.getJSONObject(i);
                    Notification notification = new Notification();
                    notification.setIdx(noti_object.getString("idx"));
                    notification.setDate(noti_object.getString("date"));
                    notification.setTitle(noti_object.getString("title"));
                    notification.setUrl(noti_object.getString("url"));

                    noti_list.add(i, notification);
                }

                setupViews();
            } catch (Exception e) {
                Log.e("mynoti ac", "get json excep", e);
            }
        }
    }

    private void setCustomActionbar() {

        ActionBar actionBar = getSupportActionBar();

        // Custom Actionbar를 사용하기 위해 CustomEnabled을 true 시키고 필요 없는 것은 false 시킨다

        actionBar.setDisplayShowCustomEnabled(true);

        actionBar.setDisplayHomeAsUpEnabled(false);

        actionBar.setDisplayShowTitleEnabled(false);

        // Set custom view layout

        View mCustomView = LayoutInflater.from(this).inflate(R.layout.actionbar_simple, null);

        actionBar.setCustomView(mCustomView);


        // Set no padding both side

        Toolbar parent = (Toolbar) mCustomView.getParent(); // first get parent toolbar of current action bar

        parent.setContentInsetsAbsolute(0, 0);              // set padding programmatically to 0dp

        // Set actionbar background image

        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.border_layout_bottom));

        // Set actionbar layout layoutparams

        ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);

        actionBar.setCustomView(mCustomView, params);

        ImageView img_actionbar_back = (ImageView) findViewById(R.id.img_actionbar_back);
        img_actionbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        TextView txt_actionbar = (TextView) findViewById(R.id.txt_actionbar_simple);
        txt_actionbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        txt_actionbar.setText("공지사항");

    }
}
