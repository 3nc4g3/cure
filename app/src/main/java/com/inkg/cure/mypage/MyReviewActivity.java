package com.inkg.cure.mypage;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.inkg.cure.R;
import com.inkg.cure.adapters.ReviewAdapter;
import com.inkg.cure.classes.CureSP;
import com.inkg.cure.classes.HttpConnect;
import com.inkg.cure.classes.LoadingDialog;
import com.inkg.cure.classes.Review;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by inkg on 2017. 2. 22..
 */

public class MyReviewActivity extends AppCompatActivity {

    private ImageView img_my_review_default;
    private TextView txt_my_review_default;
    private ListView list_my_review;
    private ReviewAdapter reviewAdapter;
    private ArrayList<Review> review_list = new ArrayList<>();

    private HttpConnect httpConnect;
    private CureSP csp;
    private LoadingDialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_review);

        httpConnect = new HttpConnect(getApplicationContext());
        csp = new CureSP(getApplicationContext());
        loadingDialog = new LoadingDialog(MyReviewActivity.this);

        setCustomActionbar();
        setupView();
        ReviewAsync ra = new ReviewAsync();
        ra.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void setupView() {
        img_my_review_default = (ImageView) findViewById(R.id.img_my_review_default);
        txt_my_review_default = (TextView) findViewById(R.id.txt_my_review_default);
        list_my_review = (ListView) findViewById(R.id.list_my_review);
        reviewAdapter = new ReviewAdapter(getApplicationContext(), R.layout.adapter_review_read, review_list);
        list_my_review.setAdapter(reviewAdapter);
    }

    private void setCustomActionbar() {

        ActionBar actionBar = getSupportActionBar();

        // Custom Actionbar를 사용하기 위해 CustomEnabled을 true 시키고 필요 없는 것은 false 시킨다

        actionBar.setDisplayShowCustomEnabled(true);

        actionBar.setDisplayHomeAsUpEnabled(false);

        actionBar.setDisplayShowTitleEnabled(false);

        // Set custom view layout

        View mCustomView = LayoutInflater.from(this).inflate(R.layout.actionbar_simple, null);

        actionBar.setCustomView(mCustomView);


        // Set no padding both side

        Toolbar parent = (Toolbar) mCustomView.getParent(); // first get parent toolbar of current action bar

        parent.setContentInsetsAbsolute(0, 0);              // set padding programmatically to 0dp

        // Set actionbar background image

        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.border_layout_bottom));

        // Set actionbar layout layoutparams

        ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);

        actionBar.setCustomView(mCustomView, params);

        ImageView img_actionbar_back = (ImageView) findViewById(R.id.img_actionbar_back);
        img_actionbar_back.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    finish();
                }
                return false;
            }
        });

        TextView txt_actionbar = (TextView) findViewById(R.id.txt_actionbar_simple);
        txt_actionbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        txt_actionbar.setText("내가 쓴 리뷰");
    }

    class ReviewAsync extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loadingDialog.show();
                }
            });
        }

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            result = httpConnect.send("", httpConnect.getServerWithVersion() + getString(R.string.rest_review_member) + "?id=" + csp.getValue("id", ""), csp.getValue("access_token", ""), "GET");
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.e("result", result);
            try {
                review_list.clear();
                JSONObject review_json = new JSONObject(result);
                JSONArray items_json = review_json.getJSONArray("Items");
                for (int i = 0; i < items_json.length(); i++) {
                    JSONObject review_object = items_json.getJSONObject(i);
                    Review review = new Review();
                    review.setType(review_object.getString("type"));
                    review.setAge(review_object.getString("age"));
                    review.setBad_point(review_object.getString("bad_point"));
                    review.setDate(review_object.getString("date"));
                    review.setEvaluation(review_object.getString("evaluation"));
                    review.setGender(review_object.getString("gender"));
                    review.setGood_point(review_object.getString("good_point"));
                    review.setId(review_object.getString("id"));
                    review.setLikes(review_object.getString("likes"));
                    review.setMember(review_object.getString("member"));
                    review.setNickname(review_object.getString("nickname"));
                    review.setPeriod(review_object.getString("period"));
//                    review.setPicture();
                    review.setProduct_name(review_object.getString("product_name"));
                    if (review_object.has("profile")) {
                        review.setProfile(review_object.getString("profile"));
                    } else {
                        review.setProfile("null");
                    }
                    review.setRepurchase(review_object.getString("repurchase"));
                    review.setTarget(review_object.getString("target"));
                    review.setTip(review_object.getString("tip"));
                    review.setViews(review_object.getString("views"));

                    review_list.add(i, review);
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (review_list.size() == 0) {
                            img_my_review_default.setVisibility(View.VISIBLE);
                            txt_my_review_default.setVisibility(View.VISIBLE);
                            list_my_review.setVisibility(View.GONE);
                        }
                        reviewAdapter.notifyDataSetChanged();
                    }
                });

            } catch (Exception e) {
                Log.e("myreview ac", "get review excep", e);
            } finally {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loadingDialog.dismiss();
                    }
                });
            }
        }
    }

    class ProfileAsync extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            return null;
        }
    }
}