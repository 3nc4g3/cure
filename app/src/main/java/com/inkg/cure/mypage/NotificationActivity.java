package com.inkg.cure.mypage;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.inkg.cure.R;
import com.inkg.cure.classes.CureSP;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by inkg on 2017. 3. 4..
 */

public class NotificationActivity extends AppCompatActivity {

    private WebView webview_noti;
    private String url;
    private CureSP csp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        csp = new CureSP(getApplicationContext());

        Intent intent = getIntent();
        url = intent.getStringExtra("url");

        setCustomActionbar();

        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", csp.getValue("access_token", ""));
        webview_noti = (WebView) findViewById(R.id.webview_noti);
        webview_noti.setWebViewClient(new WebViewClient() {
            // 로딩이 시작될 때
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }

            // 로딩이 완료됬을 때 한번 호출
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
            }

            // 오류가 났을 경우, 오류는 복수할 수 없음
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);

                switch (errorCode) {
                    case ERROR_AUTHENTICATION: // 서버에서 사용자 인증 실패
                        break;
                    case ERROR_BAD_URL: // 잘못된 URL
                        break;
                    case ERROR_CONNECT: // 서버로 연결 실패
                        break;
                    case ERROR_FAILED_SSL_HANDSHAKE: // SSL handshake 수행 실패
                        break;
                    case ERROR_FILE: // 일반 파일 오류
                        break;
                    case ERROR_FILE_NOT_FOUND: // 파일을 찾을 수 없습니다
                        break;
                    case ERROR_HOST_LOOKUP: // 서버 또는 프록시 호스트 이름 조회 실패
                        break;
                    case ERROR_IO: // 서버에서 읽거나 서버로 쓰기 실패
                        break;
                    case ERROR_PROXY_AUTHENTICATION: // 프록시에서 사용자 인증 실패
                        break;
                    case ERROR_REDIRECT_LOOP: // 너무 많은 리디렉션
                        break;
                    case ERROR_TIMEOUT: // 연결 시간 초과
                        break;
                    case ERROR_TOO_MANY_REQUESTS: // 페이지 로드중 너무 많은 요청 발생
                        break;
                    case ERROR_UNKNOWN: // 일반 오류
                        break;
                    case ERROR_UNSUPPORTED_AUTH_SCHEME: // 지원되지 않는 인증 체계
                        break;
                    case ERROR_UNSUPPORTED_SCHEME: // URI가 지원되지 않는 방식
                        break;
                }

            }
        });
        webview_noti.loadUrl(url, headers);
    }

    private void setCustomActionbar() {

        ActionBar actionBar = getSupportActionBar();

        // Custom Actionbar를 사용하기 위해 CustomEnabled을 true 시키고 필요 없는 것은 false 시킨다

        actionBar.setDisplayShowCustomEnabled(true);

        actionBar.setDisplayHomeAsUpEnabled(false);

        actionBar.setDisplayShowTitleEnabled(false);

        // Set custom view layout

        View mCustomView = LayoutInflater.from(this).inflate(R.layout.actionbar_simple, null);

        actionBar.setCustomView(mCustomView);


        // Set no padding both side

        Toolbar parent = (Toolbar) mCustomView.getParent(); // first get parent toolbar of current action bar

        parent.setContentInsetsAbsolute(0, 0);              // set padding programmatically to 0dp

        // Set actionbar background image

        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.border_layout_bottom));

        // Set actionbar layout layoutparams

        ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);

        actionBar.setCustomView(mCustomView, params);

        ImageView img_actionbar_back = (ImageView) findViewById(R.id.img_actionbar_back);
        img_actionbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        TextView txt_actionbar = (TextView) findViewById(R.id.txt_actionbar_simple);
        txt_actionbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        txt_actionbar.setText("공지사항");
    }
}
