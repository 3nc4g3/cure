package com.inkg.cure.mypage;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.inkg.cure.R;
import com.inkg.cure.adapters.JoinAdapter;
import com.inkg.cure.classes.CureSP;
import com.inkg.cure.classes.HttpConnect;
import com.inkg.cure.classes.JsonMaker;
import com.inkg.cure.classes.RequestCodes;
import com.inkg.cure.classes.ResultCodes;
import com.inkg.cure.classes.Utils;
import com.inkg.cure.review.WriteActivity;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by inkg on 2017. 2. 22..
 */

public class MyProfileActivity extends AppCompatActivity {

    private ImageView img_mypage_photo;
    private EditText edit_mypage_nickname;
    private EditText edit_mypage_email;
    private EditText edit_mypage_password;
    private EditText edit_mypage_password_confirm;
    private RadioGroup rg_profile_gender;
    private RadioButton rb_profile_female;
    private RadioButton rb_profile_male;
    private Button btn_profile_age;
    private TextView txt_profile_age;
    private Button btn_mypage_leave;
    private Button btn_mypage_save;
    private Button btn_mypage_photo_select;

    private HttpConnect httpConnect;
    private CureSP csp;
    private Utils utils;
    private JsonMaker jsonMaker;

    private String json_data;
    private int[] profile_checker;
    private boolean is_done = false;

    private String nickname;
    private String email;
    private String password;
    private String password_confirm;
    private String gender;
    private String age;
    private String id;
    private String profile = "";

    private Bitmap bm;
    private Uri mImageCaptureUri;

    private String photo = "";
    private Bitmap photo_bitmap = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);

        httpConnect = new HttpConnect(getApplicationContext());
        csp = new CureSP(getApplicationContext());
        utils = new Utils();
        jsonMaker = new JsonMaker();

        Intent intent = getIntent();
        json_data = intent.getStringExtra("json");
        profile_checker = new int[4];
        for (int i = 0; i < 4; i++) {
            profile_checker[i] = 0;
        }
        setUpViews();
        setUpValueChecker();
        setCustomActionbar();
    }

    private void setUpViews() {
        img_mypage_photo = (ImageView) findViewById(R.id.img_mypage_photo);
        edit_mypage_nickname = (EditText) findViewById(R.id.edit_mypage_nickname);
        edit_mypage_email = (EditText) findViewById(R.id.edit_mypage_email);
        edit_mypage_password = (EditText) findViewById(R.id.edit_mypage_password);
        edit_mypage_password_confirm = (EditText) findViewById(R.id.edit_mypage_password_confirm);
        rg_profile_gender = (RadioGroup) findViewById(R.id.rg_profile_gender);
        rb_profile_female = (RadioButton) findViewById(R.id.rb_profile_female);
        rb_profile_male = (RadioButton) findViewById(R.id.rb_profile_male);
        btn_profile_age = (Button) findViewById(R.id.btn_profile_age);
        txt_profile_age = (TextView) findViewById(R.id.txt_profile_age);
        btn_mypage_leave = (Button) findViewById(R.id.btn_mypage_leave);
        btn_mypage_save = (Button) findViewById(R.id.btn_mypage_save);
        btn_mypage_photo_select = (Button) findViewById(R.id.btn_mypage_photo_select);

        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        final ArrayList<String> ages = new ArrayList<>();
        for (int i = currentYear; i > 1939; i--) {
            ages.add(String.valueOf(i));
        }
        final JoinAdapter age_adapter = new JoinAdapter(getApplicationContext(), R.layout.adapter_join, ages);
        final LayoutInflater title_inflater = (LayoutInflater) getApplicationContext().getSystemService(LAYOUT_INFLATER_SERVICE);

        btn_mypage_photo_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder ad = new AlertDialog.Builder(MyProfileActivity.this);
                ad.setMessage("업로드할 이미지 선택")
                        .setPositiveButton("앨범선택", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                doTakeAlbumAction();
                            }
                        })
                        .setNeutralButton("사진촬영", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                doTakePhotoAction();
                            }
                        })
                        .setNegativeButton("취소", null)
                        .show();
            }
        });

        img_mypage_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder ad = new AlertDialog.Builder(MyProfileActivity.this);
                ad.setMessage("업로드할 이미지 선택")
                        .setPositiveButton("앨범선택", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                doTakeAlbumAction();
                            }
                        })
                        .setNeutralButton("사진촬영", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                doTakePhotoAction();
                            }
                        })
                        .setNegativeButton("취소", null)
                        .show();
            }
        });

        btn_profile_age.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder ab = new AlertDialog.Builder(MyProfileActivity.this);
                View title_view = title_inflater.inflate(R.layout.dialog_custom_title, null);
                TextView txt_dialog_title = (TextView) title_view.findViewById(R.id.txt_dialog_custom_title);
                ab.setCustomTitle(title_view);
                txt_dialog_title.setText("출생년도");
                ab.setAdapter(age_adapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        txt_profile_age.setText(ages.get(i));
                        age = ages.get(i);
                        dialogInterface.dismiss();
                        finish_checker();
                    }
                }).create();
                ab.show();
            }
        });

        btn_mypage_leave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(MyProfileActivity.this);
                View title_view = title_inflater.inflate(R.layout.dialog_custom_title, null);
                TextView txt_dialog_title = (TextView) title_view.findViewById(R.id.txt_dialog_custom_title);
                alertDialog.setCustomTitle(title_view);
                txt_dialog_title.setText("탈퇴");
                alertDialog.setMessage("정말로 탈퇴하시겠습니까? 탈퇴한 회원의 정보는 저장되지 않습니다.");
                alertDialog.setPositiveButton("취소", null);
                alertDialog.setNegativeButton("확인", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // LeaveAsync
                        LeaveAsync la = new LeaveAsync();
                        la.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                });
                alertDialog.show();
            }
        });

        btn_mypage_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (is_done) {
                    password = edit_mypage_password.getText().toString();
                    password_confirm = edit_mypage_password_confirm.getText().toString();
                    if (!password.equals(password_confirm)) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "패스워드가 서로 다릅니다.", Toast.LENGTH_SHORT).show();
                            }
                        });
                    } else {
                        // SaveAsync
                        SaveAsync sa = new SaveAsync();
                        sa.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                    }
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "필수 항목을 모두 채워주세요.", Toast.LENGTH_SHORT).show();
                        }
                    });
                }

            }
        });

        rb_profile_female.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gender = "female";
            }
        });

        rb_profile_male.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gender = "male";
            }
        });

        Log.e("result", json_data);

        try {
            JSONObject json = new JSONObject(json_data);
            if (json.has("profile")) {
                profile = json.getString("profile");
            }
            nickname = json.getString("nickname");
            edit_mypage_nickname.setText(nickname);
            if (!"".equals(edit_mypage_nickname.getText().toString())) {
                profile_checker[0] = 1;
            }
            if (json.has("email")) {
                email = json.getString("email");
            }
            edit_mypage_email.setText(email);
            if (!"".equals(edit_mypage_email.getText().toString())) {
                profile_checker[1] = 1;
            }
            if ("male".equals(json.getString("gender"))) {
                gender = "male";
                rb_profile_male.setChecked(true);
                rb_profile_female.setChecked(false);
            } else {
                gender = "female";
                rb_profile_male.setChecked(false);
                rb_profile_female.setChecked(true);
            }
            age = json.getString("age");
            txt_profile_age.setText(age);
            id = json.getString("id");
        } catch (Exception e) {
            Log.e("profile ac", "get json excep", e);
        }

        IconAsync ia = new IconAsync(profile);
        ia.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case RequestCodes.PICK_FROM_ALBUM: {
                if (resultCode != 0) {
                    mImageCaptureUri = data.getData();
                    photo = mImageCaptureUri.getPath().toString();
                }
            }
            case RequestCodes.PICK_FROM_CAMERA: {
                if (resultCode != 0) {
                    Intent intent = new Intent("com.android.camera.action.CROP");
                    intent.setDataAndType(mImageCaptureUri, "image/*");

                    // CROP할 이미지를 200*200 크기로 저장
                    intent.putExtra("outputX", 200); // CROP한 이미지의 x축 크기
                    intent.putExtra("outputY", 200); // CROP한 이미지의 y축 크기
                    intent.putExtra("aspectX", 1); // CROP 박스의 X축 비율
                    intent.putExtra("aspectY", 1); // CROP 박스의 Y축 비율
                    intent.putExtra("scale", true);
                    intent.putExtra("return-data", true);
                    startActivityForResult(intent, RequestCodes.CROP_FROM_IMAGE); // CROP_FROM_CAMERA case문 이동
                }
                break;
            }
            case RequestCodes.CROP_FROM_IMAGE: {

                if (resultCode != 0) {
                    final Bundle extras = data.getExtras();

                    if (extras != null) {
                        photo_bitmap = extras.getParcelable("data"); // CROP된 BITMAP
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        photo_bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                        byte[] byteArray = byteArrayOutputStream.toByteArray();
                        photo = Base64.encodeToString(byteArray, Base64.DEFAULT);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                img_mypage_photo.setImageBitmap(photo_bitmap);
                                ProfileAsync pa = new ProfileAsync();
                                pa.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                            }
                        });
                    }
                    // 임시 파일 삭제
                    File f = new File(mImageCaptureUri.getPath());
                    if (f.exists()) {
                        f.delete();
                    }
                }
                break;
            }
        }

    }

    class ProfileAsync extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            String photo_json = "";
            photo_json = jsonMaker.makeJson(new Pair("picture", photo));
            result = httpConnect.send(photo_json, httpConnect.getServerWithVersion() + getString(R.string.rest_members_pictures), csp.getValue("access_token", ""), "PUT");
            return result;
        }
    }

    class SaveAsync extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            String change_json = "";
            change_json = jsonMaker.makeJson(new Pair("nickname", nickname), new Pair("age", age), new Pair("email", email), new Pair("password", password), new Pair("gender", gender));
            result = httpConnect.send(change_json, httpConnect.getServerWithVersion() + getString(R.string.rest_members), csp.getValue("access_token", ""), "PATCH");
            Log.e("myprofile save result", result);
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

        }
    }

    class LeaveAsync extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            String json = jsonMaker.makeJson(new Pair("id", id));
            result = httpConnect.send(json, httpConnect.getServerWithVersion() + getString(R.string.rest_members), csp.getValue("access_token", ""), "DELETE");
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (!"400".equals(result) && !"403".equals(result)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "탈퇴가 완료되었습니다.", Toast.LENGTH_SHORT).show();
                    }
                });
                setResult(ResultCodes.CURE_LOGOUT);
                finish();
            }
        }
    }

    private void setUpValueChecker() {
        TextWatcher nickname_watcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    profile_checker[0] = 1;
                }
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    profile_checker[0] = 1;
                } else {
                    profile_checker[0] = 0;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                finish_checker();
            }
        };

        edit_mypage_nickname.addTextChangedListener(nickname_watcher);

        TextWatcher email_watcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    profile_checker[1] = 1;
                }
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (utils.validateEmail(charSequence.toString())) {
                    profile_checker[1] = 1;
                } else {
                    profile_checker[1] = 0;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                finish_checker();
            }
        };

        edit_mypage_email.addTextChangedListener(email_watcher);

        TextWatcher password_watcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (utils.validatePassword(charSequence.toString())) {
                    profile_checker[2] = 1;
                } else {
                    profile_checker[2] = 0;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                finish_checker();
            }
        };

        edit_mypage_password.addTextChangedListener(password_watcher);

        TextWatcher password_confirm_watcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (utils.validatePassword(charSequence.toString())) {
                    profile_checker[3] = 1;
                } else {
                    profile_checker[3] = 0;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                finish_checker();
            }
        };

        edit_mypage_password_confirm.addTextChangedListener(password_confirm_watcher);
    }

    private void finish_checker() {
        boolean is_ok = true;
        for (int i = 0; i < profile_checker.length; i++) {
            if (profile_checker[i] == 0) {
                is_ok = false;
            }
        }

        Log.e("finish check", profile_checker[0] + " / " + profile_checker[1] + " / " + profile_checker[2] + " / " + profile_checker[3]);

        if (is_ok) {
            is_done = true;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    btn_mypage_save.setBackgroundColor(ContextCompat.getColor(MyProfileActivity.this, R.color.maya_blue1));
                }
            });
        } else {
            is_done = false;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    btn_mypage_save.setBackgroundColor(ContextCompat.getColor(MyProfileActivity.this, R.color.silver));
                }
            });
        }
    }

    private void setCustomActionbar() {

        ActionBar actionBar = getSupportActionBar();

        // Custom Actionbar를 사용하기 위해 CustomEnabled을 true 시키고 필요 없는 것은 false 시킨다

        actionBar.setDisplayShowCustomEnabled(true);

        actionBar.setDisplayHomeAsUpEnabled(false);

        actionBar.setDisplayShowTitleEnabled(false);

        // Set custom view layout

        View mCustomView = LayoutInflater.from(this).inflate(R.layout.actionbar_mypage, null);

        actionBar.setCustomView(mCustomView);


        // Set no padding both side

        Toolbar parent = (Toolbar) mCustomView.getParent(); // first get parent toolbar of current action bar

        parent.setContentInsetsAbsolute(0, 0);              // set padding programmatically to 0dp

        // Set actionbar background image

        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.border_layout_bottom));

        // Set actionbar layout layoutparams

        ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);

        actionBar.setCustomView(mCustomView, params);

        ImageView img_actionbar_back = (ImageView) findViewById(R.id.img_actionbar_mypage_back);
        img_actionbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        TextView txt_actionbar = (TextView) findViewById(R.id.txt_actionbar_mypage);
        txt_actionbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        txt_actionbar.setText(getString(R.string.edit_mypage));

        Button btn_actionbar_setting = (Button) findViewById(R.id.btn_actionbar_setting);
        btn_actionbar_setting.setVisibility(View.GONE);

        Button btn_actionbar_alarm = (Button) findViewById(R.id.btn_actionbar_alarm);
        btn_actionbar_alarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    class IconAsync extends AsyncTask<String, String, String> {

        private String image;

        IconAsync() {

        }

        IconAsync(String image) {
            this.image = image;
        }

        @Override
        protected String doInBackground(String... strings) {
            final URL[] imageURL = new URL[1];

            try {
                if ("null".equals(image) || "None".equals(image) || "".equals(image)) {

                } else {
                    imageURL[0] = new URL(image);

                    HttpURLConnection conn = (HttpURLConnection) imageURL[0].openConnection();
                    BufferedInputStream bis = new BufferedInputStream(conn.getInputStream(), 1024);
                    bm = BitmapFactory.decodeStream(bis);
                    bis.close();

                }
            } catch (Exception e) {
                Log.e("search result adap", "image url excep", e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if ("null".equals(image) || "None".equals(image) || "".equals(image)) {
                        img_mypage_photo.setImageResource(R.drawable.profile);
                    } else {
                        img_mypage_photo.setImageBitmap(bm);
                    }
                    img_mypage_photo.invalidate();
                }
            });

        }
    }

    public void doTakeAlbumAction() // 앨범에서 이미지 가져오기
    {
        // 앨범 호출
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(android.provider.MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, RequestCodes.PICK_FROM_ALBUM);
    }

    public void doTakePhotoAction() // 카메라 촬영 후 이미지 가져오기
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        // 임시로 사용할 파일의 경로를 생성
        String url = "tmp_" + String.valueOf(System.currentTimeMillis()) + ".jpg";
        mImageCaptureUri = Uri.fromFile(new File(Environment.getExternalStorageDirectory(), url));

        intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
        startActivityForResult(intent, RequestCodes.PICK_FROM_CAMERA);
    }

}
