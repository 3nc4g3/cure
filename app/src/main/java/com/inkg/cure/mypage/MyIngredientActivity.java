package com.inkg.cure.mypage;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.inkg.cure.R;
import com.inkg.cure.adapters.Ingr1Adapter;
import com.inkg.cure.adapters.IngredientAdapter;
import com.inkg.cure.classes.CureSP;
import com.inkg.cure.classes.HttpConnect;
import com.inkg.cure.classes.Ingredient;
import com.inkg.cure.classes.LoadingDialog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by inkg on 2017. 2. 22..
 */

public class MyIngredientActivity extends AppCompatActivity {

    private ImageView img_my_ingredient_default;
    private TextView txt_my_ingredient_default;
    private ListView list_my_ingredient;
    private IngredientAdapter ingrAdapter;
    private ArrayList<Ingredient> ingr_list = new ArrayList<>();

    private CureSP csp;
    private HttpConnect httpConnect;
    private LoadingDialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_ingredient);
        csp = new CureSP(getApplicationContext());
        httpConnect = new HttpConnect(getApplicationContext());
        loadingDialog = new LoadingDialog(MyIngredientActivity.this);

        setupViews();
        setCustomActionbar();

        IngrAsync ia = new IngrAsync();
        ia.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void setupViews() {
        img_my_ingredient_default = (ImageView) findViewById(R.id.img_my_ingredient_default);
        txt_my_ingredient_default = (TextView) findViewById(R.id.txt_my_ingredient_default);
        list_my_ingredient = (ListView) findViewById(R.id.list_my_ingredient);
        ingrAdapter = new IngredientAdapter(getApplicationContext(), R.layout.adapter_ingredient, ingr_list);
        list_my_ingredient.setAdapter(ingrAdapter);


    }

    class IngrAsync extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loadingDialog.show();
                }
            });
        }

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            result = httpConnect.send("", httpConnect.getServerWithVersion() + getString(R.string.rest_members), csp.getValue("access_token", ""), "GET");
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                JSONObject ingredient_json = new JSONObject(result);
                Ingredient ingredient = new Ingredient();
                JSONArray ingr_array = ingredient_json.getJSONArray("ingredient_likes");
                for (int i = 0; i < ingr_array.length(); i++) {
                    ingredient.setName(ingr_array.getString(i));
                    ingr_list.add(i, ingredient);
                }

                if (ingr_list.size() == 0) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            img_my_ingredient_default.setVisibility(View.VISIBLE);
                            txt_my_ingredient_default.setVisibility(View.VISIBLE);
                            list_my_ingredient.setVisibility(View.GONE);
                            ingrAdapter.notifyDataSetChanged();
                        }
                    });
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            img_my_ingredient_default.setVisibility(View.GONE);
                            txt_my_ingredient_default.setVisibility(View.GONE);
                            list_my_ingredient.setVisibility(View.VISIBLE);
                            ingrAdapter.notifyDataSetChanged();
                        }
                    });
                }

            } catch (Exception e) {
                Log.e("myingr ac", "json excep", e);
            } finally {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        loadingDialog.dismiss();
                    }
                });
            }

        }
    }

    private void setCustomActionbar() {

        ActionBar actionBar = getSupportActionBar();

        // Custom Actionbar를 사용하기 위해 CustomEnabled을 true 시키고 필요 없는 것은 false 시킨다

        actionBar.setDisplayShowCustomEnabled(true);

        actionBar.setDisplayHomeAsUpEnabled(false);

        actionBar.setDisplayShowTitleEnabled(false);

        // Set custom view layout

        View mCustomView = LayoutInflater.from(this).inflate(R.layout.actionbar_simple, null);

        actionBar.setCustomView(mCustomView);


        // Set no padding both side

        Toolbar parent = (Toolbar) mCustomView.getParent(); // first get parent toolbar of current action bar

        parent.setContentInsetsAbsolute(0, 0);              // set padding programmatically to 0dp

        // Set actionbar background image

        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.border_layout_bottom));

        // Set actionbar layout layoutparams

        ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);

        actionBar.setCustomView(mCustomView, params);

        ImageView img_actionbar_back = (ImageView) findViewById(R.id.img_actionbar_back);
        img_actionbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        TextView txt_actionbar = (TextView) findViewById(R.id.txt_actionbar_simple);
        txt_actionbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        txt_actionbar.setText("즐겨찾는 성분");
    }
}
