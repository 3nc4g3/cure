package com.inkg.cure.mypage;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.inkg.cure.R;
import com.inkg.cure.SettingActivity;
import com.inkg.cure.classes.CureSP;
import com.inkg.cure.classes.Fonts;
import com.inkg.cure.classes.HttpConnect;
import com.inkg.cure.classes.LoadingDialog;
import com.inkg.cure.classes.ResultCodes;
import com.inkg.cure.classes.Scores;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by inkg on 2016. 12. 21..
 */

public class MypageActivity extends AppCompatActivity {

    // profile
    private TextView txt_mypage_nickname;
    private Button btn_mypage_profile;
    private ImageView img_mypage_profile;
    private Button btn_mypage_age;

    // reviews
    private Button btn_mypage_review;
    private TextView txt_mypage_review;

    // followers
    private Button btn_mypage_follower;
    private TextView txt_mypage_follower;

    // followings
    private TextView txt_mypage_following;
    private Button btn_mypage_following;

    // requests
    private TextView txt_mypage_request;
    private Button btn_mypage_request;

    // scraps
    private TextView txt_mypage_scrap;
    private Button btn_mypage_scrap;

    // pharmacy
    private TextView txt_mypage_pharmacy;
    private Button btn_mypage_pharmacy;

    // products
    private TextView txt_mypage_product;
    private Button btn_mypage_product;

    // ingredients
    private TextView txt_mypage_ingredient;
    private Button btn_mypage_ingredient;

    private HttpConnect httpConnect;
    private CureSP csp;

    private String my_result = "";
    private Bitmap bm;

    private LoadingDialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mypage);
        httpConnect = new HttpConnect(getApplicationContext());
        csp = new CureSP(getApplicationContext());
        loadingDialog = new LoadingDialog(MypageActivity.this);

        setUpViews();
        setCustomActionbar();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (resultCode) {
            case ResultCodes.CURE_LOGOUT:
                setResult(ResultCodes.CURE_LOGOUT);
                finish();
                break;
        }
    }

    private void setUpViews() {

        txt_mypage_nickname = (TextView) findViewById(R.id.txt_mypage_nickname);
        btn_mypage_profile = (Button) findViewById(R.id.btn_mypage_profile);
        img_mypage_profile = (ImageView) findViewById(R.id.img_mypage_profile);
        btn_mypage_age = (Button) findViewById(R.id.btn_mypage_age);

        btn_mypage_review = (Button) findViewById(R.id.btn_mypage_review);
        txt_mypage_review = (TextView) findViewById(R.id.txt_mypage_review);

        btn_mypage_follower = (Button) findViewById(R.id.btn_mypage_follower);
        txt_mypage_follower = (TextView) findViewById(R.id.txt_mypage_follower);

        txt_mypage_following = (TextView) findViewById(R.id.txt_mypage_following);
        btn_mypage_following = (Button) findViewById(R.id.btn_mypage_following);

        txt_mypage_request = (TextView) findViewById(R.id.txt_mypage_request);
        btn_mypage_request = (Button) findViewById(R.id.btn_mypage_request);

        txt_mypage_scrap = (TextView) findViewById(R.id.txt_mypage_scrap);
        btn_mypage_scrap = (Button) findViewById(R.id.btn_mypage_scrap);

        txt_mypage_pharmacy = (TextView) findViewById(R.id.txt_mypage_pharmacy);
        btn_mypage_pharmacy = (Button) findViewById(R.id.btn_mypage_pharmacy);

        txt_mypage_product = (TextView) findViewById(R.id.txt_mypage_product);
        btn_mypage_product = (Button) findViewById(R.id.btn_mypage_product);

        txt_mypage_ingredient = (TextView) findViewById(R.id.txt_mypage_ingredient);
        btn_mypage_ingredient = (Button) findViewById(R.id.btn_mypage_ingredient);

        MyDataGetAsync mdga = new MyDataGetAsync();
        mdga.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        btn_mypage_profile.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    Intent profile_intent = new Intent(MypageActivity.this, MyProfileActivity.class);
                    profile_intent.putExtra("json", my_result);
                    startActivity(profile_intent);
                }
                return false;
            }
        });

        btn_mypage_review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent review_intent = new Intent(MypageActivity.this, MyReviewActivity.class);
                startActivity(review_intent);
            }
        });

        btn_mypage_follower.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent follower_intent = new Intent(MypageActivity.this, MyFollowerActivity.class);
                startActivity(follower_intent);
            }
        });

        btn_mypage_following.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent following_intent = new Intent(MypageActivity.this, MyFollowingActivity.class);
                startActivity(following_intent);
            }
        });

        btn_mypage_request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent request_intent = new Intent(MypageActivity.this, MyRequestActivity.class);
//                startActivity(request_intent);
            }
        });

        btn_mypage_scrap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent scrap_intent = new Intent(MypageActivity.this, MyScrapActivity.class);
                startActivity(scrap_intent);
            }
        });

        btn_mypage_pharmacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pharmacy_intent = new Intent(MypageActivity.this, MyPharmacyActivity.class);
                startActivity(pharmacy_intent);
            }
        });

        btn_mypage_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent product_intent = new Intent(MypageActivity.this, MyProductActivity.class);
                startActivity(product_intent);
            }
        });

        btn_mypage_ingredient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ingredient_intent = new Intent(MypageActivity.this, MyIngredientActivity.class);
                startActivity(ingredient_intent);
            }
        });
    }

    class MyDataGetAsync extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loadingDialog.show();
                }
            });
            String result = "";
            result = httpConnect.send("", httpConnect.getServerWithVersion() + getString(R.string.rest_members), csp.getValue("access_token", ""), "GET");
            Log.e("mypage result", result);
            my_result = result;
        }

        @Override
        protected String doInBackground(String... strings) {
            setJsonData(my_result);
            return null;
        }
    }

    private void setJsonData(final String json) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    JSONObject my_json = new JSONObject(json);
                    JSONObject score_json = my_json.getJSONObject("scores");
                    JSONArray ingredient = my_json.getJSONArray("ingredient_likes");
                    String nickname = my_json.getString("nickname");
                    String profile = "";
                    String age = my_json.getString("age");
                    if (my_json.has("uuid_profile")) {
                        profile = my_json.getString("uuid_profile");
                    }

                    IconAsync ia = new IconAsync(profile);
                    ia.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                    txt_mypage_follower.setText(score_json.getString("follower"));
                    txt_mypage_following.setText(score_json.getString("following"));
                    txt_mypage_ingredient.setText(String.valueOf(ingredient.length()));
                    txt_mypage_pharmacy.setText(score_json.getString("drugstore_likes"));
                    txt_mypage_product.setText(score_json.getString("bookmarks"));
                    txt_mypage_request.setText(score_json.getString("request_drugs"));
                    txt_mypage_review.setText(score_json.getString("reviews"));
                    txt_mypage_scrap.setText(score_json.getString("review_srape"));

                    txt_mypage_nickname.setText(nickname);
                    btn_mypage_age.setText(age);

                } catch (Exception e) {
                    Log.e("my ac", "get json excep", e);
                } finally {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            loadingDialog.dismiss();
                        }
                    });
                }
            }
        });
    }

    private void setCustomActionbar() {

        ActionBar actionBar = getSupportActionBar();

        // Custom Actionbar를 사용하기 위해 CustomEnabled을 true 시키고 필요 없는 것은 false 시킨다

        actionBar.setDisplayShowCustomEnabled(true);

        actionBar.setDisplayHomeAsUpEnabled(false);

        actionBar.setDisplayShowTitleEnabled(false);

        // Set custom view layout
        View mCustomView = LayoutInflater.from(this).inflate(R.layout.actionbar_mypage, null);

        actionBar.setCustomView(mCustomView);


        // Set no padding both side

        Toolbar parent = (Toolbar) mCustomView.getParent(); // first get parent toolbar of current action bar

        parent.setContentInsetsAbsolute(0, 0);              // set padding programmatically to 0dp

        // Set actionbar background image

        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.border_layout_bottom));

        // Set actionbar layout layoutparams

        ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);

        actionBar.setCustomView(mCustomView, params);

        ImageView img_actionbar_back = (ImageView) findViewById(R.id.img_actionbar_mypage_back);
        img_actionbar_back.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    setResult(ResultCodes.BACK);
                    finish();
                }
                return false;
            }
        });

        TextView txt_actionbar = (TextView) findViewById(R.id.txt_actionbar_mypage);
        txt_actionbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(ResultCodes.BACK);
                finish();
            }
        });
        txt_actionbar.setText(getString(R.string.title_mypage));

        Button btn_actionbar_setting = (Button) findViewById(R.id.btn_actionbar_setting);
        btn_actionbar_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent setting_intent = new Intent(MypageActivity.this, SettingActivity.class);
                startActivityForResult(setting_intent, 111);
            }
        });

        Button btn_actionbar_alarm = (Button) findViewById(R.id.btn_actionbar_alarm);
        btn_actionbar_alarm.setBackground(getResources().getDrawable(R.drawable.alarm, null));
        btn_actionbar_alarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent noti_intent = new Intent(MypageActivity.this, MyNotificationActivity.class);
                startActivity(noti_intent);
            }
        });

    }

    class IconAsync extends AsyncTask<String, String, String> {

        private String image;

        IconAsync() {

        }

        IconAsync(String image) {
            this.image = image;
        }

        @Override
        protected String doInBackground(String... strings) {
            final URL[] imageURL = new URL[1];

            try {
                if ("null".equals(image) || "None".equals(image) || "".equals(image)) {

                } else {
                    imageURL[0] = new URL(image);

                    HttpURLConnection conn = (HttpURLConnection) imageURL[0].openConnection();
                    BufferedInputStream bis = new BufferedInputStream(conn.getInputStream(), 1024);
                    bm = BitmapFactory.decodeStream(bis);
                    bis.close();

                }
            } catch (Exception e) {
                Log.e("search result adap", "image url excep", e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if ("null".equals(image) || "None".equals(image) || "".equals(image)) {
                        img_mypage_profile.setImageResource(R.drawable.profile);
                    } else {
                        img_mypage_profile.setImageBitmap(bm);
                    }
                    img_mypage_profile.invalidate();
                }
            });

        }
    }

    @Override
    public void onBackPressed() {
        setResult(ResultCodes.BACK);
        finish();
    }
}
