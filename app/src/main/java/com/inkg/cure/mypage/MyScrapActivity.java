package com.inkg.cure.mypage;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.inkg.cure.R;
import com.inkg.cure.adapters.ReviewAdapter;
import com.inkg.cure.classes.CureSP;
import com.inkg.cure.classes.HttpConnect;
import com.inkg.cure.classes.LoadingDialog;
import com.inkg.cure.classes.Review;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by inkg on 2017. 2. 22..
 */

public class MyScrapActivity extends AppCompatActivity {

    private ImageView img_my_scrap_default;
    private TextView txt_my_scrap_default;
    private ListView list_my_scrap;
    private ReviewAdapter scrapAdapter;
    private ArrayList<Review> scrap_list = new ArrayList<>();

    private HttpConnect httpConnect;
    private CureSP csp;
    private LoadingDialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_scrap);
        httpConnect = new HttpConnect(getApplicationContext());
        csp = new CureSP(getApplicationContext());
        loadingDialog = new LoadingDialog(MyScrapActivity.this);

        setupView();
        setCustomActionbar();

        ScrapAsync sa = new ScrapAsync();
        sa.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void setupView() {
        img_my_scrap_default = (ImageView) findViewById(R.id.img_my_scrap_default);
        txt_my_scrap_default = (TextView) findViewById(R.id.txt_my_scrap_default);
        list_my_scrap = (ListView) findViewById(R.id.list_my_scrap);
        scrapAdapter = new ReviewAdapter(getApplicationContext(), R.layout.adapter_review_read, scrap_list);
        list_my_scrap.setAdapter(scrapAdapter);
    }

    private void setCustomActionbar() {

        ActionBar actionBar = getSupportActionBar();

        // Custom Actionbar를 사용하기 위해 CustomEnabled을 true 시키고 필요 없는 것은 false 시킨다

        actionBar.setDisplayShowCustomEnabled(true);

        actionBar.setDisplayHomeAsUpEnabled(false);

        actionBar.setDisplayShowTitleEnabled(false);

        // Set custom view layout

        View mCustomView = LayoutInflater.from(this).inflate(R.layout.actionbar_simple, null);

        actionBar.setCustomView(mCustomView);


        // Set no padding both side

        Toolbar parent = (Toolbar) mCustomView.getParent(); // first get parent toolbar of current action bar

        parent.setContentInsetsAbsolute(0, 0);              // set padding programmatically to 0dp

        // Set actionbar background image

        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.border_layout_bottom, null));

        // Set actionbar layout layoutparams

        ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);

        actionBar.setCustomView(mCustomView, params);

        ImageView img_actionbar_back = (ImageView) findViewById(R.id.img_actionbar_back);
        img_actionbar_back.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    finish();
                }
                return false;
            }
        });

        TextView txt_actionbar = (TextView) findViewById(R.id.txt_actionbar_simple);
        txt_actionbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        txt_actionbar.setText("스크랩");
    }

    class ScrapAsync extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loadingDialog.show();
                }
            });
        }

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            result = httpConnect.send("", httpConnect.getServerWithVersion() + getString(R.string.rest_review_scrap), csp.getValue("access_token", ""), "GET");
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.e("result", result);
            try {
                scrap_list.clear();
                JSONObject review_json = new JSONObject(result);
                JSONArray items_json = review_json.getJSONArray("Items");
                for (int i = 0; i < items_json.length(); i++) {
                    JSONObject review_object = items_json.getJSONObject(i);
                    Review review = new Review();
                    review.setTarget(review_object.getString("target"));

                    String detail_result = "";
                    // detail
                    DetailAsync da = new DetailAsync();
                    detail_result = da.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, review.getTarget()).get();
                    Log.e("detail", detail_result);
                    JSONObject detail_object = new JSONObject(detail_result);

                    review.setType(detail_object.getString("type"));
                    review.setAge(detail_object.getString("age"));
                    review.setBad_point(detail_object.getString("bad_point"));
                    review.setDate(detail_object.getString("date"));
                    review.setEvaluation(detail_object.getString("evaluation"));
                    review.setGender(detail_object.getString("gender"));
                    review.setGood_point(detail_object.getString("good_point"));
                    review.setId(detail_object.getString("id"));
                    review.setLikes(detail_object.getString("likes"));
                    review.setMember(detail_object.getString("member"));
                    review.setNickname(detail_object.getString("nickname"));
                    review.setPeriod(detail_object.getString("period"));
//                    review.setPicture();
                    review.setProduct_name(detail_object.getString("product_name"));
                    review.setProfile(detail_object.getString("profile"));
                    review.setRepurchase(detail_object.getString("repurchase"));

                    review.setTip(detail_object.getString("tip"));
                    review.setViews(detail_object.getString("views"));

                    scrap_list.add(i, review);
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (scrap_list.size() == 0) {
                            img_my_scrap_default.setVisibility(View.VISIBLE);
                            txt_my_scrap_default.setVisibility(View.VISIBLE);
                            list_my_scrap.setVisibility(View.GONE);
                        }
                        scrapAdapter.notifyDataSetChanged();
                    }
                });

            } catch (Exception e) {
                Log.e("myscrap ac", "get scrap excep", e);
            } finally {
                loadingDialog.dismiss();
            }
        }
    }

    class DetailAsync extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            result = httpConnect.send("", httpConnect.getServerWithVersion() + getString(R.string.rest_review) + "?id=" + strings[0], csp.getValue("access_token", ""), "GET");
            return result;
        }
    }
}