package com.inkg.cure.mypage;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.inkg.cure.R;
import com.inkg.cure.adapters.FollowingAdapter;
import com.inkg.cure.classes.CureSP;
import com.inkg.cure.classes.Follow;
import com.inkg.cure.classes.HttpConnect;
import com.inkg.cure.classes.LoadingDialog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by inkg on 2017. 2. 22..
 */

public class MyFollowingActivity extends AppCompatActivity {

    private HttpConnect httpConnect;
    private CureSP csp;
    private ArrayList<Follow> following_list = new ArrayList<>();
    private FollowingAdapter followingAdapter;
    private ImageView img_my_following_default;
    private TextView txt_my_following_default;
    private ListView list_my_following;
    private LoadingDialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_following);
        httpConnect = new HttpConnect(getApplicationContext());
        csp = new CureSP(getApplicationContext());
        loadingDialog = new LoadingDialog(MyFollowingActivity.this);

        setCustomActionbar();

        FollowingAsync fa = new FollowingAsync();
        fa.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void setupViews() {
        img_my_following_default = (ImageView) findViewById(R.id.img_my_following_default);
        txt_my_following_default = (TextView) findViewById(R.id.txt_my_following_default);
        list_my_following = (ListView) findViewById(R.id.list_my_following);
        followingAdapter = new FollowingAdapter(getApplicationContext(), R.layout.adapter_my_following, following_list);
        list_my_following.setAdapter(followingAdapter);
    }

    private void setCustomActionbar() {

        ActionBar actionBar = getSupportActionBar();

        // Custom Actionbar를 사용하기 위해 CustomEnabled을 true 시키고 필요 없는 것은 false 시킨다

        actionBar.setDisplayShowCustomEnabled(true);

        actionBar.setDisplayHomeAsUpEnabled(false);

        actionBar.setDisplayShowTitleEnabled(false);

        // Set custom view layout

        View mCustomView = LayoutInflater.from(this).inflate(R.layout.actionbar_simple, null);

        actionBar.setCustomView(mCustomView);


        // Set no padding both side

        Toolbar parent = (Toolbar) mCustomView.getParent(); // first get parent toolbar of current action bar

        parent.setContentInsetsAbsolute(0, 0);              // set padding programmatically to 0dp

        // Set actionbar background image

        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.border_layout_bottom));

        // Set actionbar layout layoutparams

        ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);

        actionBar.setCustomView(mCustomView, params);

        ImageView img_actionbar_back = (ImageView) findViewById(R.id.img_actionbar_back);
        img_actionbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        TextView txt_actionbar = (TextView) findViewById(R.id.txt_actionbar_simple);
        txt_actionbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        txt_actionbar.setText("팔로잉");
    }

    class FollowingAsync extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loadingDialog.show();
                }
            });
        }

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            result = httpConnect.send("", httpConnect.getServerWithVersion() + getString(R.string.rest_follow) + "?view=following", csp.getValue("access_token", ""), "GET");
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.e("result", result);

            try {
                following_list = new ArrayList<>();
                JSONObject following_json = new JSONObject(result);
                JSONArray items_array = following_json.getJSONArray("Items");
                for (int i = 0; i < items_array.length(); i++) {
                    Follow follow = new Follow();
                    follow.setNickname(items_array.getJSONObject(i).getString("nickname"));
                    String target = items_array.getJSONObject(i).getString("target");
                    follow.setTarget(target);
                    follow.setProfile(getString(R.string.cdn) + getString(R.string.thumbnail_profile) + "/" + target + "/" + target);
                    following_list.add(i, follow);
                }
                setupViews();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (following_list.size() == 0) {
                            img_my_following_default.setVisibility(View.VISIBLE);
                            txt_my_following_default.setVisibility(View.VISIBLE);
                            list_my_following.setVisibility(View.GONE);
                        } else {
                            img_my_following_default.setVisibility(View.GONE);
                            txt_my_following_default.setVisibility(View.GONE);
                            list_my_following.setVisibility(View.VISIBLE);
                        }
                    }
                });
            } catch (Exception e) {
                Log.e("following ac", "get data excep", e);
            } finally {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loadingDialog.dismiss();
                    }
                });
            }
        }
    }
}
