package com.inkg.cure.mypage;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.inkg.cure.R;
import com.inkg.cure.adapters.MyPharmacyAdapter;
import com.inkg.cure.classes.CureSP;
import com.inkg.cure.classes.HttpConnect;
import com.inkg.cure.classes.Pharmacy;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by inkg on 2017. 2. 22..
 */

public class MyPharmacyActivity extends AppCompatActivity {

    private ImageView img_my_pharmacy_default;
    private TextView txt_my_pharmacy_default;
    private ListView list_my_pharmacy;
    private MyPharmacyAdapter myPharmacyAdapter;

    private HttpConnect httpConnect;
    private CureSP csp;

    private ArrayList<Pharmacy> like_list = new ArrayList<>();
    private ArrayList<Pharmacy> pharmacy_list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_pharmacy);
        httpConnect = new HttpConnect(getApplicationContext());
        csp = new CureSP(getApplicationContext());

        setCustomActionbar();
        setupViews();
        PharmacyAsync pa = new PharmacyAsync();
        pa.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }
    
    private void setupViews() {
        img_my_pharmacy_default = (ImageView) findViewById(R.id.img_my_pharmacy_default);
        txt_my_pharmacy_default = (TextView) findViewById(R.id.txt_my_pharmacy_default);
        list_my_pharmacy = (ListView) findViewById(R.id.list_my_pharmacy);
        myPharmacyAdapter = new MyPharmacyAdapter(getApplicationContext(), R.layout.adapter_my_pharmacy, pharmacy_list);
        list_my_pharmacy.setAdapter(myPharmacyAdapter);
    }

    private void setCustomActionbar() {

        ActionBar actionBar = getSupportActionBar();

        // Custom Actionbar를 사용하기 위해 CustomEnabled을 true 시키고 필요 없는 것은 false 시킨다

        actionBar.setDisplayShowCustomEnabled(true);

        actionBar.setDisplayHomeAsUpEnabled(false);

        actionBar.setDisplayShowTitleEnabled(false);

        // Set custom view layout

        View mCustomView = LayoutInflater.from(this).inflate(R.layout.actionbar_simple, null);

        actionBar.setCustomView(mCustomView);


        // Set no padding both side

        Toolbar parent = (Toolbar) mCustomView.getParent(); // first get parent toolbar of current action bar

        parent.setContentInsetsAbsolute(0, 0);              // set padding programmatically to 0dp

        // Set actionbar background image

        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.border_layout_bottom));

        // Set actionbar layout layoutparams

        ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);

        actionBar.setCustomView(mCustomView, params);

        ImageView img_actionbar_back = (ImageView) findViewById(R.id.img_actionbar_back);
        img_actionbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        TextView txt_actionbar = (TextView) findViewById(R.id.txt_actionbar_simple);
        txt_actionbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        txt_actionbar.setText("즐겨찾는 약국");
    }

    class PharmacyAsync extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            result = httpConnect.send("", httpConnect.getServerWithVersion() + getString(R.string.rest_pharmacy_me), csp.getValue("access_token", ""), "GET");
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                like_list = new ArrayList<>();
                JSONArray pharmacy_array = new JSONArray(result);
                for (int i = 0; i < pharmacy_array.length(); i++) {
                    JSONObject pharmacy_object = pharmacy_array.getJSONObject(i);
                    Pharmacy pharmacy = new Pharmacy();
                    pharmacy.setHpid(pharmacy_object.getString("target"));
                    pharmacy.setDutyname(pharmacy_object.getString("name"));

                    like_list.add(i, pharmacy);
                }
            } catch (Exception e) {
                Log.e("mypharmacy ac", "get json excep", e);
            }
            PharmacyDataAsync pda = new PharmacyDataAsync();
            pda.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    class PharmacyDataAsync extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            for (int i = 0; i < like_list.size(); i++) {
                String result = "";
                result = httpConnect.send("", httpConnect.getServerWithVersion() + getString(R.string.rest_pharmacy_detail) + "?id=" + like_list.get(i).getHpid(), csp.getValue("access_token", ""), "GET");

                Log.e("result", result);

                try {
                    JSONObject pharmacy_json = new JSONObject(result);
                    Pharmacy pharmacy = new Pharmacy();
                    pharmacy.setDutyaddr(pharmacy_json.getString("DUTYADDR"));
                    pharmacy.setDutyname(pharmacy_json.getString("DUTYNAME"));
                    pharmacy_list.add(i, pharmacy);
                } catch (Exception e) {
                    Log.e("asd", "myphar json", e);
                }
            }
            if (pharmacy_list.size() == 0) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        img_my_pharmacy_default.setVisibility(View.VISIBLE);
                        txt_my_pharmacy_default.setVisibility(View.VISIBLE);
                        list_my_pharmacy.setVisibility(View.GONE);
                        myPharmacyAdapter.notifyDataSetChanged();
                    }
                });
            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        img_my_pharmacy_default.setVisibility(View.GONE);
                        txt_my_pharmacy_default.setVisibility(View.GONE);
                        list_my_pharmacy.setVisibility(View.VISIBLE);
                        myPharmacyAdapter.notifyDataSetChanged();
                    }
                });
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
        }
    }

}
