package com.inkg.cure;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.inkg.cure.classes.ResultCodes;
import com.inkg.cure.mypage.MyNotificationActivity;
import com.inkg.cure.settings.AllianceActivity;
import com.inkg.cure.settings.AskActivity;
import com.inkg.cure.settings.EULAActivity;

/**
 * Created by inkg on 2016. 12. 25..
 */

public class SettingActivity extends AppCompatActivity {

    private Button btn_setting_logout;
    private Button btn_setting_version;
    private ToggleButton btn_setting_alarm;
    private Button btn_setting_faq;
    private Button btn_setting_ask;
    private Button btn_setting_alliance;
    private Button btn_setting_eula;
    private RelativeLayout layout_setting_alarm;
    private ImageView img_setting_alarm_blue;
    private TextView txt_setting_alarm_on;
    private ImageView img_setting_alarm_gray;
    private TextView txt_setting_alarm_off;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        setupViews();
        setCustomActionbar();
    }

    private void setupViews() {

        btn_setting_version = (Button) findViewById(R.id.btn_setting_version);
        btn_setting_alarm = (ToggleButton) findViewById(R.id.btn_setting_alarm);
        btn_setting_faq = (Button) findViewById(R.id.btn_setting_faq);
        btn_setting_ask = (Button) findViewById(R.id.btn_setting_ask);
        btn_setting_alliance = (Button) findViewById(R.id.btn_setting_alliance);
        btn_setting_eula = (Button) findViewById(R.id.btn_setting_eula);
        layout_setting_alarm = (RelativeLayout) findViewById(R.id.layout_setting_alarm);
        img_setting_alarm_blue = (ImageView) findViewById(R.id.img_setting_alarm_blue);
        txt_setting_alarm_on = (TextView) findViewById(R.id.txt_setting_alarm_on);
        img_setting_alarm_gray = (ImageView) findViewById(R.id.img_setting_alarm_gray);
        txt_setting_alarm_off = (TextView) findViewById(R.id.txt_setting_alarm_off);

        final LayoutInflater title_inflater = (LayoutInflater) getApplicationContext().getSystemService(LAYOUT_INFLATER_SERVICE);

        btn_setting_version.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(SettingActivity.this);
                View title_view = title_inflater.inflate(R.layout.dialog_custom_title, null);
                TextView txt_dialog_title = (TextView) title_view.findViewById(R.id.txt_dialog_custom_title);
                alertDialog.setCustomTitle(title_view);
                txt_dialog_title.setText("버전정보");
                alertDialog.setMessage("현재 버전 : " + getString(R.string.cureya_version) + "\n\n" + "최신 버전 : " + getString(R.string.cureya_version));
                alertDialog.setPositiveButton("확인", null);
                alertDialog.show();
            }
        });

        btn_setting_alarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (btn_setting_alarm.isChecked()) { // agree behavior
                    layout_setting_alarm.setBackground(ContextCompat.getDrawable(SettingActivity.this, R.drawable.round_blue_btn));
                    img_setting_alarm_blue.setVisibility(View.VISIBLE);
                    txt_setting_alarm_on.setVisibility(View.VISIBLE);
                    img_setting_alarm_gray.setVisibility(View.INVISIBLE);
                    txt_setting_alarm_off.setVisibility(View.INVISIBLE);
                } else { // disagree behavior
                    layout_setting_alarm.setBackground(ContextCompat.getDrawable(SettingActivity.this, R.drawable.round_gray_btn));
                    img_setting_alarm_blue.setVisibility(View.INVISIBLE);
                    txt_setting_alarm_on.setVisibility(View.INVISIBLE);
                    img_setting_alarm_gray.setVisibility(View.VISIBLE);
                    txt_setting_alarm_off.setVisibility(View.VISIBLE);
                }
            }
        });

        btn_setting_faq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        btn_setting_ask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ask_intent = new Intent(SettingActivity.this, AskActivity.class);
                startActivity(ask_intent);
            }
        });

        btn_setting_alliance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent alliance_intent = new Intent(SettingActivity.this, AllianceActivity.class);
                startActivity(alliance_intent);
            }
        });

        btn_setting_eula.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent eula_intent = new Intent(SettingActivity.this, EULAActivity.class);
                startActivity(eula_intent);
            }
        });

        btn_setting_logout = (Button) findViewById(R.id.btn_setting_logout);
        btn_setting_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(SettingActivity.this);
                View title_view = title_inflater.inflate(R.layout.dialog_custom_title, null);
                TextView txt_dialog_title = (TextView) title_view.findViewById(R.id.txt_dialog_custom_title);
                alertDialog.setCustomTitle(title_view);
                txt_dialog_title.setText("로그아웃");
                alertDialog.setMessage("로그아웃 하시겠습니까?");
                alertDialog.setPositiveButton("취소", null);
                alertDialog.setNegativeButton("확인", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        setResult(ResultCodes.CURE_LOGOUT);
                        finish();
                    }
                });
                alertDialog.show();
            }
        });

    }

    private void setCustomActionbar() {

        ActionBar actionBar = getSupportActionBar();

        // Custom Actionbar를 사용하기 위해 CustomEnabled을 true 시키고 필요 없는 것은 false 시킨다

        actionBar.setDisplayShowCustomEnabled(true);

        actionBar.setDisplayHomeAsUpEnabled(false);

        actionBar.setDisplayShowTitleEnabled(false);

        // Set custom view layout

        View mCustomView = LayoutInflater.from(this).inflate(R.layout.actionbar_mypage, null);

        actionBar.setCustomView(mCustomView);


        // Set no padding both side

        Toolbar parent = (Toolbar) mCustomView.getParent(); // first get parent toolbar of current action bar

        parent.setContentInsetsAbsolute(0, 0);              // set padding programmatically to 0dp

        // Set actionbar background image

        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.border_layout_bottom));

        // Set actionbar layout layoutparams

        ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);

        actionBar.setCustomView(mCustomView, params);

        ImageView img_actionbar_back = (ImageView) findViewById(R.id.img_actionbar_mypage_back);
        img_actionbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        TextView txt_actionbar = (TextView) findViewById(R.id.txt_actionbar_mypage);
        txt_actionbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        txt_actionbar.setText(getString(R.string.title_setting));

        Button btn_actionbar_setting = (Button) findViewById(R.id.btn_actionbar_setting);
        btn_actionbar_setting.setVisibility(View.GONE);

        Button btn_actionbar_alarm = (Button) findViewById(R.id.btn_actionbar_alarm);
        btn_actionbar_alarm.setBackground(getResources().getDrawable(R.drawable.alarm, null));
        btn_actionbar_alarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent noti_intent = new Intent(SettingActivity.this, MyNotificationActivity.class);
                startActivity(noti_intent);
            }
        });

    }

}
