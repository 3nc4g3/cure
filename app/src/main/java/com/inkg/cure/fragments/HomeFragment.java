package com.inkg.cure.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ScrollView;

import com.inkg.cure.R;
import com.inkg.cure.classes.ShareDialog;
import com.inkg.cure.search.CategorySearchActivity;

/**
 * Created by inkg on 2016. 11. 24..
 */

public class HomeFragment extends Fragment implements View.OnClickListener {

    private View parentView;

    private Button btn_main_flu;
    private Button btn_main_pain;
    private Button btn_main_stomach;
    private Button btn_main_intestine;
    private Button btn_main_liver;
    private Button btn_main_bone;
    private Button btn_main_pregnant;
    private Button btn_main_baby;
    private Button btn_main_eye;
    private Button btn_main_face;
    private Button btn_main_vitamin;
    private Button btn_main_blood;
    private Button btn_main_skin;
    private Button btn_main_hair;
    private Button btn_main_female;
    private Button btn_main_male;
    private ImageView img_main_flu;
    private ImageView img_main_pain;
    private ImageView img_main_stomach;
    private ImageView img_main_intestine;
    private ImageView img_main_liver;
    private ImageView img_main_bone;
    private ImageView img_main_pregnant;
    private ImageView img_main_baby;
    private ImageView img_main_eye;
    private ImageView img_main_face;
    private ImageView img_main_vitamin;
    private ImageView img_main_blood;
    private ImageView img_main_skin;
    private ImageView img_main_hair;
    private ImageView img_main_female;
    private ImageView img_main_male;
    private FrameLayout img_main_recommend;
    private FrameLayout img_main_praise;
    private ScrollView scroll_home;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (parentView == null) {
            parentView = inflater.inflate(R.layout.fragment_home, container, false);
        }
        SetupViewAsync sva = new SetupViewAsync();
        sva.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        return parentView;
    }

    class SetupViewAsync extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            setupView();
            return null;
        }
    }

    private void setupView() {
        scroll_home = (ScrollView) parentView.findViewById(R.id.scroll_home);
        img_main_flu = (ImageView) parentView.findViewById(R.id.img_main_flu);
        img_main_pain = (ImageView) parentView.findViewById(R.id.img_main_pain);
        img_main_stomach = (ImageView) parentView.findViewById(R.id.img_main_stomach);
        img_main_intestine = (ImageView) parentView.findViewById(R.id.img_main_intestine);
        img_main_liver = (ImageView) parentView.findViewById(R.id.img_main_liver);
        img_main_bone = (ImageView) parentView.findViewById(R.id.img_main_bone);
        img_main_pregnant = (ImageView) parentView.findViewById(R.id.img_main_pregnant);
        img_main_baby = (ImageView) parentView.findViewById(R.id.img_main_baby);
        img_main_eye = (ImageView) parentView.findViewById(R.id.img_main_eye);
        img_main_face = (ImageView) parentView.findViewById(R.id.img_main_face);
        img_main_vitamin = (ImageView) parentView.findViewById(R.id.img_main_vitamin);
        img_main_blood = (ImageView) parentView.findViewById(R.id.img_main_blood);
        img_main_skin = (ImageView) parentView.findViewById(R.id.img_main_skin);
        img_main_hair = (ImageView) parentView.findViewById(R.id.img_main_hair);
        img_main_female = (ImageView) parentView.findViewById(R.id.img_main_female);
        img_main_male = (ImageView) parentView.findViewById(R.id.img_main_male);
        img_main_recommend = (FrameLayout) parentView.findViewById(R.id.img_main_recommend);
        img_main_praise = (FrameLayout) parentView.findViewById(R.id.img_main_praise);


        scroll_home.setSmoothScrollingEnabled(true);
        btn_main_flu = (Button) parentView.findViewById(R.id.btn_main_flu);
        btn_main_pain = (Button) parentView.findViewById(R.id.btn_main_pain);
        btn_main_stomach = (Button) parentView.findViewById(R.id.btn_main_stomach);
        btn_main_intestine = (Button) parentView.findViewById(R.id.btn_main_intestine);
        btn_main_liver = (Button) parentView.findViewById(R.id.btn_main_liver);
        btn_main_bone = (Button) parentView.findViewById(R.id.btn_main_bone);
        btn_main_pregnant = (Button) parentView.findViewById(R.id.btn_main_pregnant);
        btn_main_baby = (Button) parentView.findViewById(R.id.btn_main_baby);
        btn_main_eye = (Button) parentView.findViewById(R.id.btn_main_eye);
        btn_main_face = (Button) parentView.findViewById(R.id.btn_main_face);
        btn_main_vitamin = (Button) parentView.findViewById(R.id.btn_main_vitamin);
        btn_main_blood = (Button) parentView.findViewById(R.id.btn_main_blood);
        btn_main_skin = (Button) parentView.findViewById(R.id.btn_main_skin);
        btn_main_hair = (Button) parentView.findViewById(R.id.btn_main_hair);
        btn_main_female = (Button) parentView.findViewById(R.id.btn_main_female);
        btn_main_male = (Button) parentView.findViewById(R.id.btn_main_male);

        btn_main_flu.setOnClickListener(this);
        btn_main_pain.setOnClickListener(this);
        btn_main_stomach.setOnClickListener(this);
        btn_main_intestine.setOnClickListener(this);
        btn_main_liver.setOnClickListener(this);
        btn_main_bone.setOnClickListener(this);
        btn_main_pregnant.setOnClickListener(this);
        btn_main_baby.setOnClickListener(this);
        btn_main_eye.setOnClickListener(this);
        btn_main_face.setOnClickListener(this);
        btn_main_vitamin.setOnClickListener(this);
        btn_main_blood.setOnClickListener(this);
        btn_main_skin.setOnClickListener(this);
        btn_main_hair.setOnClickListener(this);
        btn_main_female.setOnClickListener(this);
        btn_main_male.setOnClickListener(this);
        img_main_recommend.setOnClickListener(this);
        img_main_praise.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        Intent category_search_intent = new Intent(getActivity(), CategorySearchActivity.class);

        switch (view.getId()) {
            case R.id.btn_main_flu:
                category_search_intent.putExtra("category", "감기/호흡");
                break;
            case R.id.btn_main_pain:
                category_search_intent.putExtra("category", "통증");
                break;
            case R.id.btn_main_stomach:
                category_search_intent.putExtra("category", "위");
                break;
            case R.id.btn_main_intestine:
                category_search_intent.putExtra("category", "대장/항문");
                break;
            case R.id.btn_main_liver:
                category_search_intent.putExtra("category", "간");
                break;
            case R.id.btn_main_bone:
                category_search_intent.putExtra("category", "관절/뼈");
                break;
            case R.id.btn_main_pregnant:
                category_search_intent.putExtra("category", "임산부");
                break;
            case R.id.btn_main_baby:
                category_search_intent.putExtra("category", "어린이");
                break;
            case R.id.btn_main_eye:
                category_search_intent.putExtra("category", "눈");
                break;
            case R.id.btn_main_face:
                category_search_intent.putExtra("category", "코입귀");
                break;
            case R.id.btn_main_vitamin:
                category_search_intent.putExtra("category", "비타민영양제");
                break;
            case R.id.btn_main_blood:
                category_search_intent.putExtra("category", "혈액순환");
                break;
            case R.id.btn_main_skin:
                category_search_intent.putExtra("category", "피부");
                break;
            case R.id.btn_main_hair:
                category_search_intent.putExtra("category", "탈모");
                break;
            case R.id.btn_main_female:
                category_search_intent.putExtra("category", "여성");
                break;
            case R.id.btn_main_male:
                category_search_intent.putExtra("category", "남성");
                break;
//            case R.id.img_main_recommend:
//                category_search_intent.putExtra("category", "위");
//                break;
//            case R.id.img_main_praise:
//                category_search_intent.putExtra("category", "위");
//                break;
            default:
                break;
        }

        if (view.getId() == R.id.img_main_recommend) {
            ShareDialog sd = new ShareDialog(getActivity());
            sd.setupDialog("큐어야 추천하기", "건강과 행복의 연결고리 큐어야!", "https://scontent.xx.fbcdn.net/v/t1.0-9/17103772_1440708652665732_1129909667651037500_n.png?oh=ac7dd151f2b74bbd2f2cff0fd87e68c0&oe=592DA37E", "https://www.facebook.com/%ED%81%90%EC%96%B4%EC%95%BC-1432584283478169/?ref=nf");
            sd.show();
        } else if (view.getId() == R.id.img_main_praise) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getActivity().getApplicationContext().getPackageName()));
            startActivity(intent);

        } else {
            startActivity(category_search_intent);
        }

    }
}