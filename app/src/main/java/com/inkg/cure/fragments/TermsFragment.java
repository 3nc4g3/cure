package com.inkg.cure.fragments;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.inkg.cure.R;
import com.inkg.cure.classes.HttpConnect;

/**
 * Created by inkg on 2017. 2. 4..
 */

public class TermsFragment extends Fragment {

    private View parentView;
    private HttpConnect httpConnect;
    private TextView txt_terms;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        parentView = inflater.inflate(R.layout.fragment_service, container, false);
        httpConnect = new HttpConnect(getActivity().getApplicationContext());
        setupViews();
        return parentView;
    }

    private void setupViews() {
        txt_terms = (TextView) parentView.findViewById(R.id.txt_terms);

        TermsAsync ta = new TermsAsync();
        ta.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    class TermsAsync extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            result = httpConnect.send("", httpConnect.getServerWithVersion() + getString(R.string.rest_terms), "", "GET");
            result = Html.fromHtml(result).toString();
            return result;
        }

        @Override
        protected void onPostExecute(final String result) {
            super.onPostExecute(result);
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    txt_terms.setText(result);
                }
            });
        }
    }
}