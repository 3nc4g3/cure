package com.inkg.cure.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.inkg.cure.DurActivity;
import com.inkg.cure.IngrCategory1Activity;
import com.inkg.cure.IngrCategory2Activity;
import com.inkg.cure.IngrCategory3Activity;
import com.inkg.cure.R;

/**
 * Created by inkg on 2016. 11. 24..
 */

public class IngredientFragment extends Fragment {

    private View parentView;
    private Button btn_ingr_kids; // 1
    private Button btn_ingr_medicine; // 1
    private Button btn_ingr_combination; // 1
    private Button btn_ingr_contains; // 1
    private Button btn_ingr_dur; // 2
    private Button btn_ingr_ensemble; // 2
    private Intent startIntent;

    private Button btn_ingr_mix;
    private Button btn_ingr_baby;
    private Button btn_ingr_pregnant;
    private Button btn_ingr_contain;
    private Button btn_ingr_period;
    private Button btn_ingr_old;
    private Button btn_ingr_blood;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        parentView = inflater.inflate(R.layout.fragment_ingredient, container, false);
        setupView();
        return parentView;
    }

    private void setupView() {

        btn_ingr_mix = (Button) parentView.findViewById(R.id.btn_ingr_mix);
        btn_ingr_baby = (Button) parentView.findViewById(R.id.btn_ingr_baby);
        btn_ingr_pregnant = (Button) parentView.findViewById(R.id.btn_ingr_pregnant);
        btn_ingr_contain = (Button) parentView.findViewById(R.id.btn_ingr_contain);
        btn_ingr_period = (Button) parentView.findViewById(R.id.btn_ingr_period);
        btn_ingr_old = (Button) parentView.findViewById(R.id.btn_ingr_old);
        btn_ingr_blood = (Button) parentView.findViewById(R.id.btn_ingr_blood);

        btn_ingr_mix.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent dur_intent = new Intent(getActivity(), DurActivity.class);
                dur_intent.putExtra("keyword", "mix");
                startActivity(dur_intent);
            }
        });
        
        btn_ingr_baby.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent dur_intent = new Intent(getActivity(), DurActivity.class);
                dur_intent.putExtra("keyword", "baby");
                startActivity(dur_intent);
            }
        });
        btn_ingr_pregnant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent dur_intent = new Intent(getActivity(), DurActivity.class);
                dur_intent.putExtra("keyword", "pregnant");
                startActivity(dur_intent);
            }
        });
        btn_ingr_contain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent dur_intent = new Intent(getActivity(), DurActivity.class);
                dur_intent.putExtra("keyword", "contain");
                startActivity(dur_intent);
            }
        });
        btn_ingr_period.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent dur_intent = new Intent(getActivity(), DurActivity.class);
                dur_intent.putExtra("keyword", "period");
                startActivity(dur_intent);
            }
        });
        btn_ingr_old.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent dur_intent = new Intent(getActivity(), DurActivity.class);
                dur_intent.putExtra("keyword", "old");
                startActivity(dur_intent);
            }
        });
        btn_ingr_blood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent dur_intent = new Intent(getActivity(), DurActivity.class);
                dur_intent.putExtra("keyword", "blood");
                startActivity(dur_intent);
            }
        });


//        // category 1
//        btn_ingr_kids = (Button) parentView.findViewById(R.id.btn_ingr_kids);
//        btn_ingr_kids.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                startIntent = new Intent(getActivity(), IngrCategory1Activity.class);
//                startIntent.putExtra("component", "kids");
//                startActivity(startIntent);
//            }
//        });
//        btn_ingr_old = (Button) parentView.findViewById(R.id.btn_ingr_old);
//        btn_ingr_old.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                startIntent = new Intent(getActivity(), IngrCategory1Activity.class);
//                startIntent.putExtra("component", "old");
//                startActivity(startIntent);
//            }
//        });
//
//        btn_ingr_medicine = (Button) parentView.findViewById(R.id.btn_ingr_medicine);
//        btn_ingr_medicine.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                startIntent = new Intent(getActivity(), IngrCategory1Activity.class);
//                startIntent.putExtra("component", "medicine");
//                startActivity(startIntent);
//            }
//        });
//
//        btn_ingr_combination = (Button) parentView.findViewById(R.id.btn_ingr_combination);
//        btn_ingr_combination.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                startIntent = new Intent(getActivity(), IngrCategory1Activity.class);
//                startIntent.putExtra("component", "combination");
//                startActivity(startIntent);
//            }
//        });
//
//        btn_ingr_contains = (Button) parentView.findViewById(R.id.btn_ingr_contains);
//        btn_ingr_contains.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                startIntent = new Intent(getActivity(), IngrCategory1Activity.class);
//                startIntent.putExtra("component", "contains");
//                startActivity(startIntent);
//            }
//        });
//
//        btn_ingr_period = (Button) parentView.findViewById(R.id.btn_ingr_period);
//        btn_ingr_period.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                startIntent = new Intent(getActivity(), IngrCategory1Activity.class);
//                startIntent.putExtra("component", "period");
//                startActivity(startIntent);
//            }
//        });
//
//
//        // category 2
//        btn_ingr_dur = (Button) parentView.findViewById(R.id.btn_ingr_dur);
//        btn_ingr_dur.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                startIntent = new Intent(getActivity(), IngrCategory2Activity.class);
//                startIntent.putExtra("component", "dur");
//                startActivity(startIntent);
//            }
//        });
//
//        btn_ingr_ensemble = (Button) parentView.findViewById(R.id.btn_ingr_ensemble);
//        btn_ingr_ensemble.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                startIntent = new Intent(getActivity(), IngrCategory2Activity.class);
//                startIntent.putExtra("component", "ensemble");
//                startActivity(startIntent);
//            }
//        });
//
//        // category 3
//        btn_ingr_pregnant = (Button) parentView.findViewById(R.id.btn_ingr_pregnant);
//        btn_ingr_pregnant.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                startIntent = new Intent(getActivity(), IngrCategory3Activity.class);
//                startIntent.putExtra("component", "pregnant");
//                startActivity(startIntent);
//            }
//        });
    }
}