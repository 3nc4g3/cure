package com.inkg.cure.fragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.util.Pair;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.inkg.cure.R;
import com.inkg.cure.adapters.PharmacySearchAdapter;
import com.inkg.cure.adapters.ShareAdapter;
import com.inkg.cure.classes.Addr;
import com.inkg.cure.classes.CureSP;
import com.inkg.cure.classes.HttpConnect;
import com.inkg.cure.classes.JsonMaker;
import com.inkg.cure.classes.Pharmacy;
import com.inkg.cure.classes.ShareDialog;

import net.daum.android.map.util.URLEncoder;
import net.daum.mf.map.api.MapPOIItem;
import net.daum.mf.map.api.MapPoint;
import net.daum.mf.map.api.MapView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ExecutionException;

import static android.content.Context.LOCATION_SERVICE;

/**
 * Created by inkg on 2016. 11. 24..
 */

public class PharmacyFragment extends Fragment implements MapView.MapViewEventListener, MapView.POIItemEventListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private View parentView;
    private String mLongitude;
    private String mLatitude;
    private MapView mapView;
    private LocationRequest mLocationRequest;
    private Location mLastLocation;
    private MapPOIItem user_marker;
    private ArrayList<MapPOIItem> marker_list = new ArrayList<>();

    private HttpConnect httpConnect;
    private JsonMaker jsonMaker;

    private String access_token;
    private CureSP csp;

    private RelativeLayout view_map;
    private FloatingActionButton fab_pharmacy_call;
    private RelativeLayout layout_pharmacy_below;
    private RelativeLayout layout_pharmacy_search_result;
    private RelativeLayout layout_pharmacy_search_now;
    private EditText edit_pharamacy_search;
    private Button btn_pharmacy_reset;

    private GoogleApiClient mGoogleApiClient;

    private ArrayList<Pharmacy> pharmacy_list = new ArrayList<>();

    private Button btn_pharmacy_status;
    private Button btn_pharmacy_share;
    private TextView txt_pharmacy_address_road;
    private TextView txt_pharmacy_address_lot;
    private TextView txt_pharmacy_start;
    private TextView txt_pharmacy_close;
    private TextView txt_pharmacy_like_count;
    private TextView txt_pharmacy_name;
    private Button btn_pharmacy_like;
    private Button btn_pharmacy_next;
    private Button btn_pharmacy_previous;

    private TextView txt_pharmacy_search_now;
    private Button btn_pharmacy_search_now;
    private ListView list_pharmacy;
    private PharmacySearchAdapter pharmacySearchAdapter;

    private String current_call_number = "";
    private String current_pharmacy_name = "";
    private boolean current_like = false;
    private String current_hpid = "";
    private int current_tag = 0;
    private String current_addr = "";

    private String target_lat = "";
    private String target_lng = "";

    private ArrayList<Addr> addr_list = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        parentView = inflater.inflate(R.layout.fragment_pharmacy, container, false);
        csp = new CureSP(getActivity().getApplicationContext());
        httpConnect = new HttpConnect(getActivity());
        jsonMaker = new JsonMaker();
        buildGoogleApiClient();
        access_token = csp.getValue("access_token", "");
        setupView();
        return parentView;
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible) { // screen up
            LocationSettingAsync lsa = new LocationSettingAsync();
            lsa.execute();
        } else { // screen down
            if (mGoogleApiClient != null)
                mGoogleApiClient.disconnect();
        }
    }

    private void setupView() {
        // normal status layouts
        view_map = (RelativeLayout) parentView.findViewById(R.id.view_map);
        fab_pharmacy_call = (FloatingActionButton) parentView.findViewById(R.id.fab_pharmacy_call);
        layout_pharmacy_below = (RelativeLayout) parentView.findViewById(R.id.layout_pharmacy_below);
        btn_pharmacy_status = (Button) parentView.findViewById(R.id.btn_pharmacy_status);
        btn_pharmacy_share = (Button) parentView.findViewById(R.id.btn_pharmacy_share);
        txt_pharmacy_address_road = (TextView) parentView.findViewById(R.id.txt_pharmacy_address_road);
        txt_pharmacy_address_lot = (TextView) parentView.findViewById(R.id.txt_pharmacy_address_lot);
        txt_pharmacy_start = (TextView) parentView.findViewById(R.id.txt_pharmacy_start);
        txt_pharmacy_close = (TextView) parentView.findViewById(R.id.txt_pharmacy_close);
        txt_pharmacy_like_count = (TextView) parentView.findViewById(R.id.txt_pharmacy_like_count);
        btn_pharmacy_like = (Button) parentView.findViewById(R.id.btn_pharmacy_like);
        txt_pharmacy_name = (TextView) parentView.findViewById(R.id.txt_pharmacy_name);
        btn_pharmacy_previous = (Button) parentView.findViewById(R.id.btn_pharmacy_previous);
        btn_pharmacy_next = (Button) parentView.findViewById(R.id.btn_pharmacy_next);
        list_pharmacy = (ListView) parentView.findViewById(R.id.list_pharmacy);
        list_pharmacy.setItemsCanFocus(false);
        pharmacySearchAdapter = new PharmacySearchAdapter(getActivity(), R.layout.adapter_pharmacy_search_result, addr_list);
        list_pharmacy.setAdapter(pharmacySearchAdapter);

        btn_pharmacy_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!"".equals(current_pharmacy_name)) {
                    if (current_like) { // current like
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                int like_count = Integer.valueOf(txt_pharmacy_like_count.getText().toString());
                                txt_pharmacy_like_count.setText(String.valueOf(like_count - 1));
                            }
                        });

                        PharmacyLikeButtonAsync plba = new PharmacyLikeButtonAsync("DELETE");
                        plba.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                        current_like = !current_like;

                    } else { // dislike
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                int like_count = Integer.valueOf(txt_pharmacy_like_count.getText().toString());
                                txt_pharmacy_like_count.setText(String.valueOf(like_count + 1));
                            }
                        });

                        PharmacyLikeButtonAsync plba = new PharmacyLikeButtonAsync("POST");
                        plba.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                        current_like = !current_like;
                    }
                }
            }
        });

        final ShareAdapter share_adapter = new ShareAdapter(getActivity(), R.layout.dialog_share, pharmacy_list);
        final LayoutInflater title_inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        btn_pharmacy_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShareDialog sd = new ShareDialog(getActivity());
                sd.setupDialog(current_pharmacy_name, current_addr + "\n" + current_call_number, "", "");
                sd.show();
            }
        });

        fab_pharmacy_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!"".equals(current_call_number)) {
                    AlertDialog.Builder call_dialog = new AlertDialog.Builder(getActivity());
                    call_dialog.setTitle(current_pharmacy_name);
                    call_dialog.setMessage(current_call_number);
                    call_dialog.setPositiveButton("전화걸기", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
//                            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + current_call_number));
//                            startActivity(intent);

                            PermissionListener permissionListener = new PermissionListener() {
                                @Override
                                public void onPermissionGranted() {
                                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + current_call_number));
                                    startActivity(intent);
                                }

                                @Override
                                public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                                    Log.e("permission denied", "yes");
                                }
                            };

                            new TedPermission(getActivity())
                                    .setPermissionListener(permissionListener)
                                    .setRationaleMessage("전화 기능을 사용하시기 위해서는 전화걸기 권한이 필요합니다")
                                    .setDeniedMessage("전화 기능을 사용하시기 위해서는 전화걸기 권한이 필요합니다. [설정] > [권한] 에서 전화걸기 권한을 허용하실 수 있습니다.")
                                    .setPermissions(Manifest.permission.CALL_PHONE)
                                    .setGotoSettingButton(true)
                                    .setGotoSettingButtonText("설정")
                                    .check();
                        }
                    });
                    call_dialog.setNegativeButton("취소", null);
                    call_dialog.show();
                }
            }
        });

        // pharmacy search phase
        layout_pharmacy_search_result = (RelativeLayout) parentView.findViewById(R.id.layout_pharmacy_search_result);
        layout_pharmacy_search_now = (RelativeLayout) parentView.findViewById(R.id.layout_pharmacy_search_now);
        btn_pharmacy_search_now = (Button) parentView.findViewById(R.id.btn_pharmacy_search_now);
        btn_pharmacy_search_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                target_lat = "";
                target_lng = "";
                edit_pharamacy_search.setText("");
                view_map.setVisibility(View.VISIBLE);
                mapView.refreshMapTiles();
                fab_pharmacy_call.setVisibility(View.VISIBLE);
                layout_pharmacy_below.setVisibility(View.VISIBLE);
                layout_pharmacy_search_result.setVisibility(View.GONE);
                layout_pharmacy_search_now.setVisibility(View.GONE);
                edit_pharamacy_search.clearFocus();
                addr_list.clear();
                pharmacySearchAdapter.notifyDataSetChanged();
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);

                LocationSettingAsync lsa = new LocationSettingAsync();
                lsa.execute();
            }
        });

        // MapView 객체생성 및 API Key 설정
        mapView = new MapView(getContext());
        mapView.setDaumMapApiKey(getResources().getString(R.string.daum_android_API_key));
        mapView.setPOIItemEventListener(PharmacyFragment.this);
        mapView.setMapViewEventListener(PharmacyFragment.this);

        user_marker = new MapPOIItem();

        view_map.addView(mapView);

        edit_pharamacy_search = (EditText) parentView.findViewById(R.id.edit_pharmacy_search);
        edit_pharamacy_search.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                view_map.setVisibility(View.GONE);
                fab_pharmacy_call.setVisibility(View.GONE);
                layout_pharmacy_below.setVisibility(View.GONE);
                layout_pharmacy_search_result.setVisibility(View.VISIBLE);
                layout_pharmacy_search_now.setVisibility(View.VISIBLE);
                return false;
            }
        });

        edit_pharamacy_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                switch (actionId) {
                    case EditorInfo.IME_ACTION_SEARCH:
                        String query = "";
                        query = edit_pharamacy_search.getText().toString();
                        if ("".equals(query)) {
                            Toast.makeText(getActivity(), "검색어를 입력해주세요.", Toast.LENGTH_SHORT).show();
                        } else {
                            target_lat = "";
                            target_lng = "";
                            try {
                                SearchAsync sa = new SearchAsync();
                                sa.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, URLEncoder.encode(query.toString().getBytes()));
                            } catch (Exception e) {
                                Log.e("pharmacy search", "excep", e);
                            }
                        }
                        break;

                    default:
                        return false;
                }
                return true;
            }
        });

        btn_pharmacy_reset = (Button) parentView.findViewById(R.id.btn_pharmacy_reset);
        btn_pharmacy_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edit_pharamacy_search.setText("");
                view_map.setVisibility(View.VISIBLE);
                mapView.refreshMapTiles();
                fab_pharmacy_call.setVisibility(View.VISIBLE);
                layout_pharmacy_below.setVisibility(View.VISIBLE);
                layout_pharmacy_search_result.setVisibility(View.GONE);
                layout_pharmacy_search_now.setVisibility(View.GONE);
                edit_pharamacy_search.clearFocus();
                addr_list.clear();
                pharmacySearchAdapter.notifyDataSetChanged();

                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
            }
        });

        btn_pharmacy_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (current_tag <= 0) {

                } else {
                    if (current_tag == 0) {

                    } else {
                        int index = current_tag;
                        if (index == 1) { // marker index 1 -> marker last index
                            mapView.selectPOIItem(marker_list.get(marker_list.size() - 1), true);
                            onPOIItemSelected(mapView, marker_list.get(marker_list.size() - 1));
                        } else { // tag == index
                            index = index - 1;
                            mapView.selectPOIItem(marker_list.get(index), true);
                            onPOIItemSelected(mapView, marker_list.get(index));
                        }
                    }
                }
            }
        });

        btn_pharmacy_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (current_tag > marker_list.size()) {

                } else {
                    if (current_tag == 0) {

                    } else {
                        int index = current_tag;
                        if (index == marker_list.size() - 1) { // marker last index -> marker index 1
                            mapView.selectPOIItem(marker_list.get(1), true);
                            onPOIItemSelected(mapView, marker_list.get(1));
                        } else {
                            index = index + 1;
                            mapView.selectPOIItem(marker_list.get(index), true);
                            onPOIItemSelected(mapView, marker_list.get(index));
                        }
                    }
                }
            }
        });
    }

    class SearchAsync extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            result = httpConnect.send("", getString(R.string.rest_daum_map) + "?apikey=" + getString(R.string.daum_API_key) + "&q=" + strings[0].toString() + "&output=json", "", "GET");
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                JSONObject daum_json = new JSONObject(result);
                JSONObject channel_json = daum_json.getJSONObject("channel");
                int total_count = 0;
                total_count = Integer.valueOf(channel_json.getString("totalCount"));
                if (total_count == 0) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getActivity(), "결과가 없습니다. 주소를 조금 더 정확히 입력해주세요.", Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {
                    addr_list.clear();
                    JSONArray item_array = channel_json.getJSONArray("item");
                    for (int i = 0; i < item_array.length(); i++) {
                        JSONObject item_object = item_array.getJSONObject(i);
                        Addr addr = new Addr();
                        addr.setTitle(item_object.getString("title"));
                        addr.setBuildingAddress(item_object.getString("buildingAddress"));
                        addr.setLat(item_object.getString("lat"));
                        addr.setLng(item_object.getString("lng"));

                        addr_list.add(i, addr);
                    }
                    pharmacySearchAdapter.notifyDataSetChanged();
                    setupAddrView();
                }
            } catch (Exception e) {
                Log.e("phar fr", "get ordinate excep", e);
            }
        }
    }

    private void setupAddrView() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                layout_pharmacy_search_result.setVisibility(View.VISIBLE);
                list_pharmacy.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                        target_lat = addr_list.get(position).getLat();
                        target_lng = addr_list.get(position).getLng();
                        Log.e("lat, lng", target_lat + " / " + target_lng);

                        edit_pharamacy_search.setText("");
                        view_map.setVisibility(View.VISIBLE);
                        mapView.refreshMapTiles();
                        fab_pharmacy_call.setVisibility(View.VISIBLE);
                        layout_pharmacy_below.setVisibility(View.VISIBLE);
                        layout_pharmacy_search_result.setVisibility(View.GONE);
                        layout_pharmacy_search_now.setVisibility(View.GONE);
                        edit_pharamacy_search.clearFocus();
                        addr_list.clear();
                        pharmacySearchAdapter.notifyDataSetChanged();

                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);

                        LocationSettingAsync la = new LocationSettingAsync();
                        la.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                });
            }
        });
    }


    private class LocationSettingAsync extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            PermissionListener permissionListener = new PermissionListener() {
                @Override
                public void onPermissionGranted() {
                    // Toast.makeText(getActivity(), "위치 권한이 승인되었습니다. 현 위치 약국찾기 서비스를 사용하실 수 있습니다.", Toast.LENGTH_SHORT).show();
                    mGoogleApiClient.connect();
                }

                @Override
                public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                    Log.e("permission denied", "yes");
                }
            };

            new TedPermission(getActivity())
                    .setPermissionListener(permissionListener)
                    .setRationaleMessage("주변 약국찾기 서비스를 사용하기 위해서는 위치 접근 권한이 필요합니다")
                    .setDeniedMessage("주변 약국찾기 서비스를 사용하기 위해 위치 접근 권한이 필요합니다. [설정] > [권한] 에서 해당 위치정보 권한을 허용하실 수 있습니다.")
                    .setPermissions(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
                    .setGotoSettingButton(true)
                    .setGotoSettingButtonText("설정")
                    .check();
        }

        @Override
        protected String doInBackground(String... strings) {
            return null;
        }
    }

    private void updateData() {

        marker_list.clear();
        // 마커 생성 및 설정
        user_marker.setItemName("현위치");
        user_marker.setTag(0);
        user_marker.setMapPoint(MapPoint.mapPointWithGeoCoord(Double.valueOf(mLatitude), Double.valueOf(mLongitude)));
        user_marker.setMarkerType(MapPOIItem.MarkerType.CustomImage); // 마커타입을 커스텀 마커로 지정.
        user_marker.setCustomImageResourceId(R.drawable.here_marker_small); // 마커 이미지.
//        user_marker.setSelectedMarkerType(MapPOIItem.MarkerType.RedPin); // 마커를 클릭했을때, 기본으로 제공하는 RedPin 마커 모양.
        user_marker.setCustomImageAutoscale(true); // hdpi, xhdpi 등 안드로이드 플랫폼의 스케일을 사용할 경우 지도 라이브러리의 스케일 기능을 꺼줌.
        user_marker.setCustomImageAnchor(0.5f, 0.0f); // 마커 이미지중 기준이 되는 위치(앵커포인트) 지정 - 마커 이미지 좌측 상단 기준 x(0.0f ~ 1.0f), y(0.0f ~ 1.0f) 값.
        mapView.addPOIItem(user_marker);
        marker_list.add(0, user_marker);

        InitPharmacyData ipd = new InitPharmacyData();
        ipd.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

//        String drug_result = "";
//        String json_ordinate = "";
//        json_ordinate = jsonMaker.makeJson(new Pair("lat", mLatitude), new Pair("lon", mLongitude));
//        drug_result = httpConnect.send(json_ordinate, getString(R.string.server_url) + getString(R.string.cure_version) + "/drugstore/like", csp.getValue("access_token", ""), "POST");
//        Log.e("drug_result", drug_result);
    }

    private class InitPharmacyData extends AsyncTask<String, String, String> {

        String pharmacy_json = "";

        @Override
        protected void onPreExecute() {
            String result = "";
            result = httpConnect.send("", getString(R.string.server_url) + getString(R.string.cureya_version) + getString(R.string.rest_pharmacy) + "?lat=" + mLatitude + "&lon=" + mLongitude, access_token, "GET");
            pharmacy_json = result;
            Log.e("pharmacy data", pharmacy_json);
        }

        @Override
        protected String doInBackground(String... strings) {
            getPharmacyData(pharmacy_json);
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            setPharmacyView();
            mGoogleApiClient.disconnect();
        }
    }

    private void getPharmacyData(String pharmacy_json) {
        pharmacy_list.clear();
        try {
            JSONArray pharmacy_array = new JSONArray(pharmacy_json);
            for (int i = 0; i < pharmacy_array.length(); i++) {
                JSONObject pharmacy_object = pharmacy_array.getJSONObject(i);
                Pharmacy pharmacy = new Pharmacy();
                pharmacy.setDutyaddr(pharmacy_object.getString("DUTYADDR"));
                pharmacy.setDutyetc(pharmacy_object.getString("DUTYETC"));
                pharmacy.setDutyinf(pharmacy_object.getString("DUTYINF"));
                pharmacy.setDutymapimg(pharmacy_object.getString("DUTYMAPIMG"));
                pharmacy.setDutyname(pharmacy_object.getString("DUTYNAME"));
                pharmacy.setDutytel1(pharmacy_object.getString("DUTYTEL1"));
                pharmacy.setDutytime1c(pharmacy_object.getString("DUTYTIME1C"));
                pharmacy.setDutytime1s(pharmacy_object.getString("DUTYTIME1S"));
                pharmacy.setDutytime2c(pharmacy_object.getString("DUTYTIME2C"));
                pharmacy.setDutytime2s(pharmacy_object.getString("DUTYTIME2S"));
                pharmacy.setDutytime3c(pharmacy_object.getString("DUTYTIME3C"));
                pharmacy.setDutytime3s(pharmacy_object.getString("DUTYTIME3S"));
                pharmacy.setDutytime4c(pharmacy_object.getString("DUTYTIME4C"));
                pharmacy.setDutytime4s(pharmacy_object.getString("DUTYTIME4S"));
                pharmacy.setDutytime5c(pharmacy_object.getString("DUTYTIME5C"));
                pharmacy.setDutytime5s(pharmacy_object.getString("DUTYTIME5S"));
                pharmacy.setDutytime6c(pharmacy_object.getString("DUTYTIME6C"));
                pharmacy.setDutytime6s(pharmacy_object.getString("DUTYTIME6S"));
                pharmacy.setDutytime7c(pharmacy_object.getString("DUTYTIME7C"));
                pharmacy.setDutytime7s(pharmacy_object.getString("DUTYTIME7S"));
                pharmacy.setDutytime8c(pharmacy_object.getString("DUTYTIME8C"));
                pharmacy.setDutytime8s(pharmacy_object.getString("DUTYTIME8S"));
                pharmacy.setHpid(pharmacy_object.getString("HPID"));
                pharmacy.setPostcdn1(pharmacy_object.getString("POSTCDN1"));
                pharmacy.setPostcdn2(pharmacy_object.getString("POSTCDN2"));
                pharmacy.setWgs84lat(pharmacy_object.getString("WGS84LAT"));
                pharmacy.setWgs84lon(pharmacy_object.getString("WGS84LON"));
                pharmacy.setDistance(pharmacy_object.getString("distance"));
                pharmacy.setLikes(pharmacy_object.getString("likes"));

                pharmacy_list.add(pharmacy);
            }
        } catch (Exception e) {
            Log.e("pha frag", "get pharmacy excep", e);
        }
    }

    private void setPharmacyView() {
        int tag = 1;
        ArrayList<Pharmacy> temp_pharmacy_list = (ArrayList<Pharmacy>) pharmacy_list.clone();
        String start_time = "";
        String close_time = "";

        for (Pharmacy pharmacy : temp_pharmacy_list) {

            switch (getDayofWeek()) {
                case 1: // monday
                    start_time = pharmacy.getDutytime1s();
                    close_time = pharmacy.getDutytime1c();
                    break;
                case 2: // tuesday
                    start_time = pharmacy.getDutytime2s();
                    close_time = pharmacy.getDutytime2c();
                    break;
                case 3: // wednesday
                    start_time = pharmacy.getDutytime3s();
                    close_time = pharmacy.getDutytime3c();
                    break;
                case 4: // thursday
                    start_time = pharmacy.getDutytime4s();
                    close_time = pharmacy.getDutytime4c();
                    break;
                case 5: // friday
                    start_time = pharmacy.getDutytime5s();
                    close_time = pharmacy.getDutytime5c();
                    break;
                case 6: // saturday
                    start_time = pharmacy.getDutytime6s();
                    close_time = pharmacy.getDutytime6c();
                    break;
                case 7: // sunday
                    start_time = pharmacy.getDutytime7s();
                    close_time = pharmacy.getDutytime7c();
                    break;
                case 8: // holiday
                    start_time = pharmacy.getDutytime8s();
                    close_time = pharmacy.getDutytime8c();
                    break;
            }

            MapPOIItem pharmacy_marker = new MapPOIItem();
            pharmacy_marker.setItemName(pharmacy.getDutyname());
            pharmacy_marker.setTag(tag);
            pharmacy_marker.setMapPoint(MapPoint.mapPointWithGeoCoord(Double.valueOf(pharmacy.getWgs84lat()), Double.valueOf(pharmacy.getWgs84lon())));
            pharmacy_marker.setMarkerType(MapPOIItem.MarkerType.CustomImage);
            // calculate open or close
            if (getOpenClose(start_time, close_time)) {
                pharmacy_marker.setCustomImageResourceId(R.drawable.pharmacy_open_small);
            } else {
                pharmacy_marker.setCustomImageResourceId(R.drawable.pharmacy_close_small);
            }
            pharmacy_marker.setSelectedMarkerType(MapPOIItem.MarkerType.CustomImage);
            pharmacy_marker.setCustomSelectedImageResourceId(R.drawable.pharmacy_active_small);
            pharmacy_marker.setCustomImageAutoscale(false);
            pharmacy_marker.setCustomImageAnchor(0.5f, 1.0f);
            mapView.addPOIItem(pharmacy_marker);
            marker_list.add(tag, pharmacy_marker);
            if (tag == 1) {
                mapView.selectPOIItem(marker_list.get(1), true);
                onPOIItemSelected(mapView, marker_list.get(1));
            }
            tag++;
        }

    }

    //Google Play 서비스 접근승인 요청
    synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    public void checkGPS() {
        LocationManager lm = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
        boolean isGPS = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (isGPS) {
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setMessage("사용자 근처의 약국 검색을 위해 GPS 기능을 켜주시기 바랍니다.");
            alertDialog.setPositiveButton("설정", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                }
            });
            alertDialog.setNegativeButton("닫기", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    AlertDialog.Builder cancel_dialog = new AlertDialog.Builder(getActivity());
                    cancel_dialog.setMessage("GPS 기능을 켜지 않으면 주변 약국 검색 기능을 사용하실 수 없습니다.");
                    cancel_dialog.setPositiveButton("확인", null);
                    cancel_dialog.show();
                }
            });
            alertDialog.show();
        }
    }

    // override methods
    // fused location provider
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.e("gs connection success", "connection onConnected");

        if ("".equals(target_lat)) {
            mLocationRequest = LocationRequest.create();
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            mLocationRequest.setInterval(10000); // Update location

            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            checkGPS();
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (mLastLocation != null) {
                mLatitude = String.valueOf(mLastLocation.getLatitude());
                mLongitude = String.valueOf(mLastLocation.getLongitude());
                // 중심점 변경
                mapView.setMapCenterPoint(MapPoint.mapPointWithGeoCoord(Double.valueOf(mLatitude), Double.valueOf(mLongitude)), true);
                mapView.setZoomLevel(0, true);
                updateData();
                Log.e("geo", mLatitude + " / " + mLongitude);
            }
        } else {
            mLatitude = target_lat;
            mLongitude = target_lng;

            // 중심점 변경
            mapView.setMapCenterPoint(MapPoint.mapPointWithGeoCoord(Double.valueOf(mLatitude), Double.valueOf(mLongitude)), true);
            mapView.setZoomLevel(0, true);
            updateData();
        }

    }

    @Override
    public void onConnectionSuspended(int cause) {
        Log.e("gs connection suspended", "Suspended cause : " + cause);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e("gs connection failed", "Connected Failed:" + connectionResult.getErrorCode());
        buildGoogleApiClient();
    }

    @Override
    public void onLocationChanged(Location location) {
        mLatitude = String.valueOf(location.getLatitude());
        mLongitude = String.valueOf(location.getLongitude());
        updateData();
        Log.e("geo changed", mLatitude + " / " + mLongitude);
    }

    // daum map view
    @Override
    public void onMapViewInitialized(MapView mapView) {

    }

    @Override
    public void onMapViewCenterPointMoved(MapView mapView, MapPoint mapPoint) {

    }

    @Override
    public void onMapViewZoomLevelChanged(MapView mapView, int i) {

    }

    @Override
    public void onMapViewSingleTapped(MapView mapView, MapPoint mapPoint) {

    }

    @Override
    public void onMapViewDoubleTapped(MapView mapView, MapPoint mapPoint) {

    }

    @Override
    public void onMapViewLongPressed(MapView mapView, MapPoint mapPoint) {

    }

    @Override
    public void onMapViewDragStarted(MapView mapView, MapPoint mapPoint) {
    }

    @Override
    public void onMapViewDragEnded(MapView mapView, MapPoint mapPoint) {
    }

    @Override
    public void onMapViewMoveFinished(MapView mapView, MapPoint mapPoint) {

    }

    @Override
    public void onPOIItemSelected(final MapView mapView, final MapPOIItem mapPOIItem) {
        int tag_numb = mapPOIItem.getTag();
        if (tag_numb == 0) {

        } else {
            current_tag = tag_numb;
            final Pharmacy selected_pharmacy = pharmacy_list.get(tag_numb - 1);
            final ProgressDialog ld = new ProgressDialog(getActivity());
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ld.show();
                    mapView.setMapCenterPoint(MapPoint.mapPointWithGeoCoord(Double.valueOf(selected_pharmacy.getWgs84lat()), Double.valueOf(selected_pharmacy.getWgs84lon())), true);
                }
            });
            final String[] addr = selected_pharmacy.getDutyaddr().split("\\(");
            boolean is_liked = false;

            PharmacyGetLikeAsync pgla = new PharmacyGetLikeAsync(selected_pharmacy.getHpid());
            try {
                is_liked = Boolean.valueOf(pgla.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR).get());
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }

            String start_time = "정보가";
            String close_time = "없습니다";

            switch (getDayofWeek()) {
                case 1: // monday
                    start_time = selected_pharmacy.getDutytime1s();
                    close_time = selected_pharmacy.getDutytime1c();
                    break;
                case 2: // tuesday
                    start_time = selected_pharmacy.getDutytime2s();
                    close_time = selected_pharmacy.getDutytime2c();
                    break;
                case 3: // wednesday
                    start_time = selected_pharmacy.getDutytime3s();
                    close_time = selected_pharmacy.getDutytime3c();
                    break;
                case 4: // thursday
                    start_time = selected_pharmacy.getDutytime4s();
                    close_time = selected_pharmacy.getDutytime4c();
                    break;
                case 5: // friday
                    start_time = selected_pharmacy.getDutytime5s();
                    close_time = selected_pharmacy.getDutytime5c();
                    break;
                case 6: // saturday
                    start_time = selected_pharmacy.getDutytime6s();
                    close_time = selected_pharmacy.getDutytime6c();
                    break;
                case 7: // sunday
                    start_time = selected_pharmacy.getDutytime7s();
                    close_time = selected_pharmacy.getDutytime7c();
                    break;
                case 8: // holiday
                    start_time = selected_pharmacy.getDutytime8s();
                    close_time = selected_pharmacy.getDutytime8c();
                    break;
            }

            final String finalStart_time = start_time;
            final String finalClose_time = close_time;

            final StringBuilder start_builder = new StringBuilder();
            final StringBuilder close_builder = new StringBuilder();
            start_builder.append(start_time);
            close_builder.append(close_time);
            start_builder.insert(2, ":");
            close_builder.insert(2, ":");

            final boolean finalIs_liked = is_liked;
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    txt_pharmacy_name.setText(selected_pharmacy.getDutyname());
                    btn_pharmacy_status.setText("휴점중");
                    txt_pharmacy_address_road.setText(addr[0]);
                    if (addr.length > 1)
                        txt_pharmacy_address_lot.setText("(" + addr[1]);
                    txt_pharmacy_start.setText(start_builder.toString());
                    txt_pharmacy_close.setText(close_builder.toString());
                    txt_pharmacy_like_count.setText(selected_pharmacy.getLikes());
                    if (getOpenClose(finalStart_time, finalClose_time)) {
                        btn_pharmacy_status.setText("영업중");
                        btn_pharmacy_status.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.sunglow));
                    } else {
                        btn_pharmacy_status.setText("휴점중");
                        btn_pharmacy_status.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.ash_grey1));
                    }
                    current_call_number = selected_pharmacy.getDutytel1();
                    current_pharmacy_name = selected_pharmacy.getDutyname();
                    current_addr = selected_pharmacy.getDutyaddr();
                    current_like = finalIs_liked;
                    current_hpid = selected_pharmacy.getHpid();

                    mapView.refreshDrawableState();
                    ld.dismiss();
                }
            });
        }
    }

    @Override
    public void onCalloutBalloonOfPOIItemTouched(MapView mapView, MapPOIItem mapPOIItem) {

    }

    @Override
    public void onCalloutBalloonOfPOIItemTouched(MapView mapView, MapPOIItem mapPOIItem, MapPOIItem.CalloutBalloonButtonType calloutBalloonButtonType) {

    }

    @Override
    public void onDraggablePOIItemMoved(MapView mapView, MapPOIItem mapPOIItem, MapPoint mapPoint) {

    }

    class PharmacyGetLikeAsync extends AsyncTask<String, String, String> {

        String pharmacy_id;

        public PharmacyGetLikeAsync() {

        }

        public PharmacyGetLikeAsync(String pharmacy_id) {
            this.pharmacy_id = pharmacy_id;
        }

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            String like_result = "";
            result = httpConnect.send("", getString(R.string.server_url) + getString(R.string.cureya_version) + getString(R.string.rest_pharmacy_like) + "?id=" + pharmacy_id, csp.getValue("access_token", ""), "GET");
            try {
                JSONObject like_json = new JSONObject(result);
                like_result = like_json.getString("result");
            } catch (Exception e) {
                Log.e("ph fr", "get pharmacy like exception", e);
            }
            return like_result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
        }
    }

    class PharmacyLikeButtonAsync extends AsyncTask<String, String, String> {

        String method = "";

        public PharmacyLikeButtonAsync() {

        }

        public PharmacyLikeButtonAsync(String method) {
            this.method = method;
        }

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            String json = jsonMaker.makeJson(new Pair("target", current_hpid));
            if ("POST".equals(method)) {
                result = httpConnect.send(json, getString(R.string.server_url) + getString(R.string.cureya_version) + getString(R.string.rest_pharmacy_like), csp.getValue("access_token", ""));
            } else {
                result = httpConnect.send(json, getString(R.string.server_url) + getString(R.string.cureya_version) + getString(R.string.rest_pharmacy_like), csp.getValue("access_token", ""), method);
            }
            return null;
        }
    }

    private int getDayofWeek() {
        Calendar cal = Calendar.getInstance();
        int result = cal.get(Calendar.DAY_OF_WEEK);
        if (result == 1) {
            result = 8;
        }
        return result - 1;
    }

    private boolean getOpenClose(String start, String close) {
        boolean result = false;
        Calendar mCalendar = Calendar.getInstance();
        String current = String.valueOf(mCalendar.get(Calendar.HOUR_OF_DAY)) + String.valueOf(mCalendar.get(Calendar.MINUTE));

        if ("null".equals(start) || "null".equals(close)) {
            return false;
        } else {
            try {
                SimpleDateFormat dateFormat = new SimpleDateFormat("HHmm");
                Date start_date = dateFormat.parse(start);
                Date close_date = dateFormat.parse(close);
                Date current_date = dateFormat.parse(current);

                if (start_date.before(current_date) && close_date.after(current_date)) {
                    result = true;
                }
            } catch (Exception e) {
                Log.e("phar frag", "get open exception", e);
            }
            return result;
        }
    }
}