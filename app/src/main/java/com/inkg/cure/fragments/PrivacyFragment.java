package com.inkg.cure.fragments;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.inkg.cure.R;
import com.inkg.cure.classes.CureSP;
import com.inkg.cure.classes.HttpConnect;

/**
 * Created by inkg on 2017. 2. 4..
 */

public class PrivacyFragment extends Fragment {

    private View parentView;
    private HttpConnect httpConnect;
    private TextView txt_privacy;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        parentView = inflater.inflate(R.layout.fragment_privacy, container, false);
        httpConnect = new HttpConnect(getActivity().getApplicationContext());
        setupViews();
        return parentView;
    }

    private void setupViews() {
        txt_privacy = (TextView) parentView.findViewById(R.id.txt_privacy);

        PrivacyAsync pa = new PrivacyAsync();
        pa.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    class PrivacyAsync extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            result = httpConnect.send("", httpConnect.getServerWithVersion() + getString(R.string.rest_privacy), "", "GET");
            result = Html.fromHtml(result).toString();
            return result;
        }

        @Override
        protected void onPostExecute(final String result) {
            super.onPostExecute(result);
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    txt_privacy.setText(result);
                }
            });
        }
    }
}