package com.inkg.cure.share;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.kakao.kakaolink.KakaoLink;
import com.kakao.kakaolink.KakaoTalkLinkMessageBuilder;

/**
 * Created by inkg on 2017. 2. 24..
 */

public class KaKaoTalkShareActivity extends AppCompatActivity {

    String title;
    String contents;
    String image;
    String link;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        title = intent.getStringExtra("title");
        contents = intent.getStringExtra("contents");
        image = intent.getStringExtra("image");
        link = intent.getStringExtra("link");

        shareKakao();
    }

    public void shareKakao() {
        try {
            final KakaoLink kakaoLink = KakaoLink.getKakaoLink(this);
            final KakaoTalkLinkMessageBuilder kakaoBuilder = kakaoLink.createKakaoTalkLinkMessageBuilder();

        /*메시지 추가*/
            kakaoBuilder.addText(title);

        /*이미지 가로/세로 사이즈는 80px 보다 커야하며, 이미지 용량은 500kb 이하로 제한된다.*/
            String url = image;
            kakaoBuilder.addImage(url, 160, 160);

        /*앱 실행버튼 추가*/
            kakaoBuilder.addAppButton(link);

        /*메시지 발송*/
            kakaoLink.sendMessage(kakaoBuilder, this);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
