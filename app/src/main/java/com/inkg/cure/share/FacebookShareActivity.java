package com.inkg.cure.share;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

/**
 * Created by inkg on 2017. 2. 24..
 */

public class FacebookShareActivity extends AppCompatActivity {

    String title;
    String image;
    String link;
    String contents;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        Intent intent = getIntent();
        title = intent.getStringExtra("title");
        image = intent.getStringExtra("image");
        link = intent.getStringExtra("link");
        contents = intent.getStringExtra("contents");
        
        shareFacebook();

    }

    public void shareFacebook() {
        ShareLinkContent content = new ShareLinkContent.Builder()
                //링크의 콘텐츠 제목
                .setContentTitle(title)

                //게시물에 표시될 썸네일 이미지의 URL
                .setImageUrl(Uri.parse(image))

                //공유될 링크
                .setContentUrl(Uri.parse(link))

                //일반적으로 2~4개의 문장으로 구성된 콘텐츠 설명
                .setContentDescription(contents)
                .build();

        ShareDialog shareDialog = new ShareDialog(this);
        shareDialog.show(content, ShareDialog.Mode.AUTOMATIC);   //AUTOMATIC, FEED, NATIVE, WEB 등이 있으며 이는 다이얼로그 형식을 말합니다.
    }
}
