package com.inkg.cure.review;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.inkg.cure.R;
import com.inkg.cure.adapters.JoinAdapter;
import com.inkg.cure.classes.CureSP;
import com.inkg.cure.classes.HttpConnect;
import com.inkg.cure.classes.JsonMaker;
import com.inkg.cure.classes.LoadingDialog;
import com.inkg.cure.classes.RequestCodes;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;

/**
 * Created by inkg on 2017. 1. 15..
 */

public class WriteActivity extends AppCompatActivity {

    private TextView txt_review_write_good;
    private Button btn_review_write_good;
    private TextView txt_review_write_bad;
    private Button btn_review_write_bad;
    private Button btn_review_write_period;
    private TextView txt_review_write_period;
    private RelativeLayout layout_review_write_repurchase_yes;
    private TextView txt_review_write_repurchase_yes;
    private Button btn_review_write_repurchase_yes;
    private RelativeLayout layout_review_write_repurchase_no;
    private TextView txt_review_write_repurchase_no;
    private Button btn_review_write_repurchase_no;

    private RelativeLayout layout_review_write_good;
    private RelativeLayout layout_review_write_bad;


    private EditText edit_review_write_good;
    private TextView txt_review_write_good_count;

    private EditText edit_review_write_bad;
    private TextView txt_review_write_bad_count;

    private EditText edit_review_write_tip;
    private TextView txt_review_write_tip_count;

    private Button btn_review_write_picture;

    private TextView txt_actionbar_mypage_write;

    private int[] write_checker;
    private boolean is_done = false;

    private CureSP csp;
    private HttpConnect httpConnect;
    private JsonMaker jsonMaker;

    private String id;
    private String evaluation;
    private String period;
    private String repurchase;
    private String good_point = "";
    private String bad_point = "";
    private String tip = "팁";

    private Uri mImageCaptureUri;
    private String photo = "";
    private Bitmap photo_bitmap = null;

    private LoadingDialog loadingDialog;
    private ImageView img_review_write;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_write);
        csp = new CureSP(getApplicationContext());
        httpConnect = new HttpConnect(getApplicationContext());
        jsonMaker = new JsonMaker();
        loadingDialog = new LoadingDialog(WriteActivity.this);

        Intent intent = getIntent();
        id = intent.getStringExtra("id");

        write_checker = new int[5];
        for (int i = 0; i < write_checker.length; i++) {
            write_checker[i] = 0;
        }
        write_checker[0] = 1;

        setupViews();
        setCustomActionbar();
    }

    private void setupViews() {

        layout_review_write_good = (RelativeLayout) findViewById(R.id.layout_review_write_good);
        layout_review_write_bad = (RelativeLayout) findViewById(R.id.layout_review_write_bad);

        txt_review_write_good = (TextView) findViewById(R.id.txt_review_write_good);
        txt_review_write_bad = (TextView) findViewById(R.id.txt_review_write_bad);
        btn_review_write_good = (Button) findViewById(R.id.btn_review_write_good);
        btn_review_write_bad = (Button) findViewById(R.id.btn_review_write_bad);
        btn_review_write_good.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                evaluation = "0";
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        layout_review_write_good.setBackground(getResources().getDrawable(R.drawable.rounded_border_btn_blue, null));
                        layout_review_write_bad.setBackground(getResources().getDrawable(R.drawable.rounded_border_btn_gray, null));
                        txt_review_write_good.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.maya_blue1));
                        txt_review_write_bad.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.manatee));
                    }
                });
                write_checker[0] = 1;
                finish_checker();
            }
        });
        btn_review_write_bad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                evaluation = "1";
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        layout_review_write_good.setBackground(getResources().getDrawable(R.drawable.rounded_border_btn_gray, null));
                        layout_review_write_bad.setBackground(getResources().getDrawable(R.drawable.rounded_border_btn_blue, null));
                        txt_review_write_good.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.manatee));
                        txt_review_write_bad.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.maya_blue1));
                    }
                });
                write_checker[0] = 1;
                finish_checker();
            }
        });

        final ArrayList<String> periods = new ArrayList<>();
        periods.add(0, "1주");
        periods.add(1, "2주");
        periods.add(2, "3주");
        periods.add(3, "1개월");
        periods.add(4, "2개월");
        periods.add(5, "3개월");
        periods.add(6, "6개월");
        periods.add(7, "12개월");
        txt_review_write_period = (TextView) findViewById(R.id.txt_review_write_period);
        btn_review_write_period = (Button) findViewById(R.id.btn_review_write_period);
        btn_review_write_period.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final JoinAdapter period_adapter = new JoinAdapter(getApplicationContext(), R.layout.adapter_join, periods);
                final LayoutInflater title_inflater = (LayoutInflater) getApplicationContext().getSystemService(LAYOUT_INFLATER_SERVICE);
                AlertDialog.Builder ab = new AlertDialog.Builder(WriteActivity.this);
                View title_view = title_inflater.inflate(R.layout.dialog_custom_title, null);
                TextView txt_dialog_title = (TextView) title_view.findViewById(R.id.txt_dialog_custom_title);
                ab.setCustomTitle(title_view);
                txt_dialog_title.setText("복용 기간");
                ab.setAdapter(period_adapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialogInterface, final int i) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                txt_review_write_period.setText(periods.get(i));
                                period = periods.get(i);
                                dialogInterface.dismiss();
                                write_checker[1] = 1;
                                finish_checker();
                            }
                        });
                    }
                });
                ab.show();
            }
        });

        layout_review_write_repurchase_yes = (RelativeLayout) findViewById(R.id.layout_review_write_repurchase_yes);
        txt_review_write_repurchase_yes = (TextView) findViewById(R.id.txt_review_write_repurchase_yes);
        layout_review_write_repurchase_no = (RelativeLayout) findViewById(R.id.layout_review_write_repurchase_no);
        txt_review_write_repurchase_no = (TextView) findViewById(R.id.txt_review_write_repurchase_no);

        btn_review_write_repurchase_yes = (Button) findViewById(R.id.btn_review_write_repurchase_yes);
        btn_review_write_repurchase_no = (Button) findViewById(R.id.btn_review_write_repurchase_no);
        btn_review_write_repurchase_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        layout_review_write_repurchase_yes.setBackground(getResources().getDrawable(R.drawable.review_back_blue, null));
                        txt_review_write_repurchase_yes.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
                        layout_review_write_repurchase_no.setBackground(getResources().getDrawable(R.drawable.review_back_blue_border, null));
                        txt_review_write_repurchase_no.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.maya_blue1));
                        repurchase = "0";
                        write_checker[2] = 1;
                        finish_checker();
                    }
                });
            }
        });
        btn_review_write_repurchase_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layout_review_write_repurchase_yes.setBackground(getResources().getDrawable(R.drawable.review_back_blue_border, null));
                txt_review_write_repurchase_yes.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.maya_blue1));
                layout_review_write_repurchase_no.setBackground(getResources().getDrawable(R.drawable.review_back_blue, null));
                txt_review_write_repurchase_no.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
                repurchase = "1";
                write_checker[2] = 1;
                finish_checker();
            }
        });

        edit_review_write_good = (EditText) findViewById(R.id.edit_review_write_good);
        txt_review_write_good_count = (TextView) findViewById(R.id.txt_review_write_good_count);

        TextWatcher good_watcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(final CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 19) {
                    write_checker[3] = 1;
                    finish_checker();
                } else {
                    write_checker[3] = 0;
                    finish_checker();
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        txt_review_write_good_count.setText(charSequence.length() + "/500");
                    }
                });
            }

            @Override
            public void afterTextChanged(Editable editable) {
                good_point = edit_review_write_good.getText().toString();
            }
        };

        edit_review_write_good.addTextChangedListener(good_watcher);

        edit_review_write_bad = (EditText) findViewById(R.id.edit_review_write_bad);
        txt_review_write_bad_count = (TextView) findViewById(R.id.txt_review_write_bad_count);

        TextWatcher bad_watcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(final CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 19) {
                    write_checker[4] = 1;
                    finish_checker();
                } else {
                    write_checker[4] = 0;
                    finish_checker();
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        txt_review_write_bad_count.setText(charSequence.length() + "/500");
                    }
                });
            }

            @Override
            public void afterTextChanged(Editable editable) {
                bad_point = edit_review_write_bad.getText().toString();
            }
        };

        edit_review_write_bad.addTextChangedListener(bad_watcher);

        edit_review_write_tip = (EditText) findViewById(R.id.edit_review_write_tip);
        txt_review_write_tip_count = (TextView) findViewById(R.id.txt_review_write_tip_count);

        TextWatcher tip_watcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(final CharSequence charSequence, int i, int i1, int i2) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        txt_review_write_tip_count.setText(charSequence.length() + "/500");
                    }
                });
            }

            @Override
            public void afterTextChanged(Editable editable) {
                tip = edit_review_write_tip.getText().toString();
            }
        };

        edit_review_write_tip.addTextChangedListener(tip_watcher);
        btn_review_write_picture = (Button) findViewById(R.id.btn_review_write_picture);
        btn_review_write_picture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder ad = new AlertDialog.Builder(WriteActivity.this);
                ad.setMessage("업로드할 이미지 선택")
                        .setPositiveButton("앨범선택", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                doTakeAlbumAction();
                            }
                        })
                        .setNeutralButton("사진촬영", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                doTakePhotoAction();
                            }
                        })
                        .setNegativeButton("취소", null)
                        .show();
            }
        });

        img_review_write = (ImageView) findViewById(R.id.img_review_write);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
    }

    private void setCustomActionbar() {

        ActionBar actionBar = getSupportActionBar();

        // Custom Actionbar를 사용하기 위해 CustomEnabled을 true 시키고 필요 없는 것은 false 시킨다

        actionBar.setDisplayShowCustomEnabled(true);

        actionBar.setDisplayHomeAsUpEnabled(false);

        actionBar.setDisplayShowTitleEnabled(false);

        // Set custom view layout

        View mCustomView = LayoutInflater.from(this).inflate(R.layout.actionbar_mypage, null);

        actionBar.setCustomView(mCustomView);


        // Set no padding both side

        Toolbar parent = (Toolbar) mCustomView.getParent(); // first get parent toolbar of current action bar

        parent.setContentInsetsAbsolute(0, 0);              // set padding programmatically to 0dp

        // Set actionbar background image

        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.border_layout_bottom));

        // Set actionbar layout layoutparams

        ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);

        actionBar.setCustomView(mCustomView, params);

        ImageView img_actionbar_back = (ImageView) findViewById(R.id.img_actionbar_mypage_back);
        img_actionbar_back.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    finish();
                }
                return false;
            }
        });

        TextView txt_actionbar = (TextView) findViewById(R.id.txt_actionbar_mypage);
        txt_actionbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        txt_actionbar.setText("리뷰쓰기");

        Button btn_actionbar_setting = (Button) findViewById(R.id.btn_actionbar_setting);
        btn_actionbar_setting.setVisibility(View.GONE);

        txt_actionbar_mypage_write = (TextView) findViewById(R.id.txt_actionbar_mypage_write);

        RelativeLayout layout_actionbar_mypage = (RelativeLayout) findViewById(R.id.layout_actionbar_mypage);
        layout_actionbar_mypage.setVisibility(View.VISIBLE);
        layout_actionbar_mypage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (is_done) {
                    PostReviewAsync pra = new PostReviewAsync();
                    pra.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, id, period, repurchase, good_point, bad_point, tip, evaluation);
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "필수 항목을 다 채워주셔야 리뷰 등록이 가능합니다.", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });
    }

    private void finish_checker() {
        boolean is_ok = true;
        for (int i = 0; i < write_checker.length; i++) {
            if (write_checker[i] == 0) {
                is_ok = false;
            }
        }

//        for (int i = 0; i < write_checker.length; i++) {
//            Log.e("?", write_checker[i] + "");
//        }

        if (is_ok) {
            is_done = true;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    txt_actionbar_mypage_write.setTextColor(ContextCompat.getColor(WriteActivity.this, R.color.maya_blue1));
                }
            });
        } else {
            is_done = false;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    txt_actionbar_mypage_write.setTextColor(ContextCompat.getColor(WriteActivity.this, R.color.manatee));
                }
            });
        }
    }

    class PostReviewAsync extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loadingDialog.show();
                }
            });
        }

        @Override
        protected String doInBackground(String... strings) {
            String id = strings[0];
            String period = strings[1];
            String repurchase = strings[2];
            String good_point = strings[3];
            String bad_point = strings[4];
            String tip = strings[5];
            String evaluation = strings[6];
//            String picture = strings[7];
            String result = "";
            String json = "";
            if ("".equals(photo)) {
                json = jsonMaker.makeJson(new Pair("target", id), new Pair("period", period), new Pair("repurchase", repurchase), new Pair("good_point", good_point)
                        , new Pair("bad_point", bad_point), new Pair("tip", tip), new Pair("evaluation", evaluation));
            } else {
                json = jsonMaker.makeJson(new Pair("target", id), new Pair("period", period), new Pair("repurchase", repurchase), new Pair("good_point", good_point)
                        , new Pair("bad_point", bad_point), new Pair("tip", tip), new Pair("evaluation", evaluation), new Pair("picture", "[" + photo + "]"));
            }
            result = httpConnect.send(json, httpConnect.getServerWithVersion() + getString(R.string.rest_review), csp.getValue("access_token", ""));
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                JSONObject result_json = new JSONObject(result);
                final String message = result_json.getString("message");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if ("success".equals(message)) {
                            Toast.makeText(WriteActivity.this, "리뷰가 작성되었습니다.", Toast.LENGTH_SHORT).show();
                            csp.put("my_review", String.valueOf(Integer.valueOf(csp.getValue("my_review", "0")) + 1));
                            loadingDialog.dismiss();
                            finish();
                        } else {
                            Toast.makeText(WriteActivity.this, "오류가 발생했습니다. 다시 시도해 주세요.", Toast.LENGTH_SHORT).show();
                            loadingDialog.dismiss();
                            finish();
                        }
                    }
                });
            } catch (Exception e) {
                Log.e("re write ac", "write excep", e);
            } finally {
                if (loadingDialog.isShowing())
                    loadingDialog.dismiss();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case RequestCodes.PICK_FROM_ALBUM: {
                if (resultCode != 0) {
                    mImageCaptureUri = data.getData();
                    photo = mImageCaptureUri.getPath().toString();
                }
            }
            case RequestCodes.PICK_FROM_CAMERA: {
                if (resultCode != 0) {
                    Intent intent = new Intent("com.android.camera.action.CROP");
                    intent.setDataAndType(mImageCaptureUri, "image/*");

                    // CROP할 이미지를 200*200 크기로 저장
                    intent.putExtra("outputX", 200); // CROP한 이미지의 x축 크기
                    intent.putExtra("outputY", 200); // CROP한 이미지의 y축 크기
                    intent.putExtra("aspectX", 1); // CROP 박스의 X축 비율
                    intent.putExtra("aspectY", 1); // CROP 박스의 Y축 비율
                    intent.putExtra("scale", true);
                    intent.putExtra("return-data", true);
                    startActivityForResult(intent, RequestCodes.CROP_FROM_IMAGE); // CROP_FROM_CAMERA case문 이동
                }
                break;
            }
            case RequestCodes.CROP_FROM_IMAGE: {
                if (resultCode != 0) {
                    final Bundle extras = data.getExtras();

                    if (extras != null) {
                        photo_bitmap = extras.getParcelable("data"); // CROP된 BITMAP
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        photo_bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                        byte[] byteArray = byteArrayOutputStream.toByteArray();
                        photo = Base64.encodeToString(byteArray, Base64.DEFAULT);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                img_review_write.setImageBitmap(photo_bitmap);
                            }
                        });
                    }
                    // 임시 파일 삭제
                    File f = new File(mImageCaptureUri.getPath());
                    if (f.exists()) {
                        f.delete();
                    }
                }
                break;
            }
        }

    }

    public void doTakeAlbumAction() // 앨범에서 이미지 가져오기
    {
        // 앨범 호출
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(android.provider.MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, RequestCodes.PICK_FROM_ALBUM);
    }

    public void doTakePhotoAction() // 카메라 촬영 후 이미지 가져오기
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        // 임시로 사용할 파일의 경로를 생성
        String url = "tmp_" + String.valueOf(System.currentTimeMillis()) + ".jpg";
        mImageCaptureUri = Uri.fromFile(new File(Environment.getExternalStorageDirectory(), url));

        intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
        startActivityForResult(intent, RequestCodes.PICK_FROM_CAMERA);
    }


}
