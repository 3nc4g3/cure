package com.inkg.cure.review;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.inkg.cure.R;
import com.inkg.cure.classes.CureSP;
import com.inkg.cure.classes.HttpConnect;
import com.inkg.cure.classes.JsonMaker;
import com.inkg.cure.classes.RequestCodes;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;

/**
 * Created by inkg on 2017. 2. 14..
 */

public class EditActivity extends AppCompatActivity {

    private ImageView img_review_edit_product;
    private TextView txt_review_edit_company;
    private TextView txt_review_edit_product;
    private TextView txt_review_edit_category;
    private RelativeLayout layout_review_edit_content;
    private LinearLayout layout_review_edit_category;
    private EditText edit_review_edit_content;
    private Button btn_review_edit_discontinue;
    private Button btn_review_edit_company;
    private Button btn_review_edit_category;
    private Button btn_review_edit_product;
    private Button btn_review_edit_other;
    private Button btn_review_edit_submit;
    private JsonMaker jsonMaker;
    private HttpConnect httpConnect;
    private CureSP cureSP;

    private String name;
    private String company;
    private Bitmap bm;
    private String id;

    private ImageView img_review_edit_photo;

    private boolean is_done = false;

    private Uri mImageCaptureUri;
    private String photo = "";
    private Bitmap photo_bitmap = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_edit);
        jsonMaker = new JsonMaker();
        httpConnect = new HttpConnect(getApplicationContext());
        cureSP = new CureSP(getApplicationContext());

        Intent intent = getIntent();
        name = intent.getStringExtra("product_name");
        company = intent.getStringExtra("company_name");
        byte[] bytes = intent.getByteArrayExtra("bitmap");
        bm = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
        id = intent.getStringExtra("id");

        setupViews();
        setCustomActionbar();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case RequestCodes.PICK_FROM_ALBUM: {
                if (resultCode != 0) {
                    mImageCaptureUri = data.getData();
                    photo = mImageCaptureUri.getPath().toString();
                }
            }
            case RequestCodes.PICK_FROM_CAMERA: {
                if (resultCode != 0) {
                    Intent intent = new Intent("com.android.camera.action.CROP");
                    intent.setDataAndType(mImageCaptureUri, "image/*");

                    // CROP할 이미지를 200*200 크기로 저장
                    intent.putExtra("outputX", 200); // CROP한 이미지의 x축 크기
                    intent.putExtra("outputY", 200); // CROP한 이미지의 y축 크기
                    intent.putExtra("aspectX", 1); // CROP 박스의 X축 비율
                    intent.putExtra("aspectY", 1); // CROP 박스의 Y축 비율
                    intent.putExtra("scale", true);
                    intent.putExtra("return-data", true);
                    startActivityForResult(intent, RequestCodes.CROP_FROM_IMAGE); // CROP_FROM_CAMERA case문 이동
                }
                break;
            }
            case RequestCodes.CROP_FROM_IMAGE: {
                if (resultCode != 0) {
                    final Bundle extras = data.getExtras();

                    if (extras != null) {
                        photo_bitmap = extras.getParcelable("data"); // CROP된 BITMAP
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        photo_bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                        byte[] byteArray = byteArrayOutputStream.toByteArray();
                        photo = Base64.encodeToString(byteArray, Base64.DEFAULT);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                img_review_edit_photo.setImageBitmap(photo_bitmap);
                            }
                        });
                    }
                    // 임시 파일 삭제
                    File f = new File(mImageCaptureUri.getPath());
                    if (f.exists()) {
                        f.delete();
                    }
                }
                break;
            }
        }

    }

    private void setupViews() {
        img_review_edit_product = (ImageView) findViewById(R.id.img_review_edit_product);
        txt_review_edit_company = (TextView) findViewById(R.id.txt_review_edit_company);
        txt_review_edit_product = (TextView) findViewById(R.id.txt_review_edit_product);
        txt_review_edit_category = (TextView) findViewById(R.id.txt_review_edit_category);
        layout_review_edit_content = (RelativeLayout) findViewById(R.id.layout_review_edit_content);
        layout_review_edit_category = (LinearLayout) findViewById(R.id.layout_review_edit_category);
        edit_review_edit_content = (EditText) findViewById(R.id.edit_review_edit_content);
        btn_review_edit_discontinue = (Button) findViewById(R.id.btn_review_edit_discontinue);
        btn_review_edit_company = (Button) findViewById(R.id.btn_review_edit_company);
        btn_review_edit_category = (Button) findViewById(R.id.btn_review_edit_category);
        btn_review_edit_product = (Button) findViewById(R.id.btn_review_edit_product);
        btn_review_edit_other = (Button) findViewById(R.id.btn_review_edit_other);
        btn_review_edit_submit = (Button) findViewById(R.id.btn_review_edit_submit);
        img_review_edit_photo = (ImageView) findViewById(R.id.img_review_edit_photo);

        img_review_edit_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder ad = new AlertDialog.Builder(EditActivity.this);
                ad.setMessage("업로드할 이미지 선택")
                        .setPositiveButton("앨범선택", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                doTakeAlbumAction();
                            }
                        })
                        .setNeutralButton("사진촬영", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                doTakePhotoAction();
                            }
                        })
                        .setNegativeButton("취소", null)
                        .show();
            }
        });

        img_review_edit_product.setImageBitmap(bm);
        txt_review_edit_company.setText(company);
        txt_review_edit_product.setText(name);

        btn_review_edit_discontinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layout_review_edit_category.setVisibility(View.GONE);
                layout_review_edit_content.setVisibility(View.VISIBLE);
                txt_review_edit_category.setText("1. 단종됐어요!");
            }
        });

        btn_review_edit_company.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layout_review_edit_category.setVisibility(View.GONE);
                txt_review_edit_category.setText("2. 회사명 수정해주세요!");
                layout_review_edit_content.setVisibility(View.VISIBLE);
            }
        });

        btn_review_edit_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layout_review_edit_category.setVisibility(View.GONE);
                layout_review_edit_content.setVisibility(View.VISIBLE);
                txt_review_edit_category.setText("3. 카테고리명 수정해주세요!");
            }
        });

        btn_review_edit_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layout_review_edit_category.setVisibility(View.GONE);
                layout_review_edit_content.setVisibility(View.VISIBLE);
                txt_review_edit_category.setText("4. 제품명 수정해주세요!");
            }
        });

        btn_review_edit_other.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layout_review_edit_category.setVisibility(View.GONE);
                layout_review_edit_content.setVisibility(View.VISIBLE);
                txt_review_edit_category.setText("5. 기타");
            }
        });

        TextWatcher edit_watcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(final CharSequence charSequence, int i, int i1, int i2) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (charSequence.length() > 0) {
                            is_done = true;
                            btn_review_edit_submit.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.maya_blue1));
                        } else {
                            is_done = false;
                            btn_review_edit_submit.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.silver));
                        }
                    }
                });

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };

        edit_review_edit_content.addTextChangedListener(edit_watcher);

        btn_review_edit_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (is_done) {
                    EditCountAsync eca = new EditCountAsync();
                    eca.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
            }
        });
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
    }

    class EditCountAsync extends AsyncTask<String, String, String> {


        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            result = httpConnect.send("", httpConnect.getServerWithVersion() + getString(R.string.rest_counteroffers), cureSP.getValue("access_token", ""), "GET");
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                JSONObject json = new JSONObject(result);
                String count = json.getString("count");

                if (Integer.valueOf(count) == 0) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "가능한 수정 요청 횟수가 없습니다. 내일 다시 시도해 주세요.", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    });
                } else {
                    EditAsync ea = new EditAsync();
                    ea.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, id, edit_review_edit_content.getText().toString(), cureSP.getValue("email", ""));
                }
            } catch (Exception e) {
                Log.e("edit", "json ex", e);
            }
        }

    }

    class EditAsync extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            String json = "";
            if ("".equals(photo)) {
                json = jsonMaker.makeJson(new Pair("target", strings[0]), new Pair("content", strings[1]), new Pair("email", strings[2]));
            } else {
                json = jsonMaker.makeJson(new Pair("target", strings[0]), new Pair("content", strings[1]), new Pair("email", strings[2]), new Pair("image", "[" + photo + "]"));
            }

            result = httpConnect.send(json, httpConnect.getServerWithVersion() + getString(R.string.rest_counteroffers), cureSP.getValue("access_token", ""));
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getApplicationContext(), "요청이 등록되었습니다.", Toast.LENGTH_SHORT).show();
                    finish();
                }
            });
        }
    }

    private void setCustomActionbar() {

        ActionBar actionBar = getSupportActionBar();

        // Custom Actionbar를 사용하기 위해 CustomEnabled을 true 시키고 필요 없는 것은 false 시킨다

        actionBar.setDisplayShowCustomEnabled(true);

        actionBar.setDisplayHomeAsUpEnabled(false);

        actionBar.setDisplayShowTitleEnabled(false);

        // Set custom view layout

        View mCustomView = LayoutInflater.from(this).inflate(R.layout.actionbar_simple, null);

        actionBar.setCustomView(mCustomView);


        // Set no padding both side

        Toolbar parent = (Toolbar) mCustomView.getParent(); // first get parent toolbar of current action bar

        parent.setContentInsetsAbsolute(0, 0);              // set padding programmatically to 0dp

        // Set actionbar background image

        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.border_layout_bottom));

        // Set actionbar layout layoutparams

        ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);

        actionBar.setCustomView(mCustomView, params);

        ImageView img_actionbar_back = (ImageView) findViewById(R.id.img_actionbar_back);
        img_actionbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        TextView txt_actionbar = (TextView) findViewById(R.id.txt_actionbar_simple);
        txt_actionbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        txt_actionbar.setText("수정요청");

    }

    public void doTakeAlbumAction() // 앨범에서 이미지 가져오기
    {
        // 앨범 호출
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(android.provider.MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, RequestCodes.PICK_FROM_ALBUM);
    }

    public void doTakePhotoAction() // 카메라 촬영 후 이미지 가져오기
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        // 임시로 사용할 파일의 경로를 생성
        String url = "tmp_" + String.valueOf(System.currentTimeMillis()) + ".jpg";
        mImageCaptureUri = Uri.fromFile(new File(Environment.getExternalStorageDirectory(), url));

        intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
        startActivityForResult(intent, RequestCodes.PICK_FROM_CAMERA);
    }
}