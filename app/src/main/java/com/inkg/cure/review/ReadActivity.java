package com.inkg.cure.review;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.inkg.cure.R;
import com.inkg.cure.adapters.JoinAdapter;
import com.inkg.cure.adapters.ReviewAdapter;
import com.inkg.cure.classes.CureSP;
import com.inkg.cure.classes.HttpConnect;
import com.inkg.cure.classes.LoadingDialog;
import com.inkg.cure.classes.Review;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by inkg on 2017. 1. 15..
 */

public class ReadActivity extends AppCompatActivity {

    private TextView txt_review_read_gender;
    private ImageView img_review_read_gender;

    private Button btn_review_read_gender;
    private TextView txt_review_read_age;
    private ImageView img_review_read_age;
    private Button btn_review_read_age;
    private TextView txt_review_read_total_count;
    private RelativeLayout layout_review_detail_like;
    private Button btn_review_detail_like;
    private TextView txt_review_read_like;
    private RelativeLayout layout_review_detail_dislike;
    private Button btn_review_detail_dislike;
    private TextView txt_review_read_dislike;
    private Button btn_review_read_order_recent;
    private Button btn_review_read_order_like;
    private TextView txt_review_read_total;
    private Button btn_review_read_write;

    private ListView list_review_read;
    private ReviewAdapter reviewAdapter;

    private HttpConnect httpConnect;
    private CureSP csp;

    private ArrayList<Review> review_array = new ArrayList<>();

    private String product_id;
    private String filter_key = "";
    private String filter_value = "";
    private String sort = "";
    private String evaluation = "";

    private LoadingDialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_read);

        httpConnect = new HttpConnect(getApplicationContext());
        csp = new CureSP(getApplicationContext());
        loadingDialog = new LoadingDialog(ReadActivity.this);

        Intent intent = getIntent();
        product_id = intent.getStringExtra("id");

        setupViews();
        setCustomActionbar();

    }

    @Override
    protected void onStart() {
        super.onStart();
        InitReviewAsync ira = new InitReviewAsync();
        ira.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void setupViews() {
        txt_review_read_gender = (TextView) findViewById(R.id.txt_review_read_gender);
        img_review_read_gender = (ImageView) findViewById(R.id.img_review_read_gender);
        btn_review_read_gender = (Button) findViewById(R.id.btn_review_read_gender);
        btn_review_read_gender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        final ArrayList<String> genders = new ArrayList<String>();
                        genders.add(0, "전체");
                        genders.add(1, "남성");
                        genders.add(2, "여성");
                        JoinAdapter gender_adapter = new JoinAdapter(getApplicationContext(), R.layout.adapter_join, genders);
                        LayoutInflater title_inflater = (LayoutInflater) getApplicationContext().getSystemService(LAYOUT_INFLATER_SERVICE);

                        AlertDialog.Builder ab = new AlertDialog.Builder(ReadActivity.this);
                        View title_view = title_inflater.inflate(R.layout.dialog_custom_title, null);
                        TextView txt_dialog_title = (TextView) title_view.findViewById(R.id.txt_dialog_custom_title);
                        ab.setCustomTitle(title_view);
                        txt_dialog_title.setText("성별");
                        ab.setAdapter(gender_adapter, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                txt_review_read_gender.setText(genders.get(i));
                                txt_review_read_gender.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.maya_blue1));
                                img_review_read_gender.setBackground(getResources().getDrawable(R.drawable.review_triangle_a, null));
                                txt_review_read_age.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.manatee));
                                img_review_read_age.setBackground(getResources().getDrawable(R.drawable.review_triangle_p, null));
                                filter_key = "gender";
                                if (i == 0) {
                                    filter_value = "";
                                } else if (i == 1) {
                                    filter_value = "male";
                                } else {
                                    filter_value = "female";
                                }

                                InitReviewAsync ira = new InitReviewAsync();
                                ira.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                                dialogInterface.dismiss();
                            }
                        }).create();
                        ab.show();
                    }
                });
            }
        });
        txt_review_read_age = (TextView) findViewById(R.id.txt_review_read_age);
        img_review_read_age = (ImageView) findViewById(R.id.img_review_read_age);
        btn_review_read_age = (Button) findViewById(R.id.btn_review_read_age);
        btn_review_read_age.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        final ArrayList<String> ages = new ArrayList<String>();
                        ages.add(0, "전체");
                        ages.add(1, "20");
                        ages.add(2, "30");
                        ages.add(3, "40");
                        ages.add(4, "50");
                        ages.add(5, "60");
                        JoinAdapter age_adapter = new JoinAdapter(getApplicationContext(), R.layout.adapter_join, ages);
                        LayoutInflater title_inflater = (LayoutInflater) getApplicationContext().getSystemService(LAYOUT_INFLATER_SERVICE);

                        AlertDialog.Builder ab = new AlertDialog.Builder(ReadActivity.this);
                        View title_view = title_inflater.inflate(R.layout.dialog_custom_title, null);
                        TextView txt_dialog_title = (TextView) title_view.findViewById(R.id.txt_dialog_custom_title);
                        ab.setCustomTitle(title_view);
                        txt_dialog_title.setText("연령");
                        ab.setAdapter(age_adapter, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                txt_review_read_age.setText(ages.get(i));
                                txt_review_read_age.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.maya_blue1));
                                img_review_read_age.setBackground(getResources().getDrawable(R.drawable.review_triangle_a, null));
                                txt_review_read_gender.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.manatee));
                                img_review_read_gender.setBackground(getResources().getDrawable(R.drawable.review_triangle_p, null));
                                filter_key = "age";
                                switch (i) {
                                    case 0:
                                        filter_value = "";
                                        break;
                                    case 1:
                                        filter_value = "20";
                                        break;
                                    case 2:
                                        filter_value = "30";
                                        break;
                                    case 3:
                                        filter_value = "40";
                                        break;
                                    case 4:
                                        filter_value = "50";
                                        break;
                                    case 5:
                                        filter_value = "60";
                                        break;
                                }

                                InitReviewAsync ira = new InitReviewAsync();
                                ira.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                                dialogInterface.dismiss();
                            }
                        }).create();
                        ab.show();
                    }
                });
            }
        });

        txt_review_read_total_count = (TextView) findViewById(R.id.txt_review_read_total_count);

        txt_review_read_like = (TextView) findViewById(R.id.txt_review_read_like);
        layout_review_detail_like = (RelativeLayout) findViewById(R.id.layout_review_detail_like);
        btn_review_detail_like = (Button) findViewById(R.id.btn_review_detail_like);

        txt_review_read_dislike = (TextView) findViewById(R.id.txt_review_read_dislike);
        layout_review_detail_dislike = (RelativeLayout) findViewById(R.id.layout_review_detail_dislike);
        btn_review_detail_dislike = (Button) findViewById(R.id.btn_review_detail_dislike);

        btn_review_detail_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        layout_review_detail_like.setBackground(getResources().getDrawable(R.drawable.rounded_border_btn_blue, null));
                        layout_review_detail_dislike.setBackground(getResources().getDrawable(R.drawable.rounded_border_btn_gray, null));
                        evaluation = "0";
                        InitReviewAsync ira = new InitReviewAsync();
                        ira.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                });
            }
        });

        btn_review_detail_dislike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        layout_review_detail_like.setBackground(getResources().getDrawable(R.drawable.rounded_border_btn_gray, null));
                        layout_review_detail_dislike.setBackground(getResources().getDrawable(R.drawable.rounded_border_btn_blue, null));
                        evaluation = "1";
                        InitReviewAsync ira = new InitReviewAsync();
                        ira.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                });
            }
        });


        btn_review_read_order_recent = (Button) findViewById(R.id.btn_review_read_order_recent);
        btn_review_read_order_like = (Button) findViewById(R.id.btn_review_read_order_like);

        btn_review_read_order_recent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        btn_review_read_order_recent.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.maya_blue1));
                        btn_review_read_order_like.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.manatee));
                        sort = "date";

                        InitReviewAsync ira = new InitReviewAsync();
                        ira.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                });

            }
        });

        btn_review_read_order_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        btn_review_read_order_recent.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.manatee));
                        btn_review_read_order_like.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.maya_blue1));
                        sort = "likes";

                        InitReviewAsync ira = new InitReviewAsync();
                        ira.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                });
            }
        });

        txt_review_read_total = (TextView) findViewById(R.id.txt_review_read_total);
        btn_review_read_write = (Button) findViewById(R.id.btn_review_read_write);
        btn_review_read_write.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent write_intent = new Intent(ReadActivity.this, WriteActivity.class);
                startActivity(write_intent);
            }
        });

        list_review_read = (ListView) findViewById(R.id.list_review_read);
        reviewAdapter = new ReviewAdapter(getApplicationContext(), R.layout.adapter_review_read, review_array);
        list_review_read.setAdapter(reviewAdapter);
    }

    private void setCustomActionbar() {

        ActionBar actionBar = getSupportActionBar();

        // Custom Actionbar를 사용하기 위해 CustomEnabled을 true 시키고 필요 없는 것은 false 시킨다

        actionBar.setDisplayShowCustomEnabled(true);

        actionBar.setDisplayHomeAsUpEnabled(false);

        actionBar.setDisplayShowTitleEnabled(false);

        // Set custom view layout

        View mCustomView = LayoutInflater.from(this).inflate(R.layout.actionbar_mypage, null);

        actionBar.setCustomView(mCustomView);


        // Set no padding both side

        Toolbar parent = (Toolbar) mCustomView.getParent(); // first get parent toolbar of current action bar

        parent.setContentInsetsAbsolute(0, 0);              // set padding programmatically to 0dp

        // Set actionbar background image

        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.border_layout_bottom));

        // Set actionbar layout layoutparams

        ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);

        actionBar.setCustomView(mCustomView, params);

        ImageView img_actionbar_back = (ImageView) findViewById(R.id.img_actionbar_mypage_back);
        img_actionbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        TextView txt_actionbar = (TextView) findViewById(R.id.txt_actionbar_mypage);
        txt_actionbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        txt_actionbar.setText("리뷰보기");

        Button btn_actionbar_setting = (Button) findViewById(R.id.btn_actionbar_setting);
        btn_actionbar_setting.setVisibility(View.GONE);

        Button btn_actionbar_alarm = (Button) findViewById(R.id.btn_actionbar_alarm);
        btn_actionbar_alarm.setBackground(getResources().getDrawable(R.drawable.setting_version, null));
        btn_actionbar_alarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InitReviewAsync ira = new InitReviewAsync();
                ira.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        });

    }

    private void setReviewData(final String json) {
        review_array.clear();

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    Log.e("review result", json);
                    JSONObject result = new JSONObject(json);
                    JSONArray review_json_array = result.getJSONArray("Items");
                    txt_review_read_total_count.setText("리뷰 남긴 사람 총 " + review_json_array.length() + "명");
                    txt_review_read_total.setText("총 리뷰 " + review_json_array.length() + "개");
                    for (int i = 0; i < review_json_array.length(); i++) {
                        JSONObject review_object = review_json_array.getJSONObject(i);
                        Review review = new Review();
                        review.setType(review_object.getString("type"));
                        review.setAge(review_object.getString("age"));
                        review.setBad_point(review_object.getString("bad_point"));
                        review.setDate(review_object.getString("date"));
//            review.setEvaluation(review_object.getString("evaluation"));
                        review.setGender(review_object.getString("gender"));
                        review.setGood_point(review_object.getString("good_point"));
                        review.setId(review_object.getString("id"));
                        review.setLikes(review_object.getString("likes"));
                        review.setMember(review_object.getString("member"));
                        review.setNickname(review_object.getString("nickname"));
                        review.setPeriod(review_object.getString("period"));
//            review.setPicture()
                        review.setProduct_name(review_object.getString("product_name"));
                        if (review_object.has("profile")) {
                            review.setProfile(review_object.getString("profile"));
                        }
                        review.setRepurchase(review_object.getString("repurchase"));
                        review.setTarget(review_object.getString("target"));
                        review.setTip(review_object.getString("tip"));
                        review.setViews(review_object.getString("views"));

                        review_array.add(i, review);
                    }

                    reviewAdapter.notifyDataSetChanged();
                } catch (Exception e) {
                    Log.e("re ac", "get review excep", e);
                }
            }
        });
    }

    class InitReviewAsync extends AsyncTask<String, String, String> {


        InitReviewAsync() {
        }

        @Override
        protected void onPreExecute() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loadingDialog.show();
                }
            });

        }

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            String url = httpConnect.getServerWithVersion() + getString(R.string.rest_review_list) + "?id=" + product_id;
            if (!"".equals(filter_key)) {
                url = url + "&filter_key=" + filter_key;
            }
            if (!"".equals(filter_value)) {
                url = url + "&filter_value=" + filter_value;
            }
            if (!"".equals(sort)) {
                url = url + "&sort=" + sort;
            }
            if (!"".equals(evaluation)) {
                url = url + "&evaluation=" + evaluation;
            }

            result = httpConnect.send("", url, csp.getValue("access_token", ""), "GET");
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            setReviewData(result);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loadingDialog.dismiss();
                }
            });
        }
    }
}