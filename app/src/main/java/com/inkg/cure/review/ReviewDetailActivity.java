package com.inkg.cure.review;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.inkg.cure.R;
import com.inkg.cure.adapters.CommentAdapter;
import com.inkg.cure.classes.Comment;
import com.inkg.cure.classes.CureSP;
import com.inkg.cure.classes.HttpConnect;
import com.inkg.cure.classes.JsonMaker;
import com.inkg.cure.classes.LoadingDialog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by inkg on 2017. 2. 25..
 */

public class ReviewDetailActivity extends AppCompatActivity {

    private ImageView img_review_detail_profile;
    private TextView txt_review_detail_nickname;
    private TextView txt_review_detail_review_count;
    private RelativeLayout layout_review_detail_follow;
    private Button btn_review_detail_follow;
    private ImageView img_review_detail_item;
    private TextView txt_review_detail_manufacturer;
    private TextView txt_review_detail_product;
    private ImageView img_review_detail_like;
    private TextView txt_review_detail_like;
    private TextView txt_review_detail_period;
    private TextView txt_review_detail_price;
    private TextView txt_review_detail_good;
    private TextView txt_review_detail_bad;
    private TextView txt_review_detail_tip;
    private ImageView img_review_detail_photo;
    private TextView txt_review_detail_likes;
    private TextView txt_review_detail_reply;
    private TextView txt_review_detail_view;
    private ImageView img_review_detail_good;
    private Button btn_review_detail_good;
    private ImageView img_review_detail_scrap;
    private Button btn_review_detail_scrap;
    private ListView list_review_detail_comment;
    private CommentAdapter commentAdapter;
    private EditText edit_review_detail_reply;
    private Button btn_review_detail_reply;

    private RelativeLayout layout_review_detail_reply;

    private ArrayList<Comment> comment_array = new ArrayList<>();

    private HttpConnect httpConnect;
    private CureSP csp;
    private JsonMaker jsonMaker;
    private String id;
    private boolean is_reply = false;
    private boolean is_like = false;
    private boolean is_scrap = false;

    private LoadingDialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_detail);
        httpConnect = new HttpConnect(getApplicationContext());
        csp = new CureSP(getApplicationContext());
        jsonMaker = new JsonMaker();
        loadingDialog = new LoadingDialog(ReviewDetailActivity.this);

        Intent intent = getIntent();
        id = intent.getStringExtra("id");

        setupViews();
        setCustomActionbar();
    }

    @Override
    protected void onStart() {
        super.onStart();
        InitReviewDetailAsync irda = new InitReviewDetailAsync();
        irda.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void setupViews() {
        img_review_detail_profile = (ImageView) findViewById(R.id.img_review_detail_profile);
        txt_review_detail_nickname = (TextView) findViewById(R.id.txt_review_detail_nickname);
        txt_review_detail_review_count = (TextView) findViewById(R.id.txt_review_detail_review_count);
        layout_review_detail_follow = (RelativeLayout) findViewById(R.id.layout_review_detail_follow);
        btn_review_detail_follow = (Button) findViewById(R.id.btn_review_detail_follow);
        btn_review_detail_follow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        img_review_detail_item = (ImageView) findViewById(R.id.img_review_detail_item);
        txt_review_detail_manufacturer = (TextView) findViewById(R.id.txt_review_detail_manufacturer);
        txt_review_detail_product = (TextView) findViewById(R.id.txt_review_detail_product);
        img_review_detail_like = (ImageView) findViewById(R.id.img_review_detail_like);
        txt_review_detail_like = (TextView) findViewById(R.id.txt_review_detail_like);
        txt_review_detail_period = (TextView) findViewById(R.id.txt_review_detail_period);
        txt_review_detail_price = (TextView) findViewById(R.id.txt_review_detail_price);
        txt_review_detail_good = (TextView) findViewById(R.id.txt_review_detail_good);
        txt_review_detail_bad = (TextView) findViewById(R.id.txt_review_detail_bad);
        txt_review_detail_tip = (TextView) findViewById(R.id.txt_review_detail_tip);
        img_review_detail_photo = (ImageView) findViewById(R.id.img_review_detail_photo);
        txt_review_detail_likes = (TextView) findViewById(R.id.txt_review_detail_likes);
        txt_review_detail_reply = (TextView) findViewById(R.id.txt_review_detail_reply);
        txt_review_detail_view = (TextView) findViewById(R.id.txt_review_detail_view);
        img_review_detail_good = (ImageView) findViewById(R.id.img_review_detail_good);
        btn_review_detail_good = (Button) findViewById(R.id.btn_review_detail_good);
        btn_review_detail_good.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final LikeAsync la = new LikeAsync();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (is_like) {
                            is_like = false;
                            img_review_detail_good.setImageDrawable(getResources().getDrawable(R.drawable.product_icon_heart_nor, null));
                            la.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "delete");
                        } else {
                            is_like = true;
                            img_review_detail_good.setImageDrawable(getResources().getDrawable(R.drawable.product_icon_heart_active, null));
                            la.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "post");
                        }
                    }
                });
            }
        });
        img_review_detail_scrap = (ImageView) findViewById(R.id.img_review_detail_scrap);
        btn_review_detail_scrap = (Button) findViewById(R.id.btn_review_detail_scrap);
        btn_review_detail_scrap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ScrapAsync sa = new ScrapAsync();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (is_scrap) {
                            is_scrap = false;
                            img_review_detail_scrap.setImageDrawable(getResources().getDrawable(R.drawable.scrap_inactive, null));
                            sa.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "delete");
                        } else {
                            is_scrap = true;
                            img_review_detail_scrap.setImageDrawable(getResources().getDrawable(R.drawable.scrap_active, null));
                            sa.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "post");
                        }
                    }
                });
            }
        });

        list_review_detail_comment = (ListView) findViewById(R.id.list_review_detail_comment);
        commentAdapter = new CommentAdapter(getApplicationContext(), R.layout.adapter_review_comment, comment_array);
        list_review_detail_comment.setAdapter(commentAdapter);

        layout_review_detail_reply = (RelativeLayout) findViewById(R.id.layout_review_detail_reply);
        edit_review_detail_reply = (EditText) findViewById(R.id.edit_review_detail_reply);

        TextWatcher reply_watcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            layout_review_detail_reply.setBackgroundColor(getResources().getColor(R.color.maya_blue1, null));
                            is_reply = true;
                        }
                    });
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            layout_review_detail_reply.setBackgroundColor(getResources().getColor(R.color.manatee, null));
                            is_reply = false;
                        }
                    });
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };

        edit_review_detail_reply.addTextChangedListener(reply_watcher);

        btn_review_detail_reply = (Button) findViewById(R.id.btn_review_detail_reply);
        btn_review_detail_reply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (is_reply) {
                    WriteCommentAsync wca = new WriteCommentAsync(edit_review_detail_reply.getText().toString());
                    wca.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
            }
        });
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
    }

    class LikeAsync extends AsyncTask<String, String, String> {

        String type = "";

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            type = strings[0];
            String json = jsonMaker.makeJson(new Pair("target", id));
            if ("post".equals(type)) {
                result = httpConnect.send(json, httpConnect.getServerWithVersion() + getString(R.string.rest_review_likes), csp.getValue("access_token", ""));
            } else {
                result = httpConnect.send(json, httpConnect.getServerWithVersion() + getString(R.string.rest_review_likes), csp.getValue("access_token", ""), "DELETE");
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

        }

    }

    class ScrapAsync extends AsyncTask<String, String, String> {
        String type = "";

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            type = strings[0];
            String json = jsonMaker.makeJson(new Pair("target", id));
            if ("post".equals(type)) {
                result = httpConnect.send(json, httpConnect.getServerWithVersion() + getString(R.string.rest_review_scrap), csp.getValue("access_token", ""));
            } else {
                result = httpConnect.send(json, httpConnect.getServerWithVersion() + getString(R.string.rest_review_scrap), csp.getValue("access_token", ""), "DELETE");
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

        }
    }

    private void setCustomActionbar() {

        ActionBar actionBar = getSupportActionBar();

        // Custom Actionbar를 사용하기 위해 CustomEnabled을 true 시키고 필요 없는 것은 false 시킨다

        actionBar.setDisplayShowCustomEnabled(true);

        actionBar.setDisplayHomeAsUpEnabled(false);

        actionBar.setDisplayShowTitleEnabled(false);

        // Set custom view layout

        View mCustomView = LayoutInflater.from(this).inflate(R.layout.actionbar_mypage, null);

        actionBar.setCustomView(mCustomView);


        // Set no padding both side

        Toolbar parent = (Toolbar) mCustomView.getParent(); // first get parent toolbar of current action bar

        parent.setContentInsetsAbsolute(0, 0);              // set padding programmatically to 0dp

        // Set actionbar background image

        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.border_layout_bottom));

        // Set actionbar layout layoutparams

        ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);

        actionBar.setCustomView(mCustomView, params);

        ImageView img_actionbar_back = (ImageView) findViewById(R.id.img_actionbar_mypage_back);
        img_actionbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        TextView txt_actionbar = (TextView) findViewById(R.id.txt_actionbar_mypage);
        txt_actionbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        txt_actionbar.setText("리뷰보기");

        Button btn_actionbar_setting = (Button) findViewById(R.id.btn_actionbar_setting);
        btn_actionbar_setting.setVisibility(View.GONE);

        Button btn_actionbar_alarm = (Button) findViewById(R.id.btn_actionbar_alarm);
        btn_actionbar_alarm.setVisibility(View.GONE);

    }

    private void setDetailData(final String json) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    JSONObject review_json = new JSONObject(json);
                    final URL[] imageURL = new URL[1];
                    final Bitmap[] bm = new Bitmap[1];
                    String img_item = "";
                    if (review_json.has("image")) {
                        img_item = review_json.getString("image");
                    } else {
                        img_item = "null";
                    }

                    final String finalImg_item = img_item;
                    Thread mThread = new Thread() {
                        @Override
                        public void run() {
                            try {
                                if ("null".equals(finalImg_item)) {

                                } else {
                                    imageURL[0] = new URL(finalImg_item);

                                    HttpURLConnection conn = (HttpURLConnection) imageURL[0].openConnection();
                                    BufferedInputStream bis = new BufferedInputStream(conn.getInputStream(), 1024);
                                    bm[0] = BitmapFactory.decodeStream(bis);
                                    bis.close();

                                }
                            } catch (Exception e) {
                                Log.e("medi deta acti", "image url excep", e);
                            }
                        }
                    };

                    mThread.start();

                    try {
                        mThread.join();
                        if ("null".equals(img_item)) {
                            img_review_detail_item.setImageResource(R.drawable.no_image);
                        } else {
                            img_review_detail_item.setImageBitmap(bm[0]);
                        }
                    } catch (Exception e) {
                        Log.e("review deta acti", "img set excep", e);
                    }

                    txt_review_detail_product.setText(review_json.getString("product_name"));
//                        score:4,
                    txt_review_detail_good.setText(review_json.getString("good_point"));
                    txt_review_detail_bad.setText(review_json.getString("bad_point"));
                    txt_review_detail_tip.setText(review_json.getString("tip"));

                } catch (Exception e1) {
                    Log.e("review deta acti", "get json excep", e1);
                }
            }
        });
    }

    private void setCommentData(final String json) {
        comment_array.clear();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    JSONObject comment_json = new JSONObject(json);
                    JSONArray comment_item_array = comment_json.getJSONArray("Items");
                    for (int i = 0; i < comment_item_array.length(); i++) {
                        JSONObject comment_object = comment_item_array.getJSONObject(i);
                        Comment comment = new Comment();
                        comment.setNickname(comment_object.getString("nickname"));
                        comment.setContent(comment_object.getString("comment"));
                        comment.setTime(comment_object.getString("date"));

                        comment_array.add(i, comment);
                    }
                    commentAdapter.notifyDataSetChanged();
                } catch (Exception e) {
                    Log.e("review deta acti", "get comment excep", e);
                } finally {
                    loadingDialog.dismiss();
                }

//                    "Items": [
//                    {
//                        "comment": "댓글",
//                            "date": "2016-12-03T10:38:50.219833",
//                            "member": "iidd58@gmail.com",
//                            "id": "9dfed196ca703854a613736116be5060",
//                            "nickname": "dla12",
//                            "target": "리뷰ID"
//                    }
//                    ],
//                    "count": 10,
//                        "lastkey": "eyJkYXRl
            }
        });
    }

    class InitReviewDetailAsync extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loadingDialog.show();
                }
            });
        }

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            result = httpConnect.send("", httpConnect.getServerWithVersion() + getString(R.string.rest_review) + "?id=" + id, csp.getValue("access_token", ""), "GET");
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            setDetailData(result);

            CommentAsync ca = new CommentAsync();
            ca.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    class CommentAsync extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            result = httpConnect.send("", httpConnect.getServerWithVersion() + getString(R.string.rest_review_comment) + "?id=" + id, csp.getValue("access_token", ""), "GET");
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            setCommentData(result);
        }
    }

    class WriteCommentAsync extends AsyncTask<String, String, String> {

        private String comment;

        WriteCommentAsync(String comment) {
            this.comment = comment;
        }

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            String json = jsonMaker.makeJson(new Pair("target", id), new Pair("comment", this.comment));
            result = httpConnect.send(json, httpConnect.getServerWithVersion() + getString(R.string.rest_review_comment), csp.getValue("access_token", ""));
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getApplicationContext(), "댓글이 등록되었습니다.", Toast.LENGTH_SHORT).show();
                    edit_review_detail_reply.setText("");
                    CommentAsync ca = new CommentAsync();
                    ca.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    commentAdapter.notifyDataSetChanged();

//                    InitReviewDetailAsync irda = new InitReviewDetailAsync();
//                    irda.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
            });
            Log.e("comment id", result);
        }
    }
}