package com.inkg.cure;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.inkg.cure.classes.BackPressCloseHandler;
import com.inkg.cure.classes.ResultCodes;
import com.kakao.auth.ErrorCode;
import com.kakao.auth.ISessionCallback;
import com.kakao.auth.Session;
import com.kakao.network.ErrorResult;
import com.kakao.usermgmt.LoginButton;
import com.kakao.usermgmt.UserManagement;
import com.kakao.usermgmt.callback.LogoutResponseCallback;
import com.kakao.usermgmt.callback.MeResponseCallback;
import com.kakao.usermgmt.response.model.UserProfile;
import com.kakao.util.exception.KakaoException;
import com.nhn.android.naverlogin.OAuthLogin;
import com.nhn.android.naverlogin.OAuthLoginHandler;

import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import static com.kakao.util.helper.Utility.getPackageInfo;

public class LoginActivity extends FragmentActivity implements
        GoogleApiClient.OnConnectionFailedListener {

    private CallbackManager callbackManager;
    private Button btn_login_facebook;
    private GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 9001;
    private static final int RC_FB = 64206;
    private OAuthLogin mOAuthLoginModule;
    private Button btn_login_naver;
    private LoginButton btn_login_kakao;
    private Button btn_login_another;
    private Button btn_login_google;
    private TextView txt_login;
    private OAuthLoginHandler mOAuthLoginHandler;
    private static final String OAUTH_CLIENT_ID = "ar8whH4o7Xvw5S378VG6";
    private static final String OAUTH_CLIENT_SECRET = "YuLH4AGZX_";
    private AccessToken accessToken;
    private AccessTokenTracker accessTokenTracker;
    private SessionCallback sessionCallback;      //콜백 선언

    private BackPressCloseHandler bpc;
    private LoginButton btn_kakao_login;

    private RelativeLayout layout_login_facebook;
    private ImageView img_login_facebook;
    private TextView txt_login_facebook;

    private RelativeLayout layout_login_kakao;
    private ImageView img_login_kakao;
    private TextView txt_login_kakao;

    private RelativeLayout layout_login_google;
    private ImageView img_login_google;
    private TextView txt_login_google;

    private RelativeLayout layout_login_another;
    private ImageView img_login_another;
    private TextView txt_login_another;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // facebook
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_login);
//        Log.e("keyhash", getKeyHash(this));

        bpc = new BackPressCloseHandler(this);

        setFacebookLogin();
        setGoogleLogin();
//        setNaverLogin();
        setKakaoLogin();

        layout_login_another = (RelativeLayout) findViewById(R.id.layout_login_another);
        img_login_another = (ImageView) findViewById(R.id.img_login_another);
        txt_login_another = (TextView) findViewById(R.id.txt_login_another);

        btn_login_another = (Button) findViewById(R.id.btn_login_another);
        btn_login_another.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    layout_login_another.setBackground(getResources().getDrawable(R.drawable.rounded_border_btn_blue, null));
                    img_login_another.setImageDrawable(getResources().getDrawable(R.drawable.email, null));
                    txt_login_another.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.maya_blue1));
                    Intent joinIntent = new Intent(LoginActivity.this, JoinActivity.class);
                    startActivityForResult(joinIntent, 0);
                }

                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    layout_login_another.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.bright_cerulean));
                    img_login_another.setImageDrawable(getResources().getDrawable(R.drawable.email_active, null));
                    txt_login_another.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
                }
                return false;
            }
        });

        txt_login = (TextView) findViewById(R.id.txt_login);
        SpannableString content = new SpannableString(" 로그인");
        content.setSpan(new UnderlineSpan(), 1, content.length(), 0);
        txt_login.setText(content);
        txt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent anotherLoginIntent = new Intent(LoginActivity.this, AnotherLoginActivity.class);
                startActivityForResult(anotherLoginIntent, 0);
            }
        });
    }

    public static String getKeyHash(final Context context) {
        PackageInfo packageInfo = getPackageInfo(context, PackageManager.GET_SIGNATURES);
        if (packageInfo == null)
            return null;

        for (Signature signature : packageInfo.signatures) {
            try {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                return android.util.Base64.encodeToString(md.digest(), android.util.Base64.NO_WRAP);
            } catch (NoSuchAlgorithmException e) {
                Log.e("TAG", "Unable to get MessageDigest. signature=" + signature, e);
            }
        }
        return null;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.e("oauth request code", requestCode + "");
        Log.e("oauth result code", resultCode + "");

        if (resultCode == ResultCodes.AN_LOGIN) {
            Intent an_intent = new Intent();
            setResult(resultCode, an_intent);
            finish();
        }

        if (resultCode == ResultCodes.KK_LOGIN) {
            Intent kk_intent = new Intent();
            kk_intent.putExtra("token", data.getStringExtra("token"));
            setResult(resultCode, kk_intent);
            finish();
        }

        if (resultCode == ResultCodes.AN_JOIN) {
            setResult(resultCode);
            finish();
        }

        callbackManager.onActivityResult(requestCode, resultCode, data);

        // google login
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
//            GoogleSignInResult mResult = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
//            handleGoogleSignInResult(mResult);
            GoogleResultAsync gra = new GoogleResultAsync(data);
            gra.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }

        //kakao
        if (Session.getCurrentSession().handleActivityResult(requestCode, resultCode, data)) {
//            requestMe();
            return;
        }
    }

    private void setFacebookLogin() {

//        AppEventsLogger.activateApp(this);

        callbackManager = CallbackManager.Factory.create();

        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(
                    AccessToken oldAccessToken,
                    AccessToken currentAccessToken) {
                // Set the access token using
                // currentAccessToken when it's loaded or set.
            }
        };
        // If the access token is available already assign it.
        accessToken = AccessToken.getCurrentAccessToken();

        layout_login_facebook = (RelativeLayout) findViewById(R.id.layout_login_facebook);
        img_login_facebook = (ImageView) findViewById(R.id.img_login_facebook);
        txt_login_facebook = (TextView) findViewById(R.id.txt_login_facebook);

        btn_login_facebook = (Button) findViewById(R.id.btn_login_facebook);
        btn_login_facebook.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    layout_login_facebook.setBackground(getResources().getDrawable(R.drawable.rounded_border_btn_blue, null));
                    img_login_facebook.setImageDrawable(getResources().getDrawable(R.drawable.facebook, null));
                    txt_login_facebook.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.maya_blue1));
                    LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Arrays.asList("public_profile", "user_friends", "email", "user_birthday"));
                }

                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    layout_login_facebook.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.bright_cerulean));
                    img_login_facebook.setImageDrawable(getResources().getDrawable(R.drawable.facebook_active, null));
                    txt_login_facebook.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
                }
                return false;
            }
        });
//        btn_login_facebook.setReadPermissions("email");
        // If using in a fragment
//        btn_login_facebook.setFragment(this);
        // Other app specific specialization

        // Callback registration
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {

            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                accessToken = AccessToken.getCurrentAccessToken();
                Log.e("success!", "yes!");

                FacebookProfileAsync fpa = new FacebookProfileAsync(loginResult);
                fpa.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }

            @Override
            public void onCancel() {
                // App code
                Log.e("cancel!", "yes!");
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Log.e("error!", "yes!");
                Log.e("TAG", "fb login exception", exception);
                AccessToken.setCurrentAccessToken(null);
                LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Arrays.asList("public_profile", "user_friends", "email", "user_birthday"));
            }
        });
    }

    private void setFacebookData(final LoginResult loginResult, final Profile profile) {
        GraphRequest request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {

                        // Application code
                        try {
                            Log.e("Response", response.toString());

                            String email = "email@email.com";
                            if (response.getJSONObject().has("email")) {
                                email = response.getJSONObject().getString("email");
                            }

                            String firstName = "firstName";
                            if (response.getJSONObject().has("first_name")) {
                                firstName = response.getJSONObject().getString("first_name");
                            }

                            String lastName = "lastName";
                            if (response.getJSONObject().has("last_name")) {
                                lastName = response.getJSONObject().getString("last_name");
                            }

                            String gender = "female";
                            if (response.getJSONObject().has("gender")) {
                                gender = response.getJSONObject().getString("gender");
                            }
//                            String bday = response.getJSONObject().getString("birthday");
                            String id = response.getJSONObject().getString("id");
                            String bday = "birthday";

                            if (Profile.getCurrentProfile() != null) {
//                                Log.i("Login", "ProfilePic" + Profile.getCurrentProfile().getProfilePictureUri(200, 200));
                            } else {
                                Thread.sleep(2000);
                            }
                            Intent fb_intent = new Intent();
                            fb_intent.putExtra("email", email);
                            fb_intent.putExtra("token", accessToken.getToken());
                            fb_intent.putExtra("name", firstName + " " + lastName);
                            fb_intent.putExtra("gender", gender);
                            fb_intent.putExtra("age", bday);
                            setResult(ResultCodes.FB_LOGIN, fb_intent);
                            finish();

                        } catch (Exception e) {
                            Log.e("facebook set data ex", "??", e);
                            Toast.makeText(getApplicationContext(), "로그인에 실패했습니다. 다시 시도해주세요.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,email,first_name,last_name,gender, birthday");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private class FacebookProfileAsync extends AsyncTask<String, String, String> {

        private ProfileTracker mProfileTracker;
        private Profile mProfile;
        private LoginResult mLoginResult;

        private FacebookProfileAsync(LoginResult loginResult) {
            this.mLoginResult = loginResult;
        }

        @Override
        protected String doInBackground(String... strings) {

            if (Profile.getCurrentProfile() == null) {
                mProfileTracker = new ProfileTracker() {
                    @Override
                    protected void onCurrentProfileChanged(Profile profile, Profile profile2) {
                        // profile2 is the new profile
                        Log.e("facebook - profile", profile2.getName());
                        mProfile = profile2;
                        mProfileTracker.stopTracking();
                    }
                };
                // no need to call startTracking() on mProfileTracker
                // because it is called by its constructor, internally.
            } else {
                Profile profile = Profile.getCurrentProfile();
                mProfile = profile;
                Log.e("facebook - profile", profile.getName());
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            setFacebookData(mLoginResult, mProfile);
        }
    }

    private class GoogleResultAsync extends AsyncTask<String, String, String> {

        private GoogleSignInResult mResult;
        private Intent mData;

        private GoogleResultAsync(Intent data) {
            this.mData = data;
        }

        @Override
        protected String doInBackground(String... strings) {
            mResult = Auth.GoogleSignInApi.getSignInResultFromIntent(mData);
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            handleGoogleSignInResult(mResult);
        }
    }

    private void setGoogleLogin() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestScopes(new Scope(Scopes.PLUS_LOGIN))
                .requestIdToken(getString(R.string.google_web_api_key))
                .requestServerAuthCode(getString(R.string.google_web_api_key), false)
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(LoginActivity.this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        layout_login_google = (RelativeLayout) findViewById(R.id.layout_login_google);
        img_login_google = (ImageView) findViewById(R.id.img_login_google);
        txt_login_google = (TextView) findViewById(R.id.txt_login_google);

        btn_login_google = (Button) findViewById(R.id.btn_login_google);
//        signInButton.setSize(SignInButton.SIZE_STANDARD);
//        signInButton.setScopes(gso.getScopeArray());
        btn_login_google.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    layout_login_google.setBackground(getResources().getDrawable(R.drawable.rounded_border_btn_blue, null));
                    img_login_google.setImageDrawable(getResources().getDrawable(R.drawable.google, null));
                    txt_login_google.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.maya_blue1));

                    Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                    startActivityForResult(signInIntent, RC_SIGN_IN);
                }

                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    layout_login_google.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.bright_cerulean));
                    img_login_google.setImageDrawable(getResources().getDrawable(R.drawable.google_active, null));
                    txt_login_google.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
                }
                return false;
            }
        });
    }

    private void handleGoogleSignInResult(GoogleSignInResult result) {
        Log.e("GoogleResult", "handleGoogleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleAccessTokenAsync gata = new GoogleAccessTokenAsync(result);
            gata.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        } else {
            // Signed out, show unauthenticated UI.
            Log.e("GoogleSignInResult", result.isSuccess() + "");
            Toast.makeText(getApplicationContext(), "로그인에 실패했습니다. 다시 시도해주세요.", Toast.LENGTH_SHORT).show();
//            updateUI(false);
        }
    }

    class GoogleAccessTokenAsync extends AsyncTask<String, String, String> {

        GoogleSignInResult m_sign_result;
        String auth_code = "";
        String name = "";
        String id = "";
        String id_token = "";

        public GoogleAccessTokenAsync() {

        }

        public GoogleAccessTokenAsync(GoogleSignInResult m_sign_result) {
            this.m_sign_result = m_sign_result;
            GoogleSignInAccount acct = m_sign_result.getSignInAccount();
            auth_code = acct.getServerAuthCode();
            id_token = acct.getIdToken();
            Log.e("auth_code", auth_code);
            Log.e("id_token", id_token);
            name = acct.getDisplayName();
            id = acct.getId();
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(String... strings) {

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Intent gg_intent = new Intent();
            gg_intent.putExtra("name", name);
            gg_intent.putExtra("id", id);
            gg_intent.putExtra("access_token", auth_code);
            Log.e("google result", name + " / " + id + " / " + auth_code);
            setResult(ResultCodes.GG_LOGIN, gg_intent);
            finish();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onBackPressed() {
        bpc.onBackPressed();
    }

//    private void setNaverLogin() {
//        mOAuthLoginModule = OAuthLogin.getInstance();
//        mOAuthLoginModule.init(
//                LoginActivity.this
//                , OAUTH_CLIENT_ID
//                , OAUTH_CLIENT_SECRET
////                , OAUTH_CLIENT_NAME
//                , "Cure_naver"
//                //,OAUTH_CALLBACK_INTENT
//                // SDK 4.1.4 버전부터는 OAUTH_CALLBACK_INTENT변수를 사용하지 않습니다.
//        );
//
//        mOAuthLoginHandler = new OAuthLoginHandler() {
//            @Override
//            public void run(boolean success) {
//                if (success) {
//                    String accessToken = mOAuthLoginModule.getAccessToken(getApplicationContext());
//                    String refreshToken = mOAuthLoginModule.getRefreshToken(getApplicationContext());
//                    long expiresAt = mOAuthLoginModule.getExpiresAt(getApplicationContext());
//                    String tokenType = mOAuthLoginModule.getTokenType(getApplicationContext());
////                mOauthAT.setText(accessToken);
////                mOauthRT.setText(refreshToken);
////                mOauthExpires.setText(String.valueOf(expiresAt));
////                mOauthTokenType.setText(tokenType);
////                mOAuthState.setText(mOAuthLoginModule.getState(getApplicationContext()).toString());
//
//                    Intent nv_intent = new Intent();
//                    nv_intent.putExtra("token", accessToken);
//                    setResult(ResultCodes.NV_LOGIN, nv_intent);
//                    finish();
//
//                } else {
//                    String errorCode = mOAuthLoginModule.getLastErrorCode(getApplicationContext()).getCode();
//                    String errorDesc = mOAuthLoginModule.getLastErrorDesc(getApplicationContext());
//                    Toast.makeText(getApplicationContext(), "errorCode:" + errorCode
//                            + ", errorDesc:" + errorDesc, Toast.LENGTH_SHORT).show();
//                }
//            }
//        };
//
//        btn_login_naver = (Button) findViewById(R.id.btn_login_naver);
////        btn_login_naver.setOAuthLoginHandler(mOAuthLoginHandler);
////        btn_login_naver.setBgResourceId(R.drawable.img_loginbtn_usercustom);
//        btn_login_naver.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                mOAuthLoginModule.startOauthLoginActivity(LoginActivity.this, mOAuthLoginHandler);
//            }
//        });
//    }

    /**
     * OAuthLoginHandler를 startOAuthLoginActivity() 메서드 호출 시 파라미터로 전달하거나 OAuthLoginButton
     * 객체에 등록하면 인증이 종료되는 것을 확인할 수 있습니다.
     */

    private void setKakaoLogin() {

        Session.getCurrentSession().getAccessToken();
        UserManagement.requestLogout(new LogoutResponseCallback() {
            @Override
            public void onCompleteLogout() {
                //로그아웃 성공 후 하고싶은 내용 코딩 ~
                Log.e("lo ca", "kakao logout");
            }
        });

        layout_login_kakao = (RelativeLayout) findViewById(R.id.layout_login_kakao);
        img_login_kakao = (ImageView) findViewById(R.id.img_login_kakao);
        txt_login_kakao = (TextView) findViewById(R.id.txt_login_kakao);
        btn_login_kakao = (LoginButton) findViewById(R.id.btn_login_kakao);
        btn_login_kakao.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    layout_login_kakao.setBackground(getResources().getDrawable(R.drawable.rounded_border_btn_blue, null));
                    img_login_kakao.setImageDrawable(getResources().getDrawable(R.drawable.kakao, null));
                    txt_login_kakao.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.maya_blue1));
                }

                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    layout_login_kakao.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.bright_cerulean));
                    img_login_kakao.setImageDrawable(getResources().getDrawable(R.drawable.kakao_active, null));
                    txt_login_kakao.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
                }
                return false;
            }
        });

        sessionCallback = new SessionCallback();
        Session.getCurrentSession().addCallback(sessionCallback);
    }

    private class SessionCallback implements ISessionCallback {

        @Override
        public void onSessionOpened() {
            requestMe();
        }

        @Override
        public void onSessionOpenFailed(KakaoException exception) {
            // 세션 연결이 실패했을때
            Log.e("lo ac", "kakao onSessionopenFailed", exception);
        }
    }

    private void requestMe() {
        UserManagement.requestMe(new MeResponseCallback() {

            @Override
            public void onFailure(ErrorResult errorResult) {
                String message = "failed to get user info. msg=" + errorResult;
                Log.e("kakao onfailure", message);
                ErrorCode result = ErrorCode.valueOf(errorResult.getErrorCode());
                if (result == ErrorCode.CLIENT_ERROR_CODE) {
                    finish();
                } else {
                    //redirectMainActivity();
                }
            }

            @Override
            public void onSessionClosed(ErrorResult errorResult) {
                Log.e("kakao onSessionClosed", errorResult + "");
            }

            @Override
            public void onNotSignedUp() {
                Log.e("kakao onNotSignedUp", "");
            }

            @Override
            public void onSuccess(UserProfile userProfile) {
                //로그인에 성공하면 로그인한 사용자의 일련번호, 닉네임, 이미지url등을 리턴합니다.
                //사용자 ID는 보안상의 문제로 제공하지 않고 일련번호는 제공합니다.
                Log.e("UserProfile", userProfile.toString());
                Intent kk_intent = new Intent();
                kk_intent.putExtra("token", Session.getCurrentSession().getAccessToken());
                kk_intent.putExtra("nickname", userProfile.getNickname());
                kk_intent.putExtra("gender", "female");
                kk_intent.putExtra("age", "20");
                setResult(ResultCodes.KK_LOGIN, kk_intent);
                finish();
            }
        });
    }
}