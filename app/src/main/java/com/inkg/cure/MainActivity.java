package com.inkg.cure;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.inkg.cure.adapters.MainAdapter;
import com.inkg.cure.classes.BackPressCloseHandler;
import com.inkg.cure.classes.CureSP;
import com.inkg.cure.classes.HttpConnect;
import com.inkg.cure.classes.JsonMaker;
import com.inkg.cure.classes.LoadingDialog;
import com.inkg.cure.classes.NonSwipeViewPager;
import com.inkg.cure.classes.ResultCodes;
import com.inkg.cure.fragments.HomeFragment;
import com.inkg.cure.fragments.IngredientFragment;
import com.inkg.cure.fragments.PharmacyFragment;
import com.inkg.cure.mypage.MypageActivity;
import com.inkg.cure.search.SearchActivity;

import org.json.JSONObject;

import java.util.List;
import java.util.Vector;

public class MainActivity extends FragmentActivity {

    private CureSP csp;
    private boolean login;
    private HttpConnect httpConnect;
    private JsonMaker jsonMaker;
    private Button btn_main_home;
    private Button btn_main_pharmacy;
    private Button btn_main_ingredient;
    private Button btn_main_mypage;
    private ImageView img_main_logo;
    private NonSwipeViewPager viewPager;
    private EditText edit_main_search;
    private String access_token;
    private String category;
    private ProgressDialog progressDialog;
    private LoadingDialog ld;

    private BackPressCloseHandler bpc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bpc = new BackPressCloseHandler(this);
        progressDialog = new ProgressDialog(MainActivity.this);

        csp = new CureSP(getApplicationContext());
        httpConnect = new HttpConnect(getApplicationContext());
        jsonMaker = new JsonMaker();
        setupViews();
        login = false;

        login = csp.getValue("login", false);

        if (login) {

            changeFragment(new HomeFragment());
            initialisePaging();
        } else {
            Intent loginIntent = new Intent(MainActivity.this, LoginActivity.class);
            startActivityForResult(loginIntent, 0);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("main result code", resultCode + "");

        switch (resultCode) {
            case 0: // backpressed
                finish();
                break;
            case ResultCodes.FB_LOGIN:
                String fb_token = data.getStringExtra("token");
                String fb_name = data.getStringExtra("name");
                String fb_gender = data.getStringExtra("gender");
                String fb_age = data.getStringExtra("age");
                String fb_json_data = jsonMaker.makeJson(new Pair("access_token", fb_token));
                String fb_json_data_2 = jsonMaker.makeJson(new Pair("nickname", fb_name),
                        new Pair("gender", fb_gender),
                        new Pair("age", fb_age),
                        new Pair("pathtojoin", "facebook"));
                String fb_url = getString(R.string.server_url) + getString(R.string.rest_fb_token);
                FirstLoginAsync fb_fla = new FirstLoginAsync();
                fb_fla.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, fb_json_data, fb_url, fb_json_data_2);
                break;
            case ResultCodes.KK_LOGIN:
                String kk_token = data.getStringExtra("token");
                String kk_nickname = data.getStringExtra("nickname");
                if ("".equals(kk_nickname)) {
                    kk_nickname = "nickname";
                }
                String kk_gender = data.getStringExtra("gender");
                String kk_age = data.getStringExtra("age");
                String kk_json_data = jsonMaker.makeJson(new Pair("access_token", kk_token));
                String kk_json_data_2 = jsonMaker.makeJson(new Pair("nickname", kk_nickname), new Pair("gender", kk_gender), new Pair("age", kk_age), new Pair("pathtojoin", "kakao"));
                String kk_url = getString(R.string.server_url) + getString(R.string.rest_kk_token);
                FirstLoginAsync kk_fla = new FirstLoginAsync();
                kk_fla.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, kk_json_data, kk_url, kk_json_data_2);

                break;
            case ResultCodes.GG_LOGIN:
                String gg_result = "";
                String gg_access_token = data.getStringExtra("access_token");
                String gg_id_token = data.getStringExtra("id_token");
                String gg_email = data.getStringExtra("id");
                String gg_name = data.getStringExtra("name");
                String gg_gender = "female";
                String gg_age = "20";
                String gg_json_data = jsonMaker.makeJson(new Pair("access_token", gg_access_token));
                String gg_json_data_2 = jsonMaker.makeJson(new Pair("nickname", gg_name), new Pair("gender", gg_gender), new Pair("age", gg_age), new Pair("pathtojoin", "google"));
                String gg_url = getString(R.string.server_url) + getString(R.string.rest_gg_token);
//                Log.e("google token", gg_access_token);
//                gg_result = httpConnect.send("", getString(R.string.server_url) + "/login/google?code=" + gg_access_token, "", "GET");
                FirstLoginAsync gg_fla = new FirstLoginAsync();
                gg_fla.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, gg_json_data, gg_url, gg_json_data_2);
//                gg_result = httpConnect.send(gg_json_data, getString(R.string.server_url) + getString(R.string.rest_gg_token), "");
//                csp.put("login", true);
//                Log.e("first_result", gg_result);
                break;
            case ResultCodes.NV_LOGIN:
                String nv_result = "";
                String nv_token = data.getStringExtra("token");
                String nv_json_data = jsonMaker.makeJson(new Pair("access_token", nv_token));
                nv_result = httpConnect.send(nv_json_data, getString(R.string.server_url) + getString(R.string.rest_nv_token), "");
                Log.e("first_result", nv_result);

                break;
            case ResultCodes.AN_LOGIN:
                csp.put("login", true);
                break;

            case ResultCodes.AN_JOIN:
                csp.put("login", true);
                break;

            case ResultCodes.CURE_LOGOUT:
                csp.put("login", false);
                Intent login_intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivityForResult(login_intent, 0);
                break;

            case ResultCodes.BACK:
                break;

        }
    }
    
    class FirstLoginAsync extends AsyncTask<String, String, String> {

        String second_json = "";

        @Override
        protected void onPreExecute() {
            ld = new LoadingDialog(MainActivity.this);
            ld.setCancelable(false);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ld.show();
                }
            });

        }

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            String json = strings[0];
            String url = strings[1];
            second_json = strings[2];
            result = httpConnect.send(json, url, "");
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.e("first_result", result);

            try {
                JSONObject result_json = new JSONObject(result);
                if ("1".equals(result_json.get("click_wrap").toString())) { // input additional data
                    String tempToken = result_json.get("temp_token").toString();
                    String second_url = httpConnect.getServerWithVersion() + getString(R.string.rest_tokens);
                    SecondLoginAsync sla = new SecondLoginAsync();
                    sla.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, second_json, second_url, tempToken);
                } else {

                    String at = result_json.get("access_token").toString();
                    String expire = result_json.get("expire").toString();
                    String refresh_token = result_json.get("refresh_token").toString();
                    String refresh_expire = result_json.get("refresh_exp").toString();

                    csp.put("refresh_token", refresh_token);
                    csp.put("refresh_expire", refresh_expire);
                    csp.put("expire", expire);
                    csp.put("access_token", at);
                    csp.put("login", true);
                }
            } catch (Exception e) {
                Log.e("main", "login first excep", e);
            } finally {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ld.dismiss();
                    }
                });
                initialisePaging();
            }
        }

    }

    class SecondLoginAsync extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            String second_json = strings[0];
            String url = strings[1];
            String tempToken = strings[2];
            Log.e("??", second_json + " / " + url + " / " + tempToken);
            result = httpConnect.send(second_json, url, tempToken);
            Log.e("second result", result);
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                JSONObject result_json = new JSONObject(result);
                String at = result_json.get("access_token").toString();
                String expire = result_json.get("expire").toString();
                String refresh_token = result_json.get("refresh_token").toString();
                String refresh_expire = result_json.get("refresh_exp").toString();

                csp.put("refresh_token", refresh_token);
                csp.put("refresh_expire", refresh_expire);
                csp.put("expire", expire);
                csp.put("access_token", at);
                csp.put("login", true);

            } catch (Exception e) {
                Log.e("main", "login second excep", e);
            } finally {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ld.dismiss();
                    }
                });
                initialisePaging();
            }
        }

    }

    @Override
    public void onBackPressed() {
        bpc.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setupViews() {

        btn_main_home = (Button) findViewById(R.id.btn_main_home);
        btn_main_pharmacy = (Button) findViewById(R.id.btn_main_pharmacy);
        btn_main_ingredient = (Button) findViewById(R.id.btn_main_ingredient);

        img_main_logo = (ImageView) findViewById(R.id.img_main_logo);
        img_main_logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewPager.setCurrentItem(0);
            }
        });

        btn_main_mypage = (Button) findViewById(R.id.btn_main_mypage);
        btn_main_mypage.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    // mypage
                    Intent my_intent = new Intent(MainActivity.this, MypageActivity.class);
                    startActivityForResult(my_intent, 111);

                }
                return false;
            }
        });

        btn_main_home.setTextColor(getResources().getColor(R.color.maya_blue1, null));
        btn_main_home.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, R.drawable.main_menubar_bottom_small);

    }

    private void changeFragment(Fragment targetFragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.vp_main, targetFragment, "fragment")
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    private void initialisePaging() {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
            }
        });
        // test token
//        access_token = "K98S9CoErFYyfCAM_1lttzofNfTt4BttYc91GF1QppUZyDQ_Y_uc9bgV0NkSytkykN-_Uuomth0LAsw4VeTIr23tn0UwKnQMu25tc-NxnNIE38yYN_f_kWEpyHELdjGE2pO77_FUnGVtqU0u6ZTG4_vGrdHIpJzoX2hIriyIYnkE9kdQbRDK_nS2WwPE192HpPt6P6zPLx54UhpuLw67Ji3k9nx0oAN8eQfTz5c5JbX_ElBbtj62kmIpCvt2wEP0942Qgn2m-bxhXOjoqPqoMLbcu0oyHxpt_dABfD4g72c=";
//        String id = "107963046933679803338";
//        csp.put("access_token", access_token);
//        csp.put("id", id);

        String fcm_token = FirebaseInstanceId.getInstance().getToken();
        csp.put("fcm_token", fcm_token);

        List<Fragment> fragments = new Vector<Fragment>();
        fragments.add(Fragment.instantiate(this, HomeFragment.class.getName()));
        fragments.add(Fragment.instantiate(this, PharmacyFragment.class.getName()));
        fragments.add(Fragment.instantiate(this, IngredientFragment.class.getName()));
        MainAdapter mainAdapter = new MainAdapter(super.getSupportFragmentManager(), fragments);

        viewPager = (NonSwipeViewPager) super.findViewById(R.id.vp_main);
        viewPager.setEnabled(false);
        viewPager.setOffscreenPageLimit(3);
        viewPager.setAdapter(mainAdapter);

//        viewPager.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
//                return true; // disable scroll
//            }
//        });
        viewPager.addOnPageChangeListener(new NonSwipeViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onPageSelected(int position) {
                btn_main_home.setTextColor(getResources().getColor(R.color.manatee1, null));
                btn_main_pharmacy.setTextColor(getResources().getColor(R.color.manatee1, null));
                btn_main_ingredient.setTextColor(getResources().getColor(R.color.manatee1, null));
                btn_main_home.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                btn_main_pharmacy.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                btn_main_ingredient.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

                switch (position) {
                    case 0:
                        btn_main_home.setTextColor(getResources().getColor(R.color.maya_blue1, null));
                        btn_main_home.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, R.drawable.main_menubar_bottom_small);
                        break;
                    case 1:
                        btn_main_pharmacy.setTextColor(getResources().getColor(R.color.maya_blue1, null));
                        btn_main_pharmacy.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, R.drawable.main_menubar_bottom_small);
                        break;
                    case 2:
                        btn_main_ingredient.setTextColor(getResources().getColor(R.color.maya_blue1, null));
                        btn_main_ingredient.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, R.drawable.main_menubar_bottom_small);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                viewPager.setCurrentItem(0);
                btn_main_home.setTextColor(getResources().getColor(R.color.manatee1, null));
                btn_main_pharmacy.setTextColor(getResources().getColor(R.color.manatee1, null));
                btn_main_ingredient.setTextColor(getResources().getColor(R.color.manatee1, null));
                btn_main_home.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                btn_main_pharmacy.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                btn_main_ingredient.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                btn_main_home.setTextColor(getResources().getColor(R.color.maya_blue1, null));
                btn_main_home.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, R.drawable.main_menubar_bottom_small);
            }
        });

        btn_main_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewPager.setCurrentItem(0);
            }
        });

        btn_main_pharmacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewPager.setCurrentItem(1);

            }
        });

        btn_main_ingredient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewPager.setCurrentItem(2);
            }
        });

        edit_main_search = (EditText) findViewById(R.id.edit_main_search);
        edit_main_search.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_UP:
                        Intent search_intent = new Intent(MainActivity.this, SearchActivity.class);
                        startActivity(search_intent);
                        break;
                }
                return false;
            }
        });

        MyDataGetAsync mdga = new MyDataGetAsync();
        mdga.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    class MyDataGetAsync extends AsyncTask<String, String, String> {

        String my_result = "";

        @Override
        protected void onPreExecute() {
            String result = "";
            result = httpConnect.send("", httpConnect.getServerWithVersion() + getString(R.string.rest_members), csp.getValue("access_token", ""), "GET");
            Log.e("main my data", result);
            my_result = result;
        }

        @Override
        protected String doInBackground(String... strings) {
            setJsonData(my_result);
            return null;
        }
    }

    private void setJsonData(final String json) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    JSONObject my_json = new JSONObject(json);
                    JSONObject score_json = my_json.getJSONObject("scores");
                    String id = my_json.getString("id");
                    csp.put("id", id);

                    String reviews = score_json.getString("reviews");
                    csp.put("my_review", reviews);

                    FCMAsync fa = new FCMAsync();
                    fa.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                    if (my_json.has("email")) {
                        String email = my_json.getString("email");
                        csp.put("email", email);
                    }

                } catch (Exception e) {
                    Log.e("my ac", "get json excep", e);
                }
            }
        });
    }

    class FCMAsync extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            String json = jsonMaker.makeJson(new Pair("token", csp.getValue("fcm_token", "")));
            result = httpConnect.send(json, httpConnect.getServerWithVersion() + getString(R.string.rest_members_push), csp.getValue("access_token", ""));
            return result;
        }
    }
}