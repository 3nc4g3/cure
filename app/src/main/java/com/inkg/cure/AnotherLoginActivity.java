package com.inkg.cure;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.app.LoaderManager.LoaderCallbacks;

import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.util.Pair;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.inkg.cure.classes.CureSP;
import com.inkg.cure.classes.HttpConnect;
import com.inkg.cure.classes.JsonMaker;
import com.inkg.cure.classes.ResultCodes;
import com.inkg.cure.classes.Utils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A login screen that offers login via email/password.
 */
public class AnotherLoginActivity extends AppCompatActivity {

    private TextView txt_another_cure;
    private TextView txt_another_find_password;

    private EditText edit_another_email;
    private EditText edit_another_password;

    private Button btn_email_sign_in;

    private HttpConnect httpConnect;
    private JsonMaker jsonMaker;
    private CureSP csp;

    private String email;
    private String password;

    private Utils utils;

    private int[] login_checker;
    private boolean is_done = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_another_login);
        // Set up the login form.

        httpConnect = new HttpConnect(getApplicationContext());
        jsonMaker = new JsonMaker();
        utils = new Utils();
        csp = new CureSP(getApplicationContext());
        login_checker = new int[2];
        login_checker[0] = 0;
        login_checker[1] = 0;

        txt_another_cure = (TextView) findViewById(R.id.txt_another_cure);
        String string_another_cure = getResources().getString(R.string.another_cure);
        int color = Color.parseColor("#5cc7fc");
        SpannableStringBuilder builder = new SpannableStringBuilder(string_another_cure);
        builder.setSpan(new ForegroundColorSpan(color), 0, string_another_cure.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        txt_another_cure.append(builder);

        edit_another_email = (EditText) findViewById(R.id.edit_another_email);
        edit_another_password = (EditText) findViewById(R.id.edit_another_password);

        btn_email_sign_in = (Button) findViewById(R.id.btn_email_sign_in);

        txt_another_find_password = (TextView) findViewById(R.id.txt_another_find_password);

        btn_email_sign_in.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (is_done) {
                    email = edit_another_email.getText().toString();
                    password = edit_another_password.getText().toString();

                    LoginAsync la = new LoginAsync();
                    la.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
            }
        });

        txt_another_find_password.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                Uri find_passwd = Uri.parse(getString(R.string.server_url) + getString(R.string.rest_find_passwd));
                intent.setData(find_passwd);
                startActivity(intent);
            }
        });

        setValuechecker();
        setCustomActionbar();
    }

    private void setValuechecker() {
        TextWatcher emailTextWatcher = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (utils.validateEmail(charSequence.toString())) {
                    login_checker[0] = 1;
                } else {
                    login_checker[0] = 0;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                finish_checker();
            }
        };

        edit_another_email.addTextChangedListener(emailTextWatcher);

        TextWatcher pwTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (utils.validatePassword(charSequence.toString())) {
                    login_checker[1] = 1;
                } else {
                    login_checker[1] = 0;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                finish_checker();
            }
        };

        edit_another_password.addTextChangedListener(pwTextWatcher);
    }

    private void finish_checker() {

        boolean is_ok = true;
        for (int i = 0; i < login_checker.length; i++) {
            if (login_checker[i] == 0) {
                is_ok = false;
            }
        }

        if (is_ok) {
            is_done = true;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    btn_email_sign_in.setBackgroundColor(ContextCompat.getColor(AnotherLoginActivity.this, R.color.maya_blue1));
                }
            });
        } else {
            is_done = false;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    btn_email_sign_in.setBackgroundColor(ContextCompat.getColor(AnotherLoginActivity.this, R.color.silver));
                }
            });
        }
    }

    class LoginAsync extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            String json_login = jsonMaker.makeJson(new Pair("email", email), new Pair("password", password));
            String login_result = "";
            login_result = httpConnect.send(json_login, getString(R.string.server_url) + getString(R.string.rest_another_login), "");
            return login_result;
        }

        @Override
        protected void onPostExecute(String result) {
            LoginDataAsync lda = new LoginDataAsync(result);
            lda.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    class LoginDataAsync extends AsyncTask<String, String, String> {

        String result = "";

        public LoginDataAsync(String result) {
            this.result = result;
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(String... strings) {

            String login_result = "";

            if ("403".equals(result)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "차단된 회원입니다. 다른 계정으로 로그인해주세요.", Toast.LENGTH_SHORT).show();
                    }
                });

                login_result = "no";
            } else if ("400".equals(result)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "로그인에 실패했습니다. 이메일, 비밀번호가 맞는지 다시 확인해주세요", Toast.LENGTH_SHORT).show();
                    }
                });

                login_result = "no";

            } else {
                try {
                    JSONObject result_json = new JSONObject(result);
                    String access_token = result_json.getString("access_token");
                    String expire = result_json.getString("expire");
                    String refresh_exp = result_json.getString("refresh_exp");
                    String refresh_token = result_json.getString("refresh_token");

                    csp.put("access_token", access_token);
                    csp.put("expire", expire);
                    csp.put("refresh_exp", refresh_exp);
                    csp.put("refresh_token", refresh_token);

                    login_result = "yes";
                } catch (Exception e) {
                    Log.e("anlo ac", "get json excep", e);
                }
            }
            return login_result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if ("yes".equals(result)) {
                setResult(ResultCodes.AN_LOGIN);
                finish();
            }
        }
    }

    private void setCustomActionbar() {

        ActionBar actionBar = getSupportActionBar();

        // Custom Actionbar를 사용하기 위해 CustomEnabled을 true 시키고 필요 없는 것은 false 시킨다

        actionBar.setDisplayShowCustomEnabled(true);

        actionBar.setDisplayHomeAsUpEnabled(false);

        actionBar.setDisplayShowTitleEnabled(false);

        // Set custom view layout

        View mCustomView = LayoutInflater.from(this).inflate(R.layout.actionbar_simple, null);

        actionBar.setCustomView(mCustomView);


        // Set no padding both side

        Toolbar parent = (Toolbar) mCustomView.getParent(); // first get parent toolbar of current action bar

        parent.setContentInsetsAbsolute(0, 0);              // set padding programmatically to 0dp

        // Set actionbar background image

        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.border_layout_bottom));

        // Set actionbar layout layoutparams

        ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);

        actionBar.setCustomView(mCustomView, params);

        ImageView img_actionbar_back = (ImageView) findViewById(R.id.img_actionbar_back);
        img_actionbar_back.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        TextView txt_actionbar = (TextView) findViewById(R.id.txt_actionbar_simple);
        txt_actionbar.setText(getString(R.string.action_sign_in));
    }

}

